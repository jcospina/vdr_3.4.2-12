{
	"title":"El Guando"
	"fragments":[
		{
			"text":"Cuando las campanas anuncian la medianoche, puede observarse la espesa niebla que comienza cubrir las calles, se oyen las cadenas arrastradas como un sonido de ultratumba, y la piel se eriza ante el murmullo de una muchedumbre entonando rezos y oraciones."
		},
		{
			"text":"Es el guando que ha iniciado su marcha fúnebre, anunciando que pronto habrá una calamidad cerca al sitio por el cual está pasando. Aquellos que lo han visto, luego de reponerse del terrible susto, han comentado que su apariencia es de lo más aterradora que existe."
		},
		{
			"text":"Aquella procesión nocturna, es encabezada por cuatro fantasmales cargueros de aspecto cadavérico, unidos entre sí, por cadenas y grilletes en sus pies, las cuales arrastran produciendo un temible sonido. Estos espectros, llevan en sus hombros una camilla madera, cubierta con una manta blanca de inusual brillo."
		},
		{
			"text":"Siguiéndolos, va una procesión de ánimas en pena, llevando en sus manos grandes velas de cera espectral que nunca se apagan, a menos que esta ánima, la entregue a un incauto observado para que éste, dedique oraciones que le permitan descansar abandonando el fúnebre cortejo."			
		},
		{
			"text":"De lo contrario, seguirán murmurando oraciones en pos del mal augurio que se avecina, generando temor entre los habitantes del sector."
		}
	]
}