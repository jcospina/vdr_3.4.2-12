{
	"title":"La Patasola"
	"fragments":[
		{
			"text":"Habita entre la maraña espesa de las selvas y en las cumbres de la llanura. Con la única pata que tiene avanza con rapidez asombrosa. Es temido por colonos, mineros, cazadores,  caminantes, agricultores y leñadores."
		},
		{
			"text":"Algunos aventureros dicen que es una mujer bellísima que los llama y los atrae para enamorarlos, pero avanza hacía la oscuridad del bosque donde los va conduciendo con sus miradas cautivantes, allí, se transforma en una mujer horrible, con ojos de fuego, boca desproporcionada con enormes dientes filosos y una cabellera corta y despeinada que, al caer sobre el rostro oculta su fealdad."
		},
		{
			"text":"Cuando ya está muy cerca, se convierte en una fiera enorme que se lanza sobre su víctima, robando con sus agudos colmillos, toda la fuerza vital de la persona. La única defensa contra este espantoso ser, es rodearse de animales domésticos, que espantan su maligna presencia."
		},
		{
			"text":"Se dice, que este personaje fue inventado por los hombres celosos para asustar a sus esposas infieles, infundirles terror y al mismo tiempo, reconocer las bondades de la selva."
		},
		{
			"text":"Cuentan que este espanto, es el fruto de un marido celoso, que al darse cuenta que su esposa lo engañaba con su amigo, le cortó una pierna con el machete para que no escapará, ella adolorida, se escondió en casa, pero el marido furioso, le prendió fuego a la casa y ella quedó atrapada. El alma de la mujer infiel, fue condenada a vagar saltando en una solo pierna buscando saciar su sed de amor y agua."
		}
	]
}