{
	"title":"La Leyenda de la Piedra del Letrero"
	"fragments":[
		{
			"text":"Allá por el macizo, muy cerca de un poblado llamado San Sebastián, existe un  camino milenario hacia un páramo conocido como El Letrero. Por un tiempo, este sendero mítico fue un paso arriesgado para los viajeros que se arriesgaban a cruzarlo. Aquellos que no caían agotados por elfríoo del páramo, enfrentaban a un temible diablo que se escondía tras una enorme piedra asaltando  a cada individuo que se acercaba en busca de reposo."
		},
		{
			"text":"Cansados de tanta tragedia, los habitantes del páramo enviaron un cura capuchino para que detuviera al malicioso diablo que robaba las almas de los viajeros. El cura retó al malvado, luchando cuerpo a cuerpo entre cruces y cachos. En la infernal batalla, el cura logró romperle un cuerno al diablo, obligándolo a postrarse de rodillas."
		},
		{
			"text":"El monje, aprovechando este momento, amarró con destreza al vencido demonio, forzandolo a sacar el agua de una laguna cercana como castigo por sus fechorías. Si lo hacía en siete días, el monje lo dejaría libre, de lo contrario, perdería la vida."
		},
		{
			"text":"Imposible fue para el diablo cumplir la tarea, así que el cura lo obligó a tallar una cruz en la piedra como castigo final, pero el diablo, que no sabía que era una cruz, escribió un garabato sin sentido. El cura, tuvo que dibujar al lado una cruz real para que los viajeros se sintieran seguros al pasar."
		},
		{
			"text":"Desde ese día, el diablo no volvió a aparecer, pero en el viento del páramo, aún se escuchan las almas atrapadas de los viajeros."
		}
	]
}