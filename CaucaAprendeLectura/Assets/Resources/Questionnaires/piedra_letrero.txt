{
	"title":"La Leyenda de la Piedra del Letrero",
	"questions":[
		{
			"text":"La historia se desarrolla en"
			"options":"Un páramo:Un letrero:Una Laguna cerca al letrero:Cerca de un poblado llamado San Sebastián",
			"answer":3
		},
		{
			"text":"El anterior texto se Argumentativo porque"
			"options":"Expone las razones del diablo para acosar a los viajeros:Describe como es el monje capuchino:El texto no es argumentativo, es narrativo porque relata acontecimientos con un orden temporal:Cuenta como es el lugar donde sucedieron los hechos",
			"answer":2
		},
		{
			"text":"Si el monje no hubiera ganado la batalla, el diablo"
			"options":"No habría vuelto a aparecer en la piedra del letrero:Seguiría asaltando a los viajeros  que buscan reposo:Se habría vuelto amigo del monje capuchino:El monje igual lo habría castigado",
			"answer":1
		},
		{
			"text":"La palabra fechorías en el texto significa"
			"options":"Maldades:Juegos:Monedas:Lagunas",
			"answer":0
		}
	]
}