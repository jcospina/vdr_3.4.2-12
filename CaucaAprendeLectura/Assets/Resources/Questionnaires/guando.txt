{
	"title":"El Guando",
	"questions":[
		{
			"text":"¿En qué momentos del día aparece el guando?"
			"options":"Cuando el sol esta en lo alto:Cuando la luna se esconde en el horizonte:Cuando es el clima es caluroso:Cuando las campanas anuncian la media noche",
			"answer":3
		},
		{
			"text":"Según el texto ¿cómo se liberan las ánimas?"
			"options":"Cuando se entonan oraciones en su nombre:Cuando aparece el sol:Cuando sucede una muerte por donde pasan:Al final de su aparición",
			"answer":0
		},
		{
			"text":"Al leer la historia, podríamos decir que"
			"options":"Se creó para advertir sobre malos augurios:Se utiliza para que los niños no estén en la calle a la media noche:Es una invención para que los niños vayan a la escuela:El guando no hace daño a los muertos",
			"answer":1
		},
		{
			"text":"Del texto, podría inferirse que"
			"options":"El Guando roba las almas de los difuntos:Los niños pueden perderse al ver el Guando:El Guando es un anunciante de malos augurios o tragedias:Las personas no le tienen miedo al Guando",
			"answer":2
		}
	]
}