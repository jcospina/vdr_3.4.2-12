{
	"title":"La Madremonte",
	"questions":[
		{
			"text":"Quienes la han visto dice que es"
			"options":"Una mujer pequeña y muy fea:Como una hadita de agua:Una señora corpulenta, elegante, vestida de hojas frescas y musgo verde, con un sombrero cubierto de hojas y plumas verdes:Una mujer con rostro de calavera",
			"answer":2
		},
		{
			"text":"Según lo leído, la leyenda de la Madremonte nació como"
			"options":"Una forma de prevenir sobre los peligro del bosque:Una explicación de los fenómenos naturales y un control sobre los recursos naturales:Una explicación de la desaparición de los ancestros:Un cuento para dormir",
			"answer":1
		},
		{
			"text":"¿Cuál es el enigma más grande de la Madremonte?"
			"options":"Su estruendoso aullido:Su nauseabundo olor:El extraño ropaje:El rostro que nunca deja ver",
			"answer":3
		},
		{
			"text":"La historia es una alegoría a"
			"options":"Los peligros de andar por una mala vida y no cuidar la naturaleza:Los peligros de cuidar el bosque:El temor de no llevar un crucifijo:Los peligros que acechan en el bosque",
			"answer":0
		}
	]
}