{
	"title":"La Muelona",
	"questions":[
		{
			"text":"Según lo leído, ¿por qué crees que la Muelona prefiere salir a asustar entre las 6 y las 9 de la noche?"
			"options":"A esa hora no hay niños en las calles:Justo a esa hora no salen más espantos:Es en los momentos que hay más personas perversas en la calle:Le teme a la luz del sol",
			"answer":2
		},
		{
			"text":"La única forma de salvarse de la Muelona es:"
			"options":"No salir después de las seis de la tarde:Que en la casa haya un niño recién nacido:Que no hayan bebes en la casa:Estar siempre al lado de un mayor",
			"answer":1
		},
		{
			"text":"Una de las razones de la existencia de esta historia es:"
			"options":"Ayudar a mantener limpia la ciudad:Que las personas teman salir en las noches:La falta de seguridad en las calles de la ciudad:Ayudar a la sociedad  a preservar los valores y mantener la seguridad",
			"answer":3
		},
		{
			"text":"¿Qué sucede si la Muelona entra en un hogar?"
			"options":"Se escucha 'tengo que vengarme de los hombres jugadores y mujeriegos, yo soy la Muelona':Se roba a los niños:Hace desmayar a los maridos y mujeres borrachines:La casa cae en desgracia",
			"answer":0
		}
	]
}