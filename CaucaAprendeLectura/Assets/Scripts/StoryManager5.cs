﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MadLevelManager;

public class StoryManager5 : MonoBehaviour {
	
	public UILabel titleLabel;
	public UILabel storyFragmentLabel;
	public UISprite nextBtn;
	public UISprite prevBtn;
	public UISprite recordBtn;
	public UISprite stopRecordBtn;
	public GameObject questionnaire;
	
	//Manejo de las animaciones  de cada historia
	public GameObject illivanH1;
	public GameObject illivanH2;
	public GameObject illivanH3;
	public GameObject illivanH4;
	public GameObject illivanH5;
	public GameObject juanTamaH1;
	public GameObject juanTamaH2;
	public GameObject juanTamaH3;
	public GameObject juanTamaH4;
	public GameObject juanTamaH5;
	private int currentStory;
	
	public AudioRecorderBridge bridge;
	
	private string storyToLoad;
	private List<string> fragmentList = new List<string>();
	private int currentFragment = 0;
	
	void Start () {
		
		if (!string.IsNullOrEmpty(MadLevel.arguments)) {
			storyToLoad = MadLevel.arguments;
			TextAsset story = Resources.Load<TextAsset>("Stories/" + storyToLoad);
			parseJson(story.text);
			Debug.Log(storyToLoad);
		}
		
		if (storyToLoad == "llivan") {
			Debug.Log ("HISTORIA DEL LLIVAN");
			currentStory = 13;
			illivanH1.SetActive (true);
		}

		if (storyToLoad == "juan_tama") {
			Debug.Log ("HISTORIA DE JUAN TAMA");
			currentStory = 14	;
			juanTamaH1.SetActive (true);
		}
	}
	
	private void parseJson(string jsonText){
		Hashtable parsedJson = (Hashtable) JSON.JsonDecode(jsonText);
		titleLabel.text = (string) parsedJson["title"];
		ArrayList fragmentArray  = (ArrayList) parsedJson["fragments"];
		foreach(object o in fragmentArray){
			Hashtable currentFragmentObj = o as Hashtable;
			fragmentList.Add((string)currentFragmentObj["text"]);
		}
		storyFragmentLabel.text = fragmentList[currentFragment];
	}
	
	public void nextFragment(){
		currentFragment++;
		storyFragmentLabel.text = fragmentList[currentFragment];
		if(currentFragment == (fragmentList.Count - 1)){
			questionnaire.SetActive(true);
		}
		Debug.Log ("Fragmento Actual: "+currentFragment);
		
		if (currentFragment == 1) {
			juanTamaH1.SetActive (false);	
			juanTamaH2.SetActive (true);

		}
		
		if (currentFragment == 2) {

			if (currentStory == 13){
				illivanH1.SetActive (false);	
				illivanH2.SetActive (true);
			}

			if (currentStory == 14){
				juanTamaH2.SetActive (false);	
				juanTamaH3.SetActive (true);
			}
		}
		
		if (currentFragment == 3) {

			if (currentStory == 13){
				illivanH2.SetActive (false);	
				illivanH3.SetActive (true);
			}

			if (currentStory == 14){
				juanTamaH3.SetActive (false);	
				juanTamaH4.SetActive (true);
			}

		}
		
		if (currentFragment == 4) {

			if (currentStory == 14){
				juanTamaH4.SetActive (false);	
				juanTamaH5.SetActive (true);
			}

		}

		if (currentFragment == 5) {

			if (currentStory == 13){
				illivanH3.SetActive (false);	
				illivanH4.SetActive (true);
			}
		}

		if (currentFragment == 6) {
				
			if (currentStory == 13){
				illivanH4.SetActive (false);	
				illivanH5.SetActive (true);
			}
		}
	}
	
	public void prevFragment(){
		currentFragment--;
		storyFragmentLabel.text = fragmentList[currentFragment];
	}
	
	void Update(){
		
		if(currentFragment == 0){
			prevBtn.enabled = false;
		} else if(currentFragment == (fragmentList.Count - 1)){
			nextBtn.enabled = false;
		} else{
			prevBtn.enabled = true;
			nextBtn.enabled = true;
		}
	}
	
	public void loadQuestionnaire(){
		PlayerPrefs.SetString("questionnaire",storyToLoad);
		if(bridge.isRecordingNow()){
			bridge.recordAudio(titleLabel.text);
		}
		Application.LoadLevel("TriviaGUI");
	}
	
	public void record(){
		if(!bridge.isRecordingNow()){
			recordBtn.gameObject.SetActive(false);
			stopRecordBtn.gameObject.SetActive(true);
		} else{
			recordBtn.gameObject.SetActive(true);
			stopRecordBtn.gameObject.SetActive(false);
		}
		bridge.recordAudio(titleLabel.text);
	}
	
}
