﻿using UnityEngine;
using System.Collections;

public class Question{

	private string text;
	private string[] options;
	private int answer;

	public Question(string text, string[] options, int answer){
		this.text = text;
		this.options = options;
		this.answer = answer;
	}

	public string getText(){
		return this.text;
	}

	public void setText(string text){
		this.text =  text;
	}

	public string[] getOptions(){
		return this.options;
	}

	public void setOptions(string[] options){
		this.options = options;
	}

	public int getAnswer(){
		return this.answer;
	}

	public void setAnswer(int answer){
		this.answer = answer;
	}

}
