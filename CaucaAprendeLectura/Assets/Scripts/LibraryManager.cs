﻿using UnityEngine;
using System.Collections;

public class LibraryManager : MonoBehaviour {

	public GameObject list;
	public GameObject listItem;
	public AudioRecorderBridge bridge;


	void Start () {
		string[] recordsList = bridge.getLectures();
		foreach(string record in recordsList){
			GameObject newListItem = Instantiate (listItem) as GameObject;
			newListItem.transform.parent = list.transform;
			newListItem.transform.localScale = new Vector3(1,1,1);
			newListItem.GetComponentInChildren<UILabel>().text = record;
			UIEventListener.Get(newListItem).onClick += clickDelegate;
		}
		list.GetComponent<UIGrid>().Reposition();
	}

	public void clickDelegate (GameObject go) {
		bridge.playAudio(go.GetComponentInChildren<UILabel>().text);
	}

	void Update(){
		if(Input.GetKeyDown(KeyCode.Escape)){
			Application.LoadLevel("MainMenu");
		}
	}
}
