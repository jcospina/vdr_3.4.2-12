﻿	using UnityEngine;
using System.Collections;

public class TweenImage : MonoBehaviour {

	public GameObject imageObject;
	public float delayTime;
	
	private bool isActive = true;

	void Update () {
	
		if (isActive) {
			StartCoroutine (Delay (delayTime));
		}
		else {
			StartCoroutine (Delay (delayTime));
		}
	}

	IEnumerator Delay(float delayTime) {
	
		if (isActive) {
			yield return new WaitForSeconds(delayTime);
			imageObject.SetActive (false);
			isActive = false;
		}
		else {
			yield return new WaitForSeconds(delayTime);
			imageObject.SetActive (true);
			isActive = true;
		}
	}
}
