﻿using UnityEngine;
using System.Collections;
using MadLevelManager;

public class Utils : MonoBehaviour {


	void Awake(){
		PlayerPrefs.DeleteKey("region");
		PlayerPrefs.DeleteKey("questionnaire");
	}

	public void setRegion(string region){
		PlayerPrefs.SetString("region", region);
	}

	public string getRegion(){
		return PlayerPrefs.GetString("region");
	}

	public void loadRegionSelector(){
		Application.LoadLevel("RegionSelector");
	}

	public void loadLevelSelection(){
		Application.LoadLevel("LevelSelection");
	}

	public void loadLibrary(){
		Application.LoadLevel("AudiosLibrary");
	}

}
