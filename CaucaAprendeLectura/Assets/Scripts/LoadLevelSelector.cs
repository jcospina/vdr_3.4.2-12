﻿using UnityEngine;
using System.Collections;
using MadLevelManager;

public class LoadLevelSelector : MonoBehaviour {

	public GameObject[] layouts;
	
	void Awake () {
		string region = PlayerPrefs.GetString("region");
		Debug.Log(region);
		switch(region){
		case "Centro":
			layouts[0].SetActive(true);
			break;
		case "Norte":
			layouts[1].SetActive(true);
			break;
		case "Sur":
			layouts[2].SetActive(true);
			break;
		case "Oriente":
			layouts[3].SetActive(true);
			break;
		case "Pacifico":
			layouts[4].SetActive(true);
			break;
		}

	}

	void Update(){
		if(Input.GetKeyDown(KeyCode.Escape)){
			Application.LoadLevel("RegionSelector");
		}
	}
}
