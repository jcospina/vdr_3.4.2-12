﻿using UnityEngine;
using System.Collections;

public class MenuSettings : MonoBehaviour {
	
	public GameObject bgButton;
	public GameObject btnAbout;
	public GameObject btnSound;
	
	private bool settingsActive = false;
	
	void OnClick (){
		
		Debug.Log ("Click en Boton Settings");
		
		if (!settingsActive){
			Debug.Log ("AQUI");
			bgButton.SetActive (true);
			btnAbout.SetActive (true);
			btnSound.SetActive (true);
			settingsActive = true;
		}
		
		else if (settingsActive){
			bgButton.SetActive (false);
			btnAbout.SetActive (false);
			btnSound.SetActive (false);
			settingsActive = false;
		}
	}
	
}
