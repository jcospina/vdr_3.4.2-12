﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MadLevelManager;

public class StoryManager : MonoBehaviour {

	public UILabel titleLabel;
	public UILabel storyFragmentLabel;
	public UISprite nextBtn;
	public UISprite prevBtn;
	public UISprite recordBtn;
	public UISprite stopRecordBtn;
	public GameObject questionnaire;

	//Manejo de las animaciones  de cada historia
	public GameObject muelonaH1;
	public GameObject muelonaH2;
	public GameObject muelonaH3;
	public GameObject muelonaH4;
	public GameObject muelonaH5;
	public GameObject viudaH1;
	public GameObject viudaH2;
	public GameObject viudaH3;
	public GameObject viudaH4;
	private int currentStory;

	public AudioRecorderBridge bridge;

	private string storyToLoad;
	private List<string> fragmentList = new List<string>();
	private int currentFragment = 0;

	void Start () {

		if (!string.IsNullOrEmpty(MadLevel.arguments)) {
			storyToLoad = MadLevel.arguments;
			TextAsset story = Resources.Load<TextAsset>("Stories/" + storyToLoad);
			parseJson(story.text);
		}

		if (storyToLoad == "muelona") {
			Debug.Log ("HISTORIA DE LA MUELONA");
			currentStory = 6;
			muelonaH1.SetActive (true);
		}

		if (storyToLoad == "viuda") {
			Debug.Log ("HISTORIA DE LA VIUDA");
			currentStory = 7;
			viudaH1.SetActive (true);
		}
	}
	
	private void parseJson(string jsonText){
		Hashtable parsedJson = (Hashtable) JSON.JsonDecode(jsonText);
		titleLabel.text = (string) parsedJson["title"];
		ArrayList fragmentArray  = (ArrayList) parsedJson["fragments"];
		foreach(object o in fragmentArray){
			Hashtable currentFragmentObj = o as Hashtable;
			fragmentList.Add((string)currentFragmentObj["text"]);
		}
		storyFragmentLabel.text = fragmentList[currentFragment];
	}

	public void nextFragment(){
		currentFragment++;
		storyFragmentLabel.text = fragmentList[currentFragment];
		if(currentFragment == (fragmentList.Count - 1)){
			questionnaire.SetActive(true);
		}
		Debug.Log ("Fragmento Actual: "+currentFragment);

		if (currentFragment == 1) {

			if (currentStory == 6){
				muelonaH1.SetActive (false);	
				muelonaH2.SetActive (true);
			}
			if (currentStory == 7){
				viudaH1.SetActive (false);	
				viudaH2.SetActive (true);
			}
		}

		if (currentFragment == 2) {

			if (currentStory == 6){
				muelonaH2.SetActive (false);	
				muelonaH3.SetActive (true);
			}
			if (currentStory == 7){
				viudaH2.SetActive (false);	
				viudaH3.SetActive (true);
			}
		}

		if (currentFragment == 3) {

			if (currentStory == 6){
				muelonaH3.SetActive (false);	
				muelonaH4.SetActive (true);
			}
			if (currentStory == 7){
				viudaH3.SetActive (false);	
				viudaH4.SetActive (true);
			}
		}

		if (currentFragment == 4) {

			if (currentStory == 6){
				muelonaH4.SetActive (false);	
				muelonaH5.SetActive (true);
			}
		}
	}

	public void prevFragment(){
		currentFragment--;
		storyFragmentLabel.text = fragmentList[currentFragment];
	}

	void Update(){

		if(currentFragment == 0){
			prevBtn.enabled = false;
		} else if(currentFragment == (fragmentList.Count - 1)){
			nextBtn.enabled = false;
		} else{
			prevBtn.enabled = true;
			nextBtn.enabled = true;
		}
	}

	public void loadQuestionnaire(){
		PlayerPrefs.SetString("questionnaire",storyToLoad);
		if(bridge.isRecordingNow()){
			bridge.recordAudio(titleLabel.text);
		}
		Application.LoadLevel("TriviaGUI");
	}

	public void record(){
		if(!bridge.isRecordingNow()){
			recordBtn.gameObject.SetActive(false);
			stopRecordBtn.gameObject.SetActive(true);
		} else{
			recordBtn.gameObject.SetActive(true);
			stopRecordBtn.gameObject.SetActive(false);
		}
		bridge.recordAudio(titleLabel.text);
	}

}
