﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MadLevelManager;

public class StoryManager5_1 : MonoBehaviour {
	
	public UILabel titleLabel;
	public UILabel storyFragmentLabel;
	public UISprite nextBtn;
	public UISprite prevBtn;
	public UISprite recordBtn;
	public UISprite stopRecordBtn;
	public GameObject questionnaire;
	
	//Manejo de las animaciones  de cada historia
	public GameObject origenTHombreH1;
	public GameObject origenTHombreH2;
	public GameObject origenTHombreH3;
	public GameObject origenTHombreH4;
	public GameObject origenTHombreH5;
	private int currentStory;
	
	public AudioRecorderBridge bridge;
	
	private string storyToLoad;
	private List<string> fragmentList = new List<string>();
	private int currentFragment = 0;
	
	void Start () {
		
		if (!string.IsNullOrEmpty(MadLevel.arguments)) {
			storyToLoad = MadLevel.arguments;
			TextAsset story = Resources.Load<TextAsset>("Stories/" + storyToLoad);
			parseJson(story.text);
			Debug.Log(storyToLoad);
		}
		
		if (storyToLoad == "origen_tierra_hombre") {
			Debug.Log ("HISTORIA ORIGEN DE LA TIERRA Y EL HOMBRE");
			currentStory = 15	;
			origenTHombreH1.SetActive (true);
		}
	}
	
	private void parseJson(string jsonText){
		Hashtable parsedJson = (Hashtable) JSON.JsonDecode(jsonText);
		titleLabel.text = (string) parsedJson["title"];
		ArrayList fragmentArray  = (ArrayList) parsedJson["fragments"];
		foreach(object o in fragmentArray){
			Hashtable currentFragmentObj = o as Hashtable;
			fragmentList.Add((string)currentFragmentObj["text"]);
		}
		storyFragmentLabel.text = fragmentList[currentFragment];
	}
	
	public void nextFragment(){
		currentFragment++;
		storyFragmentLabel.text = fragmentList[currentFragment];
		if(currentFragment == (fragmentList.Count - 1)){
			questionnaire.SetActive(true);
		}
		Debug.Log ("Fragmento Actual: "+currentFragment);
		
		if (currentFragment == 1) {
			origenTHombreH1.SetActive (false);	
			origenTHombreH2.SetActive (true);
			
		}
		
		if (currentFragment == 2) {
			
			if (currentStory == 15){
				origenTHombreH2.SetActive (false);	
				origenTHombreH3.SetActive (true);
			}
		}
		
		if (currentFragment == 3) {
			
			if (currentStory == 15){
				origenTHombreH3.SetActive (false);	
				origenTHombreH4.SetActive (true);
			}
			
		}
		
		if (currentFragment == 4) {
			
			if (currentStory == 15){
				origenTHombreH4.SetActive (false);	
				origenTHombreH5.SetActive (true);
			}
			
		}

	}
	
	public void prevFragment(){
		currentFragment--;
		storyFragmentLabel.text = fragmentList[currentFragment];
	}
	
	void Update(){
		
		if(currentFragment == 0){
			prevBtn.enabled = false;
		} else if(currentFragment == (fragmentList.Count - 1)){
			nextBtn.enabled = false;
		} else{
			prevBtn.enabled = true;
			nextBtn.enabled = true;
		}
	}
	
	public void loadQuestionnaire(){
		PlayerPrefs.SetString("questionnaire",storyToLoad);
		if(bridge.isRecordingNow()){
			bridge.recordAudio(titleLabel.text);
		}
		Application.LoadLevel("TriviaGUI");
	}
	
	public void record(){
		if(!bridge.isRecordingNow()){
			recordBtn.gameObject.SetActive(false);
			stopRecordBtn.gameObject.SetActive(true);
		} else{
			recordBtn.gameObject.SetActive(true);
			stopRecordBtn.gameObject.SetActive(false);
		}
		bridge.recordAudio(titleLabel.text);
	}
	
}
