﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MadLevelManager;
using System.Text;
using System;

public class QuestionnaireManager : MonoBehaviour {

	public UILabel questionnaireLabel;
	public UILabel currentQuestionLabel;
	public UILabel questionText;
	public UILabel optionA;
	public UILabel optionB;
	public UILabel optionC;
	public UILabel optionD;

	public UILabel totalCorrectAnswersLabel;

	private string storyToLoad;
	private List<Question> questionList = new List<Question>();
	private int currentQuestionIndex = 0;
	private int totalCorrectAnswers = 0;

	public int option1 = 0;
	public int option2 = 1;
	public int option3 = 2;
	public int option4 = 3;

	public AudioClip wrong;
	public AudioClip right;
	
	private Question currentQuestion;

	public GameObject gameOverScreen;
	public GameObject optionsObj;

	void Start () {
		storyToLoad = PlayerPrefs.GetString("questionnaire");
		TextAsset story = Resources.Load<TextAsset>("Questionnaires/" + storyToLoad);
		parseJson(story.text);
	}
	
	private void parseJson(string jsonText){
		Hashtable parsedJson = (Hashtable) JSON.JsonDecode(jsonText);
		questionnaireLabel.text = (string) parsedJson["title"];
		ArrayList questionArray  = (ArrayList) parsedJson["questions"];
		foreach(object o in questionArray){
			Hashtable currentQuestionObj = o as Hashtable;
			string questionText = (string) currentQuestionObj["text"];
			string rawOptions = (string) currentQuestionObj["options"];
			int answer = Convert.ToInt32(currentQuestionObj["answer"]);
			char[] delimiterChars = {':'};
			string[] options = rawOptions.Split(delimiterChars);
			questionList.Add(new Question(questionText,options,answer));
		}
		loadQuestion();
	}

	public void loadQuestion(){
		if(currentQuestionIndex < questionList.Count){
			currentQuestionLabel.text = "Pregunta " + (currentQuestionIndex + 1) + "/" + questionList.Count;
			currentQuestion = questionList[currentQuestionIndex];
			questionText.text = currentQuestion.getText();
			optionA.text = currentQuestion.getOptions()[0];
			optionB.text = currentQuestion.getOptions()[1];
			optionC.text = currentQuestion.getOptions()[2];
			optionD.text = currentQuestion.getOptions()[3];
		} else{
			gameOverScreen.SetActive(true);
			optionsObj.SetActive(false);
			MadLevelProfile.SetLevelBoolean(MadLevel.currentLevelName,"star_1",true);
			MadLevelProfile.SetCompleted(MadLevel.currentLevelName,true);
			totalCorrectAnswersLabel.text = "Total correctas: " + totalCorrectAnswers + "/" + questionList.Count;
		}
	}

	public void validateAnswer(int option){
		if(option == currentQuestion.getAnswer()){
			totalCorrectAnswers++;
			this.gameObject.GetComponent<AudioSource>().clip = right;
		} else{
			this.gameObject.GetComponent<AudioSource>().clip = wrong;
		}
		this.gameObject.GetComponent<AudioSource>().Play();
		currentQuestionIndex++;
		loadQuestion();
	}

	public void loadLevelSelection(){
		Application.LoadLevel("LevelSelection");
	}
	

}
