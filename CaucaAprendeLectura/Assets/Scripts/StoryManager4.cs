﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MadLevelManager;

public class StoryManager4 : MonoBehaviour {
	
	public UILabel titleLabel;
	public UILabel storyFragmentLabel;
	public UISprite nextBtn;
	public UISprite prevBtn;
	public UISprite recordBtn;
	public UISprite stopRecordBtn;
	public GameObject questionnaire;
	
	//Manejo de las animaciones  de cada historia
	public GameObject rivielH1;
	public GameObject rivielH2;
	public GameObject rivielH3;
	public GameObject rivielH4;
	public GameObject rivielH5;
	public GameObject maravelliH1;
	public GameObject maravelliH2;
	public GameObject maravelliH3;
	public GameObject maravelliH4;
	private int currentStory;
	
	public AudioRecorderBridge bridge;
	
	private string storyToLoad;
	private List<string> fragmentList = new List<string>();
	private int currentFragment = 0;
	
	void Start () {
		
		if (!string.IsNullOrEmpty(MadLevel.arguments)) {
			storyToLoad = MadLevel.arguments;
			TextAsset story = Resources.Load<TextAsset>("Stories/" + storyToLoad);
			parseJson(story.text);
		}

		if (storyToLoad == "riviel") {
			Debug.Log ("HISTORIA DEL RIVIEL");
			currentStory = 4;
			rivielH1.SetActive (true);
		}
		
		if (storyToLoad == "maravelli") {
			Debug.Log ("HISTORIA DEL MARAVELLI");
			currentStory = 5;
			maravelliH1.SetActive (true);
		}

	}
	
	private void parseJson(string jsonText){
		Hashtable parsedJson = (Hashtable) JSON.JsonDecode(jsonText);
		titleLabel.text = (string) parsedJson["title"];
		ArrayList fragmentArray  = (ArrayList) parsedJson["fragments"];
		foreach(object o in fragmentArray){
			Hashtable currentFragmentObj = o as Hashtable;
			fragmentList.Add((string)currentFragmentObj["text"]);
		}
		storyFragmentLabel.text = fragmentList[currentFragment];
	}
	
	public void nextFragment(){
		currentFragment++;
		storyFragmentLabel.text = fragmentList[currentFragment];
		if(currentFragment == (fragmentList.Count - 1)){
			questionnaire.SetActive(true);
		}
		Debug.Log ("Fragmento Actual: "+currentFragment);
		
		if (currentFragment == 1) {

			if (currentStory == 4){
				rivielH1.SetActive (false);	
				rivielH2.SetActive (true);
			}
			if (currentStory == 5){
				maravelliH1.SetActive (false);	
				maravelliH2.SetActive (true);
			}

		}
		
		if (currentFragment == 2) {		

			if (currentStory == 4){
				rivielH2.SetActive (false);	
				rivielH3.SetActive (true);
			}
			if (currentStory == 5){
				maravelliH2.SetActive (false);	
				maravelliH3.SetActive (true);
			}
		}
		
		if (currentFragment == 3) {

			if (currentStory == 4){
				rivielH3.SetActive (false);	
				rivielH4.SetActive (true);
			}
			if (currentStory == 5){
				maravelliH3.SetActive (false);	
				maravelliH4.SetActive (true);
			}

		}
		
		if (currentFragment == 4) {

			if (currentStory == 4){
				rivielH4.SetActive (false);	
				rivielH5.SetActive (true);
			}
		}
	}
	
	public void prevFragment(){
		currentFragment--;
		storyFragmentLabel.text = fragmentList[currentFragment];
	}
	
	void Update(){
		
		if(currentFragment == 0){
			prevBtn.enabled = false;
		} else if(currentFragment == (fragmentList.Count - 1)){
			nextBtn.enabled = false;
		} else{
			prevBtn.enabled = true;
			nextBtn.enabled = true;
		}
	}
	
	public void loadQuestionnaire(){
		PlayerPrefs.SetString("questionnaire",storyToLoad);
		if(bridge.isRecordingNow()){
			bridge.recordAudio(titleLabel.text);
		}
		Application.LoadLevel("TriviaGUI");
	}
	
	public void record(){
		if(!bridge.isRecordingNow()){
			recordBtn.gameObject.SetActive(false);
			stopRecordBtn.gameObject.SetActive(true);
		} else{
			recordBtn.gameObject.SetActive(true);
			stopRecordBtn.gameObject.SetActive(false);
		}
		bridge.recordAudio(titleLabel.text);
	}
	
}
