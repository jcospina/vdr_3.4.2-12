﻿using UnityEngine;
using System.Collections;

public class AudioLibrary{

	string folderPath;
	string[] audioFiles;

	public AudioLibrary(string folderPath, string[] audioFiles){
		this.folderPath = folderPath;
		this.audioFiles = audioFiles;
	}

	public string[] getAudioFiles(){
		return this.audioFiles;
	}

	public void setAudioFiles(string[] audioFiles){
		this.audioFiles = audioFiles;
	}

	public string getFolderPath(){
		return folderPath;
	}

	public void setFolderPath(string folderPath){
		this.folderPath = folderPath;
	}

}
