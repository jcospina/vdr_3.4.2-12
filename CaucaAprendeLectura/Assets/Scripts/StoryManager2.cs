﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MadLevelManager;

public class StoryManager2 : MonoBehaviour {
	
	public UILabel titleLabel;
	public UILabel storyFragmentLabel;
	public UISprite nextBtn;
	public UISprite prevBtn;
	public UISprite recordBtn;
	public UISprite stopRecordBtn;
	public GameObject questionnaire;
	
	//Manejo de las animaciones  de cada historia
	public GameObject guandoH1;
	public GameObject guandoH2;
	public GameObject guandoH3;
	public GameObject guandoH4;
	public GameObject madreMonteH1;
	public GameObject madreMonteH2;
	public GameObject madreMonteH3;
	public GameObject madreMonteH4;
	public GameObject madreMonteH5;
	public GameObject pataSolaH1;
	public GameObject pataSolaH2;
	public GameObject pataSolaH3;
	public GameObject pataSolaH4;

	private int currentStory;
	
	public AudioRecorderBridge bridge;
	
	private string storyToLoad;
	private List<string> fragmentList = new List<string>();
	private int currentFragment = 0;
	
	void Start () {
		
		if (!string.IsNullOrEmpty(MadLevel.arguments)) {
			storyToLoad = MadLevel.arguments;
			TextAsset story = Resources.Load<TextAsset>("Stories/" + storyToLoad);
			parseJson(story.text);
		}
		Debug.Log ("Historia Actual: "+storyToLoad);
		if (storyToLoad == "guando") {
			Resources.Load ("Atlases/Norte");
			Debug.Log ("HISTORIA DEL GUANDO");
			currentStory = 1;
			guandoH1.SetActive (true);
		}
		
		if (storyToLoad == "madremonte") {
			Resources.UnloadUnusedAssets ();
			Resources.Load ("Atlases/Norte");
			Debug.Log ("HISTORIA DE LA MADRE MONTE");
			currentStory = 2;
			madreMonteH1.SetActive (true);
		}
		
		if (storyToLoad == "patasola") {
			Resources.UnloadUnusedAssets ();
			Resources.Load ("Atlases/Norte");
			Debug.Log ("HISTORIA DE LA PATA SOLA");
			currentStory = 3;
			pataSolaH1.SetActive (true);
		}
	}
	
	private void parseJson(string jsonText){
		Hashtable parsedJson = (Hashtable) JSON.JsonDecode(jsonText);
		titleLabel.text = (string) parsedJson["title"];
		ArrayList fragmentArray  = (ArrayList) parsedJson["fragments"];
		foreach(object o in fragmentArray){
			Hashtable currentFragmentObj = o as Hashtable;
			fragmentList.Add((string)currentFragmentObj["text"]);
		}
		storyFragmentLabel.text = fragmentList[currentFragment];
	}
	
	public void nextFragment(){
		currentFragment++;
		storyFragmentLabel.text = fragmentList[currentFragment];
		if(currentFragment == (fragmentList.Count - 1)){
			questionnaire.SetActive(true);
		}
		Debug.Log ("Fragmento Actual: "+currentFragment);
		
		if (currentFragment == 1) {
			
			if (currentStory == 1){
				guandoH1.SetActive (false);	
				guandoH2.SetActive (true);
			}
			if (currentStory == 2){
				madreMonteH1.SetActive (false);	
				madreMonteH2.SetActive (true);
			}
			if (currentStory == 3){
				pataSolaH1.SetActive (false);	
				pataSolaH2.SetActive (true);
			}
		}
		
		if (currentFragment == 2) {
			
			if (currentStory == 1){
				guandoH2.SetActive (false);	
				guandoH3.SetActive (true);
			}
			if (currentStory == 2){
				madreMonteH2.SetActive (false);	
				madreMonteH3.SetActive (true);
			}
			if (currentStory == 3){
				pataSolaH2.SetActive (false);	
				pataSolaH3.SetActive (true);
			}
		}
		
		if (currentFragment == 3) {
			
			if (currentStory == 1){
				guandoH3.SetActive (false);	
				guandoH4.SetActive (true);
			}
			if (currentStory == 2){
				madreMonteH3.SetActive (false);	
				madreMonteH4.SetActive (true);
			}
			if (currentStory == 3){
				pataSolaH3.SetActive (false);	
				pataSolaH4.SetActive (true);
			}
		}
		
		if (currentFragment == 4) {
			
			if (currentStory == 2){
				madreMonteH4.SetActive (false);	
				madreMonteH5.SetActive (true);
			}

		}

	}
	
	public void prevFragment(){
		currentFragment--;
		storyFragmentLabel.text = fragmentList[currentFragment];
	}
	
	void Update(){
		
		if(currentFragment == 0){
			prevBtn.enabled = false;
		} else if(currentFragment == (fragmentList.Count - 1)){
			nextBtn.enabled = false;
		} else{
			prevBtn.enabled = true;
			nextBtn.enabled = true;
		}
	}
	
	public void loadQuestionnaire(){
		PlayerPrefs.SetString("questionnaire",storyToLoad);
		if(bridge.isRecordingNow()){
			bridge.recordAudio(titleLabel.text);
		}
		Application.LoadLevel("TriviaGUI");
	}
	
	public void record(){
		if(!bridge.isRecordingNow()){
			recordBtn.gameObject.SetActive(false);
			stopRecordBtn.gameObject.SetActive(true);
		} else{
			recordBtn.gameObject.SetActive(true);
			stopRecordBtn.gameObject.SetActive(false);
		}
		bridge.recordAudio(titleLabel.text);
	}
	
}
