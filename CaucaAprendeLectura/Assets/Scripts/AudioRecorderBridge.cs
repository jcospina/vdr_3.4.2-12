﻿using UnityEngine;
using System.Collections;

public class AudioRecorderBridge : MonoBehaviour {

	private AndroidJavaObject recorder;
	private string[] audioFiles;
	private string folderPath;

	private bool isRecording = false;
	private bool isPlaying = false;	

	void Awake(){
		recorder = new AndroidJavaObject("com.aplimedia.audiorecorder.AudioRecorder");
		folderPath = recorder.Call<string>("setupFolder");
	}

	public void recordAudio(string fileName){
		if(!isRecording){
			recorder.Call("startRecording",folderPath + "/" + fileName + ".3gp");
		} else{
			recorder.Call("stopRecording");
		}
		isRecording = !isRecording;
	}

	public void playAudio(string fileName){
		if(!isPlaying){
			recorder.Call("startPlaying",folderPath + "/" + fileName);
		} else{
			recorder.Call("stopPlaying");
		}
		isPlaying = !isPlaying;
	}

	public string[] getLectures(){
		audioFiles = recorder.Call<string[]>("getLectures");
		return audioFiles;
	}

	void onCompletion(string param){

	}

	public bool isRecordingNow(){
		return isRecording;
	}

}
