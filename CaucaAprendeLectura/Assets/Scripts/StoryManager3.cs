﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MadLevelManager;

public class StoryManager3 : MonoBehaviour {
	
	public UILabel titleLabel;
	public UILabel storyFragmentLabel;
	public UISprite nextBtn;
	public UISprite prevBtn;
	public UISprite recordBtn;
	public UISprite stopRecordBtn;
	public GameObject questionnaire;
	
	//Manejo de las animaciones  de cada historia
	public GameObject vallePatiaH1;
	public GameObject vallePatiaH2;
	public GameObject vallePatiaH3;
	public GameObject vallePatiaH4;
	public GameObject piedraLetreroH1;
	public GameObject piedraLetreroH2;
	public GameObject piedraLetreroH3;
	public GameObject origenMacizoH1;
	public GameObject origenMacizoH2;
	public GameObject origenMacizoH3;
	public GameObject origenMacizoH4;
	public GameObject origenMacizoH5;
	public GameObject origenMacizoH6;
	private int currentStory;
	
	public AudioRecorderBridge bridge;
	
	private string storyToLoad;
	private List<string> fragmentList = new List<string>();
	private int currentFragment = 0;
	
	void Start () {
		
		if (!string.IsNullOrEmpty(MadLevel.arguments)) {
			storyToLoad = MadLevel.arguments;
			TextAsset story = Resources.Load<TextAsset>("Stories/" + storyToLoad);
			parseJson(story.text);
		}

		if (storyToLoad == "mito_valle_patia") {
			Debug.Log ("HISTORIA DEL VALLE DEL PATIA");
			currentStory = 8;
			vallePatiaH1.SetActive (true);
		}
		if (storyToLoad == "piedra_letrero") {
			Debug.Log ("HISTORIA DE LA PIEDRA DEL LETRERO");
			currentStory = 9;
			piedraLetreroH1.SetActive (true);
		}
		if (storyToLoad == "origen_macizo") {
			Debug.Log ("HISTORIA DEL ORIGNE DEL MACIZO");
			currentStory = 10;
			origenMacizoH1.SetActive (true);
		}
	}
	
	private void parseJson(string jsonText){
		Hashtable parsedJson = (Hashtable) JSON.JsonDecode(jsonText);
		titleLabel.text = (string) parsedJson["title"];
		ArrayList fragmentArray  = (ArrayList) parsedJson["fragments"];
		foreach(object o in fragmentArray){
			Hashtable currentFragmentObj = o as Hashtable;
			fragmentList.Add((string)currentFragmentObj["text"]);
		}
		storyFragmentLabel.text = fragmentList[currentFragment];
	}
	
	public void nextFragment(){
		currentFragment++;
		storyFragmentLabel.text = fragmentList[currentFragment];
		if(currentFragment == (fragmentList.Count - 1)){
			questionnaire.SetActive(true);
		}
		Debug.Log ("Fragmento Actual: "+currentFragment);
		
		if (currentFragment == 1) {

			if (currentStory == 9){
				piedraLetreroH1.SetActive (false);	
				piedraLetreroH2.SetActive (true);	
			}
		}
		
		if (currentFragment == 2) {

			if (currentStory == 8){
				vallePatiaH1.SetActive (false);	
				vallePatiaH2.SetActive (true);
			}
			if (currentStory == 9){
				piedraLetreroH2.SetActive (false);	
				piedraLetreroH3.SetActive (true);	
			}
			if (currentStory == 10){
				origenMacizoH1.SetActive (false);	
				origenMacizoH2.SetActive (true);	
			}
		}
		
		if (currentFragment == 3) {

			if (currentStory == 8){
				vallePatiaH2.SetActive (false);	
				vallePatiaH3.SetActive (true);
			}
			if (currentStory == 10){
				origenMacizoH2.SetActive (false);	
				origenMacizoH3.SetActive (true);	
			}
		}
		
		if (currentFragment == 4) {

			if (currentStory == 8){
				vallePatiaH3.SetActive (false);	
				vallePatiaH4.SetActive (true);
			}
			if (currentStory == 10){
				origenMacizoH3.SetActive (false);	
				origenMacizoH4.SetActive (true);	
			}
		}
		
		if (currentFragment == 5) {
			if (currentStory == 10){
				origenMacizoH4.SetActive (false);	
				origenMacizoH6.SetActive (true);	
			}		
		}
	}
	
	public void prevFragment(){
		currentFragment--;
		storyFragmentLabel.text = fragmentList[currentFragment];
	}
	
	void Update(){
		
		if(currentFragment == 0){
			prevBtn.enabled = false;
		} else if(currentFragment == (fragmentList.Count - 1)){
			nextBtn.enabled = false;
		} else{
			prevBtn.enabled = true;
			nextBtn.enabled = true;
		}
	}
	
	public void loadQuestionnaire(){
		PlayerPrefs.SetString("questionnaire",storyToLoad);
		if(bridge.isRecordingNow()){
			bridge.recordAudio(titleLabel.text);
		}
		Application.LoadLevel("TriviaGUI");
	}
	
	public void record(){
		if(!bridge.isRecordingNow()){
			recordBtn.gameObject.SetActive(false);
			stopRecordBtn.gameObject.SetActive(true);
		} else{
			recordBtn.gameObject.SetActive(true);
			stopRecordBtn.gameObject.SetActive(false);
		}
		bridge.recordAudio(titleLabel.text);
	}
	
}
