﻿using UnityEngine;
using System.Collections;
using MadLevelManager;

public class LevelState : MonoBehaviour {

	void Awake(){
		MadLevelProfile.RegisterProfile("CaucaAprende");
		MadLevelProfile.profile = "CaucaAprende";
	}

	void Start () {
		//Centro
		MadLevelProfile.SetLevelString("centro 1", "storyName", "La Muelona");
		MadLevelProfile.SetCompleted("centro 1",true);
		MadLevelProfile.SetLevelString("centro 2", "storyName", "La Viuda");
		MadLevelProfile.SetCompleted("centro 2",true);
		MadLevelProfile.SetLevelString("centro 3", "storyName", "El Monje Descabezado");
		MadLevelProfile.SetCompleted("centro 3",true);
		//Sur
		MadLevelProfile.SetLevelString("sur 1", "storyName", "El Origen ");
		MadLevelProfile.SetCompleted("sur 1",true);
		MadLevelProfile.SetLevelString("sur 2", "storyName", "La piedra del letrero");
		MadLevelProfile.SetCompleted("sur 2",true);
		MadLevelProfile.SetLevelString("sur 3", "storyName", "El Mito del Valle");
		MadLevelProfile.SetCompleted("sur 3",true);
		//Norte
		MadLevelProfile.SetLevelString("norte 1", "storyName", "El Guando");
		MadLevelProfile.SetCompleted("norte 1",true);
		MadLevelProfile.SetLevelString("norte 2", "storyName", "La Patasola");
		MadLevelProfile.SetCompleted("norte 2",true);
		MadLevelProfile.SetLevelString("norte 3", "storyName", "La Madremonte");
		MadLevelProfile.SetCompleted("norte 3",true);
		//Oriente
		MadLevelProfile.SetLevelString("oriente 1", "storyName", "Juan Tama");
		MadLevelProfile.SetCompleted("oriente 1",true);
		MadLevelProfile.SetLevelString("oriente 2", "storyName", "El Origen");
		MadLevelProfile.SetCompleted("oriente 2",true);
		MadLevelProfile.SetLevelString("oriente 3", "storyName", "Llivan");
		MadLevelProfile.SetCompleted("oriente 3",true);
		//Pacifico
		MadLevelProfile.SetLevelString("pacifico 1", "storyName", "El Maravelli");
		MadLevelProfile.SetCompleted("pacifico 1",true);
		MadLevelProfile.SetLevelString("pacifico 2", "storyName", "El Riviel");
		MadLevelProfile.SetCompleted("pacifico 2",true);
		MadLevelProfile.SetLevelString("pacifico 3", "storyName", "Las Tunduraguas");
		MadLevelProfile.SetCompleted("pacifico 3",true);
	}

}
