/*
* Copyright (c) Mad Pixel Machine
* http://www.madpixelmachine.com/
*/

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class MadLevelUpdater : MadUpdater {

    #region Constants

    private const string Prefix = "MadLevelManager_";

    #endregion

    #region Methods

    public static void OnPostprocessAllAssets(string[] i, string[] d, string[] m, string[] mf) {
        if (i.Length == 0) {
            return;
        }

        AssetMoved(Prefix + "grid.png",
            "Assets/Mad Level Manager/Editor/Resources/MadLevelManager/Textures/grid.png",
            "Assets/Mad Level Manager/Scripts/Mad2D/Editor/Resources/Mad2D/Textures/grid.png"
            );
    }

    public static void ResetUpdateData() {
        Reset(Prefix + "grid.png");
        EditorUtility.DisplayDialog("Update Data removed!", "Don't worry if you did it by accident :-)", "OK");
    }

    #endregion
}