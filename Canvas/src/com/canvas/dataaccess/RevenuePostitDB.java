package com.canvas.dataaccess;

public class RevenuePostitDB {
	private int id;
	private double revenue;
	private double otherRevenue;
	private int idPostit;
	private double salesRevenues;
	private double subscriptionRevenue;
	private double payPerUseRevenue;
	private double rentalRevenue;
	private double transFeeRevenue;
	private double salesFeeRevenue;
	private double displayAdsRevenue;
	private double clickAdsRevenue;
	private double licensingRevenue;

	public RevenuePostitDB() {
		super();
	}

	

	public RevenuePostitDB(int id, double revenue, double otherRevenue,
			int idPostit, double salesRevenues, double subscriptionRevenue,
			double payPerUseRevenue, double rentalRevenue,
			double transFeeRevenue, double salesFeeRevenue,
			double displayAdsRevenue, double clickAdsRevenue,
			double licensingRevenue) {
		super();
		this.id = id;
		this.revenue = revenue;
		this.otherRevenue = otherRevenue;
		this.idPostit = idPostit;
		this.salesRevenues = salesRevenues;
		this.subscriptionRevenue = subscriptionRevenue;
		this.payPerUseRevenue = payPerUseRevenue;
		this.rentalRevenue = rentalRevenue;
		this.transFeeRevenue = transFeeRevenue;
		this.salesFeeRevenue = salesFeeRevenue;
		this.displayAdsRevenue = displayAdsRevenue;
		this.clickAdsRevenue = clickAdsRevenue;
		this.licensingRevenue = licensingRevenue;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getRevenue() {
		return revenue;
	}

	public void setRevenue(double revenue) {
		this.revenue = revenue;
	}

	public double getOtherRevenue() {
		return otherRevenue;
	}

	public void setOtherRevenue(double otherRevenue) {
		this.otherRevenue = otherRevenue;
	}

	public int getIdPostit() {
		return idPostit;
	}

	public void setIdPostit(int idPostit) {
		this.idPostit = idPostit;
	}



	public double getSalesRevenues() {
		return salesRevenues;
	}



	public void setSalesRevenues(double salesRevenues) {
		this.salesRevenues = salesRevenues;
	}



	public double getSubscriptionRevenue() {
		return subscriptionRevenue;
	}



	public void setSubscriptionRevenue(double subscriptionRevenue) {
		this.subscriptionRevenue = subscriptionRevenue;
	}



	public double getPayPerUseRevenue() {
		return payPerUseRevenue;
	}



	public void setPayPerUseRevenue(double payPerUseRevenue) {
		this.payPerUseRevenue = payPerUseRevenue;
	}



	public double getRentalRevenue() {
		return rentalRevenue;
	}



	public void setRentalRevenue(double rentalRevenue) {
		this.rentalRevenue = rentalRevenue;
	}



	public double getTransFeeRevenue() {
		return transFeeRevenue;
	}



	public void setTransFeeRevenue(double transFeeRevenue) {
		this.transFeeRevenue = transFeeRevenue;
	}



	public double getSalesFeeRevenue() {
		return salesFeeRevenue;
	}



	public void setSalesFeeRevenue(double salesFeeRevenue) {
		this.salesFeeRevenue = salesFeeRevenue;
	}



	public double getDisplayAdsRevenue() {
		return displayAdsRevenue;
	}



	public void setDisplayAdsRevenue(double displayAdsRevenue) {
		this.displayAdsRevenue = displayAdsRevenue;
	}



	public double getClickAdsRevenue() {
		return clickAdsRevenue;
	}



	public void setClickAdsRevenue(double clickAdsRevenue) {
		this.clickAdsRevenue = clickAdsRevenue;
	}



	public double getLicensingRevenue() {
		return licensingRevenue;
	}



	public void setLicensingRevenue(double licensingRevenue) {
		this.licensingRevenue = licensingRevenue;
	}
	
	

}
