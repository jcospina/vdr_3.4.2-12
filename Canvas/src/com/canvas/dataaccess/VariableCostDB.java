package com.canvas.dataaccess;

public class VariableCostDB {
	private int id;
	private String customerSegment;
	private double marketShare;
	private double averagePurchase;
	private int purchasePeriod;
	private int idCostPostit;
	private int relatedItemId;

	public VariableCostDB() {
		super();
	}

	public VariableCostDB(int id, String customerSegment, double marketShare,
			double averagePurchase, int purchasePeriod, int idCostPostit, int relatedItemId) {
		super();
		this.id = id;
		this.customerSegment = customerSegment;
		this.marketShare = marketShare;
		this.averagePurchase = averagePurchase;
		this.purchasePeriod = purchasePeriod;
		this.idCostPostit = idCostPostit;
		this.relatedItemId = relatedItemId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCustomerSegment() {
		return customerSegment;
	}

	public void setCustomerSegment(String customerSegment) {
		this.customerSegment = customerSegment;
	}

	public double getMarketShare() {
		return marketShare;
	}

	public void setMarketShare(double marketShare) {
		this.marketShare = marketShare;
	}

	public double getAveragePurchase() {
		return averagePurchase;
	}

	public void setAveragePurchase(double averagePurchase) {
		this.averagePurchase = averagePurchase;
	}

	public int getPurchasePeriod() {
		return purchasePeriod;
	}

	public void setPurchasePeriod(int purchasePeriod) {
		this.purchasePeriod = purchasePeriod;
	}

	public int getIdCostPostit() {
		return idCostPostit;
	}

	public void setIdCostPostit(int idCostPostit) {
		this.idCostPostit = idCostPostit;
	}
	
	public int getRelatedItemId() {
		return relatedItemId;
	}
	
	public void setRelatedItemId(int relatedItemId) {
		this.relatedItemId = relatedItemId;
	}

}
