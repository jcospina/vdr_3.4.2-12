package com.canvas.dataaccess;

public class EmpathyMapNoteDB {
	private int id;
	private int idEMPostit;
	private float pX;
	private float pY;
	private String title;
	private String description;
	private int color;

	public EmpathyMapNoteDB() {
		super();
	}

	public EmpathyMapNoteDB(int id, int idEMPostit, float pX, float pY,
			String title, String description, int color) {
		super();
		this.id = id;
		this.idEMPostit = idEMPostit;
		this.pX = pX;
		this.pY = pY;
		this.title = title;
		this.description = description;
		this.color = color;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdEMPostit() {
		return idEMPostit;
	}

	public void setIdEMPostit(int idEMPostit) {
		this.idEMPostit = idEMPostit;
	}

	public float getpX() {
		return pX;
	}

	public void setpX(float pX) {
		this.pX = pX;
	}

	public float getpY() {
		return pY;
	}

	public void setpY(float pY) {
		this.pY = pY;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

}
