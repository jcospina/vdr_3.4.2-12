package com.canvas.dataaccess;

public class SubscriptionItemDB {
	
	private int id;
	private double subscriptionPrice;
	private int idRevenuePostit;
	private int idRevenueItem;

	public SubscriptionItemDB() {
		super();
	}

	public SubscriptionItemDB(int id, double subscriptionPrice, int idRevenuePostit, int idRevenueItem) {
		super();
		this.id = id;
		this.subscriptionPrice = subscriptionPrice;
		this.idRevenuePostit = idRevenuePostit;
		this.idRevenueItem = idRevenueItem;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public double getSubscriptionPrice() {
		return subscriptionPrice;
	}

	public void setSubscriptionPrice(double subscriptionPrice) {
		this.subscriptionPrice = subscriptionPrice;
	}

	public int getIdRevenuePostit() {
		return idRevenuePostit;
	}

	public void setIdRevenuePostit(int idRevenuePostit) {
		this.idRevenuePostit = idRevenuePostit;
	}
	
	public int getIdRevenueItem() {
		return idRevenueItem;
	}
	
	public void setIdRevenueItem(int idRevenueItem) {
		this.idRevenueItem = idRevenueItem;
	}

}
