package com.canvas.dataaccess;

public class CostPostitDB {
	private int id;
	private double cost;
	private double otherCost;
	private int idPostit;
	private double variableCost;

	public CostPostitDB() {
		super();
	}

	public CostPostitDB(int id, double cost, double otherCost, int idPostit, double variableCost) {
		super();
		this.id = id;
		this.cost = cost;
		this.otherCost = otherCost;
		this.idPostit = idPostit;
		this.variableCost = variableCost;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public double getOtherCost() {
		return otherCost;
	}

	public void setOtherCost(double otherCost) {
		this.otherCost = otherCost;
	}

	public int getIdPostit() {
		return idPostit;
	}

	public void setIdPostit(int idPostit) {
		this.idPostit = idPostit;
	}
	
	public double getVariableCost() {
		return variableCost;
	}
	
	public void setVariableCost(double variableCost) {
		this.variableCost = variableCost;
	}

}
