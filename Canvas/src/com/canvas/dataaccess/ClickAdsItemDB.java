package com.canvas.dataaccess;

public class ClickAdsItemDB {
	private int id;
	private float averagePageViewPerUser;
	private float averageAdsPerPage;
	private float averageCTR;
	private float averageCPC;
	private int idRevenuePostit;
	private int idRevenueItem;

	public ClickAdsItemDB() {
		super();
	}

	public ClickAdsItemDB(int id, float averagePageViewPerUser, float averageAdsPerPage,
			float averageCTR, float averageCPC, int idRevenuePostit, int idRevenueItem) {
		super();
		this.id = id;
		this.averagePageViewPerUser = averagePageViewPerUser;
		this.averageAdsPerPage = averageAdsPerPage;
		this.averageCTR = averageCTR;
		this.averageCPC = averageCPC;
		this.idRevenuePostit = idRevenuePostit;
		this.idRevenueItem = idRevenueItem;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public float getAveragePageViewPerUser() {
		return averagePageViewPerUser;
	}

	public void setAveragePageViewPerUser(float averagePageViewPerUser) {
		this.averagePageViewPerUser = averagePageViewPerUser;
	}

	public float getAverageAdsPerPage() {
		return averageAdsPerPage;
	}

	public void setAverageAdsPerPage(float averageAdsPerPage) {
		this.averageAdsPerPage = averageAdsPerPage;
	}

	public float getAverageCTR() {
		return averageCTR;
	}

	public void setAverageCTR(float averageCTR) {
		this.averageCTR = averageCTR;
	}

	public float getAverageCPC() {
		return averageCPC;
	}

	public void setAverageCPC(float averageCPC) {
		this.averageCPC = averageCPC;
	}

	public int getIdRevenuePostit() {
		return idRevenuePostit;
	}

	public void setIdRevenuePostit(int idRevenuePostit) {
		this.idRevenuePostit = idRevenuePostit;
	}
	
	public int getIdRevenueItem() {
		return idRevenueItem;
	}
	
	public void setIdRevenueItem(int idRevenueItem) {
		this.idRevenueItem = idRevenueItem;
	}

}
