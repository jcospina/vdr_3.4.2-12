package com.canvas.dataaccess;

public class CanvasDB {

	private int id;	
	private byte[] image;
	private String name;
	private String autor;

	public CanvasDB() {
		super();
	}

	public CanvasDB(int id, byte[] image, String name, String autor) {
		super();
		this.id = id;
		this.image = image;
		this.name = name;
		this.autor = autor;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getAutor() {
		return autor;
	}
	
	public void setAutor(String autor) {
		this.autor = autor;
	}

}
