package com.canvas.dataaccess;

public class EmpathyMapPostitDB {
	private int id;
	private int idPostit;
	private byte[] bitmap;

	public EmpathyMapPostitDB() {
		super();
	}

	public EmpathyMapPostitDB(int id, int idPostit, byte[] bitmap) {
		super();
		this.id = id;
		this.idPostit = idPostit;
		this.bitmap = bitmap;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdPostit() {
		return idPostit;
	}

	public void setIdPostit(int idPostit) {
		this.idPostit = idPostit;
	}

	public byte[] getBitmap() {
		return bitmap;
	}

	public void setBitmap(byte[] bitmap) {
		this.bitmap = bitmap;
	}

}
