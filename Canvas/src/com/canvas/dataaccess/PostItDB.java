package com.canvas.dataaccess;

import com.smartsoft.Educanvas.PostIt;

public class PostItDB {
	private int id;
	private String title;
	private String description;
	private int color;
	private float pX;
	private float pY;
	private int idCanvas;
	private int block;

	public PostItDB() {
		super();
	}

	public PostItDB(int id, String title, String description, int color,
			float pX, float pY, int idCanvas, int block) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.color = color;
		this.pX = pX;
		this.pY = pY;
		this.idCanvas = idCanvas;
		this.block = block;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public float getpX() {
		return pX;
	}

	public void setpX(float pX) {
		this.pX = pX;
	}

	public float getpY() {
		return pY;
	}

	public void setpY(float pY) {
		this.pY = pY;
	}

	public int getIdCanvas() {
		return idCanvas;
	}

	public void setIdCanvas(int idCanvas) {
		this.idCanvas = idCanvas;
	}

	public int getBlock() {
		return block;
	}

	public void setBlock(int block) {
		this.block = block;
	}

	public static PostItDB fromPostit(PostIt p, int id, int canvasId) {		
		PostItDB pdb = new PostItDB(id, p.getTitle(), p.getDescription(), p
				.getColor().getARGBPackedInt(), p.getPosX(), p.getPosY(),
				canvasId, p.getParentBlock().getBlockNumber());
		return pdb;
	}
}
