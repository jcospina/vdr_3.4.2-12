package com.canvas.dataaccess;

public class KeyPartnerPostitDB {
	private int id;
	private float pSX;
	private float pSY;
	private int idPostit;

	public KeyPartnerPostitDB() {
		super();
	}

	public KeyPartnerPostitDB(int id, float pSX, float pSY, int idPostit) {
		super();
		this.id = id;
		this.pSX = pSX;
		this.pSY = pSY;
		this.idPostit = idPostit;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getpSX() {
		return pSX;
	}

	public void setpSX(float pSX) {
		this.pSX = pSX;
	}

	public float getpSY() {
		return pSY;
	}

	public void setpSY(float pSY) {
		this.pSY = pSY;
	}

	public int getIdPostit() {
		return idPostit;
	}

	public void setIdPostit(int idPostit) {
		this.idPostit = idPostit;
	}

}
