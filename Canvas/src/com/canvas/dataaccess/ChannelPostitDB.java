package com.canvas.dataaccess;

public class ChannelPostitDB {

	private int id;
	private int channel;
	private int idPostit;

	public ChannelPostitDB() {
		super();
	}

	public ChannelPostitDB(int id, int channel, int idPostit) {
		super();
		this.id = id;
		this.channel = channel;
		this.idPostit = idPostit;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getChannel() {
		return channel;
	}

	public void setChannel(int channel) {
		this.channel = channel;
	}

	public int getIdPostit() {
		return idPostit;
	}

	public void setIdPostit(int idPostit) {
		this.idPostit = idPostit;
	}

}
