package com.canvas.dataaccess;

public class SalesFeeItemDB {
	
	private int id;
	private double averagePurchase;
	private double averagePrice;
	private double saleFee;
	private int idRevenuePostit;
	private int idRevenueItem;

	public SalesFeeItemDB() {
		super();
	}

	public SalesFeeItemDB(int id, double averagePurchase, double averagePrice,
			double saleFee, int idRevenuePostit, int idRevenueItem) {
		super();
		this.id = id;
		this.averagePurchase = averagePurchase;
		this.averagePrice = averagePrice;
		this.saleFee = saleFee;
		this.idRevenuePostit = idRevenuePostit;
		this.idRevenueItem = idRevenueItem;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public double getAveragePurchase() {
		return averagePurchase;
	}

	public void setAveragePurchase(double averagePurchase) {
		this.averagePurchase = averagePurchase;
	}

	public double getAveragePrice() {
		return averagePrice;
	}

	public void setAveragePrice(double averagePrice) {
		this.averagePrice = averagePrice;
	}

	public double getSaleFee() {
		return saleFee;
	}

	public void setSaleFee(double saleFee) {
		this.saleFee = saleFee;
	}

	public int getIdRevenuePostit() {
		return idRevenuePostit;
	}

	public void setIdRevenuePostit(int idRevenuePostit) {
		this.idRevenuePostit = idRevenuePostit;
	}
	
	public int getIdRevenueItem() {
		return idRevenueItem;
	}
	
	public void setIdRevenueItem(int idRevenueItem) {
		this.idRevenueItem = idRevenueItem;
	}

}
