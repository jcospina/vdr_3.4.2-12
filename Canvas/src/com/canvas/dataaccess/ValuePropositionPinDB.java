package com.canvas.dataaccess;

public class ValuePropositionPinDB {
	private int id;
	private float pX;
	private float pY;
	private int idPostit;

	public ValuePropositionPinDB() {
		super();
	}

	public ValuePropositionPinDB(int id, float pX, float pY, int idPostit) {
		super();
		this.id = id;
		this.pX = pX;
		this.pY = pY;
		this.idPostit = idPostit;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getpX() {
		return pX;
	}

	public void setpX(float pX) {
		this.pX = pX;
	}

	public float getpY() {
		return pY;
	}

	public void setpY(float pY) {
		this.pY = pY;
	}

	public int getIdPostit() {
		return idPostit;
	}

	public void setIdPostit(int idPostit) {
		this.idPostit = idPostit;
	}

}
