package com.canvas.dataaccess;

public class RelationShipPostitDB {
	private int idPostit;
	private int idValueProposition;
	private int idCostumer;
	
	public RelationShipPostitDB() {
		super();
	}

	public RelationShipPostitDB(int idPostit, int idValueProposition,
			int idCostumer) {
		super();
		this.idPostit = idPostit;
		this.idValueProposition = idValueProposition;
		this.idCostumer = idCostumer;
	}

	public int getIdPostit() {
		return idPostit;
	}

	public void setIdPostit(int idPostit) {
		this.idPostit = idPostit;
	}

	public int getIdValueProposition() {
		return idValueProposition;
	}

	public void setIdValueProposition(int idValueProposition) {
		this.idValueProposition = idValueProposition;
	}

	public int getIdCostumer() {
		return idCostumer;
	}

	public void setIdCostumer(int idCostumer) {
		this.idCostumer = idCostumer;
	}
	
}
