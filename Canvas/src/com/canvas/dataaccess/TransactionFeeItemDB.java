package com.canvas.dataaccess;

public class TransactionFeeItemDB {
	
	private int id;
	private double averageTransPerClient;
	private double averagePricePerTrans;
	private double transFee;
	private int idRevenuePostit;
	private int idRevenueItem;

	public TransactionFeeItemDB() {
		super();
	}

	public TransactionFeeItemDB(int id, double averageTransPerClient,
			double averagePricePerTrans, double transFee, int idRevenuePostit, int idRevenueItem) {
		super();
		this.id = id;
		this.averageTransPerClient = averageTransPerClient;
		this.averagePricePerTrans = averagePricePerTrans;
		this.transFee = transFee;
		this.idRevenuePostit = idRevenuePostit;
		this.idRevenueItem = idRevenueItem;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public double getAverageTransPerClient() {
		return averageTransPerClient;
	}

	public void setAverageTransPerClient(double averageTransPerClient) {
		this.averageTransPerClient = averageTransPerClient;
	}

	public double getAveragePricePerTrans() {
		return averagePricePerTrans;
	}

	public void setAveragePricePerTrans(double averagePricePerTrans) {
		this.averagePricePerTrans = averagePricePerTrans;
	}

	public double getTransFee() {
		return transFee;
	}

	public void setTransFee(double transFee) {
		this.transFee = transFee;
	}

	public int getIdRevenuePostit() {
		return idRevenuePostit;
	}

	public void setIdRevenuePostit(int idRevenuePostit) {
		this.idRevenuePostit = idRevenuePostit;
	}
	
	public int getIdRevenueItem() {
		return idRevenueItem;
	}
	
	public void setIdRevenueItem(int idRevenueItem) {
		this.idRevenueItem = idRevenueItem;
	}

}
