package com.canvas.dataaccess;

public class SalesItemDB {
	
	private int id;
	private double averagePurchase;
	private double averagePrice;
	private int idRevenueIPostit;
	private int idRevenueItem;

	public SalesItemDB() {
		super();
	}

	public SalesItemDB(int id, double averagePurchase, double averagePrice,
			int idRevenueIPostit, int idRevenueItem) {
		super();
		this.id = id;
		this.averagePurchase = averagePurchase;
		this.averagePrice = averagePrice;
		this.idRevenueIPostit = idRevenueIPostit;
		this.idRevenueItem = idRevenueItem;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public double getAveragePurchase() {
		return averagePurchase;
	}

	public void setAveragePurchase(double averagePurchase) {
		this.averagePurchase = averagePurchase;
	}

	public double getAveragePrice() {
		return averagePrice;
	}

	public void setAveragePrice(double averagePrice) {
		this.averagePrice = averagePrice;
	}

	public int getIdRevenuePostit() {
		return idRevenueIPostit;
	}

	public void setIdRevenuePostit(int idRevenueIPostit) {
		this.idRevenueIPostit = idRevenueIPostit;
	}
	
	public int getIdRevenueItem() {
		return idRevenueItem;
	}
	
	public void setIdRevenueItem(int idRevenueItem) {
		this.idRevenueItem = idRevenueItem;
	}

}
