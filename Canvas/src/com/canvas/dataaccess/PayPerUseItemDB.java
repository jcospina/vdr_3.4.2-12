package com.canvas.dataaccess;

public class PayPerUseItemDB {
	private int id;
	private double averageUnits;
	private double averageUnitPrice;
	private int idRevenuePostit;
	private int idRevenueItem;

	public PayPerUseItemDB() {
		super();
	}

	public PayPerUseItemDB(int id, double averageUnits, double averageUnitPrice,
			int idRevenuePostit, int idRevenueItem) {
		super();
		this.id = id;
		this.averageUnits = averageUnits;
		this.averageUnitPrice = averageUnitPrice;
		this.idRevenuePostit = idRevenuePostit;
		this.idRevenueItem = idRevenueItem;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public double getAverageUnits() {
		return averageUnits;
	}
	
	public void setAverageUnits(double averageUnits) {
		this.averageUnits = averageUnits;
	}

	public double getAverageUnitPrice() {
		return averageUnitPrice;
	}

	public void setAverageUnitPrice(double averageUnitPrice) {
		this.averageUnitPrice = averageUnitPrice;
	}

	public int getIdRevenuePostit() {
		return idRevenuePostit;
	}

	public void setIdRevenuePostit(int idRevenuePostit) {
		this.idRevenuePostit = idRevenuePostit;
	}
	
	public int getIdRevenueItem() {
		return idRevenueItem;
	}
	
	public void setIdRevenueItem(int idRevenueItem) {
		this.idRevenueItem = idRevenueItem;
	}

}
