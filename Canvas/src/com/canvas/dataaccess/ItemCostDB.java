package com.canvas.dataaccess;

public class ItemCostDB {
	private int id;
	private String item;
	private int block;
	private double cost;
	private int idCostPostit;
	private double variableCost;
	private int relatedItemId;

	public ItemCostDB() {
		super();
	}

	public ItemCostDB(int id, String item, int block, double cost,
			int idCostPostit, double variableCost, int relatedItemId) {
		super();
		this.id = id;
		this.item = item;
		this.block = block;
		this.cost = cost;
		this.idCostPostit = idCostPostit;
		this.variableCost = variableCost;
		this.relatedItemId = relatedItemId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public int getBlock() {
		return block;
	}

	public void setBlock(int block) {
		this.block = block;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public int getIdCostPostit() {
		return idCostPostit;
	}

	public void setIdCostPostit(int idCostPostit) {
		this.idCostPostit = idCostPostit;

	}

	public double getVariableCost() {
		return variableCost;
	}

	public void setVariableCost(double variableCost) {
		this.variableCost = variableCost;
	}
	
	public int getRelatedItemId() {
		return relatedItemId;
	}
	
	public void setRelatedItemId(int relatedItemId) {
		this.relatedItemId = relatedItemId;
	}

}
