package com.canvas.dataaccess;

public class DisplayAdItemDB {
	
	private int id;
	private float averagePageViewPerUser;
	private float averageAdUnitsPerPage;
	private float averageCPMPerAdUnit;
	private int idRevenuePostIt;
	private int idRevenueItem;

	public DisplayAdItemDB() {
		super();
	}

	public DisplayAdItemDB(int id, float averagePageViewPerUser,
			float averageAdUnitsPerPage, float averageCPMPerAdUnit,
			int idRevenuePostIt, int idRevenueItem) {
		super();
		this.id = id;
		this.averagePageViewPerUser = averagePageViewPerUser;
		this.averageAdUnitsPerPage = averageAdUnitsPerPage;
		this.averageCPMPerAdUnit = averageCPMPerAdUnit;
		this.idRevenuePostIt = idRevenuePostIt;
		this.idRevenueItem = idRevenueItem;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public float getAveragePageViewPerUser() {
		return averagePageViewPerUser;
	}

	public void setAveragePageViewPerUser(float averagePageViewPerUser) {
		this.averagePageViewPerUser = averagePageViewPerUser;
	}

	public float getAverageAdUnitsPerPage() {
		return averageAdUnitsPerPage;
	}

	public void setAverageAdUnitsPerPage(float averageAdUnitsPerPage) {
		this.averageAdUnitsPerPage = averageAdUnitsPerPage;
	}

	public float getAverageCPMPerAdUnit() {
		return averageCPMPerAdUnit;
	}

	public void setAverageCPMPerAdUnit(float averageCPMPerAdUnit) {
		this.averageCPMPerAdUnit = averageCPMPerAdUnit;
	}

	public int getIdRevenuePostIt() {
		return idRevenuePostIt;
	}

	public void setIdRevenuePostIt(int idRevenuePostIt) {
		this.idRevenuePostIt = idRevenuePostIt;
	}
	
	public int getIdRevenueItem() {
		return idRevenueItem;
	}
	
	public void setIdRevenueItem(int idRevenueItem) {
		this.idRevenueItem = idRevenueItem;
	}

}
