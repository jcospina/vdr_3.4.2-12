package com.canvas.dataaccess.controller;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.canvas.dataaccess.ChannelPostitDB;
import com.canvas.dataaccess.SQLiteDatabaseAdapter;
import com.smartsoft.Educanvas.R;

public class ChannelPostitController {
	
	private SQLiteDatabase db;
	private Context context;
	static String tableName = "ChannelPostit";
	
	public ChannelPostitController(Context context) {		
		this.context = context;
		SQLiteDatabaseAdapter aSQLiteDatabaseAdapter = SQLiteDatabaseAdapter
				.getInstance(this.context);
		this.db = aSQLiteDatabaseAdapter.getWritableDatabase();
	}
	
	public long create(ChannelPostitDB obj) {
		try {
			ContentValues cv = new ContentValues();
			//cv.put("id", obj.getId());
			cv.put("channel", obj.getChannel());
			cv.put("idPostit", obj.getIdPostit());
			long rowid = db.insert(tableName,
					context.getString(R.string.db_name), cv);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public long update(ChannelPostitDB obj) {

		try {
			ContentValues cv = new ContentValues();
			cv.put("id", obj.getId());
			cv.put("channel", obj.getChannel());
			cv.put("idPostit", obj.getIdPostit());
			String where = "id = " + obj.getId();
			long rowid = db.update(tableName, cv, where, null);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public boolean createAll(List<ChannelPostitDB> objs) {
		boolean success = true;
		
		for (int i = 0; i < objs.size(); i++) {
			long result = create(objs.get(i));
			if (success && result == -1) {
				success = false;
			}
		}
		
		return success;
	}

	public ChannelPostitDB getById(int id) {
		String sql = "SELECT * FROM " + tableName + " WHERE id = " + id;
		Cursor c = db.rawQuery(sql, null);

		ChannelPostitDB obj = null;

		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			obj = new ChannelPostitDB();
			obj.setId(c.getInt(0));
			obj.setChannel(c.getInt(1));
			obj.setIdPostit(c.getInt(2));
		}
		c.close();
		return obj;
	}
	
	/**
	 * Obtiene el postit específico a partir del postitId
	 * @param postitId
	 * @return
	 */
	public ChannelPostitDB getByPostitId(int postitId) {
		String sql = "SELECT * FROM " + tableName + " WHERE idPostit = " + postitId;
		Cursor c = db.rawQuery(sql, null);

		ChannelPostitDB obj = null;

		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			obj = new ChannelPostitDB();
			obj.setId(c.getInt(0));
			obj.setChannel(c.getInt(1));
			obj.setIdPostit(c.getInt(2));
		}
		c.close();
		return obj;
	}

	public List<ChannelPostitDB> getAll() {
		String sql = "SELECT * FROM " + tableName;
		Cursor c = db.rawQuery(sql, null);

		List<ChannelPostitDB> objs = new ArrayList<ChannelPostitDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				ChannelPostitDB obj = new ChannelPostitDB();
				obj.setId(c.getInt(0));
				obj.setChannel(c.getInt(1));
				obj.setIdPostit(c.getInt(2));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}
	
	public boolean removeEntry(int id) {
		String sql = "DELETE FROM " + tableName + " WHERE id = " + id;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean removeAll() {
		String sql = "DELETE FROM " + tableName;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean removeAllByPostitId(int postitId) {
		String sql = "DELETE FROM " + tableName + " WHERE idPostit = "
				+ postitId;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
