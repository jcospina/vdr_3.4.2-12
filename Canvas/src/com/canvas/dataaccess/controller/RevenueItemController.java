package com.canvas.dataaccess.controller;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.canvas.dataaccess.RevenueItemDB;
import com.canvas.dataaccess.SQLiteDatabaseAdapter;
import com.smartsoft.Educanvas.R;

public class RevenueItemController {
	private SQLiteDatabase db;
	private Context context;
	static String tableName = "RevenueItem";

	public RevenueItemController(Context context) {
		this.context = context;
		SQLiteDatabaseAdapter aSQLiteDatabaseAdapter = SQLiteDatabaseAdapter
				.getInstance(this.context);
		this.db = aSQLiteDatabaseAdapter.getWritableDatabase();
	}

	public long create(RevenueItemDB obj) {
		try {
			ContentValues cv = new ContentValues();
			// cv.put("id", obj.getId());
			cv.put("name", obj.getName());
			cv.put("marketShare", obj.getMarketShare());
			cv.put("purchasePeriod", obj.getPurchasePeriod());
			cv.put("idRevenuePostit", obj.getIdRevenuePostit());
			cv.put("revenueType", obj.getRevenueType());
			cv.put("valueProposition", obj.getValueProposition());
			cv.put("relatedItemId", obj.getRelatedItemId());
			long rowid = db.insert(tableName,
					context.getString(R.string.db_name), cv);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public long update(RevenueItemDB obj) {

		try {
			ContentValues cv = new ContentValues();
			cv.put("id", obj.getId());
			cv.put("name", obj.getName());
			cv.put("marketShare", obj.getMarketShare());
			cv.put("purchasePeriod", obj.getPurchasePeriod());
			cv.put("idRevenuePostit", obj.getIdRevenuePostit());
			cv.put("revenueType", obj.getRevenueType());
			cv.put("valueProposition", obj.getValueProposition());
			cv.put("relatedItemId", obj.getRelatedItemId());
			String where = "id = " + obj.getId();
			long rowid = db.update(tableName, cv, where, null);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public boolean createAll(List<RevenueItemDB> objs) {
		boolean success = true;

		for (int i = 0; i < objs.size(); i++) {
			long result = create(objs.get(i));
			if (success && result == -1) {
				success = false;
			}
		}

		return success;
	}

	public RevenueItemDB getById(int id) {
		String sql = "SELECT * FROM " + tableName + " WHERE id = " + id;
		Cursor c = db.rawQuery(sql, null);

		RevenueItemDB obj = null;

		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			obj = new RevenueItemDB();
			obj.setId(c.getInt(0));
			obj.setName(c.getString(1));
			obj.setMarketShare(c.getDouble(2));
			obj.setPurchasePeriod(c.getInt(3));
			obj.setIdRevenuePostit(c.getInt(4));
			obj.setRevenueType(c.getInt(5));
			obj.setValueProposition(c.getString(6));
			obj.setRelatedItemId(c.getInt(7));
		}
		c.close();
		return obj;
	}

	/**
	 * Obtiene los items a partir del id del postit
	 * 
	 * @param revenuePostitId
	 * @return
	 */
	public List<RevenueItemDB> getByRevenuePostitId(int revenuePostitId) {
		String sql = "SELECT * FROM " + tableName + " WHERE idRevenuePostit = "
				+ revenuePostitId;
		Cursor c = db.rawQuery(sql, null);

		List<RevenueItemDB> objs = new ArrayList<RevenueItemDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				RevenueItemDB obj = new RevenueItemDB();
				obj.setId(c.getInt(0));
				obj.setName(c.getString(1));
				obj.setMarketShare(c.getDouble(2));
				obj.setPurchasePeriod(c.getInt(3));
				obj.setIdRevenuePostit(c.getInt(4));
				obj.setRevenueType(c.getInt(5));
				obj.setValueProposition(c.getString(6));
				obj.setRelatedItemId(c.getInt(7));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}

	/**
	 * Obtiene los items a partir del id del postit y del tipo de ingreso
	 * 
	 * @param revenuePostitId
	 * @return
	 */
	public List<RevenueItemDB> getByRevenuePostitIdAndType(int revenuePostitId,
			int type) {
		String sql = "SELECT * FROM " + tableName + " WHERE idRevenuePostit = "
				+ revenuePostitId + " AND revenueType = " + type;
		Cursor c = db.rawQuery(sql, null);

		List<RevenueItemDB> objs = new ArrayList<RevenueItemDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				RevenueItemDB obj = new RevenueItemDB();
				obj.setId(c.getInt(0));
				obj.setName(c.getString(1));
				obj.setMarketShare(c.getDouble(2));
				obj.setPurchasePeriod(c.getInt(3));
				obj.setIdRevenuePostit(c.getInt(4));
				obj.setRevenueType(c.getInt(5));
				obj.setValueProposition(c.getString(6));
				obj.setRelatedItemId(c.getInt(7));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}

	public List<RevenueItemDB> getAll() {
		String sql = "SELECT * FROM " + tableName;
		Cursor c = db.rawQuery(sql, null);

		List<RevenueItemDB> objs = new ArrayList<RevenueItemDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				RevenueItemDB obj = new RevenueItemDB();
				obj.setId(c.getInt(0));
				obj.setName(c.getString(1));
				obj.setMarketShare(c.getDouble(2));
				obj.setPurchasePeriod(c.getInt(3));
				obj.setIdRevenuePostit(c.getInt(4));
				obj.setRevenueType(c.getInt(5));
				obj.setValueProposition(c.getString(6));
				obj.setRelatedItemId(c.getInt(7));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}

	public boolean removeEntry(int id) {
		String sql = "DELETE FROM " + tableName + " WHERE id = " + id;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean removeEntries(int revenuePostitId) {
		String sql = "DELETE FROM " + tableName + " WHERE idRevenuePostit = "
				+ revenuePostitId;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean removeEntryByItemId(int itemId) {
		String sql = "DELETE FROM " + tableName + " WHERE relatedItemId = "
				+ itemId;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean removeAll() {
		String sql = "DELETE FROM " + tableName;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
