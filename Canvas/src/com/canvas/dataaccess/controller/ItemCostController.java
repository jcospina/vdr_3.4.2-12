package com.canvas.dataaccess.controller;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.canvas.dataaccess.ItemCostDB;
import com.canvas.dataaccess.SQLiteDatabaseAdapter;
import com.smartsoft.Educanvas.R;

public class ItemCostController {
	private SQLiteDatabase db;
	private Context context;
	static String tableName = "ItemCost";

	public ItemCostController(Context context) {
		this.context = context;
		SQLiteDatabaseAdapter aSQLiteDatabaseAdapter = SQLiteDatabaseAdapter
				.getInstance(this.context);
		this.db = aSQLiteDatabaseAdapter.getWritableDatabase();
	}

	public long create(ItemCostDB obj) {
		try {
			ContentValues cv = new ContentValues();
			// cv.put("id", obj.getId());
			cv.put("item", obj.getItem());
			cv.put("block", obj.getBlock());
			cv.put("cost", obj.getCost());
			cv.put("idCostPostit", obj.getIdCostPostit());
			cv.put("variableCost", obj.getVariableCost());
			cv.put("relatedItemId", obj.getRelatedItemId());
			long rowid = db.insert(tableName,
					context.getString(R.string.db_name), cv);
			return rowid;
		} catch (Exception ex) {
			// System.out.println(ex.getMessage());
			return -1;
		}
	}

	public long update(ItemCostDB obj) {

		try {
			ContentValues cv = new ContentValues();
			cv.put("id", obj.getId());
			cv.put("item", obj.getItem());
			cv.put("block", obj.getBlock());
			cv.put("cost", obj.getCost());
			cv.put("idCostPostit", obj.getIdCostPostit());
			cv.put("variableCost", obj.getVariableCost());
			cv.put("relatedItemId", obj.getRelatedItemId());
			String where = "id = " + obj.getId();
			long rowid = db.update(tableName, cv, where, null);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public boolean createAll(List<ItemCostDB> objs) {
		boolean success = true;

		for (int i = 0; i < objs.size(); i++) {
			long result = create(objs.get(i));
			if (success && result == -1) {
				success = false;
			}
		}

		return success;
	}

	public ItemCostDB getById(int id) {
		String sql = "SELECT * FROM " + tableName + " WHERE id = " + id;
		Cursor c = db.rawQuery(sql, null);

		ItemCostDB obj = null;

		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			obj = new ItemCostDB();
			obj.setId(c.getInt(0));
			obj.setItem(c.getString(1));
			obj.setBlock(c.getInt(2));
			obj.setCost(c.getDouble(3));
			obj.setIdCostPostit(c.getInt(4));
			obj.setVariableCost(c.getDouble(5));
			obj.setRelatedItemId(c.getInt(6));
		}
		c.close();
		return obj;
	}

	/**
	 * Retorna los elementos del estimador de costos a partir del id de postit
	 * 
	 * @param costPostitId
	 * @return
	 */
	public List<ItemCostDB> getByCostPostitId(int costPostitId) {
		String sql = "SELECT * FROM " + tableName + " WHERE idCostPostit = "
				+ costPostitId;
		Cursor c = db.rawQuery(sql, null);

		List<ItemCostDB> objs = new ArrayList<ItemCostDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				ItemCostDB obj = new ItemCostDB();
				obj.setId(c.getInt(0));
				obj.setItem(c.getString(1));
				obj.setBlock(c.getInt(2));
				obj.setCost(c.getDouble(3));
				obj.setIdCostPostit(c.getInt(4));
				obj.setVariableCost(c.getDouble(5));
				obj.setRelatedItemId(c.getInt(6));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}

	public List<ItemCostDB> getAll() {
		String sql = "SELECT * FROM " + tableName;
		Cursor c = db.rawQuery(sql, null);

		List<ItemCostDB> objs = new ArrayList<ItemCostDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				ItemCostDB obj = new ItemCostDB();
				obj.setId(c.getInt(0));
				obj.setItem(c.getString(1));
				obj.setBlock(c.getInt(2));
				obj.setCost(c.getDouble(3));
				obj.setIdCostPostit(c.getInt(4));
				obj.setVariableCost(c.getDouble(5));
				obj.setRelatedItemId(c.getInt(6));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}

	/**
	 * Filtra los postits de acuerdo al bloque en el que se encuentran
	 * 
	 * @param postits
	 * @param block
	 * @return
	 */
	public List<ItemCostDB> filterByBlock(List<ItemCostDB> postits, int block) {
		List<ItemCostDB> filterPostits = new ArrayList<ItemCostDB>();
		for (ItemCostDB p : postits) {
			if (p.getBlock() == block) {
				filterPostits.add(p);
			}
		}
		return filterPostits;
	}

	public boolean removeEntry(int id) {
		String sql = "DELETE FROM " + tableName + " WHERE id = " + id;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean removeEntries(int idPostit) {
		String sql = "DELETE FROM " + tableName + " WHERE idCostPostit = "
				+ idPostit;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean removeAll() {
		String sql = "DELETE FROM " + tableName;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
