package com.canvas.dataaccess.controller;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.canvas.dataaccess.SQLiteDatabaseAdapter;
import com.canvas.dataaccess.VariableCostDB;
import com.smartsoft.Educanvas.R;

public class VariableCostController {
	private SQLiteDatabase db;
	private Context context;
	static String tableName = "VariableCost";

	public VariableCostController(Context context) {
		this.context = context;
		SQLiteDatabaseAdapter aSQLiteDatabaseAdapter = SQLiteDatabaseAdapter
				.getInstance(this.context);
		this.db = aSQLiteDatabaseAdapter.getWritableDatabase();
	}

	public long create(VariableCostDB obj) {
		try {
			ContentValues cv = new ContentValues();
			// cv.put("id", obj.getId());
			cv.put("customerSegment", obj.getCustomerSegment());
			cv.put("marketShare", obj.getMarketShare());
			cv.put("averagePurchase", obj.getAveragePurchase());
			cv.put("purchasePeriod", obj.getPurchasePeriod());
			cv.put("idCostPostit", obj.getIdCostPostit());
			cv.put("relatedItemId", obj.getRelatedItemId());
			long rowid = db.insert(tableName,
					context.getString(R.string.db_name), cv);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public long update(VariableCostDB obj) {

		try {
			ContentValues cv = new ContentValues();
			cv.put("id", obj.getId());
			cv.put("customerSegment", obj.getCustomerSegment());
			cv.put("marketShare", obj.getMarketShare());
			cv.put("averagePurchase", obj.getAveragePurchase());
			cv.put("purchasePeriod", obj.getPurchasePeriod());
			cv.put("idCostPostit", obj.getIdCostPostit());
			cv.put("relatedItemId", obj.getIdCostPostit());
			String where = "id = " + obj.getId();
			long rowid = db.update(tableName, cv, where, null);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public boolean createAll(List<VariableCostDB> objs) {
		boolean success = true;

		for (int i = 0; i < objs.size(); i++) {
			long result = create(objs.get(i));
			if (success && result == -1) {
				success = false;
			}
		}

		return success;
	}

	public VariableCostDB getById(int id) {
		String sql = "SELECT * FROM " + tableName + " WHERE id = " + id;
		Cursor c = db.rawQuery(sql, null);

		VariableCostDB obj = null;

		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			obj = new VariableCostDB();
			obj.setId(c.getInt(0));
			obj.setCustomerSegment(c.getString(1));
			obj.setMarketShare(c.getDouble(2));
			obj.setAveragePurchase(c.getDouble(3));
			obj.setPurchasePeriod(c.getInt(4));
			obj.setIdCostPostit(c.getInt(5));
			obj.setRelatedItemId(c.getInt(6));
		}
		c.close();
		return obj;
	}

	/**
	 * Retorna los segmentos de clientes para el estimador de costos
	 * 
	 * @param costPostitId
	 * @return
	 */
	public List<VariableCostDB> getByCostPostitId(int costPostitId) {
		String sql = "SELECT * FROM " + tableName + " WHERE idCostPostit = "
				+ costPostitId;
		Cursor c = db.rawQuery(sql, null);

		List<VariableCostDB> objs = new ArrayList<VariableCostDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				VariableCostDB obj = new VariableCostDB();
				obj.setId(c.getInt(0));
				obj.setCustomerSegment(c.getString(1));
				obj.setMarketShare(c.getDouble(2));
				obj.setAveragePurchase(c.getDouble(3));
				obj.setPurchasePeriod(c.getInt(4));
				obj.setIdCostPostit(c.getInt(5));
				obj.setRelatedItemId(c.getInt(6));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}

	public List<VariableCostDB> getAll() {
		String sql = "SELECT * FROM " + tableName;
		Cursor c = db.rawQuery(sql, null);

		List<VariableCostDB> objs = new ArrayList<VariableCostDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				VariableCostDB obj = new VariableCostDB();
				obj.setId(c.getInt(0));
				obj.setCustomerSegment(c.getString(1));
				obj.setMarketShare(c.getDouble(2));
				obj.setAveragePurchase(c.getDouble(3));
				obj.setPurchasePeriod(c.getInt(4));
				obj.setIdCostPostit(c.getInt(5));
				obj.setRelatedItemId(c.getInt(6));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}

	public boolean removeEntry(int id) {
		String sql = "DELETE FROM " + tableName + " WHERE id = " + id;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean removeEntries(int idPostit) {
		String sql = "DELETE FROM " + tableName + " WHERE idCostPostit = "
				+ idPostit;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean removeAll() {
		String sql = "DELETE FROM " + tableName;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
