package com.canvas.dataaccess.controller;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.canvas.dataaccess.CostPostitDB;
import com.canvas.dataaccess.SQLiteDatabaseAdapter;
import com.smartsoft.Educanvas.R;

public class CostPostitController {
	
	private SQLiteDatabase db;
	private Context context;
	static String tableName = "CostPostit";

	public CostPostitController(Context context) {
		this.context = context;
		SQLiteDatabaseAdapter aSQLiteDatabaseAdapter = SQLiteDatabaseAdapter
				.getInstance(this.context);
		this.db = aSQLiteDatabaseAdapter.getWritableDatabase();
	}

	public long create(CostPostitDB obj) {
		try {
			ContentValues cv = new ContentValues();
//			cv.put("id", obj.getId());
			cv.put("cost", obj.getCost());
			cv.put("otherCost", obj.getOtherCost());
			cv.put("idPostit", obj.getIdPostit());
			cv.put("variableCost", obj.getVariableCost());
			long rowid = db.insert(tableName,
					context.getString(R.string.db_name), cv);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public long update(CostPostitDB obj) {

		try {
			ContentValues cv = new ContentValues();
			cv.put("id", obj.getId());
			cv.put("cost", obj.getCost());
			cv.put("otherCost", obj.getOtherCost());
			cv.put("idPostit", obj.getIdPostit());
			cv.put("variableCost", obj.getVariableCost());
			String where = "id = " + obj.getId();			
			long rowid = db.update(tableName, cv, where, null);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public boolean createAll(List<CostPostitDB> objs) {
		boolean success = true;

		for (int i = 0; i < objs.size(); i++) {
			long result = create(objs.get(i));
			if (success && result == -1) {
				success = false;
			}
		}

		return success;
	}

	public CostPostitDB getById(int id) {
		String sql = "SELECT * FROM " + tableName + " WHERE id = " + id;
		Cursor c = db.rawQuery(sql, null);

		CostPostitDB obj = null;

		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			obj = new CostPostitDB();
			obj.setId(c.getInt(0));
			obj.setCost(c.getDouble(1));
			obj.setOtherCost(c.getDouble(2));
			obj.setIdPostit(c.getInt(3));
			obj.setVariableCost(c.getDouble(4));
		}
		c.close();
		return obj;
	}
	
	/**
	 * Obtiene el postit específico a partir del postitId
	 * @param postitId
	 * @return
	 */
	public CostPostitDB getByPostitId(int postitId) {
		String sql = "SELECT * FROM " + tableName + " WHERE idPostit = " + postitId;
		Cursor c = db.rawQuery(sql, null);

		CostPostitDB obj = null;

		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			obj = new CostPostitDB();
			obj.setId(c.getInt(0));
			obj.setCost(c.getDouble(1));
			obj.setOtherCost(c.getDouble(2));
			obj.setIdPostit(c.getInt(3));
			obj.setVariableCost(c.getDouble(4));
		}
		c.close();
		return obj;
	}

	public List<CostPostitDB> getAll() {
		String sql = "SELECT * FROM " + tableName;
		Cursor c = db.rawQuery(sql, null);

		List<CostPostitDB> objs = new ArrayList<CostPostitDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				CostPostitDB obj = new CostPostitDB();
				obj.setId(c.getInt(0));
				obj.setCost(c.getDouble(1));
				obj.setOtherCost(c.getDouble(2));
				obj.setIdPostit(c.getInt(3));
				obj.setVariableCost(c.getDouble(4));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}
	
	public boolean removeEntry(int id) {
		String sql = "DELETE FROM " + tableName + " WHERE id = " + id;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean removeAll() {
		String sql = "DELETE FROM " + tableName;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean removeByPostitId(int postitId) {
		CostPostitDB costPostitDB = getByPostitId(postitId);
		
		if(costPostitDB == null){
			return true;
		}
		
		ItemCostController itemCostController = new ItemCostController(context);
		if(!itemCostController.removeEntries(costPostitDB.getId())){
			return false;
		}
		
		VariableCostController variableCostController = new VariableCostController(context);
		if(!variableCostController.removeEntries(costPostitDB.getId())){
			return false;
		}
		
		return removeEntry(costPostitDB.getId());
	}
}
