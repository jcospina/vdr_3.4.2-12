package com.canvas.dataaccess.controller;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.canvas.dataaccess.RevenuePostitDB;
import com.canvas.dataaccess.SQLiteDatabaseAdapter;
import com.smartsoft.Educanvas.R;

public class RevenuePostitController {
	private SQLiteDatabase db;
	private Context context;
	static String tableName = "RevenuePostit";

	public RevenuePostitController(Context context) {
		this.context = context;
		SQLiteDatabaseAdapter aSQLiteDatabaseAdapter = SQLiteDatabaseAdapter
				.getInstance(this.context);
		this.db = aSQLiteDatabaseAdapter.getWritableDatabase();
	}

	public long create(RevenuePostitDB obj) {
		try {
			ContentValues cv = new ContentValues();
			// cv.put("id", obj.getId());
			cv.put("revenue", obj.getRevenue());
			cv.put("otherRevenue", obj.getOtherRevenue());
			cv.put("idPostit", obj.getIdPostit());
			long rowid = db.insert(tableName,
					context.getString(R.string.db_name), cv);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public long update(RevenuePostitDB obj) {

		try {
			ContentValues cv = new ContentValues();
			cv.put("id", obj.getId());
			cv.put("revenue", obj.getRevenue());
			cv.put("otherRevenue", obj.getOtherRevenue());
			cv.put("idPostit", obj.getIdPostit());
			String where = "id = " + obj.getId();
			long rowid = db.update(tableName, cv, where, null);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public boolean createAll(List<RevenuePostitDB> objs) {
		boolean success = true;

		for (int i = 0; i < objs.size(); i++) {
			long result = create(objs.get(i));
			if (success && result == -1) {
				success = false;
			}
		}

		return success;
	}

	public RevenuePostitDB getById(int id) {
		String sql = "SELECT * FROM " + tableName + " WHERE id = " + id;
		Cursor c = db.rawQuery(sql, null);

		RevenuePostitDB obj = null;

		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			obj = new RevenuePostitDB();
			obj.setId(c.getInt(0));
			obj.setRevenue(c.getDouble(1));
			obj.setOtherRevenue(c.getDouble(2));
			obj.setIdPostit(c.getInt(3));
		}
		c.close();
		return obj;
	}

	/**
	 * Obtiene el postit específico a partir del postitId
	 * 
	 * @param postitId
	 * @return
	 */
	public RevenuePostitDB getByPostitId(int postitId) {
		String sql = "SELECT * FROM " + tableName + " WHERE idPostit = "
				+ postitId;
		Cursor c = db.rawQuery(sql, null);

		RevenuePostitDB obj = null;

		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			obj = new RevenuePostitDB();
			obj.setId(c.getInt(0));
			obj.setRevenue(c.getDouble(1));
			obj.setOtherRevenue(c.getDouble(2));
			obj.setIdPostit(c.getInt(3));
		}
		c.close();
		return obj;
	}

	public List<RevenuePostitDB> getAll() {
		String sql = "SELECT * FROM " + tableName;
		Cursor c = db.rawQuery(sql, null);

		List<RevenuePostitDB> objs = new ArrayList<RevenuePostitDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				RevenuePostitDB obj = new RevenuePostitDB();
				obj.setId(c.getInt(0));
				obj.setRevenue(c.getDouble(1));
				obj.setOtherRevenue(c.getDouble(2));
				obj.setIdPostit(c.getInt(3));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}

	public boolean removeEntry(int id) {
		String sql = "DELETE FROM " + tableName + " WHERE id = " + id;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean removeAll() {
		String sql = "DELETE FROM " + tableName;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean removeAllByPostitId(int postitId) {

		RevenuePostitDB revenuePostitDB = getByPostitId(postitId);

		if (revenuePostitDB == null) {
			return true;
		}

		DisplayAdItemController displayAdItemController = new DisplayAdItemController(
				context);
		if (!displayAdItemController.removeEntries(revenuePostitDB.getId())) {
			return false;
		}

		LicensingItemController licensingItemController = new LicensingItemController(
				context);
		if (!licensingItemController.removeEntries(revenuePostitDB.getId())) {
			return false;
		}

		PayPerUseItemController payPerUseItemController = new PayPerUseItemController(
				context);
		if (!payPerUseItemController.removeEntries(revenuePostitDB.getId())) {
			return false;
		}

		RentalItemController rentalItemController = new RentalItemController(
				context);
		if (!rentalItemController.removeEntries(revenuePostitDB.getId())) {
			return false;
		}

		RevenueItemController revenueItemController = new RevenueItemController(
				context);
		if (!revenueItemController.removeEntries(revenuePostitDB.getId())) {
			return false;
		}

		SalesFeeItemController salesFeeItemController = new SalesFeeItemController(
				context);
		if (!salesFeeItemController.removeEntries(revenuePostitDB.getId())) {
			return false;
		}

		SalesItemController salesItemController = new SalesItemController(
				context);
		if (!salesItemController.removeEntries(revenuePostitDB.getId())) {
			return false;
		}

		SubscriptionItemController subscriptionItemController = new SubscriptionItemController(
				context);
		if (!subscriptionItemController.removeEntries(revenuePostitDB.getId())) {
			return false;
		}

		TransactionFeeItemController transactionFeeItemController = new TransactionFeeItemController(
				context);
		if (!transactionFeeItemController
				.removeEntries(revenuePostitDB.getId())) {
			return false;
		}

		ClickAdsItemController clickAdsItemController = new ClickAdsItemController(
				context);
		if (!clickAdsItemController.removeEntries(revenuePostitDB.getId())) {
			return false;
		}

		return removeEntry(revenuePostitDB.getId());
	}
}
