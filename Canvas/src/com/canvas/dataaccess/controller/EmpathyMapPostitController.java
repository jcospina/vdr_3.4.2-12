package com.canvas.dataaccess.controller;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.canvas.dataaccess.EmpathyMapPostitDB;
import com.canvas.dataaccess.SQLiteDatabaseAdapter;
import com.smartsoft.Educanvas.R;

public class EmpathyMapPostitController {
	private SQLiteDatabase db;
	private Context context;
	static String tableName = "EmpathyMapPostit";

	public EmpathyMapPostitController(Context context) {
		this.context = context;
		SQLiteDatabaseAdapter aSQLiteDatabaseAdapter = SQLiteDatabaseAdapter
				.getInstance(this.context);
		this.db = aSQLiteDatabaseAdapter.getWritableDatabase();
	}

	public long create(EmpathyMapPostitDB obj) {
		try {
			ContentValues cv = new ContentValues();
			// cv.put("id", obj.getId());
			cv.put("idPostit", obj.getIdPostit());
			cv.put("bitmap", obj.getBitmap());
			long rowid = db.insert(tableName,
					context.getString(R.string.db_name), cv);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public long update(EmpathyMapPostitDB obj) {

		try {
			ContentValues cv = new ContentValues();
			cv.put("id", obj.getId());
			cv.put("idPostit", obj.getIdPostit());
			cv.put("bitmap", obj.getBitmap());
			String where = "id = " + obj.getId();
			long rowid = db.update(tableName, cv, where, null);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public boolean createAll(List<EmpathyMapPostitDB> objs) {
		boolean success = true;

		for (int i = 0; i < objs.size(); i++) {
			long result = create(objs.get(i));
			if (success && result == -1) {
				success = false;
			}
		}

		return success;
	}

	public EmpathyMapPostitDB getById(int id) {
		String sql = "SELECT * FROM " + tableName + " WHERE id = " + id;
		Cursor c = db.rawQuery(sql, null);

		EmpathyMapPostitDB obj = null;

		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			obj = new EmpathyMapPostitDB();
			obj.setId(c.getInt(0));
			obj.setIdPostit(c.getInt(1));
			obj.setBitmap(c.getBlob(2));
		}
		c.close();
		return obj;
	}

	/**
	 * Obtiene el postit específico a partir del postitId
	 * 
	 * @param postitId
	 * @return
	 */
	public EmpathyMapPostitDB getByPostitId(int postitId) {
		String sql = "SELECT * FROM " + tableName + " WHERE idPostit = "
				+ postitId;
		Cursor c = db.rawQuery(sql, null);

		EmpathyMapPostitDB obj = null;

		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			obj = new EmpathyMapPostitDB();
			obj.setId(c.getInt(0));
			obj.setIdPostit(c.getInt(1));
			obj.setBitmap(c.getBlob(2));
		}
		c.close();
		return obj;
	}

	public List<EmpathyMapPostitDB> getAll() {
		String sql = "SELECT * FROM " + tableName;
		Cursor c = db.rawQuery(sql, null);

		List<EmpathyMapPostitDB> objs = new ArrayList<EmpathyMapPostitDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				EmpathyMapPostitDB obj = new EmpathyMapPostitDB();
				obj.setId(c.getInt(0));
				obj.setIdPostit(c.getInt(1));
				obj.setBitmap(c.getBlob(2));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}

	public boolean removeEntry(int id) {
		String sql = "DELETE FROM " + tableName + " WHERE id = " + id;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean removeAll() {
		String sql = "DELETE FROM " + tableName;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public void createOrUpdate(EmpathyMapPostitDB empdb) {
		if (empdb.getId() == -1) {
			create(empdb);
		} else {
			update(empdb);
		}
	}

	public boolean removeAllByPostitId(int postitId) {
		EmpathyMapPostitDB empathyMapPostitDB = getByPostitId(postitId);
		if(empathyMapPostitDB == null){
			return true;
		}
		EmpathyMapNoteController empathyMapNoteController = new EmpathyMapNoteController(
				context);
		if (!empathyMapNoteController.removeAllByEMPostitId(empathyMapPostitDB
				.getId())) {
			return false;
		}
		return removeEntry(empathyMapPostitDB.getId());
	}
}
