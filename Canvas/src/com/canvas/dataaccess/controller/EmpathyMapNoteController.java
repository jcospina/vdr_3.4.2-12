package com.canvas.dataaccess.controller;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.canvas.dataaccess.EmpathyMapNoteDB;
import com.canvas.dataaccess.SQLiteDatabaseAdapter;
import com.smartsoft.Educanvas.R;

public class EmpathyMapNoteController {

	private SQLiteDatabase db;
	private Context context;
	static String tableName = "EmpathyMapNote";

	public EmpathyMapNoteController(Context context) {
		this.context = context;
		SQLiteDatabaseAdapter aSQLiteDatabaseAdapter = SQLiteDatabaseAdapter
				.getInstance(this.context);
		this.db = aSQLiteDatabaseAdapter.getWritableDatabase();
	}

	public long create(EmpathyMapNoteDB obj) {
		try {
			ContentValues cv = new ContentValues();
			// cv.put("id", obj.getId());
			cv.put("idEMPostit", obj.getIdEMPostit());
			cv.put("pX", obj.getpX());
			cv.put("pY", obj.getpY());
			cv.put("title", obj.getTitle());
			cv.put("description", obj.getDescription());
			cv.put("color", obj.getColor());
			long rowid = db.insert(tableName,
					context.getString(R.string.db_name), cv);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public long update(EmpathyMapNoteDB obj) {

		try {
			ContentValues cv = new ContentValues();
			cv.put("id", obj.getId());
			cv.put("idEMPostit", obj.getIdEMPostit());
			cv.put("pX", obj.getpX());
			cv.put("pY", obj.getpY());
			cv.put("title", obj.getTitle());
			cv.put("description", obj.getDescription());
			cv.put("color", obj.getColor());
			String where = "id = " + obj.getId();
			long rowid = db.update(tableName, cv, where, null);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public boolean createAll(List<EmpathyMapNoteDB> objs) {
		boolean success = true;

		for (int i = 0; i < objs.size(); i++) {
			long result = create(objs.get(i));
			if (success && result == -1) {
				success = false;
			}
		}

		return success;
	}

	public EmpathyMapNoteDB getById(int id) {
		String sql = "SELECT * FROM " + tableName + " WHERE id = " + id;
		Cursor c = db.rawQuery(sql, null);

		EmpathyMapNoteDB obj = null;

		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			obj = new EmpathyMapNoteDB();
			obj.setId(c.getInt(0));
			obj.setIdEMPostit(c.getInt(1));
			obj.setpX(c.getFloat(2));
			obj.setpY(c.getFloat(3));
			obj.setTitle(c.getString(4));
			obj.setDescription(c.getString(5));
			obj.setColor(c.getInt(6));
		}
		c.close();
		return obj;
	}

	/**
	 * Obtiene la lista de notas del mapa de empat�a a partir del id del mapa
	 * 
	 * @param EMId
	 * @return
	 */
	public List<EmpathyMapNoteDB> getByEMId(int EMId) {
		String sql = "SELECT * FROM " + tableName + " WHERE idEMPostit = "
				+ EMId;
		Cursor c = db.rawQuery(sql, null);
		List<EmpathyMapNoteDB> objs = new ArrayList<EmpathyMapNoteDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				EmpathyMapNoteDB obj = new EmpathyMapNoteDB();
				obj.setId(c.getInt(0));
				obj.setIdEMPostit(c.getInt(1));
				obj.setpX(c.getFloat(2));
				obj.setpY(c.getFloat(3));
				obj.setTitle(c.getString(4));
				obj.setDescription(c.getString(5));
				obj.setColor(c.getInt(6));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}

	public List<EmpathyMapNoteDB> getAll() {
		String sql = "SELECT * FROM " + tableName;
		Cursor c = db.rawQuery(sql, null);

		List<EmpathyMapNoteDB> objs = new ArrayList<EmpathyMapNoteDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				EmpathyMapNoteDB obj = new EmpathyMapNoteDB();
				obj.setId(c.getInt(0));
				obj.setIdEMPostit(c.getInt(1));
				obj.setpX(c.getFloat(2));
				obj.setpY(c.getFloat(3));
				obj.setTitle(c.getString(4));
				obj.setDescription(c.getString(5));
				obj.setColor(c.getInt(6));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}

	public boolean removeEntry(int id) {
		String sql = "DELETE FROM " + tableName + " WHERE id = " + id;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean removeAll() {
		String sql = "DELETE FROM " + tableName;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public void createOrUpdate(EmpathyMapNoteDB emndb) {
		if (emndb.getId() == -1) {
			create(emndb);
		} else {
			update(emndb);
		}
	}

	public boolean removeAllByEMPostitId(int idEmPostit) {
		String sql = "DELETE FROM " + tableName + " WHERE idEMPostit = "
				+ idEmPostit;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
