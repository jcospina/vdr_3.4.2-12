package com.canvas.dataaccess.controller;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.canvas.dataaccess.KeyPartnerPostitDB;
import com.canvas.dataaccess.SQLiteDatabaseAdapter;
import com.smartsoft.Educanvas.R;

public class KeyPartnerPostitController {
	private SQLiteDatabase db;
	private Context context;
	static String tableName = "KeyPartnerPostit";

	public KeyPartnerPostitController(Context context) {
		this.context = context;
		SQLiteDatabaseAdapter aSQLiteDatabaseAdapter = SQLiteDatabaseAdapter
				.getInstance(this.context);
		this.db = aSQLiteDatabaseAdapter.getWritableDatabase();
	}

	public long create(KeyPartnerPostitDB obj) {
		try {
			ContentValues cv = new ContentValues();
			// cv.put("id", obj.getId());
			cv.put("pSX", obj.getpSX());
			cv.put("pSY", obj.getpSY());
			cv.put("idPostit", obj.getIdPostit());
			long rowid = db.insert(tableName,
					context.getString(R.string.db_name), cv);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public long update(KeyPartnerPostitDB obj) {

		try {
			ContentValues cv = new ContentValues();
			cv.put("id", obj.getId());
			cv.put("pSX", obj.getpSX());
			cv.put("pSY", obj.getpSY());
			cv.put("idPostit", obj.getIdPostit());
			String where = "id = " + obj.getId();
			long rowid = db.update(tableName, cv, where, null);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public boolean createAll(List<KeyPartnerPostitDB> objs) {
		boolean success = true;

		for (int i = 0; i < objs.size(); i++) {
			long result = create(objs.get(i));
			if (success && result == -1) {
				success = false;
			}
		}

		return success;
	}

	public KeyPartnerPostitDB getById(int id) {
		String sql = "SELECT * FROM " + tableName + " WHERE id = " + id;
		Cursor c = db.rawQuery(sql, null);

		KeyPartnerPostitDB obj = null;

		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			obj = new KeyPartnerPostitDB();
			obj.setId(c.getInt(0));
			obj.setpSX(c.getFloat(1));
			obj.setpSY(c.getFloat(2));
			obj.setIdPostit(c.getInt(3));
		}
		c.close();
		return obj;
	}

	/**
	 * Obtiene el postit específico a partir del postitId
	 * 
	 * @param postitId
	 * @return
	 */
	public KeyPartnerPostitDB getByPostitId(int postitId) {
		String sql = "SELECT * FROM " + tableName + " WHERE idPostit = "
				+ postitId;
		Cursor c = db.rawQuery(sql, null);

		KeyPartnerPostitDB obj = null;

		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			obj = new KeyPartnerPostitDB();
			obj.setId(c.getInt(0));
			obj.setpSX(c.getFloat(1));
			obj.setpSY(c.getFloat(2));
			obj.setIdPostit(c.getInt(3));
		}
		c.close();
		return obj;
	}

	public List<KeyPartnerPostitDB> getAll() {
		String sql = "SELECT * FROM " + tableName;
		Cursor c = db.rawQuery(sql, null);

		List<KeyPartnerPostitDB> objs = new ArrayList<KeyPartnerPostitDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				KeyPartnerPostitDB obj = new KeyPartnerPostitDB();
				obj.setId(c.getInt(0));
				obj.setpSX(c.getFloat(1));
				obj.setpSY(c.getFloat(2));
				obj.setIdPostit(c.getInt(3));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}

	public boolean removeAll() {
		String sql = "DELETE FROM " + tableName;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public void createOrUpdate(KeyPartnerPostitDB kpdb) {
		if (kpdb.getId() == -1) {
			create(kpdb);
		} else {
			update(kpdb);
		}
	}

	public boolean removeAllByPostitId(int postitId) {
		String sql = "DELETE FROM " + tableName + " WHERE idPostit = "
				+ postitId;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
