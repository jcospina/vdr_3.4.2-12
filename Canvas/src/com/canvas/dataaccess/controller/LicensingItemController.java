package com.canvas.dataaccess.controller;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.canvas.dataaccess.LicensingItemDB;
import com.canvas.dataaccess.SQLiteDatabaseAdapter;
import com.smartsoft.Educanvas.R;

public class LicensingItemController {
	private SQLiteDatabase db;
	private Context context;
	static String tableName = "LicensingItem";

	public LicensingItemController(Context context) {
		this.context = context;
		SQLiteDatabaseAdapter aSQLiteDatabaseAdapter = SQLiteDatabaseAdapter
				.getInstance(this.context);
		this.db = aSQLiteDatabaseAdapter.getWritableDatabase();
	}

	public long create(LicensingItemDB obj) {
		try {
			ContentValues cv = new ContentValues();
			//cv.put("id", obj.getId());
			cv.put("averageLicensesPerCostumer", obj.getAverageLicensesPerCostumer());
			cv.put("averageLicensePrice", obj.getAverageLicensePrice());
			cv.put("idRevenuePostit", obj.getIdRevenuePostit());
			cv.put("idRevenueItem", obj.getIdRevenueItem());
			long rowid = db.insert(tableName,
					context.getString(R.string.db_name), cv);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public long update(LicensingItemDB obj) {

		try {
			ContentValues cv = new ContentValues();
			cv.put("id", obj.getId());
			cv.put("averageLicensesPerCostumer", obj.getAverageLicensesPerCostumer());
			cv.put("averageLicensePrice", obj.getAverageLicensePrice());
			cv.put("idRevenuePostit", obj.getIdRevenuePostit());
			cv.put("idRevenueItem", obj.getIdRevenueItem());
			String where = "id = " + obj.getId();
			long rowid = db.update(tableName, cv, where, null);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public boolean createAll(List<LicensingItemDB> objs) {
		boolean success = true;

		for (int i = 0; i < objs.size(); i++) {
			long result = create(objs.get(i));
			if (success && result == -1) {
				success = false;
			}
		}

		return success;
	}

	public LicensingItemDB getById(int id) {
		String sql = "SELECT * FROM " + tableName + " WHERE id = " + id;
		Cursor c = db.rawQuery(sql, null);

		LicensingItemDB obj = null;

		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			obj = new LicensingItemDB();
			obj.setId(c.getInt(0));
			obj.setAverageLicensesPerCostumer(c.getFloat(1));
			obj.setAverageLicensePrice(c.getFloat(2));
			obj.setIdRevenuePostit(c.getInt(3));
			obj.setIdRevenueItem(c.getInt(4));
		}
		c.close();
		return obj;
	}
	
	/**
	 * Obtiene los items a partir del id del postit
	 * 
	 * @param revenuePostitId
	 * @return
	 */
	public LicensingItemDB getByRevenuePostitId(int revenuePostitId) {
		String sql = "SELECT * FROM " + tableName + " WHERE idRevenueItem = "
				+ revenuePostitId;
		Cursor c = db.rawQuery(sql, null);

		LicensingItemDB obj = null;
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			obj = new LicensingItemDB();
			obj.setId(c.getInt(0));
			obj.setAverageLicensesPerCostumer(c.getFloat(1));
			obj.setAverageLicensePrice(c.getFloat(2));
			obj.setIdRevenuePostit(c.getInt(3));
			obj.setIdRevenueItem(c.getInt(4));
		}
		c.close();
		return obj;
	}
	
	public List<LicensingItemDB> getAll() {
		String sql = "SELECT * FROM " + tableName;
		Cursor c = db.rawQuery(sql, null);

		List<LicensingItemDB> objs = new ArrayList<LicensingItemDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				LicensingItemDB obj = new LicensingItemDB();
				obj.setId(c.getInt(0));
				obj.setAverageLicensesPerCostumer(c.getFloat(1));
				obj.setAverageLicensePrice(c.getFloat(2));
				obj.setIdRevenuePostit(c.getInt(3));
				obj.setIdRevenueItem(c.getInt(4));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}
	
	public boolean removeEntry(int id) {
		String sql = "DELETE FROM " + tableName + " WHERE id = " + id;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean removeEntries(int revenuePostitId) {
		String sql = "DELETE FROM " + tableName + " WHERE idRevenuePostit = " + revenuePostitId;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean removeEntryByRevItemId(int idRevenueItem) {
		String sql = "DELETE FROM " + tableName + " WHERE idRevenueItem = "
				+ idRevenueItem;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean removeAll() {
		String sql = "DELETE FROM " + tableName;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	
}
