package com.canvas.dataaccess.controller;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.canvas.dataaccess.CanvasDB;
import com.canvas.dataaccess.PostItDB;
import com.canvas.dataaccess.SQLiteDatabaseAdapter;
import com.smartsoft.Educanvas.Block;
import com.smartsoft.Educanvas.R;

public class CanvasController {

	private SQLiteDatabase db;
	private Context context;
	static String tableName = "Canvas";

	public CanvasController(Context context) {
		this.context = context;
		SQLiteDatabaseAdapter aSQLiteDatabaseAdapter = SQLiteDatabaseAdapter
				.getInstance(this.context);
		this.db = aSQLiteDatabaseAdapter.getWritableDatabase();
	}

	public long create(CanvasDB obj) {
		try {
			ContentValues cv = new ContentValues();
			// cv.put("id", obj.getId());
			if (obj.getImage() != null)
				cv.put("image", obj.getImage());
			cv.put("name", obj.getName());
			cv.put("autor", obj.getAutor());
			long rowid = db.insert(tableName,
					context.getString(R.string.db_name), cv);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public long update(CanvasDB obj) {

		try {
			ContentValues cv = new ContentValues();
			cv.put("id", obj.getId());
			cv.put("image", obj.getImage());
			cv.put("name", obj.getName());
			cv.put("autor", obj.getAutor());
			String where = "id = " + obj.getId();
			long rowid = db.update(tableName, cv, where, null);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public boolean createAll(List<CanvasDB> objs) {
		boolean success = true;

		for (int i = 0; i < objs.size(); i++) {
			long result = create(objs.get(i));
			if (success && result == -1) {
				success = false;
			}
		}

		return success;
	}

	public CanvasDB getById(int id) {
		String sql = "SELECT * FROM " + tableName + " WHERE id = " + id;
		Cursor c = db.rawQuery(sql, null);

		CanvasDB obj = null;

		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			obj = new CanvasDB();
			obj.setId(c.getInt(0));
			obj.setImage((c.getBlob(1)));
			obj.setName(c.getString(2));
			obj.setAutor(c.getString(3));
		}
		c.close();
		return obj;
	}

	public List<CanvasDB> getAll() {
		String sql = "SELECT * FROM " + tableName;
		Cursor c = db.rawQuery(sql, null);

		List<CanvasDB> objs = new ArrayList<CanvasDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				CanvasDB obj = new CanvasDB();
				obj.setId(c.getInt(0));
				obj.setImage(c.getBlob(1));
				obj.setName(c.getString(2));
				obj.setAutor(c.getString(3));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}

	public boolean removeEntry(int id) {
		String sql = "DELETE FROM " + tableName + " WHERE id = " + id;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean removeAll() {
		String sql = "DELETE FROM " + tableName;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Permite eliminar un canvas y todos los postits relacionados
	 * 
	 * @param idCanvas
	 * @return
	 */
	public boolean removeCanvas(int idCanvas) {
		CanvasDB cDB = getById(idCanvas);
		if (cDB == null) {
			System.out.println("Canvas with id = " + idCanvas
					+ " doesn't exists in the db");
			return false;
		}

		PostitController postitController = new PostitController(context);
		List<PostItDB> postits = postitController.getByIdCanvas(idCanvas);

		for (PostItDB postItDB : postits) {
			boolean readyForPostitDel = true;
			System.out.println("Removing postit with id = " + postItDB.getId()
					+ " and in block " + postItDB.getBlock());
			switch (postItDB.getBlock()) {
			case Block.BlockNumbers.KEY_PARTNERSHIPS_BN:
				KeyPartnerPostitController keyPartnerPostitController = new KeyPartnerPostitController(
						context);
				readyForPostitDel = keyPartnerPostitController
						.removeAllByPostitId(postItDB.getId());
				break;
			case Block.BlockNumbers.KEY_ACTIVITIES_BN:
			case Block.BlockNumbers.KEY_RESOURCES_BN:
				readyForPostitDel = true;
				break;
			case Block.BlockNumbers.VALUE_PROPOSITIONS_BN:
				ValuePropositionPinController valuePropositionPinController = new ValuePropositionPinController(
						context);
				readyForPostitDel = valuePropositionPinController
						.removeAllByPostitId(postItDB.getId());
				break;
			case Block.BlockNumbers.RELATIONSHIPS_BN:
				RelationshipPostitController relationshipPostitController = new RelationshipPostitController(
						context);
				readyForPostitDel = relationshipPostitController
						.removeAllByPostitId(postItDB.getId());
				break;
			case Block.BlockNumbers.CHANNELS_BN:
				ChannelVPController channelVPController = new ChannelVPController(
						context);
				readyForPostitDel = channelVPController
						.removeAllByPostitId(postItDB.getId());
				ChannelPostitController channelPostitController = new ChannelPostitController(
						context);
				readyForPostitDel &= channelPostitController
						.removeAllByPostitId(postItDB.getId());
				break;
			case Block.BlockNumbers.CUSTOMER_SEGMENTS_BN:
				EmpathyMapPostitController empathyMapPostitController = new EmpathyMapPostitController(
						context);
				readyForPostitDel = empathyMapPostitController
						.removeAllByPostitId(postItDB.getId());
				break;
			case Block.BlockNumbers.COSTS_BN:
				CostPostitController costPostitController = new CostPostitController(
						context);
				readyForPostitDel = costPostitController
						.removeByPostitId(postItDB.getId());
				break;
			case Block.BlockNumbers.REVENUEW_BN:
				RevenuePostitController revenuePostitController = new RevenuePostitController(
						context);
				readyForPostitDel = revenuePostitController
						.removeAllByPostitId(postItDB.getId());
				break;
			default:
				System.out.println("Block number = " + postItDB.getBlock()
						+ " for postitId " + postItDB.getId()
						+ " is not a valid block");
				break;
			}

			if (readyForPostitDel) {
				postitController.removeEntry(postItDB.getId());
			}
		}
		return removeEntry(idCanvas);
	}

}
