package com.canvas.dataaccess.controller;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.canvas.dataaccess.PostItDB;
import com.canvas.dataaccess.SQLiteDatabaseAdapter;
import com.smartsoft.Educanvas.R;

public class PostitController {
	private SQLiteDatabase db;
	private Context context;
	static String tableName = "Postit";

	public PostitController(Context context) {
		this.context = context;
		SQLiteDatabaseAdapter aSQLiteDatabaseAdapter = SQLiteDatabaseAdapter
				.getInstance(this.context);
		this.db = aSQLiteDatabaseAdapter.getWritableDatabase();
	}

	public long create(PostItDB obj) {
		try {
			ContentValues cv = new ContentValues();
			// cv.put("id", obj.getId());
			cv.put("title", obj.getTitle());
			cv.put("description", obj.getDescription());
			cv.put("color", obj.getColor());
			cv.put("pX", obj.getpX());
			cv.put("pY", obj.getpY());
			cv.put("idCanvas", obj.getIdCanvas());
			cv.put("block", obj.getBlock());
			long rowid = db.insert(tableName,
					context.getString(R.string.db_name), cv);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public long update(PostItDB obj) {

		try {
			ContentValues cv = new ContentValues();
			cv.put("id", obj.getId());
			cv.put("title", obj.getTitle());
			cv.put("description", obj.getDescription());
			cv.put("color", obj.getColor());
			cv.put("pX", obj.getpX());
			cv.put("pY", obj.getpY());
			cv.put("idCanvas", obj.getIdCanvas());
			cv.put("block", obj.getBlock());
			String where = "id = " + obj.getId();
			long rowid = db.update(tableName, cv, where, null);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public boolean createAll(List<PostItDB> objs) {
		boolean success = true;

		for (int i = 0; i < objs.size(); i++) {
			long result = create(objs.get(i));
			if (success && result == -1) {
				success = false;
			}
		}

		return success;
	}

	public PostItDB getById(int id) {
		String sql = "SELECT * FROM " + tableName + " WHERE id = " + id;
		Cursor c = db.rawQuery(sql, null);

		PostItDB obj = null;

		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			obj = new PostItDB();
			obj.setId(c.getInt(0));
			obj.setTitle(c.getString(1));
			obj.setDescription(c.getString(2));
			obj.setColor(c.getInt(3));
			obj.setpX(c.getFloat(4));
			obj.setpY(c.getFloat(5));
			obj.setIdCanvas(c.getInt(6));
			obj.setBlock(c.getInt(7));
		}
		c.close();
		return obj;
	}

	/**
	 * Obtiene la lista de postit pertenecientes a un Canvas específico
	 * 
	 * @param idCanvas
	 * @return
	 */
	public List<PostItDB> getByIdCanvas(int idCanvas) {
		String sql = "SELECT * FROM " + tableName + " WHERE idCanvas = "
				+ idCanvas;
		Cursor c = db.rawQuery(sql, null);
		List<PostItDB> objs = new ArrayList<PostItDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				PostItDB obj = new PostItDB();
				obj.setId(c.getInt(0));
				obj.setTitle(c.getString(1));
				obj.setDescription(c.getString(2));
				obj.setColor(c.getInt(3));
				obj.setpX(c.getFloat(4));
				obj.setpY(c.getFloat(5));
				obj.setIdCanvas(c.getInt(6));
				obj.setBlock(c.getInt(7));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}

	/**
	 * Obtiene la lista de postit de un bloque específico para un canvas
	 * determinado
	 * 
	 * @param idCanvas
	 * @param block
	 * @return
	 */
	public List<PostItDB> getByIdCanvasAndBlock(int idCanvas, int block) {
		String sql = "SELECT * FROM " + tableName + " WHERE idCanvas = "
				+ idCanvas + " AND block = " + block;
		Cursor c = db.rawQuery(sql, null);
		List<PostItDB> objs = new ArrayList<PostItDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				PostItDB obj = new PostItDB();
				obj.setId(c.getInt(0));
				obj.setTitle(c.getString(1));
				obj.setDescription(c.getString(2));
				obj.setColor(c.getInt(3));
				obj.setpX(c.getFloat(4));
				obj.setpY(c.getFloat(5));
				obj.setIdCanvas(c.getInt(6));
				obj.setBlock(c.getInt(7));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}

	/**
	 * Filtra los postits de acuerdo al bloque en el que se encuentran
	 * 
	 * @param postits
	 * @param block
	 * @return
	 */
	public List<PostItDB> filterByBlock(List<PostItDB> postits, int block) {
		List<PostItDB> filterPostits = new ArrayList<PostItDB>();
		for (PostItDB p : postits) {
			if (p.getBlock() == block) {
				filterPostits.add(p);
			}
		}
		return filterPostits;
	}

	public List<PostItDB> getAll() {
		String sql = "SELECT * FROM " + tableName;
		Cursor c = db.rawQuery(sql, null);

		List<PostItDB> objs = new ArrayList<PostItDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				PostItDB obj = new PostItDB();
				obj.setId(c.getInt(0));
				obj.setTitle(c.getString(1));
				obj.setDescription(c.getString(2));
				obj.setColor(c.getInt(3));
				obj.setpX(c.getFloat(4));
				obj.setpY(c.getFloat(5));
				obj.setIdCanvas(c.getInt(6));
				obj.setBlock(c.getInt(7));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}

	public boolean removeEntry(int id) {
		String sql = "DELETE FROM " + tableName + " WHERE id = " + id;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean removeByCanvasId(int canvasId) {
		String sql = "DELETE FROM " + tableName + " WHERE idCanvas = " + canvasId;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean removeAll() {
		String sql = "DELETE FROM " + tableName;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
