package com.canvas.dataaccess.controller;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.canvas.dataaccess.ChannelVPDB;
import com.canvas.dataaccess.SQLiteDatabaseAdapter;
import com.smartsoft.Educanvas.R;

public class ChannelVPController {

	private SQLiteDatabase db;
	private Context context;
	static String tableName = "ChannelVP";

	public ChannelVPController(Context context) {
		this.context = context;
		SQLiteDatabaseAdapter aSQLiteDatabaseAdapter = SQLiteDatabaseAdapter
				.getInstance(this.context);
		this.db = aSQLiteDatabaseAdapter.getWritableDatabase();
	}

	public long create(ChannelVPDB obj) {
		try {
			ContentValues cv = new ContentValues();
			//cv.put("id", obj.getId());
			cv.put("pX", obj.getpX());
			cv.put("pY", obj.getpY());
			cv.put("idPostit", obj.getIdPostit());
			long rowid = db.insert(tableName,
					context.getString(R.string.db_name), cv);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public long update(ChannelVPDB obj) {

		try {
			ContentValues cv = new ContentValues();
			cv.put("id", obj.getId());
			cv.put("pX", obj.getpX());
			cv.put("pY", obj.getpY());
			cv.put("idPostit", obj.getIdPostit());
			String where = "id = " + obj.getId();
			long rowid = db.update(tableName, cv, where, null);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public boolean createAll(List<ChannelVPDB> objs) {
		boolean success = true;

		for (int i = 0; i < objs.size(); i++) {
			long result = create(objs.get(i));
			if (success && result == -1) {
				success = false;
			}
		}

		return success;
	}

	public ChannelVPDB getById(int id) {
		String sql = "SELECT * FROM " + tableName + " WHERE id = " + id;
		Cursor c = db.rawQuery(sql, null);

		ChannelVPDB obj = null;

		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			obj = new ChannelVPDB();
			obj.setId(c.getInt(0));
			obj.setpX(c.getInt(1));
			obj.setpY(c.getInt(2));
			obj.setIdPostit(c.getInt(3));
		}
		c.close();
		return obj;
	}
	
	/**
	 * Retorna las propuestas de valor para un ChannelPostit
	 * @param channelPostitId
	 * @return
	 */
	public List<ChannelVPDB> getByChannelPostitId(int channelPostitId) {
		String sql = "SELECT * FROM " + tableName + " WHERE idPostit = " + channelPostitId;
		Cursor c = db.rawQuery(sql, null);

		List<ChannelVPDB> objs = new ArrayList<ChannelVPDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				ChannelVPDB obj = new ChannelVPDB();
				obj.setId(c.getInt(0));
				obj.setpX(c.getInt(1));
				obj.setpY(c.getInt(2));
				obj.setIdPostit(c.getInt(3));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}
	
	public List<ChannelVPDB> getAll() {
		String sql = "SELECT * FROM " + tableName;
		Cursor c = db.rawQuery(sql, null);

		List<ChannelVPDB> objs = new ArrayList<ChannelVPDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				ChannelVPDB obj = new ChannelVPDB();
				obj.setId(c.getInt(0));
				obj.setpX(c.getInt(1));
				obj.setpY(c.getInt(2));
				obj.setIdPostit(c.getInt(3));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}
	
	public boolean removeEntry(int id) {
		String sql = "DELETE FROM " + tableName + " WHERE id = " + id;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean removeAll() {
		String sql = "DELETE FROM " + tableName;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean removeAllByPostitId(int postitId) {
		String sql = "DELETE FROM " + tableName + " WHERE idPostit = "
				+ postitId;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
