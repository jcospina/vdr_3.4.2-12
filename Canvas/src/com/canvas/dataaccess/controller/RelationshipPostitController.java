package com.canvas.dataaccess.controller;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.canvas.dataaccess.RelationShipPostitDB;
import com.canvas.dataaccess.SQLiteDatabaseAdapter;
import com.smartsoft.Educanvas.R;

public class RelationshipPostitController {
	private SQLiteDatabase db;
	private Context context;
	static String tableName = "RelationShipPostit";

	public RelationshipPostitController(Context context) {
		this.context = context;
		SQLiteDatabaseAdapter aSQLiteDatabaseAdapter = SQLiteDatabaseAdapter
				.getInstance(this.context);
		this.db = aSQLiteDatabaseAdapter.getWritableDatabase();
	}

	public long create(RelationShipPostitDB obj) {
		try {
			ContentValues cv = new ContentValues();
			cv.put("idPostit", obj.getIdPostit());
			cv.put("idValueProposition", obj.getIdValueProposition());
			cv.put("idCostumer", obj.getIdCostumer());
			long rowid = db.insert(tableName,
					context.getString(R.string.db_name), cv);
			return rowid;
		} catch (Exception ex) {
			return -1;
		}
	}

	public boolean createAll(List<RelationShipPostitDB> objs) {
		boolean success = true;

		for (int i = 0; i < objs.size(); i++) {
			long result = create(objs.get(i));
			if (success && result == -1) {
				success = false;
			}
		}

		return success;
	}

	public RelationShipPostitDB getById(int id) {
		String sql = "SELECT * FROM " + tableName + " WHERE id = " + id;
		Cursor c = db.rawQuery(sql, null);

		RelationShipPostitDB obj = null;

		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			obj = new RelationShipPostitDB();
			obj.setIdPostit(c.getInt(0));
			obj.setIdValueProposition(c.getInt(1));
			obj.setIdCostumer(c.getInt(2));
		}
		c.close();
		return obj;
	}

	public List<RelationShipPostitDB> getByPostitId(int postitId) {
		String sql = "SELECT * FROM " + tableName + " WHERE idPostit = "
				+ postitId;
		Cursor c = db.rawQuery(sql, null);

		List<RelationShipPostitDB> objs = new ArrayList<RelationShipPostitDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				RelationShipPostitDB obj = new RelationShipPostitDB();
				obj.setIdPostit(c.getInt(0));
				obj.setIdValueProposition(c.getInt(1));
				obj.setIdCostumer(c.getInt(2));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}

	public List<RelationShipPostitDB> getByPostitIdAndValueId(int postitId,
			int valuePId) {
		String sql = "SELECT * FROM " + tableName + " WHERE idPostit = "
				+ postitId + " AND idValueProposition = " + valuePId;
		Cursor c = db.rawQuery(sql, null);

		List<RelationShipPostitDB> objs = new ArrayList<RelationShipPostitDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				RelationShipPostitDB obj = new RelationShipPostitDB();
				obj.setIdPostit(c.getInt(0));
				obj.setIdValueProposition(c.getInt(1));
				obj.setIdCostumer(c.getInt(2));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}

	public List<Integer> getDistinctValuPropositionsIdByPostitId(int postitId) {
		String sql = "SELECT DISTINCT idValueProposition FROM " + tableName
				+ " WHERE idPostit = " + postitId;
		Cursor c = db.rawQuery(sql, null);

		List<Integer> ids = new ArrayList<Integer>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				ids.add(c.getInt(0));
				c.moveToNext();
			}
		}
		c.close();
		return ids;
	}

	public List<RelationShipPostitDB> getAll() {
		String sql = "SELECT * FROM " + tableName;
		Cursor c = db.rawQuery(sql, null);

		List<RelationShipPostitDB> objs = new ArrayList<RelationShipPostitDB>();
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				RelationShipPostitDB obj = new RelationShipPostitDB();
				obj.setIdPostit(c.getInt(0));
				obj.setIdValueProposition(c.getInt(1));
				obj.setIdCostumer(c.getInt(2));
				objs.add(obj);
				c.moveToNext();
			}
		}
		c.close();
		return objs;
	}

	public boolean removeEntry(int id) {
		String sql = "DELETE FROM " + tableName + " WHERE id = " + id;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean removeAll() {
		String sql = "DELETE FROM " + tableName;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean removeAllByPostitId(int postitId) {
		String sql = "DELETE FROM " + tableName + " WHERE idPostit = "
				+ postitId;
		try {
			db.execSQL(sql);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
