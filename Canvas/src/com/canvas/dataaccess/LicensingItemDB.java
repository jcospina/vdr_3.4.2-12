package com.canvas.dataaccess;

public class LicensingItemDB {
	
	private int id;
	private float averageLicensesPerCostumer;
	private float averageLicensePrice;
	private int idRevenuePostit;
	private int idRevenueItem;

	public LicensingItemDB() {
		super();
	}

	public LicensingItemDB(int id, float averageLicensesPerCostumer,
			float averageLicensePrice, int idRevenuePostit, int idRevenueItem) {
		super();
		this.id = id;
		this.averageLicensesPerCostumer = averageLicensesPerCostumer;
		this.averageLicensePrice = averageLicensePrice;
		this.idRevenuePostit = idRevenuePostit;
		this.idRevenueItem = idRevenueItem;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public float getAverageLicensesPerCostumer() {
		return averageLicensesPerCostumer;
	}

	public void setAverageLicensesPerCostumer(float averageLicensesPerCostumer) {
		this.averageLicensesPerCostumer = averageLicensesPerCostumer;
	}

	public float getAverageLicensePrice() {
		return averageLicensePrice;
	}

	public void setAverageLicensePrice(float averageLicensePrice) {
		this.averageLicensePrice = averageLicensePrice;
	}

	public int getIdRevenuePostit() {
		return idRevenuePostit;
	}

	public void setIdRevenuePostit(int idRevenuePostit) {
		this.idRevenuePostit = idRevenuePostit;
	}
	
	public int getIdRevenueItem() {
		return idRevenueItem;
	}
	
	public void setIdRevenueItem(int idRevenueItem) {
		this.idRevenueItem = idRevenueItem;
	}

}
