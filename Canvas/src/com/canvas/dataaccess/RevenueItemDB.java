package com.canvas.dataaccess;

public class RevenueItemDB {
	private int id;
	private String name;
	private double marketShare;
	private int purchasePeriod;
	private int idRevenuePostit;
	private int revenueType;
	private String valueProposition;
	private int relatedItemId;

	public RevenueItemDB() {
		super();
	}

	public RevenueItemDB(int id, String name, double marketShare,
			int purchasePeriod, int idRevenuePostit, int revenueType, String valueProposition, int relatedItemId) {
		super();
		this.id = id;
		this.name = name;
		this.marketShare = marketShare;
		this.purchasePeriod = purchasePeriod;
		this.idRevenuePostit = idRevenuePostit;
		this.revenueType = revenueType;
		this.valueProposition = valueProposition;
		this.relatedItemId = relatedItemId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getMarketShare() {
		return marketShare;
	}

	public void setMarketShare(double marketShare) {
		this.marketShare = marketShare;
	}

	public int getPurchasePeriod() {
		return purchasePeriod;
	}

	public void setPurchasePeriod(int purchasePeriod) {
		this.purchasePeriod = purchasePeriod;
	}

	public int getIdRevenuePostit() {
		return idRevenuePostit;
	}

	public void setIdRevenuePostit(int idRevenuePostit) {
		this.idRevenuePostit = idRevenuePostit;
	}

	public int getRevenueType() {
		return revenueType;
	}

	public void setRevenueType(int revenueType) {
		this.revenueType = revenueType;
	}
	
	public String getValueProposition() {
		return valueProposition;
	}
	
	public void setValueProposition(String valueProposition) {
		this.valueProposition = valueProposition;
	}
	
	public int getRelatedItemId() {
		return relatedItemId;
	}
	
	public void setRelatedItemId(int relatedItemId) {
		this.relatedItemId = relatedItemId;
	}

}
