package com.canvas.dataaccess;

public class RentalItemDB {
	
	private int id;
	private double averageUnitsRented;
	private double rentalFeePerUnit;
	private int idRevenuePostit;
	private int idRevenueItem;

	public RentalItemDB() {
		super();
	}

	public RentalItemDB(int id, double averageUnitsRented, double rentalFeePerUnit,
			int idRevenuePostit, int idRevenueItem) {
		super();
		this.id = id;
		this.averageUnitsRented = averageUnitsRented;
		this.rentalFeePerUnit = rentalFeePerUnit;
		this.idRevenuePostit = idRevenuePostit;
		this.idRevenueItem = idRevenueItem;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public double getAverageUnitsRented() {
		return averageUnitsRented;
	}

	public void setAverageUnitsRented(double averageUnitsRented) {
		this.averageUnitsRented = averageUnitsRented;
	}

	public double getRentalFeePerUnit() {
		return rentalFeePerUnit;
	}

	public void setRentalFeePerUnit(double rentalFeePerUnit) {
		this.rentalFeePerUnit = rentalFeePerUnit;
	}

	public int getIdRevenuePostit() {
		return idRevenuePostit;
	}

	public void setIdRevenuePostit(int idRevenuePostit) {
		this.idRevenuePostit = idRevenuePostit;
	}
	
	public int getIdRevenueItem() {
		return idRevenueItem;
	}
	
	public void setIdRevenueItem(int idRevenueItem) {
		this.idRevenueItem = idRevenueItem;
	}

}
