package com.canvas.valuepropositions;

import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.canvas.dataaccess.ValuePropositionPinDB;

public class Pin extends ButtonSprite {
	
	private ValuePropositionPinDB pinDB;
	
	public Pin(float pX, float pY, ITiledTextureRegion pTiledTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager, ValuePropositionPinDB pinDB) {
		super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
		this.pinDB = pinDB;
	}

	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
			float pTouchAreaLocalX, float pTouchAreaLocalY) {

		if (pSceneTouchEvent.isActionMove()) {
			float posX = pSceneTouchEvent.getX() - this.getWidth() / 2;
			float posY = pSceneTouchEvent.getY() - this.getHeight() / 2;
			this.setPosition(posX, posY);
		}
		return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX,
				pTouchAreaLocalY);
	}

	public ValuePropositionPinDB getPinDB() {
		return pinDB;
	}

	public void setPinDB(ValuePropositionPinDB pinDB) {
		this.pinDB = pinDB;
	}
	
	

}

