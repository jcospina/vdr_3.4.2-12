package com.canvas.valuepropositions;

import java.util.ArrayList;
import java.util.List;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.AutoWrap;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.util.HorizontalAlign;
import org.andengine.util.color.Color;

import android.graphics.Typeface;

import com.canvas.dataaccess.PostItDB;
import com.canvas.dataaccess.ValuePropositionPinDB;
import com.canvas.dataaccess.controller.PostitController;
import com.canvas.dataaccess.controller.ValuePropositionPinController;
import com.canvas.vo.ValuePropositionPostitVO;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.CanvasManager;
import com.smartsoft.Educanvas.ManagedScene;

public class ValuePropositionsScene extends ManagedScene {

	public HUD gameHud = new HUD();
	private static final int PIN_COUNT = 10;
	private Sprite backgroundCircle;
	private CanvasActivity canvas;

	private ValuePropositionPostitVO valueProposition;
	private List<BitmapTextureAtlas> textures;
	private Font pFont;

	public ValuePropositionsScene(ValuePropositionPostitVO valueProposition,
			CanvasActivity canvas) {
		this.setOnSceneTouchListenerBindingOnActionDownEnabled(true);
		this.setTouchAreaBindingOnActionDownEnabled(true);
		this.setTouchAreaBindingOnActionMoveEnabled(true);
		this.valueProposition = valueProposition;
		this.canvas = canvas;
		gameHud.setScaleCenter(0f, 0f);
		gameHud.setScale(1);
	}

	@Override
	public Scene onLoadingScreenLoadAndShown() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onLoadingScreenUnloadAndHidden() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLoadScene() {

		pFont = FontFactory.create(
				CanvasManager.getInstance().engine.getFontManager(),
				CanvasManager.getInstance().engine.getTextureManager(), 256,
				256, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 24,
				Color.WHITE.getARGBPackedInt());
		pFont.load();

		textures = CanvasManager.getInstance().loadValuePropositionsResources();

		setBackground(new Background(Color.WHITE));

		backgroundCircle = new Sprite(
				(CanvasActivity.CAMERA_WIDTH - CanvasManager.vPropositionsCircleRegion
						.getWidth()) / 2,
				(CanvasActivity.CAMERA_HEIGHT - CanvasManager.vPropositionsCircleRegion
						.getHeight()) / 2,
				CanvasManager.vPropositionsCircleRegion, CanvasManager
						.getInstance().engine.getVertexBufferObjectManager());
		backgroundCircle.setCullingEnabled(true);
		backgroundCircle.setIgnoreUpdate(true);
		gameHud.attachChild(backgroundCircle);

		TextOptions tOptions = new TextOptions(AutoWrap.WORDS, 200,
				HorizontalAlign.CENTER, Text.LEADING_DEFAULT);
		Text textTitle = new Text(0, 0, pFont,
				(valueProposition.getTitle() != null && !valueProposition
						.getTitle().equals("")) ? valueProposition.getTitle()
						: "Sin titulo", tOptions,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());
		textTitle.setPosition(
				(backgroundCircle.getWidth() - textTitle.getWidth()) / 2,
				(backgroundCircle.getHeight() - textTitle.getHeight()) / 2);
		backgroundCircle.attachChild(textTitle);

		// Loading pines
		populatePinList();

		for (Pin pin : valueProposition.getPines()) {
			pin.setCullingEnabled(true);
			gameHud.attachChild(pin);
			registerTouchArea(pin);
		}

	}

	public void populatePinList() {
		ValuePropositionPinController pinController = new ValuePropositionPinController(
				CanvasManager.getInstance().context);

		if (valueProposition.getRelatedDatabaseObject() == null) {
			PostItDB postitDB = PostItDB.fromPostit(valueProposition, -1,
					CanvasActivity.canvasId);
			PostitController pc = new PostitController(
					CanvasManager.getInstance().context);
			postitDB.setId((int) pc.create(postitDB));
			valueProposition.setRelatedDatabaseObject(postitDB);
		}

		List<ValuePropositionPinDB> pinList = pinController
				.getByIdPostit(valueProposition.getRelatedDatabaseObject()
						.getId());

		// Si el listado de pines obtenido de la base de datos no se encuentra
		// vac�o
		// generamos un objeto tipo Pin por cada uno que se encuentre en la bd
		if (pinList != null && !pinList.isEmpty()) {
			List<Pin> pines = new ArrayList<Pin>();
			for (ValuePropositionPinDB pDB : pinList) {
				pines.add(new Pin(pDB.getpX(), pDB.getpY(),
						CanvasManager.pinRegion,
						CanvasManager.getInstance().engine
								.getVertexBufferObjectManager(), pDB));
			}
			valueProposition.setPines(pines);
		}
		// Generamos nuevos objetos tipo Pin con valores por defecto y les
		// asociamos un objeto
		// pin de la base de datos con id = -1 para indicarle al controlador que
		// debe tratarlo
		// como un objeto nuevo.
		else {
			List<Pin> pines = new ArrayList<Pin>();
			valueProposition.setPines(pines);
			for (int i = 0; i < PIN_COUNT; i++) {
				ValuePropositionPinDB pDB = new ValuePropositionPinDB();
				pDB.setId(-1);
				pDB.setIdPostit(valueProposition.getRelatedDatabaseObject()
						.getId());
				Pin pin = new Pin(
						(CanvasActivity.CAMERA_WIDTH - CanvasManager.pinRegion
								.getWidth() / 2) / 2,
						(CanvasActivity.CAMERA_HEIGHT - CanvasManager.pinRegion
								.getHeight()) / 2, CanvasManager.pinRegion,
						CanvasManager.getInstance().engine
								.getVertexBufferObjectManager(), pDB);

				valueProposition.getPines().add(pin);
			}
		}

	}

	@Override
	public void onShowScene() {
		CanvasManager.getInstance().engine.getCamera().setHUD(gameHud);
	}

	@Override
	public void onHideScene() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUnloadScene() {
		persistValueProposition();
		canvas.runOnUpdateThread(new Runnable() {

			@Override
			public void run() {
				gameHud.detachChildren();
				gameHud.dispose();
				ValuePropositionsScene.this.detachChildren();
				ValuePropositionsScene.this.clearEntityModifiers();
				ValuePropositionsScene.this.clearTouchAreas();
				ValuePropositionsScene.this.clearUpdateHandlers();
				
				if(pFont != null){
					pFont.unload();
				}
				
				CanvasManager.unloadTextures(textures);
			}
		});
	}

	private void persistValueProposition() {
		ValuePropositionPinController pc = new ValuePropositionPinController(
				CanvasManager.getInstance().context);
		for (Pin p : valueProposition.getPines()) {
			p.getPinDB().setpX(p.getX());
			p.getPinDB().setpY(p.getY());
			pc.saveOrUpdate(p.getPinDB());
		}

	}

}
