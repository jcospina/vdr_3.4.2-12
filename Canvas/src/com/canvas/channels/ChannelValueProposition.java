package com.canvas.channels;

import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.AutoWrap;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.HorizontalAlign;

import android.graphics.Typeface;

import com.smartsoft.Educanvas.CanvasManager;

public class ChannelValueProposition extends Sprite {

	private float pX;
	private float pY;
	Font pFont;
	private String title;
	protected boolean isMoving;

	public ChannelValueProposition(float pX, float pY,
			ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager, String title) {
		super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
		this.pX = pX;
		this.pY = pY;
		this.title = title != null ? title : "";
		init();
	}

	/**
	 * Inicializa los elementos utilizados en la creaci�n del sprite
	 */
	private void init() {
		loadPFont();
		drawTitle();
	}

	/**
	 * Carga la fuente que ser� utilizada para dibujar el t�tulo del postit
	 * cuando se encuentra minimizado
	 */
	private void loadPFont() {
		pFont = FontFactory.create(
				CanvasManager.getInstance().engine.getFontManager(),
				CanvasManager.getInstance().engine.getTextureManager(), 256,
				256, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 16);
		pFont.load();

	}

	private void drawTitle() {
		TextOptions tOptions = new TextOptions(AutoWrap.WORDS, 90,
				HorizontalAlign.CENTER, Text.LEADING_DEFAULT);
		Text text = new Text(5, 0, pFont, (this.title != null) ? this.title
				: "", tOptions,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());

		text.setText((this.title != null) ? this.title : "");
		text.setPosition((getWidth() - text.getWidth()) / 2, getHeight());
		this.attachChild(text);
	}

	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
			float pTouchAreaLocalX, float pTouchAreaLocalY) {
		switch (pSceneTouchEvent.getAction()) {
		case TouchEvent.ACTION_DOWN:
			break;

		case TouchEvent.ACTION_MOVE:

			float dX = this.getX() + (this.getWidth() / 2)
					- pSceneTouchEvent.getX();
			float dY = this.getY() + (this.getHeight() / 2)
					- pSceneTouchEvent.getY();

			if (!(dX < getWidth() / 5 && dX > -getWidth() / 5)
					|| !(dY < getHeight() / 5 && dY > -getHeight() / 5)) {
				isMoving = true;
				pX = pSceneTouchEvent.getX() - this.getWidth() / 2;
				pY = pSceneTouchEvent.getY() - this.getHeight() / 2;
				this.setPosition(pX, pY);
			}
			break;

		case TouchEvent.ACTION_UP:

			break;

		default:
			break;
		}

		return true;
	}

	public float getpX() {
		return pX;
	}

	public void setpX(float pX) {
		this.pX = pX;
	}

	public float getpY() {
		return pY;
	}

	public void setpY(float pY) {
		this.pY = pY;
	}

	public Font getpFont() {
		return pFont;
	}

	public void setpFont(Font pFont) {
		this.pFont = pFont;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
