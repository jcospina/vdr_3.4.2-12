package com.canvas.channels;

import java.util.List;
import java.util.Random;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.region.ITextureRegion;

import com.canvas.dataaccess.ChannelPostitDB;
import com.canvas.dataaccess.PostItDB;
import com.canvas.dataaccess.controller.ChannelPostitController;
import com.canvas.dataaccess.controller.PostitController;
import com.canvas.vo.ChannelPostitVO;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.CanvasManager;
import com.smartsoft.Educanvas.MainScene;
import com.smartsoft.Educanvas.ManagedScene;

public class ChannelsScene extends ManagedScene {

	public HUD gameHud = new HUD();

	private CanvasActivity canvas;

	private Channel truck;
	private Channel plane;
	private Channel ship;
	private Channel mail;
	private Channel internet;
	private Channel others;

	private ChannelPostitVO channel;

	private Channel current;

	private List<BitmapTextureAtlas> textures;

	public ChannelsScene(CanvasActivity canvas, ChannelPostitVO channel) {
		this.setOnSceneTouchListenerBindingOnActionDownEnabled(true);
		this.setTouchAreaBindingOnActionDownEnabled(true);
		this.setTouchAreaBindingOnActionMoveEnabled(true);
		this.setBackground(new Background(1, 1, 1));
		this.channel = channel;
		this.canvas = canvas;
		gameHud.setScaleCenter(0, 0);
		gameHud.setScale(1);
	}

	@Override
	public void onLoadScene() {
		textures = CanvasManager.getInstance().loadChannelsResources();
		Sprite bg = new Sprite(0, 0, CanvasManager.channelBackground,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());
		bg.setIgnoreUpdate(true);
		bg.setCullingEnabled(true);
		gameHud.attachChild(bg);
		ChannelPostitDB cpdb = null;
		int index = -1;
		ChannelPostitController cpc = new ChannelPostitController(
				CanvasManager.getInstance().context);
		if (channel.getRelatedDatabaseObject() != null) {
			index = channel.getRelatedDatabaseObject().getId();
			cpdb = cpc.getByPostitId(index);
		}
		if (cpdb == null) {
			cpdb = new ChannelPostitDB(-1, -1, index);
			loadTruck(false);
			loadPlane(false);
			loadShip(false);
			loadMail(false);
			loadInternet(false);
			loadOthers(false);
		} else {
			ITextureRegion current = null;
			switch (cpdb.getChannel()) {
			case 0:
				loadPlane(false);
				loadShip(false);
				loadMail(false);
				loadInternet(false);
				loadOthers(false);
				current = CanvasManager.truck;
				truck = loadCurrent(cpdb, current);
				break;
			case 1:
				loadTruck(false);
				loadShip(false);
				loadMail(false);
				loadInternet(false);
				loadOthers(false);
				current = CanvasManager.airplane;
				plane = loadCurrent(cpdb, current);
				break;
			case 2:
				loadTruck(false);
				loadPlane(false);
				loadMail(false);
				loadInternet(false);
				loadOthers(false);
				current = CanvasManager.ship;
				ship = loadCurrent(cpdb, current);
				break;
			case 3:
				loadTruck(false);
				loadPlane(false);
				loadShip(false);
				loadInternet(false);
				loadOthers(false);
				current = CanvasManager.mail;
				mail = loadCurrent(cpdb, current);
				break;
			case 4:
				loadTruck(false);
				loadPlane(false);
				loadShip(false);
				loadMail(false);
				loadInternet(false);
				current = CanvasManager.other;
				others = loadCurrent(cpdb, current);
				break;
			case 5:
				loadTruck(false);
				loadPlane(false);
				loadShip(false);
				loadMail(false);
				loadOthers(false);
				current = CanvasManager.internet;
				internet = loadCurrent(cpdb, current);
				break;
			default:
				loadTruck(false);
				loadPlane(false);
				loadShip(false);
				loadMail(false);
				loadInternet(false);
				loadOthers(false);
				break;
			}
					
		}
		channel.setPostitRelated(cpdb);
		loadValuePropositions();
	}
	
	private Channel loadCurrent(ChannelPostitDB cpdb, ITextureRegion current){
		Channel currentChannel = new Channel(
				CanvasActivity.CAMERA_WIDTH / 2 - 400.0f,
				CanvasActivity.CAMERA_HEIGHT / 2 - 250.0f, current,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager(), canvas,
				ChannelsScene.this, cpdb.getChannel());
		currentChannel.setCullingEnabled(true);
		gameHud.attachChild(currentChannel);
		this.setCurrent(currentChannel);	
		return currentChannel;
	}

	private void loadValuePropositions() {
		float posX = CanvasActivity.CANVAS_WIDTH * 0.93f
				- CanvasManager.valueproposition[0].getWidth() / 2;
		float posY = CanvasActivity.CANVAS_WIDTH * 0.07f
				- CanvasManager.valueproposition[0].getHeight() / 2;
		float separation = CanvasManager.valueproposition[0].getHeight()
				+ CanvasActivity.CAMERA_HEIGHT * 0.005f;
		Random r = new Random();
		for (int i = 0; i < MainScene.getInstance().valuePropositions.postits
				.size(); i++) {
			String title = MainScene.getInstance().valuePropositions.postits
					.get(i).getTitle();
			ChannelValueProposition cvp = new ChannelValueProposition(posX,
					posY + separation * i,
					CanvasManager.valueproposition[r.nextInt(2)],
					CanvasManager.getInstance().engine
							.getVertexBufferObjectManager(), title);
			if (channel.getValuePropositions().isEmpty()) {
				channel.getValuePropositions().add(i, cvp);
			} else {
				try {
					channel.getValuePropositions().get(i);
				} catch (Exception e) {
					channel.getValuePropositions().add(i, cvp);
				}
			}

		}
		for (ChannelValueProposition vp : channel.getValuePropositions()) {
			vp.setScale(0.8f);
			vp.setCullingEnabled(true);
			gameHud.attachChild(vp);
			registerTouchArea(vp);
		}
	}

	private void loadTruck(boolean restore) {
		if (!restore) {
			truck = new Channel(-240, -110, CanvasManager.truck,
					CanvasManager.getInstance().engine
							.getVertexBufferObjectManager(), canvas,
					ChannelsScene.this, 0);
			truck.setCullingEnabled(true);
			gameHud.attachChild(truck);
		} else {
			truck.setPosition(-240, -110);
		}
		truck.setScale(0.15f);
		registerTouchArea(truck);
	}

	private void loadPlane(boolean restore) {
		if (!restore) {
			plane = new Channel(-220, 0, CanvasManager.airplane,
					CanvasManager.getInstance().engine
							.getVertexBufferObjectManager(), canvas,
					ChannelsScene.this, 1);
			plane.setCullingEnabled(true);
			gameHud.attachChild(plane);
		} else {
			plane.setPosition(-220, 0);
		}
		plane.setScale(0.15f);
		registerTouchArea(plane);
	}

	private void loadShip(boolean restore) {
		if (!restore) {
			ship = new Channel(-190, 110, CanvasManager.ship,
					CanvasManager.getInstance().engine
							.getVertexBufferObjectManager(), canvas,
					ChannelsScene.this, 2);
			ship.setCullingEnabled(true);
			gameHud.attachChild(ship);
		} else {
			ship.setPosition(-190, 110);
		}
		ship.setScale(0.2f);
		registerTouchArea(ship);
	}

	private void loadMail(boolean restore) {
		if (!restore) {
			mail = new Channel(-180, 220, CanvasManager.mail,
					CanvasManager.getInstance().engine
							.getVertexBufferObjectManager(), canvas,
					ChannelsScene.this, 3);
			mail.setCullingEnabled(true);
			gameHud.attachChild(mail);
		} else {
			mail.setPosition(-180, 220);
		}
		mail.setScale(0.15f);
		registerTouchArea(mail);
	}

	private void loadInternet(boolean restore) {
		if (!restore) {
			internet = new Channel(-160, 330, CanvasManager.internet,
					CanvasManager.getInstance().engine
							.getVertexBufferObjectManager(), canvas,
					ChannelsScene.this, 5);
			internet.setCullingEnabled(true);
			gameHud.attachChild(internet);
		} else {
			internet.setPosition(-160, 330);
		}
		internet.setScale(0.15f);
		registerTouchArea(internet);
	}

	private void loadOthers(boolean restore) {
		if (!restore) {
			others = new Channel(-140, 440, CanvasManager.other,
					CanvasManager.getInstance().engine
							.getVertexBufferObjectManager(), canvas,
					ChannelsScene.this, 4);
			others.setCullingEnabled(true);
			gameHud.attachChild(others);
		} else{
			others.setPosition(-140, 440);
		}
		others.setScale(0.15f);		
		registerTouchArea(others);
	}

	@Override
	public void onShowScene() {
		CanvasManager.getInstance().engine.getCamera().setHUD(gameHud);
	}

	@Override
	public void onUnloadScene() {
		saveData();
		canvas.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				gameHud.detachChildren();
				gameHud.dispose();
				ChannelsScene.this.detachChildren();
				ChannelsScene.this.clearEntityModifiers();
				ChannelsScene.this.clearTouchAreas();
				ChannelsScene.this.clearUpdateHandlers();
				
				CanvasManager.unloadTextures(textures);
			}
		});

	}

	public void unRegister(Channel touchArea) {
		unregisterTouchArea(touchArea);
	}

	public void setCurrent(Channel current) {
		if (this.current == null) {
			this.current = current;
		} else {
			// gameHud.detachChild(this.current);
			switch (this.current.id) {
			case 0:
				loadTruck(true);
				break;
			case 1:
				loadPlane(true);
				break;
			case 2:
				loadShip(true);
				break;
			case 3:
				loadMail(true);
				break;
			case 4:
				loadOthers(true);
				break;
			case 5:
				loadInternet(true);
				break;
			default:
				break;
			}
			this.current = current;
		}
		channel.setChannel(current);
	}

	private void saveData() {
		ChannelPostitController cpController = new ChannelPostitController(
				CanvasManager.getInstance().context);
		PostitController pController = new PostitController(
				CanvasManager.getInstance().context);
		if (channel.getRelatedDatabaseObject() == null) {
			PostItDB pdb = PostItDB.fromPostit(channel, -1,
					CanvasActivity.canvasId);
			pdb.setId((int) pController.create(pdb));
			int indexInCollection = MainScene.getInstance().channels.postits
					.indexOf(channel);
			MainScene.getInstance().channels.postits.get(indexInCollection)
					.setRelatedDatabaseObject(pdb);
			channel.setRelatedDatabaseObject(pdb);
		}
		if (channel.getChannel() != null) {
			long index = channel.getPostitRelated().getId();
			channel.getPostitRelated().setIdPostit(
					channel.getRelatedDatabaseObject().getId());
			if (index == -1) {
				index = cpController.create(channel.getPostitRelated());
			} else {
				cpController.update(channel.getPostitRelated());
			}
		}
	}

	@Override
	public void onHideScene() {

	}

	@Override
	public Scene onLoadingScreenLoadAndShown() {
		return null;
	}

	@Override
	public void onLoadingScreenUnloadAndHidden() {

	}

}
