package com.canvas.channels;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.LoopEntityModifier.ILoopEntityModifierListener;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.modifier.IModifier;
import org.andengine.util.modifier.LoopModifier;
import org.andengine.util.modifier.ease.EaseLinear;

import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.PostIt;

public class Channel extends Sprite {

	private float pX;
	private float pY;
	private CanvasActivity canvas;
	protected boolean isMoving;
	private ChannelsScene scene;
	public int id = 0;
	
	private float initialX;
	private float initialY;

	public Channel(float pX, float pY, ITextureRegion bgTexture,
			VertexBufferObjectManager pVertexBufferObjectManager,
			CanvasActivity canvas, ChannelsScene scene, int id) {
		super(pX, pY, bgTexture, pVertexBufferObjectManager);
		this.pX = pX;
		this.pY = pY;
		this.initialX = pX;
		this.initialY = pY;
		this.canvas = canvas;
		this.scene = scene;
		this.id = id;
	}

	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
			float pTouchAreaLocalX, float pTouchAreaLocalY) {
		float startX = 0;		
		switch (pSceneTouchEvent.getAction()) {
		case TouchEvent.ACTION_DOWN:
			startX = pSceneTouchEvent.getX();			
			break;

		case TouchEvent.ACTION_MOVE:

			float dX = this.getX() + (this.getWidth() / 2)
					- pSceneTouchEvent.getX();
			float dY = this.getY() + (this.getHeight() / 2)
					- pSceneTouchEvent.getY();

			if (!(dX < getWidthScaled()/ 5 && dX > -getWidthScaled() / 5)
					|| !(dY < getHeightScaled() / 5 && dY > -getHeightScaled() / 5)) {
				isMoving = true;
				pX = pSceneTouchEvent.getX() - this.getWidth() / 2;
				pY = pSceneTouchEvent.getY() - this.getHeight() / 2;
				this.setPosition(pX, pY);
			}
			break;

		case TouchEvent.ACTION_UP:
			if (isMoving) {
				float deltaX = pX - startX;
				if (deltaX > CanvasActivity.CAMERA_WIDTH * 0.1f) {
					animatePostit(
							0.5f,
							0.1f,
							1,
							this.getpX(),
							CanvasActivity.CAMERA_WIDTH / 2
									- this.getWidth()/ 2,
							this.getpY(),
							CanvasActivity.CAMERA_HEIGHT / 2
									- this.getHeight() / 2,
							PostIt.SHOW_TYPE_ANIMATION);
					isMoving = false;
					deltaX = 0;
					scene.unRegister(this);
					scene.setCurrent(this);
				} else{
					this.setPosition(initialX, initialY);
				}
			} else {

			}
			break;

		default:
			break;
		}

		return true;
	}

	/**
	 * Este m�todo representa la animaci�n del postit ya sea para maximizarlo o
	 * minimizarlo. Es una combinaci�n de una animaci�n de escala y una
	 * animaci�n de desplazamiento.
	 * 
	 * @param duration
	 *            : Duraci�n en segundos de la animaci�n
	 * @param fromScale
	 *            : Escala a partir de la cual inicia la animaci�n. (tama�o del
	 *            postit al iniciar la animaci�n)
	 * @param toScale
	 *            : Escala final (tama�o del postit al terminar la animaci�n) de
	 *            la animaci�n
	 * @param fromX
	 *            : Posici�n X de inicio.
	 * @param toX
	 *            : Posici�n X de finalizaci�n.
	 * @param fromY
	 *            : Posici�n Y de inicio.
	 * @param toY
	 *            : Posici�n Y de finalizaci�n.
	 * @param animationType
	 *            : Indica expl�citamente si es una animaci�n de maximizaci�n o
	 *            de minimizaci�n
	 * 
	 *            Cuando es una animaci�n de maximizaci�n, el m�todo
	 *            showExtraInformation es ejecutado al finalizar la animaci�n.
	 *            Cuando es una minimizaci�n, es posible que haya sido
	 *            actualizada la informaci�n del postit y por tanto se ejecuta
	 *            el m�todo drawTitle para actualizar el t�tulo que ser�
	 *            desplegado y ademas se ejecuta el m�todo onMinimize de la
	 *            interfaz onPostitChangeListener.
	 */
	public void animatePostit(float duration, float fromScale, float toScale,
			float fromX, float toX, float fromY, float toY,
			final int animationType) {
		final LoopEntityModifier entityModifier = new LoopEntityModifier(
				new IEntityModifierListener() {
					@Override
					public void onModifierStarted(
							final IModifier<IEntity> pModifier,
							final IEntity pItem) {
						canvas.runOnUiThread(new Runnable() {
							@Override
							public void run() {
							}
						});
					}

					@Override
					public void onModifierFinished(
							final IModifier<IEntity> pEntityModifier,
							final IEntity pEntity) {
						canvas.runOnUiThread(new Runnable() {
							@Override
							public void run() {
							}
						});
					}
				}, 1, new ILoopEntityModifierListener() {

					@Override
					public void onLoopFinished(
							final LoopModifier<IEntity> pLoopModifier,
							final int pLoop, final int pLoopCount) {
						canvas.runOnUiThread(new Runnable() {
							@Override
							public void run() {
							}
						});
					}

					@Override
					public void onLoopStarted(
							LoopModifier<IEntity> pLoopModifier, int pLoop,
							int pLoopCount) {
						// TODO Auto-generated method stub

					}
				}, new SequenceEntityModifier(new ParallelEntityModifier(
						new ScaleModifier(duration, fromScale, toScale),
						new MoveModifier(duration, fromX, toX, fromY, toY,
								EaseLinear.getInstance()))));

		this.registerEntityModifier(entityModifier);
	}

	public float getpX() {
		return pX;
	}

	public void setpX(float pX) {
		this.pX = pX;
	}

	public float getpY() {
		return pY;
	}

	public void setpY(float pY) {
		this.pY = pY;
	}

}
