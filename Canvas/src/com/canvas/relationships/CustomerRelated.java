package com.canvas.relationships;

import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.canvas.vo.CustomerPostitVO;

public class CustomerRelated extends Sprite {

	private boolean isMoving;
	private IEntityModifierListener modifierListener;
	private CustomerPostitVO customer;
	private Font font;

	public CustomerRelated(float pX, float pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			CustomerPostitVO customer, Font font) {
		super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
		this.customer = customer;
		this.font = font;
		drawTitle();
	}

	private void drawTitle() {
		Text titleText = new Text(0, 0, font, customer.getTitle(),
				getVertexBufferObjectManager());
		titleText.setPosition(
				(CustomersBar.USER_ICON_SIZE - titleText.getWidth()) / 2,
				CustomersBar.USER_ICON_SIZE);
		attachChild(titleText);
	}

	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
			float pTouchAreaLocalX, float pTouchAreaLocalY) {

		switch (pSceneTouchEvent.getAction()) {
		case TouchEvent.ACTION_DOWN:
			isMoving = true;
			break;
		case TouchEvent.ACTION_MOVE:
			if (isMoving) {
				this.setPosition(pSceneTouchEvent.getX() - getWidth() / 2,
						pSceneTouchEvent.getY() - getHeight() / 2);
			}
			break;

		case TouchEvent.ACTION_UP:
			if (isMoving) {
				isMoving = false;
				ParallelEntityModifier pem = new ParallelEntityModifier(
						new AlphaModifier(0.1f, 1, 0), new MoveModifier(0.1f,
								getX(), getX(), getY(), getY() - 20));
				registerEntityModifier(pem);
				pem.addModifierListener(modifierListener);

			}
			break;

		default:
			break;
		}

		return true;
	}

	public IEntityModifierListener getModifierListener() {
		return modifierListener;
	}

	public void setModifierListener(IEntityModifierListener modifierListener) {
		this.modifierListener = modifierListener;
	}

	public CustomerPostitVO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerPostitVO customer) {
		this.customer = customer;
	}

	public boolean isMoving() {
		return isMoving;
	}

	public void setMoving(boolean isMoving) {
		this.isMoving = isMoving;
	}

}
