package com.canvas.relationships;

import java.util.ArrayList;
import java.util.List;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.Entity;
import org.andengine.entity.modifier.MoveXModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.canvas.dataaccess.controller.EmpathyMapPostitController;
import com.canvas.vo.CustomerPostitVO;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.CanvasManager;

public class CustomersBar extends Entity {

	protected final int w = 200;
	public static final int USER_ICON_SIZE = 90;
	private List<CustomerPostitVO> customers;
	private List<Sprite> customersSpriteList;
	private VertexBufferObjectManager vbom;
	private OnCustomerTouchedListener onCustomerTouchedListener;
	private HUD hud;
	private Font f;

	public CustomersBar(List<CustomerPostitVO> customers,
			VertexBufferObjectManager vbom, HUD hud, Font f) {
		super();
		this.customers = customers;
		this.vbom = vbom;
		this.hud = hud;
		customersSpriteList = new ArrayList<Sprite>();
		this.f = f;
		drawBar();
	}

	private void drawBar() {
		setPosition(CanvasActivity.CAMERA_WIDTH - w, 0);
		int separation = 50;
		int col = 1;
		int index = 0;
		EmpathyMapPostitController empathyMapPostitController = new EmpathyMapPostitController(
				CanvasManager.getInstance().context);
		for (int i = 0; i < customers.size(); i++) {

			final CustomerPostitVO c = customers.get(i);
			if (c.getPostitRelated() == null) {
				c.setPostitRelated(empathyMapPostitController.getByPostitId(c
						.getRelatedDatabaseObject().getId()));
			}

			if (c.getPostitRelated() != null
					&& c.getPostitRelated().getBitmap() != null
					&& c.getPhoto() == null) {
				Bitmap bmp = BitmapFactory.decodeByteArray(c.getPostitRelated()
						.getBitmap(), 0,
						c.getPostitRelated().getBitmap().length);
				c.setPhoto(bmp);
			}

			Sprite cr = new Sprite(0, 0 + index * separation,
					c.getTexturePhoto() != null ? c.getTexturePhoto()
							: CanvasManager.userIconRegion, vbom) {
				@Override
				public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
						float pTouchAreaLocalX, float pTouchAreaLocalY) {

					if (pSceneTouchEvent.isActionDown()
							&& onCustomerTouchedListener != null) {
						onCustomerTouchedListener.onCustomerTouched(c,
								CustomersBar.this.getX() + getX(), getY());
					}

					return true;
				}
			};
			if (c.getPhoto() == null) {
				cr.setColor(c.getColor());
			}
			cr.setSize(CustomersBar.USER_ICON_SIZE, CustomersBar.USER_ICON_SIZE);
			float posX = (col * w / 2) + ((w / 2) - cr.getWidth()) / 2;
			cr.setPosition(posX,
					(index + 1) * separation + (index * cr.getHeight()));

			index++;
			if (index > 4) {
				index = 0;
				col--;
			}
			Text titleText = new Text(0, 0, f, c.getTitle(), vbom);
			titleText.setPosition(cr.getX() + titleText.getWidth() / 2,
					cr.getY() + cr.getHeight());
			attachChild(titleText);
			customersSpriteList.add(cr);
			attachChild(cr);
			hud.registerTouchArea(cr);

		}
	}

	public void show() {
		MoveXModifier mov = new MoveXModifier(0.2f, getX(), getX() - w);
		registerEntityModifier(mov);
	}

	public void hide() {
		MoveXModifier mov = new MoveXModifier(0.2f, getX(), getX() + w);
		registerEntityModifier(mov);
	}

	public List<CustomerPostitVO> getCustomers() {
		return customers;
	}

	public void setCustomers(List<CustomerPostitVO> customers) {
		this.customers = customers;
	}

	public OnCustomerTouchedListener getOnCustomerTouchedListener() {
		return onCustomerTouchedListener;
	}

	public void setOnCustomerTouchedListener(
			OnCustomerTouchedListener onCustomerTouchedListener) {
		this.onCustomerTouchedListener = onCustomerTouchedListener;
	}

	public interface OnCustomerTouchedListener {
		public void onCustomerTouched(CustomerPostitVO cutomer, float x, float y);
	}

}
