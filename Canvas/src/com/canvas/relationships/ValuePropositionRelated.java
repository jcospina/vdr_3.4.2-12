package com.canvas.relationships;

import java.util.List;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.modifier.IModifier;

import com.canvas.vo.CustomerPostitVO;
import com.canvas.vo.ValuePropositionPostitVO;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.CanvasManager;

public class ValuePropositionRelated extends Sprite {
	private ValuePropositionPostitVO valueProposition;
	Font font;
	Font countFont;
	Text countText;
	ITextureRegion pTextureRegion;

	private List<CustomerPostitVO> customers;
	private ValuePropositionRelated.OnActionListener onActionListener;
	private HUD hud;

	public ValuePropositionRelated(float pX, float pY,
			ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			ValuePropositionPostitVO valueProposition, Font font,
			Font countFont, List<CustomerPostitVO> customers, HUD scene) {
		super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
		this.valueProposition = valueProposition;
		this.font = font;
		this.countFont = countFont;
		this.customers = customers;
		this.hud = scene;
		this.pTextureRegion = pTextureRegion;
		drawTitle();
		updateCountText();
	}

	private void drawTitle() {
		Text titleText = new Text(0, 0, font, valueProposition.getTitle(),
				getVertexBufferObjectManager());
		titleText.setPosition((getWidth() - titleText.getWidth()) / 2,
				getHeight()/2 + 25);
		attachChild(titleText);
	}

	public boolean addCustomer(CustomerPostitVO c) {
		if (customers.contains(c)) {
			return false;
		}
		customers.add(c);
		updateCountText();
		return true;
	}

	public boolean removeCustomer(CustomerPostitVO c) {
		if (!customers.contains(c)) {
			return false;
		}
		customers.remove(c);
		updateCountText();
		return true;
	}

	public void updateCountText() {
		if (countText != null) {
			detachChild(countText);
		}
		countText = new Text(0, 0, countFont,
				customers.size() > 0 ? String.valueOf(customers.size()) : "",
				getVertexBufferObjectManager());
		countText.setPosition(getWidth()/2, getHeight()/2);
		attachChild(countText);
	}

	public List<CustomerPostitVO> getCustomers() {
		return customers;
	}

	public void setCustomers(List<CustomerPostitVO> customers) {
		this.customers = customers;
	}

	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
			float pTouchAreaLocalX, float pTouchAreaLocalY) {
		if (pSceneTouchEvent.isActionUp()) {
			maximize();
		}
		return true;
	};

	private void maximize() {
		if (onActionListener != null) {
			onActionListener.onMaximize();
		}
		Sprite heart = createMaximizedHeart();
		// heart.setScale(0.2f);
		// Entity parent = (Entity) getParent();
		heart.setScaleCenter(0, 0);
		heart.setZIndex(10);
		hud.attachChild(heart);
		hud.sortChildren();

		float duration = 0.3f;
		float c[] = getSceneCenterCoordinates();
		heart.setPosition(c[0], c[1]);
		ParallelEntityModifier pem = new ParallelEntityModifier(
				new AlphaModifier(duration, 0, 1), new ScaleModifier(duration,
						0, 1), new MoveModifier(duration, c[0],
						(CanvasActivity.CAMERA_WIDTH - heart.getWidth()) / 2,
						c[1],
						(CanvasActivity.CAMERA_HEIGHT - heart.getHeight()) / 2));
		heart.registerEntityModifier(pem);

	}

	public void minimize(final Sprite heart) {
		float[] coordinates = getSceneCenterCoordinates();
		ParallelEntityModifier pem = new ParallelEntityModifier(
				new AlphaModifier(0.2f, 1, 0), new ScaleModifier(0.2f, 1, 0),
				new MoveModifier(0.2f, heart.getX(), coordinates[0],
						heart.getY(), coordinates[1]));
		pem.addModifierListener(new IEntityModifier.IEntityModifierListener() {

			@Override
			public void onModifierStarted(IModifier<IEntity> pModifier,
					IEntity pItem) {

			}

			@Override
			public void onModifierFinished(IModifier<IEntity> pModifier,
					IEntity pItem) {
				if(!heart.isDisposed()){
					heart.dispose();
				}
				if (onActionListener != null) {
					onActionListener.onMinimize();
				}
			}
		});
		heart.registerEntityModifier(pem);
	}

	private Sprite createMaximizedHeart() {
		final Sprite heart = new Sprite(0, 0,
				CanvasManager.relationshipsAppMax,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());

		Sprite deleteBtn = new Sprite(-305, -72,
				CanvasManager.recicleBinRegion,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());
		heart.attachChild(deleteBtn);

		createCustomerGrid(heart, deleteBtn);

		Sprite close = new Sprite(heart.getWidth() + 220, -72,
				CanvasManager.closeIconRegion,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.isActionDown()) {
					minimize(heart);
				}
				return true;
			}
		};

		heart.attachChild(close);
		hud.registerTouchArea(close);

		return heart;
	}

	private void createCustomerGrid(final Sprite heart, final Sprite delete) {
		int separation = 115;

		int maxColums = 5;

		int posXinit = 25;
		int posYinit = 100;
		int posY = posYinit;
		int index = -1;
		for (int i = 0; i < this.customers.size(); i++) {
			// final int position = i;
			final CustomerPostitVO customerPostit = customers.get(i);
			index++;
			if (index >= maxColums) {
				index = 0;
				posY += separation;
			}

			// Calcula donde debe ubicarse el primer elemento
			if (index == 0 && i + maxColums > this.customers.size()) {
				posXinit = 600 - ((this.customers.size() - i) * separation);
				posXinit = posXinit / 2;
			}

			int posX = posXinit + index * separation;

			Sprite customer = new Sprite(posX, posY,
					customerPostit.getTexturePhoto() != null ? customerPostit
							.getTexturePhoto() : CanvasManager.userIconRegion,
					CanvasManager.getInstance().engine
							.getVertexBufferObjectManager()) {
				@Override
				public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
						float pTouchAreaLocalX, float pTouchAreaLocalY) {

					if (pSceneTouchEvent.isActionMove()) {

						// float coord[] = convertSceneToLocalCoordinates(
						// pSceneTouchEvent.getX(),
						// pSceneTouchEvent.getY());
						setPosition(pSceneTouchEvent.getX() - heart.getX()
								- getWidth() / 2, pSceneTouchEvent.getY()
								- heart.getY() - getHeight() / 2);
					}
					if (pSceneTouchEvent.isActionUp()) {
						if (collidesWith(delete)) {							
							hud.unregisterTouchArea(this);
							this.detachSelf();
							if(!this.isDisposed()){
								this.dispose();
							}
							customers.remove(customerPostit);
							updateCountText();
						}
					}

					return true;
				}
			};
			if(customerPostit.getTexturePhoto() == null){
				customer.setColor(customerPostit.getColor());
			} 
			customer.setSize(CustomersBar.USER_ICON_SIZE,
					CustomersBar.USER_ICON_SIZE);
			hud.registerTouchArea(customer);
			heart.attachChild(customer);
		}
	}

	public ValuePropositionRelated.OnActionListener getOnActionListener() {
		return onActionListener;
	}

	public void setOnActionListener(
			ValuePropositionRelated.OnActionListener onActionListener) {
		this.onActionListener = onActionListener;
	}

	public ValuePropositionPostitVO getValueProposition() {
		return valueProposition;
	}

	public void setValueProposition(ValuePropositionPostitVO valueProposition) {
		this.valueProposition = valueProposition;
	}

	public interface OnActionListener {
		public void onMaximize();

		public void onMinimize();
	}
}
