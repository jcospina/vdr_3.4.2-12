package com.canvas.relationships;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.scene.background.SpriteBackground;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.util.color.Color;
import org.andengine.util.modifier.IModifier;

import android.graphics.Typeface;

import com.canvas.dataaccess.PostItDB;
import com.canvas.dataaccess.RelationShipPostitDB;
import com.canvas.dataaccess.controller.PostitController;
import com.canvas.dataaccess.controller.RelationshipPostitController;
import com.canvas.vo.CustomerPostitVO;
import com.canvas.vo.RelationshipPostitVO;
import com.canvas.vo.ValuePropositionPostitVO;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.CanvasManager;
import com.smartsoft.Educanvas.MainScene;
import com.smartsoft.Educanvas.ManagedScene;

public class RelationshipsScene extends ManagedScene {

	public HUD gameHud = new HUD();

	private List<ValuePropositionRelated> vPropositionsRelated;
	private RelationshipPostitVO relationship;
	private Font titleFont;
	private Font itemsFont;
	private Font countFont;
	private List<BitmapTextureAtlas> textures;
	CustomersBar bar;
	private CanvasActivity canvas;

	public RelationshipsScene(RelationshipPostitVO relationship,
			CanvasActivity canvas) {
//		this.setOnSceneTouchListenerBindingOnActionDownEnabled(true);
//		this.setTouchAreaBindingOnActionDownEnabled(true);
//		this.setTouchAreaBindingOnActionMoveEnabled(true);
		this.relationship = relationship;
		this.canvas = canvas;
		gameHud.setScaleCenter(0f, 0f);
		gameHud.setScale(1);

	}

	@Override
	public Scene onLoadingScreenLoadAndShown() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onLoadingScreenUnloadAndHidden() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLoadScene() {

		setBackground(new Background(Color.WHITE));

		textures = CanvasManager.getInstance().loadRelationshipsResources();

		titleFont = FontFactory.create(
				CanvasManager.getInstance().engine.getFontManager(),
				CanvasManager.getInstance().engine.getTextureManager(), 256,
				256, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 30,
				Color.BLACK.getARGBPackedInt());
		titleFont.load();

		itemsFont = FontFactory.create(
				CanvasManager.getInstance().engine.getFontManager(),
				CanvasManager.getInstance().engine.getTextureManager(), 256,
				256, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 18,
				Color.BLACK.getARGBPackedInt());
		itemsFont.load();

		countFont = FontFactory.create(
				CanvasManager.getInstance().engine.getFontManager(),
				CanvasManager.getInstance().engine.getTextureManager(), 256,
				256, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 18,
				Color.BLACK.getARGBPackedInt());
		countFont.load();

		getRelationshipsMapFromDB();
		System.out.println("Scene loaded");
	}

	private void addCustomerToScene(float x, float y, CustomerPostitVO customer) {
		final CustomerRelated cr = new CustomerRelated(x, y,
				customer.getTexturePhoto() != null ? customer.getTexturePhoto()
						: CanvasManager.userIconRegion,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager(), customer, itemsFont);
		if (customer.getTexturePhoto() == null) {
			cr.setColor(customer.getColor());
		}
		cr.setPosition(x, y);
		cr.setSize(CustomersBar.USER_ICON_SIZE, CustomersBar.USER_ICON_SIZE);
		// unregisterTouchValuePropositions();
		cr.setMoving(true);
		this.attachChild(cr);
		this.registerTouchArea(cr);
		this.setTouchAreaBindingOnActionDownEnabled(true);

		cr.setModifierListener(new IEntityModifier.IEntityModifierListener() {

			@Override
			public void onModifierStarted(IModifier<IEntity> pModifier,
					IEntity pItem) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onModifierFinished(IModifier<IEntity> pModifier,
					IEntity pItem) {
				for (ValuePropositionRelated vp : vPropositionsRelated) {
					if (cr.collidesWith(vp)) {
						vp.addCustomer(cr.getCustomer());
					}
				}
				unregisterTouchArea(cr);
				detachChild(cr);
				registerTouchValuePropositions();
				bar.show();
			}
		});
	}

	private void drawValuePropositions() {
		vPropositionsRelated = new ArrayList<ValuePropositionRelated>();
		Entity back = new Entity();

		int separation = 150;
		int maxRows = 3; // N�mero m�ximo de filas que se dibujaran en la grilla

		float temp = (float) MainScene.getInstance().valuePropositions.postits
				.size() / (float) maxRows;

		int columns = (int) Math.floor(temp); // N�mero de columnas que tendr�
												// la grilla
		if (columns < temp) {
			columns++;
		}

		temp = (float) MainScene.getInstance().valuePropositions.postits.size()
				/ (float) columns;
		// temp = 1;
		int rows = (int) Math.floor(temp); // N�mero de filas que tendra la
											// grilla
		if (rows < temp) {
			rows++;
		}

		System.out.println("Max columns = " + temp);
		int xposition = 0;
		int yposition = 0;

		int w = columns * separation; // Ancho de la grilla
		int h = rows * separation; // alto total de la grilla

		back.setPosition((CanvasActivity.CAMERA_WIDTH - w) / 2,
				75 + (CanvasActivity.CAMERA_HEIGHT - h) / 2);

		for (int i = 0; i < MainScene.getInstance().valuePropositions.postits
				.size(); i++) {
			ValuePropositionPostitVO vp = MainScene.getInstance().valuePropositions.postits
					.get(i);
			List<CustomerPostitVO> customerList = (relationship
					.getValuePropositionsMap() != null) ? relationship
					.getValuePropositionsMap().get(vp)
					: new ArrayList<CustomerPostitVO>();

			if (customerList == null) {
				customerList = new ArrayList<CustomerPostitVO>();
			}

			ValuePropositionRelated vpSprite = new ValuePropositionRelated(
					(float) (xposition * separation),
					(float) (yposition * separation),
					CanvasManager.relationshipsAppMin,
					CanvasManager.getInstance().engine
							.getVertexBufferObjectManager(), vp, itemsFont,
					countFont, customerList, gameHud);
			// vpSprite.setColor(vp.getColor());
			vPropositionsRelated.add(vpSprite);
			vpSprite.setCullingEnabled(true);
			gameHud.registerTouchArea(vpSprite);
			back.attachChild(vpSprite);
			xposition++;

			vpSprite.setOnActionListener(new ValuePropositionRelated.OnActionListener() {

				@Override
				public void onMinimize() {
					registerTouchValuePropositions();
					bar.show();
				}

				@Override
				public void onMaximize() {
					unregisterTouchValuePropositions();
					bar.hide();
				}
			});

			if (xposition >= columns) {
				yposition++;
				xposition = 0;
			}
		}
		back.setCullingEnabled(true);
		attachChild(back);
	}

	@Override
	public void onShowScene() {
		setBackground(new SpriteBackground(new Sprite(0, 0,
				CanvasManager.relationshipsBackground,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager())));
		Text title = new Text(0, 0, titleFont, relationship.getTitle(),
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());

		title.setPosition((CanvasActivity.CAMERA_WIDTH - title.getWidth()) / 2,
				100);

		gameHud.attachChild(title);

		drawValuePropositions();
		bar = new CustomersBar(
				MainScene.getInstance().customerSegments.postits,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager(), gameHud, itemsFont);

		bar.setOnCustomerTouchedListener(new CustomersBar.OnCustomerTouchedListener() {

			@Override
			public void onCustomerTouched(CustomerPostitVO customer, float x,
					float y) {
				addCustomerToScene(x, y, customer);
				bar.hide();
				unregisterTouchValuePropositions();
			}
		});
		bar.setCullingEnabled(true);
		gameHud.attachChild(bar);
		CanvasManager.getInstance().engine.getCamera().setHUD(gameHud);
	}

	@Override
	public void onHideScene() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onUnloadScene() {

		if (relationship.getValuePropositionsMap() == null) {
			relationship
					.setValuePropositionsMap(new HashMap<ValuePropositionPostitVO, List<CustomerPostitVO>>());
		}
		for (ValuePropositionRelated vp : vPropositionsRelated) {
			relationship.getValuePropositionsMap().put(
					vp.getValueProposition(), vp.getCustomers());
		}

		persistRelationship();
		canvas.runOnUpdateThread(new Runnable() {

			@Override
			public void run() {
				// Unregister all touch areas
				gameHud.unregisterTouchAreas(new ITouchArea.ITouchAreaMatcher() {

					@Override
					public boolean matches(ITouchArea pObject) {
						// TODO Auto-generated method stub
						return true;
					}
				});

				for (ValuePropositionRelated vp : vPropositionsRelated) {
					if (!vp.isDisposed()) {
						vp.dispose();
					}
				}

				gameHud.detachChildren();
				gameHud.dispose();
				RelationshipsScene.this.detachChildren();
				RelationshipsScene.this.clearEntityModifiers();
				RelationshipsScene.this.clearTouchAreas();
				RelationshipsScene.this.clearUpdateHandlers();

				if (titleFont != null) {
					titleFont.unload();
				}

				if (itemsFont != null) {
					itemsFont.unload();
				}

				if (countFont != null) {
					countFont.unload();
				}

				for (CustomerPostitVO c : MainScene.getInstance().customerSegments.postits) {
					if (c.getTexturePhoto() != null) {
						c.getTexturePhoto().getTexture().unload();
					}
				}

				CanvasManager.unloadTextures(textures);
			}
		});

	}

	private void persistRelationship() {
		PostitController pc = new PostitController(
				CanvasManager.getInstance().context);
		if (relationship.getRelatedDatabaseObject() == null) {
			PostItDB pdb = PostItDB.fromPostit(relationship, -1,
					CanvasActivity.canvasId);
			pdb.setId((int) pc.create(pdb));
			relationship.setRelatedDatabaseObject(pdb);
		}

		RelationshipPostitController rpc = new RelationshipPostitController(
				CanvasManager.getInstance().context);

		// Eliminamos todas las relaciones existentes para evitar guardar
		// registros repetidos
		rpc.removeAllByPostitId(relationship.getRelatedDatabaseObject().getId());

		// Creamos cada una de las relaciones
		for (ValuePropositionPostitVO pvo : MainScene.getInstance().valuePropositions.postits) {
			if (relationship.getValuePropositionsMap().containsKey(pvo)) {
				if (pvo.getRelatedDatabaseObject() == null) {
					PostItDB pdb = PostItDB.fromPostit(pvo, -1,
							CanvasActivity.canvasId);
					pdb.setId((int) pc.create(pdb));
					pvo.setRelatedDatabaseObject(pdb);
				}

				for (CustomerPostitVO cpvo : relationship
						.getValuePropositionsMap().get(pvo)) {
					if (cpvo.getRelatedDatabaseObject() == null) {
						PostItDB pdb = PostItDB.fromPostit(cpvo, -1,
								CanvasActivity.canvasId);
						pdb.setId((int) pc.create(pdb));
						cpvo.setRelatedDatabaseObject(pdb);
					}
					RelationShipPostitDB rpdb = new RelationShipPostitDB(
							relationship.getRelatedDatabaseObject().getId(),
							pvo.getRelatedDatabaseObject().getId(), cpvo
									.getRelatedDatabaseObject().getId());
					rpc.create(rpdb);
				}

			}
		}

	}

	private void getRelationshipsMapFromDB() {
		// Si no existe la relaci�n en la base de datos, no hay forma de cargar
		// su informaci�n
		if (relationship.getRelatedDatabaseObject() == null) {
			return;
		}

		RelationshipPostitController rc = new RelationshipPostitController(
				CanvasManager.getInstance().context);
		List<Integer> valuePropositionIds = rc
				.getDistinctValuPropositionsIdByPostitId(relationship
						.getRelatedDatabaseObject().getId());

//		PostitController pc = new PostitController(
//				CanvasManager.getInstance().context);

		// List<HashMap<ValuePropositionPostitVO, List<CustomerPostitVO>>>
		// mapList = new
		// ArrayList<HashMap<ValuePropositionPostitVO,List<CustomerPostitVO>>>();
		HashMap<ValuePropositionPostitVO, List<CustomerPostitVO>> map = new HashMap<ValuePropositionPostitVO, List<CustomerPostitVO>>();
		// Para cada una de las propuestas de valor encontradas dentro de la
		// tabla de relaciones que coinciden con el id de la relaci�n.
		// buscamos el listado de clientes que se encuetran asociados
		for (Integer vid : valuePropositionIds) {
			ValuePropositionPostitVO postitVO = null;

			for (ValuePropositionPostitVO pvo : MainScene.getInstance().valuePropositions.postits) {
				if (pvo.getRelatedDatabaseObject().getId() == vid) {
					postitVO = pvo;
					break;
				}
			}
			if (postitVO != null) {
				// Obtenemos el listado de relaciones a partir del id de la
				// relaci�n y el id de la propuesta de valor, con el objetivo de
				// encontrar el listado de clientes que se encuentran asociados
				// a la relaci�n y a la propuesta de valor.
				List<RelationShipPostitDB> relations = rc
						.getByPostitIdAndValueId(relationship
								.getRelatedDatabaseObject().getId(), vid);
				List<CustomerPostitVO> customers = new ArrayList<CustomerPostitVO>();
				for (RelationShipPostitDB r : relations) {
					// Es necesario por cada uno de los clientes encontrados
					// generar un objeto de tipo CustomerPostitVO que luego
					// pueda
					// ser convertido a un sprite y dibujado en la escena.
					for (CustomerPostitVO cvo : MainScene.getInstance().customerSegments.postits) {
						if (cvo.getRelatedDatabaseObject().getId() == r
								.getIdCostumer()) {
							customers.add(cvo);
							break;
						}
					}

				}

				map.put(postitVO, customers);
			}
		}

		relationship.setValuePropositionsMap(map);
	}

	public void unregisterTouchValuePropositions() {
		for (ValuePropositionRelated vp : vPropositionsRelated) {
			gameHud.unregisterTouchArea(vp);
		}
	}

	public void registerTouchValuePropositions() {
		for (ValuePropositionRelated vp : vPropositionsRelated) {
			gameHud.registerTouchArea(vp);
		}

	}

}
