package com.canvas.util.colorpicker;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.smartsoft.Educanvas.R;

public class DiscreteColorPickerDialog extends Dialog {
	
	private int selectedColor;
	
	public DiscreteColorPickerDialog(Context context) {
		super(context);
		this.setTitle("ColorPickerDialog");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.discrete_color_picker);
		
		GridView gridViewColors = (GridView) findViewById(R.id.gridViewColors);
		final DiscreteColorPickerAdapter adapter = new DiscreteColorPickerAdapter(getContext()); 
		gridViewColors.setAdapter(adapter);
		
		// close the dialog on item click
		gridViewColors.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				selectedColor = adapter.getColorList().get(position);
				DiscreteColorPickerDialog.this.dismiss();
			}
		});
	}

	public int getSelectedColor() {
		return selectedColor;
	}

	public void setSelectedColor(int selectedColor) {
		this.selectedColor = selectedColor;
	}
	
	
}
