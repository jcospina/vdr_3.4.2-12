package com.canvas.util;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Vector;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.Typeface;

import com.canvas.dataaccess.CanvasDB;
import com.canvas.dataaccess.CostPostitDB;
import com.canvas.dataaccess.PostItDB;
import com.canvas.dataaccess.RevenuePostitDB;
import com.canvas.dataaccess.controller.CanvasController;
import com.canvas.dataaccess.controller.CostPostitController;
import com.canvas.dataaccess.controller.RevenuePostitController;
import com.canvas.vo.ChannelPostitVO;
import com.canvas.vo.CostPostitVO;
import com.canvas.vo.CustomerPostitVO;
import com.canvas.vo.KeyActivityPostitVO;
import com.canvas.vo.KeyPartnershipPostitVO;
import com.canvas.vo.KeyResourcePostitVO;
import com.canvas.vo.RelationshipPostitVO;
import com.canvas.vo.RevenuePostitVO;
import com.canvas.vo.ValuePropositionPostitVO;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.MainScene;
import com.smartsoft.Educanvas.R;

public class ExportImage {

	private Context context;
	private String canvasName = "";
	private String autor = "";
	private Canvas canvas;
	private Paint paint;

	public ExportImage(Context context) {
		this.context = context;
	}

	public File generateReport() {
		// Se obtiene el nombre del Canvas desde la base de datos
		CanvasController cc = new CanvasController(context);
		CanvasDB cbd = cc.getById(CanvasActivity.canvasId);
		canvasName = cbd.getName();
		autor = cbd.getAutor();
		// Create bitmap
		Resources res = context.getResources();
		Bitmap background = BitmapFactory
				.decodeResource(res, R.drawable.lienzo).copy(
						Bitmap.Config.ARGB_8888, true);
		// Create Canvas
		canvas = new Canvas(background);

		paint = new Paint();
		paint.setTypeface(Typeface.DEFAULT);
		paint.setColor(Color.BLACK);
		paint.setStyle(Style.FILL);
		paint.setTextAlign(Align.LEFT);
		paint.setTextSize(30);

		// Autor
		canvas.drawText("Por: " + autor, 50, 60, paint);

		// Aliados clave
		writeKeyParterships();
		// Actividades clave
		writeKeyActivities();
		// Recursos clave
		writeKeyResources();
		// Propuestas de valor
		writeValuePropositions();
		// Relaciones
		writeRelationships();
		// Canales
		writeChannels();
		// Clientes
		writeCustomers();
		// Costos
		writeCosts();
		//Ingresos
		writeRevenue();
		
		
		//Ganancias
		paint.setTextSize(30);
		double earnings = totalRevenue - totalCost;
		canvas.drawText("Ganancias: " + earnings, 1100, 60, paint);		

		try {
			File dir = new File(
					android.os.Environment.getExternalStorageDirectory()
							+ java.io.File.separator + "Canvas"
							+ java.io.File.separator);
			if (!dir.exists()) {
				dir.mkdir();
			}
			File file = new File(dir, canvasName + ".png");
			FileOutputStream out = new FileOutputStream(file, false);
			background.compress(Bitmap.CompressFormat.PNG, 90, out);
			out.close();
			return file;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private void writeKeyParterships() {
		paint.setTextSize(20);
		ArrayList<KeyPartnershipPostitVO> partners = (ArrayList<KeyPartnershipPostitVO>) MainScene
				.getInstance().keyPartnerships.postits;
		Rect bounds = new Rect();
		int i = 0;
		int[] positions = { 200, 300, 400 };
		int j = 0;
		for (KeyPartnershipPostitVO postit : partners) {
			String text = postit.getTitle() + ": " + postit.getDescription();
			paint.getTextBounds(text, 0, text.length(), bounds);
			Vector<String> lines = wrapWord(text, 30);
			for (String line : lines) {
				canvas.drawText(line, 25, positions[j] + bounds.height() * i,
						paint);
				i++;
			}
			j++;
		}
	}

	private void writeKeyActivities() {

		ArrayList<KeyActivityPostitVO> partners = (ArrayList<KeyActivityPostitVO>) MainScene
				.getInstance().keyActivities.postits;
		paint.setTextSize(15);
		Rect bounds = new Rect();
		int i = 0;
		int[] positions = { 150, 210, 270 };
		int j = 0;
		for (KeyActivityPostitVO postit : partners) {
			String text = postit.getTitle() + ": " + postit.getDescription();
			paint.getTextBounds(text, 0, text.length(), bounds);
			Vector<String> lines = wrapWord(text, 40);
			for (String line : lines) {
				canvas.drawText(line, 400, positions[j] + bounds.height() * i,
						paint);
				i++;
			}
			j++;
		}
	}

	private void writeKeyResources() {

		ArrayList<KeyResourcePostitVO> partners = (ArrayList<KeyResourcePostitVO>) MainScene
				.getInstance().keyResources.postits;
		paint.setTextSize(15);
		Rect bounds = new Rect();
		int i = 0;
		int[] positions = { 490, 550, 610 };
		int j = 0;
		for (KeyResourcePostitVO postit : partners) {
			String text = postit.getTitle() + ": " + postit.getDescription();
			paint.getTextBounds(text, 0, text.length(), bounds);
			Vector<String> lines = wrapWord(text, 40);
			for (String line : lines) {
				canvas.drawText(line, 400, positions[j] + bounds.height() * i,
						paint);
				i++;
			}
			j++;
		}
	}

	private void writeValuePropositions() {
		paint.setTextSize(20);
		ArrayList<ValuePropositionPostitVO> partners = (ArrayList<ValuePropositionPostitVO>) MainScene
				.getInstance().valuePropositions.postits;
		Rect bounds = new Rect();
		int i = 0;
		int[] positions = { 200, 300, 400 };
		int j = 0;
		for (ValuePropositionPostitVO postit : partners) {
			String text = postit.getTitle() + ": " + postit.getDescription();
			paint.getTextBounds(text, 0, text.length(), bounds);
			Vector<String> lines = wrapWord(text, 30);
			for (String line : lines) {
				canvas.drawText(line, 725, positions[j] + bounds.height() * i,
						paint);
				i++;
			}
			j++;
		}
	}

	private void writeRelationships() {
		ArrayList<RelationshipPostitVO> partners = (ArrayList<RelationshipPostitVO>) MainScene
				.getInstance().relationships.postits;
		paint.setTextSize(15);
		Rect bounds = new Rect();
		int i = 0;
		int[] positions = { 150, 210, 270 };
		int j = 0;
		for (RelationshipPostitVO postit : partners) {
			String text = postit.getTitle() + ": " + postit.getDescription();
			paint.getTextBounds(text, 0, text.length(), bounds);
			Vector<String> lines = wrapWord(text, 40);
			for (String line : lines) {
				canvas.drawText(line, 1075, positions[j] + bounds.height() * i,
						paint);
				i++;
			}
			j++;
		}
	}

	private void writeChannels() {
		ArrayList<ChannelPostitVO> partners = (ArrayList<ChannelPostitVO>) MainScene
				.getInstance().channels.postits;
		paint.setTextSize(15);
		Rect bounds = new Rect();
		int i = 0;
		int[] positions = { 490, 550, 610 };
		int j = 0;
		for (ChannelPostitVO postit : partners) {
			String text = postit.getTitle() + ": " + postit.getDescription();
			paint.getTextBounds(text, 0, text.length(), bounds);
			Vector<String> lines = wrapWord(text, 40);
			for (String line : lines) {
				canvas.drawText(line, 1075, positions[j] + bounds.height() * i,
						paint);
				i++;
			}
			j++;
		}
	}

	private void writeCustomers() {
		ArrayList<CustomerPostitVO> partners = (ArrayList<CustomerPostitVO>) MainScene
				.getInstance().customerSegments.postits;
		paint.setTextSize(20);
		Rect bounds = new Rect();
		int i = 0;
		int[] positions = { 200, 300, 400 };
		int j = 0;
		for (CustomerPostitVO postit : partners) {
			String text = postit.getTitle() + ": " + postit.getDescription();
			paint.getTextBounds(text, 0, text.length(), bounds);
			Vector<String> lines = wrapWord(text, 30);
			for (String line : lines) {
				canvas.drawText(line, 1400, positions[j] + bounds.height() * i,
						paint);
				i++;
			}
			j++;
		}
	}

	private double totalCost = 0;

	private void writeCosts() {

		ArrayList<CostPostitVO> partners = (ArrayList<CostPostitVO>) MainScene
				.getInstance().costs.postits;
		paint.setTextSize(20);
		Rect bounds = new Rect();
		int i = 0;
		int[] positions = { 860, 900, 940 };
		int j = 0;
		for (CostPostitVO postit : partners) {
			if (postit.getRelatedDatabaseObject() != null) {
				PostItDB pdb = postit.getRelatedDatabaseObject();
				if (pdb != null) {
					CostPostitController cpc = new CostPostitController(context);
					CostPostitDB cpdb = cpc.getByPostitId(pdb.getId());
					if (cpdb != null) {
						totalCost += cpdb.getCost();
					}
				}
			}
			String text = postit.getTitle() + ": " + postit.getDescription();
			paint.getTextBounds(text, 0, text.length(), bounds);
			Vector<String> lines = wrapWord(text, 85);
			for (String line : lines) {
				canvas.drawText(line, 25, positions[j] + bounds.height() * i,
						paint);
				i++;
			}
			j++;
		}
	}

	private double totalRevenue = 0;
	
	private void writeRevenue() {
		ArrayList<RevenuePostitVO> partners = (ArrayList<RevenuePostitVO>) MainScene
				.getInstance().revenues.postits;
		paint.setTextSize(20);
		Rect bounds = new Rect();
		int i = 0;
		int[] positions = { 860, 900, 940 };
		int j = 0;
		for (RevenuePostitVO postit : partners) {
			if (postit.getRelatedDatabaseObject() != null) {
				PostItDB pdb = postit.getRelatedDatabaseObject();
				if (pdb != null) {
					RevenuePostitController rpc = new RevenuePostitController(
							context);
					RevenuePostitDB cpdb = rpc.getByPostitId(pdb.getId());
					if (cpdb != null) {
						totalRevenue += cpdb.getRevenue();
					}
				}
			}
			String text = postit.getTitle() + ": " + postit.getDescription();
			paint.getTextBounds(text, 0, text.length(), bounds);
			Vector<String> lines = wrapWord(text, 85);
			for (String line : lines) {
				canvas.drawText(line, 900, positions[j] + bounds.height() * i,
						paint);
				i++;
			}
			j++;
		}
	}

	private Vector<String> wrapWord(String stringToWrap, int wrapLenght) {
		Vector<String> lines = new Vector<String>();
		int index = 0;
		while (stringToWrap.indexOf(" ", index) != -1) {
			int currentSpace = stringToWrap.indexOf(" ", index);
			if (currentSpace > wrapLenght) {
				lines.add(stringToWrap.substring(0, index));
				stringToWrap = stringToWrap.substring(index,
						stringToWrap.length());
				index = 0;
			} else {
				index = currentSpace + 1;
			}
		}
		if (stringToWrap.length() > 0 && !stringToWrap.equals(" ")) {
			lines.add(stringToWrap);
		}
		return lines;
	}

}
