package com.canvas.util;

import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.ButtonSprite.OnClickListener;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.ui.activity.BaseGameActivity;

import android.content.DialogInterface;

import com.canvas.util.colorpicker.DiscreteColorPickerDialog;
import com.smartsoft.Educanvas.PostIt;

public class DiscreteColorPicker extends ButtonSprite implements OnClickListener {

	private BaseGameActivity mContext;
	private PostIt postit;
	

	public DiscreteColorPicker(float pX, float pY, TiledTextureRegion texture,
			VertexBufferObjectManager vbo, BaseGameActivity context,
			PostIt postit) {
		super(pX, pY, texture, vbo, null);
		mContext = context;
		this.postit = postit;
		setOnClickListener(this);
	}

	@Override
	public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX,
			float pTouchAreaLocalY) {
		showColorPicker();
	}

	public void showColorPicker() {
		mContext.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				final DiscreteColorPickerDialog pickerDialog = new DiscreteColorPickerDialog(
						mContext);
								
				/*
				pickerDialog.setButton("Ok", new android.content.DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						postit.setPostitColor(UtilColor.fromInt(pickerDialog.getColor()));
						postit.setBackgroundColor(UtilColor.fromInt(pickerDialog.getColor()));
					}
				});

				pickerDialog.setButton2("Cancel", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

					}
				});
				*/
				pickerDialog.show();
				pickerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
					
					@Override
					public void onDismiss(DialogInterface dialog) {
						// TODO Auto-generated method stub
						int selectedColor = pickerDialog.getSelectedColor();
						postit.setColor(UtilColor.fromInt(selectedColor));
					}
				});
			}
		});
	}

}
