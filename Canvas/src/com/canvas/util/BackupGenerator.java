package com.canvas.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.canvas.dataaccess.CanvasDB;
import com.canvas.dataaccess.ChannelPostitDB;
import com.canvas.dataaccess.ClickAdsItemDB;
import com.canvas.dataaccess.CostPostitDB;
import com.canvas.dataaccess.DisplayAdItemDB;
import com.canvas.dataaccess.EmpathyMapNoteDB;
import com.canvas.dataaccess.EmpathyMapPostitDB;
import com.canvas.dataaccess.ItemCostDB;
import com.canvas.dataaccess.KeyPartnerPostitDB;
import com.canvas.dataaccess.LicensingItemDB;
import com.canvas.dataaccess.PayPerUseItemDB;
import com.canvas.dataaccess.PostItDB;
import com.canvas.dataaccess.RelationShipPostitDB;
import com.canvas.dataaccess.RentalItemDB;
import com.canvas.dataaccess.RevenueItemDB;
import com.canvas.dataaccess.RevenuePostitDB;
import com.canvas.dataaccess.SalesFeeItemDB;
import com.canvas.dataaccess.SalesItemDB;
import com.canvas.dataaccess.SubscriptionItemDB;
import com.canvas.dataaccess.TransactionFeeItemDB;
import com.canvas.dataaccess.ValuePropositionPinDB;
import com.canvas.dataaccess.VariableCostDB;
import com.canvas.dataaccess.controller.CanvasController;
import com.canvas.dataaccess.controller.ChannelPostitController;
import com.canvas.dataaccess.controller.ClickAdsItemController;
import com.canvas.dataaccess.controller.CostPostitController;
import com.canvas.dataaccess.controller.DisplayAdItemController;
import com.canvas.dataaccess.controller.EmpathyMapNoteController;
import com.canvas.dataaccess.controller.EmpathyMapPostitController;
import com.canvas.dataaccess.controller.ItemCostController;
import com.canvas.dataaccess.controller.KeyPartnerPostitController;
import com.canvas.dataaccess.controller.LicensingItemController;
import com.canvas.dataaccess.controller.PayPerUseItemController;
import com.canvas.dataaccess.controller.PostitController;
import com.canvas.dataaccess.controller.RelationshipPostitController;
import com.canvas.dataaccess.controller.RentalItemController;
import com.canvas.dataaccess.controller.RevenueItemController;
import com.canvas.dataaccess.controller.RevenuePostitController;
import com.canvas.dataaccess.controller.SalesFeeItemController;
import com.canvas.dataaccess.controller.SalesItemController;
import com.canvas.dataaccess.controller.SubscriptionItemController;
import com.canvas.dataaccess.controller.TransactionFeeItemController;
import com.canvas.dataaccess.controller.ValuePropositionPinController;
import com.canvas.dataaccess.controller.VariableCostController;
import com.smartsoft.Educanvas.CanvasActivity;

public class BackupGenerator {

	private Context context;
	private CanvasActivity activity;
	private JSONObject canvas;
	private String canvasName = "";

	public BackupGenerator(Context context, CanvasActivity activity) {
		this.context = context;
		this.activity = activity;
		canvas = new JSONObject();
	}

	public void generateBackupFile() {
		// Actualiza la base de datos
		activity.updateDatabase();
		// Crea el objeto padre
		try {
			// Canvas
			int canvasId = getCanvas();
			// Postits
			ArrayList<Integer> postitIds = getPostits(canvasId);
			// Partners
			getKeyPartnerPostit(postitIds);
			// Value Propositions
			getValuePropositionPin(postitIds);
			// Empathy Map
			ArrayList<Integer> empathyMapIds = getEmpathyMapPostits(postitIds);
			// Empathy Map Notes
			getEmpathyMapNotes(empathyMapIds);
			// Channels
			getChannelPostits(postitIds);
			// Relationships
			getRelationshipPostits(postitIds);
			// Cost postits
			ArrayList<Integer> costPostitIds = getCostPostits(postitIds);
			// Items costs
			getItemCost(costPostitIds);
			// Variable costs
			getVariableCost(costPostitIds);
			// Revenue postits
			ArrayList<Integer> revenuePostitsIds = getRevenuePostits(postitIds);
			// Revenue items
			ArrayList<Integer> revenueItemsIds = getRevenueItems(revenuePostitsIds);
			// Sales items
			getSalesItems(revenueItemsIds);
			// Display Ad items
			getDisplayAdItems(revenueItemsIds);
			// Subcription items
			getSusbcriptionItems(revenueItemsIds);
			// Rental items
			getRentalItems(revenueItemsIds);
			// Transacion items
			getTransacionFeeItems(revenueItemsIds);
			// Sales fee items
			getSalesFeeItems(revenueItemsIds);
			// Pay per use items
			getPayPerUseItems(revenueItemsIds);
			// Click ad items
			getClickAdsItems(revenueItemsIds);
			// Licensing item
			getLicensingItems(revenueItemsIds);

			// Create file
			File dir = new File(
					android.os.Environment.getExternalStorageDirectory()
							+ java.io.File.separator + "Canvas"
							+ java.io.File.separator);
			if (!dir.exists()) {
				dir.mkdir();
			}
			File file = new File(dir, canvasName + ".json");
			FileWriter writer = new FileWriter(file, false);
			BufferedWriter bwriter = new BufferedWriter(writer);
			bwriter.write(canvas.toString(1));
			bwriter.flush();
			bwriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private int getCanvas() throws JSONException {
		CanvasController canvasController = new CanvasController(context);
		CanvasDB cdb = canvasController.getById(CanvasActivity.canvasId);
		canvas.put("id", cdb.getId());
		canvasName = cdb.getName();
		canvas.put("name", canvasName);
		canvas.put("autor", cdb.getAutor());
		return cdb.getId();
	}

	private ArrayList<Integer> getPostits(int canvasId) throws JSONException {
		PostitController postitController = new PostitController(context);
		ArrayList<PostItDB> postits = (ArrayList<PostItDB>) postitController
				.getByIdCanvas(canvasId);
		JSONArray postitArray = new JSONArray();
		ArrayList<Integer> postitIds = new ArrayList<Integer>();
		for (PostItDB postit : postits) {
			JSONObject currentPostit = new JSONObject();
			currentPostit.put("id", postit.getId());
			currentPostit.put("title", postit.getTitle());
			currentPostit.put("description", postit.getDescription());
			currentPostit.put("color", postit.getColor());
			currentPostit.put("pX", postit.getpX());
			currentPostit.put("pY", postit.getpY());
			currentPostit.put("block", postit.getBlock());
			postitArray.put(currentPostit);
			postitIds.add(postit.getId());
		}
		canvas.put("Postit", postitArray);
		return postitIds;
	}

	private void getKeyPartnerPostit(ArrayList<Integer> postitIds)
			throws JSONException {
		KeyPartnerPostitController kppc = new KeyPartnerPostitController(
				context);
		JSONArray keyPartnerPostitArray = new JSONArray();
		for (int id : postitIds) {
			JSONObject keyPartnerJSON = new JSONObject();
			KeyPartnerPostitDB keyPartnerPostitDB = kppc.getByPostitId(id);
			if (keyPartnerPostitDB != null) {
				keyPartnerJSON.put("id", keyPartnerPostitDB.getId());
				keyPartnerJSON.put("pSX", keyPartnerPostitDB.getpSX());
				keyPartnerJSON.put("pSY", keyPartnerPostitDB.getpSY());
				keyPartnerJSON
						.put("idPostit", keyPartnerPostitDB.getIdPostit());
				keyPartnerPostitArray.put(keyPartnerJSON);
			}
		}
		canvas.put("KeyPartnerPostit", keyPartnerPostitArray);
	}

	private void getValuePropositionPin(ArrayList<Integer> postitIds)
			throws JSONException {
		ValuePropositionPinController vppc = new ValuePropositionPinController(
				context);
		JSONObject valuePropositionPinArray = new JSONObject();
		for (int id : postitIds) {
			JSONArray vpPins = new JSONArray();
			ArrayList<ValuePropositionPinDB> vaPinDB = (ArrayList<ValuePropositionPinDB>) vppc
					.getByIdPostit(id);
			for (ValuePropositionPinDB vaPin : vaPinDB) {
				JSONObject vaPinJSON = new JSONObject();
				vaPinJSON.put("id", vaPin.getId());
				vaPinJSON.put("pX", vaPin.getpX());
				vaPinJSON.put("pY", vaPin.getpY());
				vaPinJSON.put("idPostit", vaPin.getIdPostit());
				vpPins.put(vaPinJSON);
			}
			valuePropositionPinArray.put("" + id, vpPins);
		}
		canvas.put("ValuePropositionPin", valuePropositionPinArray);
	}

	private ArrayList<Integer> getEmpathyMapPostits(ArrayList<Integer> postitIds)
			throws JSONException {
		EmpathyMapPostitController empc = new EmpathyMapPostitController(
				context);
		ArrayList<Integer> empathyMapPostits = new ArrayList<Integer>();
		JSONArray empathyMapArray = new JSONArray();
		for (int id : postitIds) {
			JSONObject empathyMapJSON = new JSONObject();
			EmpathyMapPostitDB eMapNoteDB = empc.getByPostitId(id);
			if (eMapNoteDB != null) {
				empathyMapJSON.put("id", eMapNoteDB.getId());
				empathyMapJSON.put("idPostit", eMapNoteDB.getIdPostit());
				empathyMapArray.put(empathyMapJSON);
				empathyMapPostits.add(eMapNoteDB.getId());
			}
		}
		canvas.put("EmpathyMapPostit", empathyMapArray);
		return empathyMapPostits;
	}

	private void getEmpathyMapNotes(ArrayList<Integer> empathyMapIds)
			throws JSONException {
		EmpathyMapNoteController emnc = new EmpathyMapNoteController(context);
		JSONObject empathyMapNoteArray = new JSONObject();
		for (int id : empathyMapIds) {
			JSONArray emNotes = new JSONArray();
			ArrayList<EmpathyMapNoteDB> emNotesDB = (ArrayList<EmpathyMapNoteDB>) emnc
					.getByEMId(id);
			for (EmpathyMapNoteDB emNote : emNotesDB) {
				JSONObject emNoteJSON = new JSONObject();
				emNoteJSON.put("id", emNote.getId());
				emNoteJSON.put("idEMPostit", emNote.getIdEMPostit());
				emNoteJSON.put("pX", emNote.getpX());
				emNoteJSON.put("pY", emNote.getpY());
				emNoteJSON.put("title", emNote.getTitle());
				emNoteJSON.put("description", emNote.getDescription());
				emNoteJSON.put("color", emNote.getColor());
				emNotes.put(emNoteJSON);
			}
			empathyMapNoteArray.put("" + id, emNotes);
		}
		canvas.put("EmpathyMapNote", empathyMapNoteArray);
	}

	private void getChannelPostits(ArrayList<Integer> postitIds)
			throws JSONException {
		ChannelPostitController chpc = new ChannelPostitController(context);
		JSONArray channelPostitArray = new JSONArray();
		for (int id : postitIds) {
			ChannelPostitDB channelPostitDB = chpc.getByPostitId(id);
			if (channelPostitDB != null) {
				JSONObject channelJSON = new JSONObject();
				channelJSON.put("id", channelPostitDB.getId());
				channelJSON.put("channel", channelPostitDB.getChannel());
				channelJSON.put("idPostit", channelPostitDB.getIdPostit());
				channelPostitArray.put(channelJSON);
			}
		}
		canvas.put("ChannelPostit", channelPostitArray);
	}

	private void getRelationshipPostits(ArrayList<Integer> postitIds)
			throws JSONException {
		RelationshipPostitController rpc = new RelationshipPostitController(
				context);
		JSONObject relationshipArray = new JSONObject();
		for (int id : postitIds) {
			JSONArray relationships = new JSONArray();
			ArrayList<RelationShipPostitDB> relationshipsDB = (ArrayList<RelationShipPostitDB>) rpc
					.getByPostitId(id);
			for (RelationShipPostitDB relationship : relationshipsDB) {
				JSONObject relationshipJSON = new JSONObject();
				relationshipJSON.put("idPostit", relationship.getIdPostit());
				relationshipJSON.put("idValueProposition",
						relationship.getIdValueProposition());
				relationshipJSON
						.put("idCostumer", relationship.getIdCostumer());
				relationships.put(relationshipJSON);
			}
			relationshipArray.put("" + id, relationships);
		}
		canvas.put("RelationShipPostit", relationshipArray);
	}

	private ArrayList<Integer> getCostPostits(ArrayList<Integer> postitIds)
			throws JSONException {
		CostPostitController cpc = new CostPostitController(context);
		ArrayList<Integer> costPostitsIds = new ArrayList<Integer>();
		JSONArray costPostitsArray = new JSONArray();
		for (int id : postitIds) {
			JSONObject costPostitJSON = new JSONObject();
			CostPostitDB cpdb = cpc.getByPostitId(id);
			if (cpdb != null) {
				costPostitJSON.put("id", cpdb.getId());
				costPostitJSON.put("cost", cpdb.getCost());
				costPostitJSON.put("otherCost", cpdb.getOtherCost());
				costPostitJSON.put("idPostit", cpdb.getIdPostit());
				costPostitJSON.put("variableCost", cpdb.getVariableCost());
				costPostitsArray.put(costPostitJSON);
				costPostitsIds.add(cpdb.getId());
			}
		}
		canvas.put("CostPostit", costPostitsArray);
		return costPostitsIds;
	}

	private void getItemCost(ArrayList<Integer> costPostitsIds)
			throws JSONException {
		ItemCostController icc = new ItemCostController(context);
		JSONObject itemCostArray = new JSONObject();
		for (int id : costPostitsIds) {
			JSONArray itemCosts = new JSONArray();
			ArrayList<ItemCostDB> itemCostsDB = (ArrayList<ItemCostDB>) icc
					.getByCostPostitId(id);
			for (ItemCostDB icdb : itemCostsDB) {
				JSONObject itemCostJSON = new JSONObject();
				itemCostJSON.put("id", icdb.getId());
				itemCostJSON.put("item", icdb.getItem());
				itemCostJSON.put("block", icdb.getBlock());
				itemCostJSON.put("cost", icdb.getCost());
				itemCostJSON.put("idCostPostit", icdb.getIdCostPostit());
				itemCostJSON.put("variableCost", icdb.getVariableCost());
				itemCostJSON.put("relatedItemId", icdb.getRelatedItemId());
				itemCosts.put(itemCostJSON);
			}
			itemCostArray.put("" + id,itemCosts);
		}
		canvas.put("ItemCost", itemCostArray);
	}

	private void getVariableCost(ArrayList<Integer> costPostitsId)
			throws JSONException {
		VariableCostController vcc = new VariableCostController(context);
		JSONObject variableCostArray = new JSONObject();
		for (int id : costPostitsId) {
			JSONArray variableCosts = new JSONArray();
			ArrayList<VariableCostDB> variableCostsDB = (ArrayList<VariableCostDB>) vcc
					.getByCostPostitId(id);
			for (VariableCostDB icdb : variableCostsDB) {
				JSONObject variableCostJSON = new JSONObject();
				variableCostJSON.put("id", icdb.getId());
				variableCostJSON.put("customerSegment",
						icdb.getCustomerSegment());
				variableCostJSON.put("marketShare", icdb.getMarketShare());
				variableCostJSON.put("averagePurchase",
						icdb.getAveragePurchase());
				variableCostJSON
						.put("purchasePeriod", icdb.getPurchasePeriod());
				variableCostJSON.put("idCostPostit", icdb.getIdCostPostit());
				variableCostJSON.put("relatedItemId", icdb.getRelatedItemId());
				variableCosts.put(variableCostJSON);
			}
			variableCostArray.put("" + id, variableCosts);
		}
		canvas.put("VariableCost", variableCostArray);
	}

	private ArrayList<Integer> getRevenuePostits(ArrayList<Integer> postitIds)
			throws JSONException {
		RevenuePostitController rpc = new RevenuePostitController(context);
		ArrayList<Integer> revenuePostitsIds = new ArrayList<Integer>();
		JSONArray revenuePostitsArray = new JSONArray();
		for (int id : postitIds) {
			JSONObject revenuePostitJSON = new JSONObject();
			RevenuePostitDB rpdb = rpc.getByPostitId(id);
			if (rpdb != null) {
				revenuePostitJSON.put("id", rpdb.getId());
				revenuePostitJSON.put("revenue", rpdb.getRevenue());
				revenuePostitJSON.put("otherRevenue", rpdb.getOtherRevenue());
				revenuePostitJSON.put("idPostit", rpdb.getIdPostit());
				revenuePostitJSON.put("salesRevenues", rpdb.getSalesRevenues());
				revenuePostitJSON.put("subscriptionRevenue", rpdb.getSubscriptionRevenue());
				revenuePostitJSON.put("payPerUseRevenue", rpdb.getPayPerUseRevenue());
				revenuePostitJSON.put("rentalRevenue", rpdb.getRentalRevenue());
				revenuePostitJSON.put("transFeeRevenue", rpdb.getTransFeeRevenue());
				revenuePostitJSON.put("salesFeeRevenue", rpdb.getSalesFeeRevenue());
				revenuePostitJSON.put("displayAdsRevenue", rpdb.getDisplayAdsRevenue());
				revenuePostitJSON.put("clickAdsRevenue", rpdb.getClickAdsRevenue());
				revenuePostitJSON.put("licensingRevenue", rpdb.getLicensingRevenue());
				revenuePostitsArray.put(revenuePostitJSON);
				revenuePostitsIds.add(rpdb.getId());
			}
		}
		canvas.put("RevenuePostit", revenuePostitsArray);
		return revenuePostitsIds;
	}

	private ArrayList<Integer> getRevenueItems(ArrayList<Integer> postitIds)
			throws JSONException {
		RevenueItemController ric = new RevenueItemController(context);
		ArrayList<Integer> revenueItemsIds = new ArrayList<Integer>();
		JSONObject revenueItemArray = new JSONObject();
		for (int id : postitIds) {
			JSONArray revenueItems = new JSONArray();
			ArrayList<RevenueItemDB> revenueItemsDB = (ArrayList<RevenueItemDB>) ric
					.getByRevenuePostitId(id);
			for (RevenueItemDB ridb : revenueItemsDB) {
				JSONObject revenueItemJSON = new JSONObject();
				revenueItemJSON.put("id", ridb.getId());
				revenueItemJSON.put("name", ridb.getName());
				revenueItemJSON.put("marketShare", ridb.getMarketShare());
				revenueItemJSON.put("purchasePeriod", ridb.getPurchasePeriod());
				revenueItemJSON.put("idRevenuePostit", ridb.getIdRevenuePostit());
				revenueItemJSON.put("revenueType", ridb.getRevenueType());
				revenueItemJSON.put("valueProposition", ridb.getValueProposition());
				revenueItemJSON.put("relatedItemId", ridb.getRelatedItemId());
				revenueItems.put(revenueItemJSON);
				revenueItemsIds.add(ridb.getId());
			}
			revenueItemArray.put("" + id,revenueItems);
		}
		canvas.put("RevenueItem", revenueItemArray);
		return revenueItemsIds;
	}

	private void getSalesItems(ArrayList<Integer> revenueItemIds)
			throws JSONException {
		SalesItemController controller = new SalesItemController(context);
		JSONObject objArray = new JSONObject();
		for (int id : revenueItemIds) {
			JSONArray objItems = new JSONArray();
			SalesItemDB objItemsDB = controller.getByRevenuePostitId(id);
			if (objItemsDB != null) {
				JSONObject objItemJSON = new JSONObject();
				objItemJSON.put("id", objItemsDB.getId());
				objItemJSON.put("averagePurchase",
						objItemsDB.getAveragePurchase());
				objItemJSON.put("averagePrice", objItemsDB.getAveragePrice());
				objItemJSON.put("idRevenuePostit",
						objItemsDB.getIdRevenuePostit());
				objItemJSON.put("idRevenueItem", objItemsDB.getIdRevenueItem());
				objItems.put(objItemJSON);
			}
			objArray.put("" + id, objItems);
		}
		canvas.put("SalesItem", objArray);
	}

	private void getDisplayAdItems(ArrayList<Integer> revenueItemIds)
			throws JSONException {
		DisplayAdItemController controller = new DisplayAdItemController(
				context);
		JSONObject objArray = new JSONObject();
		for (int id : revenueItemIds) {
			JSONArray objItems = new JSONArray();
			DisplayAdItemDB objItemsDB = controller.getByRevenuePostitId(id);
			if (objItemsDB != null) {
				JSONObject objItemJSON = new JSONObject();
				objItemJSON.put("id", objItemsDB.getId());
				objItemJSON.put("averagePageViewPerUser",
						objItemsDB.getAveragePageViewPerUser());
				objItemJSON.put("averageAdUnitsPerPage",
						objItemsDB.getAverageAdUnitsPerPage());
				objItemJSON.put("averageCPMPerAdUnit",
						objItemsDB.getAverageCPMPerAdUnit());
				objItemJSON.put("idRevenuePostit",
						objItemsDB.getIdRevenuePostIt());
				objItemJSON.put("idRevenueItem", objItemsDB.getIdRevenueItem());
				objItems.put(objItemJSON);
			}
			objArray.put("" + id, objItems);
		}
		canvas.put("DisplayAdItem", objArray);
	}

	private void getSusbcriptionItems(ArrayList<Integer> revenueItemIds)
			throws JSONException {
		SubscriptionItemController controller = new SubscriptionItemController(
				context);
		JSONObject objArray = new JSONObject();
		for (int id : revenueItemIds) {
			JSONArray objItems = new JSONArray();
			SubscriptionItemDB objItemsDB = controller.getByRevenuePostitId(id);
			if (objItemsDB != null) {
				JSONObject objItemJSON = new JSONObject();
				objItemJSON.put("id", objItemsDB.getId());
				objItemJSON.put("subscriptionPrice",
						objItemsDB.getSubscriptionPrice());
				objItemJSON.put("idRevenuePostit",
						objItemsDB.getIdRevenuePostit());
				objItemJSON.put("idRevenueItem", objItemsDB.getIdRevenueItem());
				objItems.put(objItemJSON);
			}
			objArray.put("" + id, objItems);
		}
		canvas.put("SubscriptionItem", objArray);
	}

	private void getRentalItems(ArrayList<Integer> revenueItemIds)
			throws JSONException {
		RentalItemController controller = new RentalItemController(context);
		JSONObject objArray = new JSONObject();
		for (int id : revenueItemIds) {
			JSONArray objItems = new JSONArray();
			RentalItemDB objItemsDB = controller.getByRevenuePostitId(id);
			if (objItemsDB != null) {
				JSONObject objItemJSON = new JSONObject();
				objItemJSON.put("id", objItemsDB.getId());
				objItemJSON.put("averageUnitsRented",
						objItemsDB.getAverageUnitsRented());
				objItemJSON.put("rentalFeePerUnit",
						objItemsDB.getRentalFeePerUnit());
				objItemJSON.put("idRevenuePostit",
						objItemsDB.getIdRevenuePostit());
				objItemJSON.put("idRevenueItem", objItemsDB.getIdRevenueItem());
				objItems.put(objItemJSON);
			}
			objArray.put("" + id, objItems);
		}
		canvas.put("RentalItem", objArray);
	}

	private void getTransacionFeeItems(ArrayList<Integer> revenueItemIds)
			throws JSONException {
		TransactionFeeItemController controller = new TransactionFeeItemController(
				context);
		JSONObject objArray = new JSONObject();
		for (int id : revenueItemIds) {
			JSONArray objItems = new JSONArray();
			TransactionFeeItemDB objItemsDB = controller
					.getByRevenuePostitId(id);
			if (objItemsDB != null) {
				JSONObject objItemJSON = new JSONObject();
				objItemJSON.put("id", objItemsDB.getId());
				objItemJSON.put("averageTransPerClient",
						objItemsDB.getAverageTransPerClient());
				objItemJSON.put("averagePricePerTrans",
						objItemsDB.getAveragePricePerTrans());
				objItemJSON.put("transFee", objItemsDB.getTransFee());
				objItemJSON.put("idRevenuePostit",
						objItemsDB.getIdRevenuePostit());
				objItemJSON.put("idRevenueItem", objItemsDB.getIdRevenueItem());
				objItems.put(objItemJSON);
			}
			objArray.put("" + id, objItems);
		}
		canvas.put("TransactionFeeItem", objArray);
	}

	private void getSalesFeeItems(ArrayList<Integer> revenueItemIds)
			throws JSONException {
		SalesFeeItemController controller = new SalesFeeItemController(context);
		JSONObject objArray = new JSONObject();
		for (int id : revenueItemIds) {
			JSONArray objItems = new JSONArray();
			SalesFeeItemDB objItemsDB = controller.getByRevenuePostitId(id);
			if (objItemsDB != null) {
				JSONObject objItemJSON = new JSONObject();
				objItemJSON.put("id", objItemsDB.getId());
				objItemJSON.put("averagePurchase",
						objItemsDB.getAveragePurchase());
				objItemJSON.put("averagePrice", objItemsDB.getAveragePrice());
				objItemJSON.put("saleFee", objItemsDB.getSaleFee());
				objItemJSON.put("idRevenuePostit",
						objItemsDB.getIdRevenuePostit());
				objItemJSON.put("idRevenueItem", objItemsDB.getIdRevenueItem());
				objItems.put(objItemJSON);
			}
			objArray.put("" + id, objItems);
		}
		canvas.put("SalesFeeItem", objArray);
	}

	private void getPayPerUseItems(ArrayList<Integer> revenueItemIds)
			throws JSONException {
		PayPerUseItemController controller = new PayPerUseItemController(
				context);
		JSONObject objArray = new JSONObject();
		for (int id : revenueItemIds) {
			JSONArray objItems = new JSONArray();
			PayPerUseItemDB objItemsDB = controller.getByRevenuePostitId(id);
			if (objItemsDB != null) {
				JSONObject objItemJSON = new JSONObject();
				objItemJSON.put("id", objItemsDB.getId());
				objItemJSON.put("averageUnits", objItemsDB.getAverageUnits());
				objItemJSON.put("averageUnitPrice",
						objItemsDB.getAverageUnitPrice());
				objItemJSON.put("idRevenuePostit",
						objItemsDB.getIdRevenuePostit());
				objItemJSON.put("idRevenueItem", objItemsDB.getIdRevenueItem());
				objItems.put(objItemJSON);
			}
			objArray.put("" + id, objItems);
		}
		canvas.put("PayPerUseItem", objArray);
	}

	private void getClickAdsItems(ArrayList<Integer> revenueItemIds)
			throws JSONException {
		ClickAdsItemController controller = new ClickAdsItemController(context);
		JSONObject objArray = new JSONObject();
		for (int id : revenueItemIds) {
			JSONArray objItems = new JSONArray();
			ClickAdsItemDB objItemsDB = controller.getByRevenuePostitId(id);
			if (objItemsDB != null) {
				JSONObject objItemJSON = new JSONObject();
				objItemJSON.put("id", objItemsDB.getId());
				objItemJSON.put("AveragePageViewsPerUser",
						objItemsDB.getAveragePageViewPerUser());
				objItemJSON.put("averageAdsPerPage",
						objItemsDB.getAverageAdsPerPage());
				objItemJSON.put("averageCTR", objItemsDB.getAverageCTR());
				objItemJSON.put("averageCPC", objItemsDB.getAverageCPC());
				objItemJSON.put("idRevenuePostit",
						objItemsDB.getIdRevenuePostit());
				objItemJSON.put("idRevenueItem", objItemsDB.getIdRevenueItem());
				objItems.put(objItemJSON);
			}
			objArray.put("" + id, objItems);
		}
		canvas.put("ClickAdsItem", objArray);
	}

	private void getLicensingItems(ArrayList<Integer> revenueItemIds)
			throws JSONException {
		LicensingItemController controller = new LicensingItemController(
				context);
		JSONObject objArray = new JSONObject();
		for (int id : revenueItemIds) {
			JSONArray objItems = new JSONArray();
			LicensingItemDB objItemsDB = controller.getByRevenuePostitId(id);
			if (objItemsDB != null) {
				JSONObject objItemJSON = new JSONObject();
				objItemJSON.put("id", objItemsDB.getId());
				objItemJSON.put("averageLicensesPerCostumer",
						objItemsDB.getAverageLicensesPerCostumer());
				objItemJSON.put("averageLicensePrice",
						objItemsDB.getAverageLicensePrice());
				objItemJSON.put("idRevenuePostit",
						objItemsDB.getIdRevenuePostit());
				objItemJSON.put("idRevenueItem", objItemsDB.getIdRevenueItem());
				objItems.put(objItemJSON);
			}
			objArray.put("" + id, objItems);
		}
		canvas.put("LicensingItem", objArray);
	}

}
