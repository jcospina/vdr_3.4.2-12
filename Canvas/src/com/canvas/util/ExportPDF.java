package com.canvas.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;

import com.canvas.dataaccess.CanvasDB;
import com.canvas.dataaccess.CostPostitDB;
import com.canvas.dataaccess.PostItDB;
import com.canvas.dataaccess.RevenuePostitDB;
import com.canvas.dataaccess.controller.CanvasController;
import com.canvas.dataaccess.controller.CostPostitController;
import com.canvas.dataaccess.controller.RevenuePostitController;
import com.canvas.vo.ChannelPostitVO;
import com.canvas.vo.CostPostitVO;
import com.canvas.vo.CustomerPostitVO;
import com.canvas.vo.KeyActivityPostitVO;
import com.canvas.vo.KeyPartnershipPostitVO;
import com.canvas.vo.KeyResourcePostitVO;
import com.canvas.vo.RelationshipPostitVO;
import com.canvas.vo.RevenuePostitVO;
import com.canvas.vo.ValuePropositionPostitVO;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfWriter;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.MainScene;

public class ExportPDF {

	private Context context;
	private String canvasName = "";
	private String autor = "";
	private Document document;

	public ExportPDF(Context context) {
		this.context = context;
		document = new Document();
	}

	public void generateReport(File imageFile) {
		// Se obtiene el nombre del Canvas desde la base de datos
		CanvasController cc = new CanvasController(context);
		CanvasDB cbd = cc.getById(CanvasActivity.canvasId);
		canvasName = cbd.getName();
		autor = cbd.getAutor();
		try {
			// Se crea el writer para escribir los datos
			File dir = new File(
					android.os.Environment.getExternalStorageDirectory()
							+ java.io.File.separator + "Canvas"
							+ java.io.File.separator);
			if (!dir.exists()) {
				dir.mkdir();
			}
			File file = new File(dir, canvasName + ".pdf");
			PdfWriter.getInstance(document, new FileOutputStream(file, false));
			// Se abre el documento
			document.open();			
			// T�tulo
			Paragraph title = new Paragraph();
			title.add(new Phrase(canvasName + "\n\n", FontFactory.getFont(
					FontFactory.HELVETICA_BOLD, 24)));
			title.setAlignment(Element.ALIGN_CENTER);
			Paragraph autorP = new Paragraph();
			autorP.add(new Phrase("Por: " + autor + "\n\n\n", FontFactory
					.getFont(FontFactory.HELVETICA, 16)));
			autorP.setAlignment(Element.ALIGN_RIGHT);
			document.add(title);
			document.add(autorP);
			// Aliados clave
			writeKeyParterships();
			writeKeyActivities();
			writeKeyResources();
			writeValuePropositions();
			writeRelationships();
			writeChannels();
			writeCustomers();
			writeCosts();
			writeRevenue();
			document.close();
			MainScene.getInstance().cancelExportDialog(file, imageFile);
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
	}

	// Escribe los aliados clave en el pdf
	private void writeKeyParterships() {
		try {
			ArrayList<KeyPartnershipPostitVO> partners = (ArrayList<KeyPartnershipPostitVO>) MainScene
					.getInstance().keyPartnerships.postits;
			Paragraph partnerParagraph = new Paragraph();
			partnerParagraph.add(new Phrase(26, "Aliados Clave:\n\n",
					FontFactory.getFont(FontFactory.HELVETICA_BOLD, 20)));
			partnerParagraph.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(partnerParagraph);
			for (KeyPartnershipPostitVO postit : partners) {
				Paragraph temp = new Paragraph();
				temp.add(new Phrase(postit.getTitle() + ":\n", FontFactory
						.getFont(FontFactory.TIMES_BOLD, 16)));
				temp.add(new Phrase(postit.getDescription() + "\n", FontFactory
						.getFont(FontFactory.TIMES_ITALIC, 14)));
				temp.setIndentationLeft(10);
				document.add(temp);
			}
			document.add(new Phrase("\n"));
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		}
	}

	// Escribe las actividades clave en el pdf
	private void writeKeyActivities() {
		try {
			ArrayList<KeyActivityPostitVO> partners = (ArrayList<KeyActivityPostitVO>) MainScene
					.getInstance().keyActivities.postits;
			Paragraph partnerParagraph = new Paragraph();
			partnerParagraph.add(new Phrase(26, "Actividades Clave:\n\n",
					FontFactory.getFont(FontFactory.HELVETICA_BOLD, 20)));
			partnerParagraph.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(partnerParagraph);
			for (KeyActivityPostitVO postit : partners) {
				Paragraph temp = new Paragraph();
				temp.add(new Phrase(postit.getTitle() + ":\n", FontFactory
						.getFont(FontFactory.TIMES_BOLD, 16)));
				temp.add(new Phrase(postit.getDescription() + "\n", FontFactory
						.getFont(FontFactory.TIMES_ITALIC, 14)));
				temp.setIndentationLeft(10);
				document.add(temp);
			}
			document.add(new Phrase("\n"));
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		}
	}

	// Escribe los recursos clave en el pdf
	private void writeKeyResources() {
		try {
			ArrayList<KeyResourcePostitVO> partners = (ArrayList<KeyResourcePostitVO>) MainScene
					.getInstance().keyResources.postits;
			Paragraph partnerParagraph = new Paragraph();
			partnerParagraph.add(new Phrase(26, "Recursos Clave:\n\n",
					FontFactory.getFont(FontFactory.HELVETICA_BOLD, 20)));
			partnerParagraph.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(partnerParagraph);
			for (KeyResourcePostitVO postit : partners) {
				Paragraph temp = new Paragraph();
				temp.add(new Phrase(postit.getTitle() + ":\n", FontFactory
						.getFont(FontFactory.TIMES_BOLD, 16)));
				temp.add(new Phrase(postit.getDescription() + "\n", FontFactory
						.getFont(FontFactory.TIMES_ITALIC, 14)));
				temp.setIndentationLeft(10);
				document.add(temp);
			}
			document.add(new Phrase("\n"));
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		}
	}

	// Escribe las propuestas de valor en el pdf
	private void writeValuePropositions() {
		try {
			ArrayList<ValuePropositionPostitVO> partners = (ArrayList<ValuePropositionPostitVO>) MainScene
					.getInstance().valuePropositions.postits;
			Paragraph partnerParagraph = new Paragraph();
			partnerParagraph.add(new Phrase(26, "Propuestas de valor:\n\n",
					FontFactory.getFont(FontFactory.HELVETICA_BOLD, 20)));
			partnerParagraph.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(partnerParagraph);
			for (ValuePropositionPostitVO postit : partners) {
				Paragraph temp = new Paragraph();
				temp.add(new Phrase(postit.getTitle() + ":\n", FontFactory
						.getFont(FontFactory.TIMES_BOLD, 16)));
				temp.add(new Phrase(postit.getDescription() + "\n", FontFactory
						.getFont(FontFactory.TIMES_ITALIC, 14)));
				temp.setIndentationLeft(10);
				document.add(temp);
			}
			document.add(new Phrase("\n"));
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		}
	}

	// Escribe las relaciones en el pdf
	private void writeRelationships() {
		try {
			ArrayList<RelationshipPostitVO> partners = (ArrayList<RelationshipPostitVO>) MainScene
					.getInstance().relationships.postits;
			Paragraph partnerParagraph = new Paragraph();
			partnerParagraph.add(new Phrase(26,
					"Relaciones con los clientes:\n\n", FontFactory.getFont(
							FontFactory.HELVETICA_BOLD, 20)));
			partnerParagraph.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(partnerParagraph);
			for (RelationshipPostitVO postit : partners) {
				Paragraph temp = new Paragraph();
				temp.add(new Phrase(postit.getTitle() + ":\n", FontFactory
						.getFont(FontFactory.TIMES_BOLD, 16)));
				temp.add(new Phrase(postit.getDescription() + "\n", FontFactory
						.getFont(FontFactory.TIMES_ITALIC, 14)));
				temp.setIndentationLeft(10);
				document.add(temp);
			}
			document.add(new Phrase("\n"));
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		}
	}

	// Escribe los canales en el pdf
	private void writeChannels() {
		try {
			ArrayList<ChannelPostitVO> partners = (ArrayList<ChannelPostitVO>) MainScene
					.getInstance().channels.postits;
			Paragraph partnerParagraph = new Paragraph();
			partnerParagraph.add(new Phrase(26, "Canales:\n\n", FontFactory
					.getFont(FontFactory.HELVETICA_BOLD, 20)));
			partnerParagraph.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(partnerParagraph);
			for (ChannelPostitVO postit : partners) {
				Paragraph temp = new Paragraph();
				temp.add(new Phrase(postit.getTitle() + ":\n", FontFactory
						.getFont(FontFactory.TIMES_BOLD, 16)));
				temp.add(new Phrase(postit.getDescription() + "\n", FontFactory
						.getFont(FontFactory.TIMES_ITALIC, 14)));
				temp.setIndentationLeft(10);
				document.add(temp);
			}
			document.add(new Phrase("\n"));
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		}
	}

	// Escribe los clientes en el pdf
	private void writeCustomers() {
		try {
			ArrayList<CustomerPostitVO> partners = (ArrayList<CustomerPostitVO>) MainScene
					.getInstance().customerSegments.postits;
			Paragraph partnerParagraph = new Paragraph();
			partnerParagraph.add(new Phrase(26, "Clientes:\n\n", FontFactory
					.getFont(FontFactory.HELVETICA_BOLD, 20)));
			partnerParagraph.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(partnerParagraph);
			for (CustomerPostitVO postit : partners) {
				Paragraph temp = new Paragraph();
				temp.add(new Phrase(postit.getTitle() + ":\n", FontFactory
						.getFont(FontFactory.TIMES_BOLD, 16)));
				temp.add(new Phrase(postit.getDescription() + "\n", FontFactory
						.getFont(FontFactory.TIMES_ITALIC, 14)));
				temp.setIndentationLeft(10);
				document.add(temp);
			}
			document.add(new Phrase("\n"));
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		}
	}

	// Escribe los clientes en el pdf
	private void writeCosts() {
		try {
			double totalCost = 0;
			ArrayList<CostPostitVO> partners = (ArrayList<CostPostitVO>) MainScene
					.getInstance().costs.postits;
			Paragraph partnerParagraph = new Paragraph();
			partnerParagraph.add(new Phrase(26, "Costos:\n\n", FontFactory
					.getFont(FontFactory.HELVETICA_BOLD, 20)));
			partnerParagraph.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(partnerParagraph);
			for (CostPostitVO postit : partners) {
				Paragraph temp = new Paragraph();
				temp.add(new Phrase(postit.getTitle() + ":\n", FontFactory
						.getFont(FontFactory.TIMES_BOLD, 16)));
				temp.add(new Phrase(postit.getDescription() + "\n", FontFactory
						.getFont(FontFactory.TIMES_ITALIC, 14)));
				temp.setIndentationLeft(10);
				if (postit.getRelatedDatabaseObject() != null) {
					PostItDB pdb = postit.getRelatedDatabaseObject();
					CostPostitController cpc = new CostPostitController(context);
					CostPostitDB cpdb = cpc.getByPostitId(pdb.getId());
					if (cpdb != null) {
						totalCost += cpdb.getCost();
					}
				}
				document.add(temp);
			}
			Paragraph totalCostP = new Paragraph(new Phrase("Costo total: "
					+ totalCost + "\n\n", FontFactory.getFont(
					FontFactory.TIMES_BOLD, 18)));
			totalCostP.setIndentationLeft(10);
			document.add(totalCostP);
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		}
	}

	// Escribe los clientes en el pdf
	private void writeRevenue() {
		try {
			double totalRevenue = 0;
			ArrayList<RevenuePostitVO> partners = (ArrayList<RevenuePostitVO>) MainScene
					.getInstance().revenues.postits;
			Paragraph partnerParagraph = new Paragraph();
			partnerParagraph.add(new Phrase(26, "Ingresos:\n\n", FontFactory
					.getFont(FontFactory.HELVETICA_BOLD, 20)));
			partnerParagraph.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(partnerParagraph);
			for (RevenuePostitVO postit : partners) {
				Paragraph temp = new Paragraph();
				temp.add(new Phrase(postit.getTitle() + ":\n", FontFactory
						.getFont(FontFactory.TIMES_BOLD, 16)));
				temp.add(new Phrase(postit.getDescription() + "\n", FontFactory
						.getFont(FontFactory.TIMES_ITALIC, 14)));
				temp.setIndentationLeft(10);
				if (postit.getRelatedDatabaseObject() != null) {
					PostItDB pdb = postit.getRelatedDatabaseObject();
					RevenuePostitController rpc = new RevenuePostitController(context);
					RevenuePostitDB cpdb = rpc.getByPostitId(pdb.getId());
					if (cpdb != null) {
						totalRevenue += cpdb.getRevenue();
					}
				}
				document.add(temp);
			}
			Paragraph totalRevenueP = new Paragraph(new Phrase("Ingresos totales: "
					+ totalRevenue + "\n\n", FontFactory.getFont(
					FontFactory.TIMES_BOLD, 18)));
			totalRevenueP.setIndentationLeft(10);
			document.add(totalRevenueP);
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		}
	}

}
