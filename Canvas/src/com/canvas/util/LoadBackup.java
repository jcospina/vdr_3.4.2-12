package com.canvas.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;

import com.canvas.dataaccess.CanvasDB;
import com.canvas.dataaccess.ChannelPostitDB;
import com.canvas.dataaccess.ClickAdsItemDB;
import com.canvas.dataaccess.CostPostitDB;
import com.canvas.dataaccess.DisplayAdItemDB;
import com.canvas.dataaccess.EmpathyMapNoteDB;
import com.canvas.dataaccess.EmpathyMapPostitDB;
import com.canvas.dataaccess.ItemCostDB;
import com.canvas.dataaccess.KeyPartnerPostitDB;
import com.canvas.dataaccess.LicensingItemDB;
import com.canvas.dataaccess.PayPerUseItemDB;
import com.canvas.dataaccess.PostItDB;
import com.canvas.dataaccess.RelationShipPostitDB;
import com.canvas.dataaccess.RentalItemDB;
import com.canvas.dataaccess.RevenueItemDB;
import com.canvas.dataaccess.RevenuePostitDB;
import com.canvas.dataaccess.SalesFeeItemDB;
import com.canvas.dataaccess.SalesItemDB;
import com.canvas.dataaccess.SubscriptionItemDB;
import com.canvas.dataaccess.TransactionFeeItemDB;
import com.canvas.dataaccess.ValuePropositionPinDB;
import com.canvas.dataaccess.VariableCostDB;
import com.canvas.dataaccess.controller.CanvasController;
import com.canvas.dataaccess.controller.ChannelPostitController;
import com.canvas.dataaccess.controller.ClickAdsItemController;
import com.canvas.dataaccess.controller.CostPostitController;
import com.canvas.dataaccess.controller.DisplayAdItemController;
import com.canvas.dataaccess.controller.EmpathyMapNoteController;
import com.canvas.dataaccess.controller.EmpathyMapPostitController;
import com.canvas.dataaccess.controller.ItemCostController;
import com.canvas.dataaccess.controller.KeyPartnerPostitController;
import com.canvas.dataaccess.controller.LicensingItemController;
import com.canvas.dataaccess.controller.PayPerUseItemController;
import com.canvas.dataaccess.controller.PostitController;
import com.canvas.dataaccess.controller.RelationshipPostitController;
import com.canvas.dataaccess.controller.RentalItemController;
import com.canvas.dataaccess.controller.RevenueItemController;
import com.canvas.dataaccess.controller.RevenuePostitController;
import com.canvas.dataaccess.controller.SalesFeeItemController;
import com.canvas.dataaccess.controller.SalesItemController;
import com.canvas.dataaccess.controller.SubscriptionItemController;
import com.canvas.dataaccess.controller.TransactionFeeItemController;
import com.canvas.dataaccess.controller.ValuePropositionPinController;
import com.canvas.dataaccess.controller.VariableCostController;
import com.canvas.vo.ChannelPostitVO;
import com.canvas.vo.CostPostitVO;
import com.canvas.vo.CustomerPostitVO;
import com.canvas.vo.KeyActivityPostitVO;
import com.canvas.vo.KeyPartnershipPostitVO;
import com.canvas.vo.KeyResourcePostitVO;
import com.canvas.vo.RelationshipPostitVO;
import com.canvas.vo.RevenuePostitVO;
import com.canvas.vo.ValuePropositionPostitVO;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.MainScene;

@SuppressLint("UseSparseArrays")
public class LoadBackup {

	private CanvasActivity activity;
	private String path;
	private JSONObject root;

	public LoadBackup(CanvasActivity activity, String path) {
		this.activity = activity;
		this.path = path;
	}

	public void loadFile() {
		File JSONFile = new File(path);
		if (JSONFile.exists()) {
			try {
				FileInputStream fis = new FileInputStream(JSONFile);
				StringBuffer fileContent = new StringBuffer("");
				byte[] buffer = new byte[1024];
				while (fis.read(buffer) != -1) {
					fileContent.append(new String(buffer));
				}
				root = new JSONObject(fileContent.toString());
				fis.close();
				clearCanvas();
				parseJSON();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	private void clearCanvas() {
		for (KeyPartnershipPostitVO postit : MainScene.getInstance().keyPartnerships.postits) {
			KeyPartnerPostitController kppc = new KeyPartnerPostitController(
					activity);
			if (postit.getRelatedDatabaseObject() != null) {
				kppc.removeAllByPostitId(postit.getRelatedDatabaseObject()
						.getId());
			}
			MainScene.getInstance().detachChild(postit);
			MainScene.getInstance().unregisterTouchArea(postit);
		}
		MainScene.getInstance().keyPartnerships.postits.clear();
		for (KeyActivityPostitVO postit : MainScene.getInstance().keyActivities.postits) {
			MainScene.getInstance().detachChild(postit);
			MainScene.getInstance().unregisterTouchArea(postit);
		}
		MainScene.getInstance().keyActivities.postits.clear();
		for (KeyResourcePostitVO postit : MainScene.getInstance().keyResources.postits) {
			MainScene.getInstance().detachChild(postit);
			MainScene.getInstance().unregisterTouchArea(postit);
		}
		MainScene.getInstance().keyResources.postits.clear();
		for (ValuePropositionPostitVO postit : MainScene.getInstance().valuePropositions.postits) {
			ValuePropositionPinController controller = new ValuePropositionPinController(
					activity);
			if (postit.getRelatedDatabaseObject() != null) {
				controller.removeAllByPostitId(postit
						.getRelatedDatabaseObject().getId());
			}
			MainScene.getInstance().detachChild(postit);
			MainScene.getInstance().unregisterTouchArea(postit);
		}
		MainScene.getInstance().valuePropositions.postits.clear();
		for (RelationshipPostitVO postit : MainScene.getInstance().relationships.postits) {
			RelationshipPostitController controller = new RelationshipPostitController(
					activity);
			if (postit.getRelatedDatabaseObject() != null) {
				controller.removeAllByPostitId(postit
						.getRelatedDatabaseObject().getId());
			}
			MainScene.getInstance().detachChild(postit);
			MainScene.getInstance().unregisterTouchArea(postit);
		}
		MainScene.getInstance().relationships.postits.clear();
		for (ChannelPostitVO postit : MainScene.getInstance().channels.postits) {
			ChannelPostitController controller = new ChannelPostitController(
					activity);
			if (postit.getRelatedDatabaseObject() != null) {
				controller.removeAllByPostitId(postit
						.getRelatedDatabaseObject().getId());
			}
			MainScene.getInstance().detachChild(postit);
			MainScene.getInstance().unregisterTouchArea(postit);
		}
		MainScene.getInstance().channels.postits.clear();
		for (CustomerPostitVO postit : MainScene.getInstance().customerSegments.postits) {
			EmpathyMapPostitController empc = new EmpathyMapPostitController(
					activity);
			if (postit.getRelatedDatabaseObject() != null) {
				EmpathyMapPostitDB empdb = empc.getByPostitId(postit
						.getRelatedDatabaseObject().getId());
				if (empdb != null) {
					EmpathyMapNoteController emnc = new EmpathyMapNoteController(
							activity);
					emnc.removeAllByEMPostitId(empdb.getId());
					empc.removeEntry(empdb.getId());
				}
			}
			MainScene.getInstance().detachChild(postit);
			MainScene.getInstance().unregisterTouchArea(postit);
		}
		MainScene.getInstance().customerSegments.postits.clear();
		for (CostPostitVO postit : MainScene.getInstance().costs.postits) {
			CostPostitController cpc = new CostPostitController(activity);
			if (postit.getRelatedDatabaseObject() != null) {
				CostPostitDB cpdb = cpc.getByPostitId(postit
						.getRelatedDatabaseObject().getId());
				if (cpdb != null) {
					ItemCostController icc = new ItemCostController(activity);
					icc.removeEntries(cpdb.getId());
					VariableCostController vcc = new VariableCostController(
							activity);
					vcc.removeEntries(cpdb.getId());
					cpc.removeEntry(cpdb.getId());
				}
			}
			MainScene.getInstance().detachChild(postit);
			MainScene.getInstance().unregisterTouchArea(postit);
			MainScene.getInstance().costs.postits.clear();
		}
		for (RevenuePostitVO postit : MainScene.getInstance().revenues.postits) {
			RevenuePostitController rpc = new RevenuePostitController(activity);
			if (postit.getRelatedDatabaseObject() != null) {
				RevenuePostitDB rpdb = rpc.getByPostitId(postit
						.getRelatedDatabaseObject().getId());
				if (rpdb != null) {
					RevenueItemController ric = new RevenueItemController(
							activity);
					ric.removeEntries(rpdb.getId());
					SalesItemController sic = new SalesItemController(activity);
					sic.removeEntries(rpdb.getId());
					SubscriptionItemController suic = new SubscriptionItemController(
							activity);
					suic.removeEntries(rpdb.getId());
					PayPerUseItemController ppuc = new PayPerUseItemController(
							activity);
					ppuc.removeEntries(rpdb.getId());
					RentalItemController reic = new RentalItemController(
							activity);
					reic.removeEntries(rpdb.getId());
					TransactionFeeItemController tfic = new TransactionFeeItemController(
							activity);
					tfic.removeEntries(rpdb.getId());
					SalesFeeItemController sfic = new SalesFeeItemController(
							activity);
					sfic.removeEntries(rpdb.getId());
					DisplayAdItemController daic = new DisplayAdItemController(
							activity);
					daic.removeEntries(rpdb.getId());
					ClickAdsItemController caic = new ClickAdsItemController(
							activity);
					caic.removeEntries(rpdb.getId());
					LicensingItemController lic = new LicensingItemController(
							activity);
					lic.removeEntries(rpdb.getId());
				}
			}
			MainScene.getInstance().detachChild(postit);
			MainScene.getInstance().unregisterTouchArea(postit);
		}
		MainScene.getInstance().revenues.postits.clear();
		PostitController pc = new PostitController(activity);
		pc.removeByCanvasId(CanvasActivity.canvasId);
	}

	@SuppressLint("UseSparseArrays")
	private void parseJSON() throws JSONException {
		String canvasName = root.getString("name");
		String autor = root.getString("autor");
		// Actualiza el canvas
		CanvasDB newCanvas = new CanvasDB(CanvasActivity.canvasId, null,
				canvasName, autor);
		CanvasController cc = new CanvasController(activity);
		cc.update(newCanvas);
		// Crea los postits a partir del archivo JSON
		JSONArray postitArray = root.getJSONArray("Postit");
		PostitController pc = new PostitController(activity);
		ArrayList<Id> postitIds = new ArrayList<Id>();
		HashMap<Integer, Integer> vpsHM = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> customersHM = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> revenuePostitHM = new HashMap<Integer, Integer>();

		HashMap<Integer, Integer> postitsHM = new HashMap<Integer, Integer>();

		ArrayList<Id> relationshipsHM = new ArrayList<Id>();
		for (int i = 0; i < postitArray.length(); i++) {
			JSONObject postitJSON = postitArray.getJSONObject(i);
			int block = postitJSON.getInt("block");
			PostItDB postItDB = new PostItDB(postitJSON.getInt("id"),
					postitJSON.getString("title"),
					postitJSON.getString("description"),
					postitJSON.getInt("color"), postitJSON.getInt("pX"),
					postitJSON.getInt("pY"), CanvasActivity.canvasId, block);
			int index = (int) pc.create(postItDB);
			postitIds.add(new Id(postitJSON.getInt("id"), index, block));
			postitsHM.put(postitJSON.getInt("id"), index);
		}
		// KeyPartnerPostit
		JSONArray keyPartnerPostitArray = root.getJSONArray("KeyPartnerPostit");
		KeyPartnerPostitController keyPartnerPostitController = new KeyPartnerPostitController(
				activity);
		// ValueProposition
		JSONObject valuePropositionPinsArray = root
				.getJSONObject("ValuePropositionPin");
		ValuePropositionPinController pinController = new ValuePropositionPinController(
				activity);
		// EmpathyMapPostit
		JSONArray empathyMapPostits = root.getJSONArray("EmpathyMapPostit");
		EmpathyMapPostitController emMapPostitController = new EmpathyMapPostitController(
				activity);
		ArrayList<Id> empathyMapIds = new ArrayList<Id>();
		// Channels
		JSONArray channelsArray = root.getJSONArray("ChannelPostit");
		ChannelPostitController channelPostitController = new ChannelPostitController(
				activity);
		// Relationships
		JSONObject relationshipsArray = root
				.getJSONObject("RelationShipPostit");
		RelationshipPostitController relPostitController = new RelationshipPostitController(
				activity);
		// Costs
		JSONArray costsArray = root.getJSONArray("CostPostit");
		CostPostitController costPostitController = new CostPostitController(
				activity);
		ArrayList<Id> costsIds = new ArrayList<Id>();
		// Revenues
		JSONArray revenuesArray = root.getJSONArray("RevenuePostit");
		RevenuePostitController revenuePostitController = new RevenuePostitController(
				activity);
		ArrayList<Id> revenueIds = new ArrayList<Id>();
		for (Id id : postitIds) {
			// KeyPartnerPostit
			if (id.block == 0) {
				for (int i = 0; i < keyPartnerPostitArray.length(); i++) {
					JSONObject keyPartnerJSON = keyPartnerPostitArray
							.getJSONObject(i);
					if (keyPartnerJSON.getInt("idPostit") == id.old_id) {
						keyPartnerPostitController
								.create(new KeyPartnerPostitDB(keyPartnerJSON
										.getInt("id"), keyPartnerJSON
										.getInt("pSX"), keyPartnerJSON
										.getInt("pSY"), id.new_id));
					}
				}
			}
			// ValueProposition
			if (id.block == 3) {
				JSONArray pins = valuePropositionPinsArray.getJSONArray(""
						+ id.old_id);
				for (int i = 0; i < pins.length(); i++) {
					JSONObject pin = pins.getJSONObject(i);
					pinController.create(new ValuePropositionPinDB(0, pin
							.getInt("pX"), pin.getInt("pY"), id.new_id));
				}
				vpsHM.put(id.old_id, id.new_id);
			}
			// EmpathyMapPostit
			if (id.block == 6) {
				for (int i = 0; i < empathyMapPostits.length(); i++) {
					JSONObject empathyMapJSON = empathyMapPostits
							.getJSONObject(i);
					if (empathyMapJSON.getInt("idPostit") == id.old_id) {
						EmpathyMapPostitDB empMapPostitDB = new EmpathyMapPostitDB(
								0, id.new_id, null);
						empathyMapIds.add(new Id(empathyMapJSON.getInt("id"),
								(int) emMapPostitController
										.create(empMapPostitDB), -1));
					}
				}
				customersHM.put(id.old_id, id.new_id);
			}
			// Channels
			if (id.block == 5) {
				for (int i = 0; i < channelsArray.length(); i++) {
					JSONObject channelJSON = channelsArray.getJSONObject(i);
					if (channelJSON.getInt("idPostit") == id.old_id) {
						channelPostitController.create(new ChannelPostitDB(0,
								channelJSON.getInt("channel"), id.new_id));
					}
				}
			}
			// Relationships
			if (id.block == 4) {
				relationshipsHM.add(new Id(id.old_id, id.new_id, -1));
			}
			// Costs
			if (id.block == 7) {
				for (int i = 0; i < costsArray.length(); i++) {
					JSONObject costJSON = costsArray.getJSONObject(i);
					if (costJSON.getInt("idPostit") == id.old_id) {
						int index = (int) costPostitController
								.create(new CostPostitDB(0, costJSON
										.getDouble("cost"), costJSON
										.getDouble("otherCost"), id.new_id,
										costJSON.getDouble("variableCost")));
						costsIds.add(new Id(costJSON.getInt("id"), index, -1));
					}
				}
			}
			// Revenues
			if (id.block == 8) {
				for (int i = 0; i < revenuesArray.length(); i++) {
					JSONObject revenueJSON = revenuesArray.getJSONObject(i);
					if (revenueJSON.getInt("idPostit") == id.old_id) {
						int index = (int) revenuePostitController
								.create(new RevenuePostitDB(
										0,
										revenueJSON.getDouble("revenue"),
										revenueJSON.getDouble("otherRevenue"),
										id.new_id,
										revenueJSON.getDouble("salesRevenues"),
										revenueJSON
												.getDouble("subscriptionRevenue"),
										revenueJSON
												.getDouble("payPerUseRevenue"),
										revenueJSON.getDouble("rentalRevenue"),
										revenueJSON
												.getDouble("transFeeRevenue"),
										revenueJSON
												.getDouble("salesFeeRevenue"),
										revenueJSON
												.getDouble("displayAdsRevenue"),
										revenueJSON
												.getDouble("clickAdsRevenue"),
										revenueJSON
												.getDouble("licensingRevenue")));
						revenueIds.add(new Id(revenueJSON.getInt("id"), index,
								-1));
						revenuePostitHM.put(revenueJSON.getInt("id"), index);
					}
				}
			}
		}
		// Relationships
		for (Id relationshipId : relationshipsHM) {
			JSONArray relations = relationshipsArray
					.getJSONArray(relationshipId.old_id + "");
			for (int i = 0; i < relations.length(); i++) {
				JSONObject currentRelation = relations.getJSONObject(i);
				int newVpId = vpsHM.get(currentRelation
						.getInt("idValueProposition"));
				int newCustId = customersHM.get(currentRelation
						.getInt("idCostumer"));
				RelationShipPostitDB relationShipPostitDB = new RelationShipPostitDB(
						relationshipId.new_id, newVpId, newCustId);
				relPostitController.create(relationShipPostitDB);
			}
		}

		// EmpathyMapNotes
		JSONObject empathyNotesArray = root.getJSONObject("EmpathyMapNote");
		EmpathyMapNoteController empathyMapNoteController = new EmpathyMapNoteController(
				activity);
		for (Id id : empathyMapIds) {
			JSONArray notes = empathyNotesArray.getJSONArray("" + id.old_id);
			for (int i = 0; i < notes.length(); i++) {
				JSONObject note = notes.getJSONObject(i);
				empathyMapNoteController
						.create(new EmpathyMapNoteDB(0, id.new_id, note
								.getInt("pX"), note.getInt("pY"), note
								.getString("title"), note
								.getString("description"), note.getInt("color")));
			}
		}
		// Item costs
		JSONObject itemCostsArray = root.getJSONObject("ItemCost");
		ItemCostController itemCostController = new ItemCostController(activity);
		for (Id id : costsIds) {
			JSONArray itemcosts = itemCostsArray.getJSONArray("" + id.old_id);
			for (int i = 0; i < itemcosts.length(); i++) {
				JSONObject itemcost = itemcosts.getJSONObject(i);
				itemCostController.create(new ItemCostDB(0, itemcost
						.getString("item"), itemcost.getInt("block"), itemcost
						.getDouble("cost"), id.new_id, itemcost
						.getDouble("variableCost"), postitsHM.get(itemcost
						.getInt("relatedItemId"))));
			}
		}
		// Variable costs
		JSONObject variableCostsArray = root.getJSONObject("VariableCost");
		VariableCostController variableCostController = new VariableCostController(
				activity);
		for (Id id : costsIds) {
			JSONArray variablecosts = variableCostsArray.getJSONArray(""
					+ id.old_id);
			for (int i = 0; i < variablecosts.length(); i++) {
				JSONObject variablecost = variablecosts.getJSONObject(i);
				variableCostController.create(new VariableCostDB(0,
						variablecost.getString("customerSegment"), variablecost
								.getDouble("marketShare"), variablecost
								.getDouble("averagePurchase"), variablecost
								.getInt("purchasePeriod"), id.new_id, postitsHM
								.get(variablecost.getInt("relatedItemId"))));
			}
		}
		// RevenueItems
		JSONObject revenueItemsArray = root.getJSONObject("RevenueItem");
		RevenueItemController revenueItemController = new RevenueItemController(
				activity);
		ArrayList<Id> revenueItemsId = new ArrayList<LoadBackup.Id>();
		for (Id id : revenueIds) {
			JSONArray revenueItems = revenueItemsArray.getJSONArray(""
					+ id.old_id);
			for (int i = 0; i < revenueItems.length(); i++) {
				JSONObject revenueItem = revenueItems.getJSONObject(i);
				int index = (int) revenueItemController
						.create(new RevenueItemDB(0, revenueItem
								.getString("name"), revenueItem
								.getDouble("marketShare"), revenueItem
								.getInt("purchasePeriod"), id.new_id,
								revenueItem.getInt("revenueType"), revenueItem
										.getString("valueProposition"),
								postitsHM.get(revenueItem
										.getInt("relatedItemId"))));
				revenueItemsId.add(new Id(revenueItem.getInt("id"), index, -1));
			}
		}
		// Sale Items
		loadSales(revenueItemsId, revenuePostitHM, "SalesItem");
		// DisplayAds
		loadDisplayAds(revenueItemsId, revenuePostitHM, "DisplayAdItem");
		// Subscriptions
		loadSubscriptions(revenueItemsId, revenuePostitHM, "SubscriptionItem");
		// Rental
		loadRentals(revenueItemsId, revenuePostitHM, "RentalItem");
		// Transaction fee
		loadTransacionFee(revenueItemsId, revenuePostitHM, "TransactionFeeItem");
		// Sales fee
		loadSalesFee(revenueItemsId, revenuePostitHM, "SalesFeeItem");
		// Pay per use
		loadPayPerUse(revenueItemsId, revenuePostitHM, "PayPerUseItem");
		// Click Ads
		loadClickAds(revenueItemsId, revenuePostitHM, "ClickAdsItem");
		// Licensing
		loadLicensing(revenueItemsId, revenuePostitHM, "LicensingItem");
		// Actualiza la escena con los valores cargados
		MainScene.getInstance().onLoadScene();
	}

	private void loadSales(ArrayList<Id> revenueItemsId,
			HashMap<Integer, Integer> revenuePostitHM, String value)
			throws JSONException {
		JSONObject objArray = root.getJSONObject(value);
		SalesItemController objController = new SalesItemController(activity);
		for (Id id : revenueItemsId) {
			JSONArray objs = objArray.getJSONArray("" + id.old_id);
			for (int i = 0; i < objs.length(); i++) {
				JSONObject obj = objs.getJSONObject(i);
				objController.create(new SalesItemDB(0, obj
						.getDouble("averagePurchase"), obj
						.getDouble("averagePrice"), revenuePostitHM.get(obj
						.getInt("idRevenuePostit")), id.new_id));
			}
		}
	}

	private void loadDisplayAds(ArrayList<Id> revenueItemsId,
			HashMap<Integer, Integer> revenuePostitHM, String value)
			throws JSONException {
		JSONObject objArray = root.getJSONObject(value);
		DisplayAdItemController objController = new DisplayAdItemController(
				activity);
		for (Id id : revenueItemsId) {
			JSONArray objs = objArray.getJSONArray("" + id.old_id);
			for (int i = 0; i < objs.length(); i++) {
				JSONObject obj = objs.getJSONObject(i);
				objController.create(new DisplayAdItemDB(0, obj
						.getInt("averagePageViewPerUser"), obj
						.getInt("averageAdUnitsPerPage"), obj
						.getInt("averageCPMPerAdUnit"), revenuePostitHM.get(obj
						.getInt("idRevenuePostit")), id.new_id));
			}
		}
	}

	private void loadSubscriptions(ArrayList<Id> revenueItemsId,
			HashMap<Integer, Integer> revenuePostitHM, String value)
			throws JSONException {
		JSONObject objArray = root.getJSONObject(value);
		SubscriptionItemController objController = new SubscriptionItemController(
				activity);
		for (Id id : revenueItemsId) {
			JSONArray objs = objArray.getJSONArray("" + id.old_id);
			for (int i = 0; i < objs.length(); i++) {
				JSONObject obj = objs.getJSONObject(i);
				objController.create(new SubscriptionItemDB(0, obj
						.getDouble("subscriptionPrice"), revenuePostitHM
						.get(obj.getInt("idRevenuePostit")), id.new_id));
			}
		}
	}

	private void loadRentals(ArrayList<Id> revenueItemsId,
			HashMap<Integer, Integer> revenuePostitHM, String value)
			throws JSONException {
		JSONObject objArray = root.getJSONObject(value);
		RentalItemController objController = new RentalItemController(activity);
		for (Id id : revenueItemsId) {
			JSONArray objs = objArray.getJSONArray("" + id.old_id);
			for (int i = 0; i < objs.length(); i++) {
				JSONObject obj = objs.getJSONObject(i);
				objController.create(new RentalItemDB(0, obj
						.getDouble("averageUnitsRented"), obj
						.getDouble("rentalFeePerUnit"), revenuePostitHM.get(obj
						.getInt("idRevenuePostit")), id.new_id));
			}
		}
	}

	private void loadTransacionFee(ArrayList<Id> revenueItemsId,
			HashMap<Integer, Integer> revenuePostitHM, String value)
			throws JSONException {
		JSONObject objArray = root.getJSONObject(value);
		TransactionFeeItemController objController = new TransactionFeeItemController(
				activity);
		for (Id id : revenueItemsId) {
			JSONArray objs = objArray.getJSONArray("" + id.old_id);
			for (int i = 0; i < objs.length(); i++) {
				JSONObject obj = objs.getJSONObject(i);
				objController.create(new TransactionFeeItemDB(0, obj
						.getDouble("averageTransPerClient"), obj
						.getDouble("averagePricePerTrans"), obj
						.getDouble("transFee"), revenuePostitHM.get(obj
						.getInt("idRevenuePostit")), id.new_id));
			}
		}
	}

	private void loadSalesFee(ArrayList<Id> revenueItemsId,
			HashMap<Integer, Integer> revenuePostitHM, String value)
			throws JSONException {
		JSONObject objArray = root.getJSONObject(value);
		SalesFeeItemController objController = new SalesFeeItemController(
				activity);
		for (Id id : revenueItemsId) {
			JSONArray objs = objArray.getJSONArray("" + id.old_id);
			for (int i = 0; i < objs.length(); i++) {
				JSONObject obj = objs.getJSONObject(i);
				objController.create(new SalesFeeItemDB(0, obj
						.getDouble("averagePurchase"), obj
						.getDouble("averagePrice"), obj.getDouble("saleFee"),
						revenuePostitHM.get(obj.getInt("idRevenuePostit")),
						id.new_id));
			}
		}
	}

	private void loadPayPerUse(ArrayList<Id> revenueItemsId,
			HashMap<Integer, Integer> revenuePostitHM, String value)
			throws JSONException {
		JSONObject objArray = root.getJSONObject(value);
		PayPerUseItemController objController = new PayPerUseItemController(
				activity);
		for (Id id : revenueItemsId) {
			JSONArray objs = objArray.getJSONArray("" + id.old_id);
			for (int i = 0; i < objs.length(); i++) {
				JSONObject obj = objs.getJSONObject(i);
				objController.create(new PayPerUseItemDB(0, obj
						.getDouble("averageUnits"), obj
						.getDouble("averageUnitPrice"), revenuePostitHM.get(obj
						.getInt("idRevenuePostit")), id.new_id));
			}
		}
	}

	private void loadClickAds(ArrayList<Id> revenueItemsId,
			HashMap<Integer, Integer> revenuePostitHM, String value)
			throws JSONException {
		JSONObject objArray = root.getJSONObject(value);
		ClickAdsItemController objController = new ClickAdsItemController(
				activity);
		for (Id id : revenueItemsId) {
			JSONArray objs = objArray.getJSONArray("" + id.old_id);
			for (int i = 0; i < objs.length(); i++) {
				JSONObject obj = objs.getJSONObject(i);
				objController.create(new ClickAdsItemDB(0, obj
						.getInt("AveragePageViewsPerUser"), obj
						.getInt("averageAdsPerPage"), obj.getInt("averageCTR"),
						obj.getInt("averageCPC"), revenuePostitHM.get(obj
								.getInt("idRevenuePostit")), id.new_id));
			}
		}
	}

	private void loadLicensing(ArrayList<Id> revenueItemsId,
			HashMap<Integer, Integer> revenuePostitHM, String value)
			throws JSONException {
		JSONObject objArray = root.getJSONObject(value);
		LicensingItemController objController = new LicensingItemController(
				activity);
		for (Id id : revenueItemsId) {
			JSONArray objs = objArray.getJSONArray("" + id.old_id);
			for (int i = 0; i < objs.length(); i++) {
				JSONObject obj = objs.getJSONObject(i);
				objController.create(new LicensingItemDB(0, obj
						.getInt("averageLicensesPerCostumer"), obj
						.getInt("averageLicensePrice"), revenuePostitHM.get(obj
						.getInt("idRevenuePostit")), id.new_id));
			}
		}
	}

	class Id {

		public int old_id;
		public int new_id;
		public int block;

		public Id(int old_id, int new_id, int block) {
			super();
			this.old_id = old_id;
			this.new_id = new_id;
			this.block = block;
		}

	}
}
