package com.canvas.util;

import org.andengine.util.color.Color;

public class UtilColor {
	
	public static Color fromInt(int color){
		float r = (float)((float)android.graphics.Color.red(color)/255f);
		float g = (float)((float)android.graphics.Color.green(color)/255f);
		float b = (float)((float)android.graphics.Color.blue(color)/255f);
		float a = (float)((float)android.graphics.Color.alpha(color)/255f);
		Color c = new Color(r,g,b,a);
				
		return c;
	}
	
}
