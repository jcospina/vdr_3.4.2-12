package com.canvas.util;

import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.ButtonSprite.OnClickListener;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.color.Color;

import android.content.DialogInterface;

import com.canvas.util.colorpicker.ColorPickerDialog;

public class ColorPicker extends ButtonSprite implements OnClickListener {

	private BaseGameActivity mContext;
	private OnColorChangeListener onColorChangeListener;
	private Color startColor;
	private Color selectedColor;

	public ColorPicker(float pX, float pY, TiledTextureRegion texture,
			VertexBufferObjectManager vbo, BaseGameActivity context,
			Color startColor, OnColorChangeListener onColorChangeListener) {
		super(pX, pY, texture, vbo, null);
		mContext = context;
		this.startColor = startColor;
		this.onColorChangeListener = onColorChangeListener;
		setOnClickListener(this);
	}

	@Override
	public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX,
			float pTouchAreaLocalY) {
		showColorPicker();
	}

	public void showColorPicker() {
		mContext.runOnUiThread(new Runnable() {
			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				final ColorPickerDialog pickerDialog = new ColorPickerDialog(
						mContext, startColor.getARGBPackedInt());
				pickerDialog.setAlphaSliderVisible(true);

				pickerDialog.setButton("Ok",
						new android.content.DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								selectedColor = UtilColor.fromInt(pickerDialog.getColor());
								startColor = selectedColor;
								if (onColorChangeListener != null) {
									onColorChangeListener.onColorChange();
								}
							}
						});

				pickerDialog.setButton2("Cancel",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {

							}
						});

				pickerDialog.show();
			}
		});
	}

	public Color getSelectedColor() {
		return selectedColor;
	}

	public void setSelectedColor(Color selectedColor) {
		this.selectedColor = selectedColor;
	}

	public OnColorChangeListener getOnColorChangeListener() {
		return onColorChangeListener;
	}

	public void setOnColorChangeListener(
			OnColorChangeListener onColorChangeListener) {
		this.onColorChangeListener = onColorChangeListener;
	}
	
	public interface OnColorChangeListener {
		public void onColorChange();
	}
 
}



