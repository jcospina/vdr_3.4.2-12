package com.canvas.vo;

import org.andengine.engine.Engine;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

import com.canvas.dataaccess.PostItDB;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.CanvasManager;
import com.smartsoft.Educanvas.PostIt;

public class GenericPostit extends PostIt{
	
	public final static String REFERENCE = "none";
	public GenericPostit(float pX, float pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, CanvasActivity canvas, String title,
			String description, Color color) {
		super(pX, pY, pTextureRegion, CanvasManager.vPropositionPostitRegionMax,pVertexBufferObjectManager, engine, canvas,
				title, description, color, GenericPostit.REFERENCE);		
	}
	
	public GenericPostit(float pX, float pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, CanvasActivity canvas, String title,
			String description, Color color, PostItDB relatedDatabaseObject) {
		super(pX, pY, pTextureRegion, CanvasManager.vPropositionPostitRegionMax,pVertexBufferObjectManager, engine, canvas,
				title, description, color, GenericPostit.REFERENCE, relatedDatabaseObject);		
	}

	public GenericPostit fromPostIt(Object p) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
