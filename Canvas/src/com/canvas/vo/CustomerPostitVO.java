package com.canvas.vo;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import org.andengine.engine.Engine;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.source.EmptyBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

import android.graphics.Bitmap;
import android.net.Uri;

import com.canvas.dataaccess.EmpathyMapPostitDB;
import com.canvas.dataaccess.PostItDB;
import com.canvas.empathymap.EmpathyMapPostit;
import com.canvas.empathymap.EmpathyMapScene;
import com.canvas.empathymap.ImageTextureBuilder;
import com.smartsoft.Educanvas.Block;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.CanvasManager;
import com.smartsoft.Educanvas.MainScene;
import com.smartsoft.Educanvas.PostIt;
import com.smartsoft.Educanvas.SceneManager;

public class CustomerPostitVO extends PostIt {

	public final static String REFERENCE = "customer_segments";
	private Uri photoUri;
	private Bitmap photo;
	private ITextureRegion texturePhoto;
	private ArrayList<EmpathyMapPostit> customerPostIt = new ArrayList<EmpathyMapPostit>();
	private EmpathyMapPostitDB postitRelated;
	private int count;

	public CustomerPostitVO(float pX, float pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, final CanvasActivity canvas, String title,
			String description, Color color) {
		super(pX, pY, pTextureRegion, CanvasManager.customersRegionMax,
				pVertexBufferObjectManager, engine, canvas, title, description,
				color, CustomerPostitVO.REFERENCE);
		setOnEditAplicationInvokeListener(new OnEditAplicationInvokeListener() {

			@Override
			public void onEditAplicationInvoke(Object postit) {
				SceneManager.getInstance().setMainSceneAsCurrent(false);
				MainScene.getInstance().onUnloadManagedScene();
				SceneManager.getInstance().showScene(
						new EmpathyMapScene(canvas, CustomerPostitVO.this));
			}
		});
	}

	public CustomerPostitVO(float pX, float pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, final CanvasActivity canvas, String title,
			String description, Color color, PostItDB p) {
		super(pX, pY, pTextureRegion, CanvasManager.customersRegionMax,
				pVertexBufferObjectManager, engine, canvas, title, description,
				color, CustomerPostitVO.REFERENCE, p);
		setOnEditAplicationInvokeListener(new OnEditAplicationInvokeListener() {

			@Override
			public void onEditAplicationInvoke(Object postit) {
				SceneManager.getInstance().setMainSceneAsCurrent(false);
				MainScene.getInstance().onUnloadManagedScene();
				SceneManager.getInstance().showScene(
						new EmpathyMapScene(canvas, CustomerPostitVO.this));
			}
		});
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public static CustomerPostitVO fromPostIt(Object p,
			Block<CustomerPostitVO> b) {
		if (p instanceof CustomerPostitVO) {
			return (CustomerPostitVO) p;
		} else if (p instanceof PostIt) {
			PostIt c = (PostIt) p;
			CustomerPostitVO cr;
			if (c.getRelatedDatabaseObject() == null) {
				cr = new CustomerPostitVO(c.getX(), c.getY(),
						CanvasManager.customersRegionNormal,
						c.getVertexBufferObjectManager(), c.engine, c.canvas,
						c.getTitle(), c.getDescription(), c.getColor());
			} else {
				cr = new CustomerPostitVO(c.getX(), c.getY(),
						CanvasManager.customersRegionNormal,
						c.getVertexBufferObjectManager(), c.engine, c.canvas,
						c.getTitle(), c.getDescription(), c.getColor(),
						c.getRelatedDatabaseObject());
			}
			cr.setParentBlock(b);
			return cr;
		}
		return null;
	}

	public void setPhotoUri(Uri photoUri) {
		this.photoUri = photoUri;
	}

	public Uri getPhotoUri() {
		return photoUri;
	}

	public Bitmap getPhoto() {
		return photo;
	}

	public void setPhoto(Bitmap photo) {
		this.photo = photo;
		this.texturePhoto = getUserBitmapRegion(null);
	}

	public ArrayList<EmpathyMapPostit> getCustomerPostIt() {
		return customerPostIt;
	}

	public void setCustomerPostIt(ArrayList<EmpathyMapPostit> customerPostIt) {
		this.customerPostIt = customerPostIt;
	}

	public void removePostit(EmpathyMapPostit postit) {
		this.customerPostIt.remove(postit);
	}

	public ITextureRegion getUserBitmapRegion(ITextureRegion defaultTexture) {
		ITextureRegion face;
		if (getPhoto() == null) {
			face = defaultTexture;
		} else {
			IBitmapTextureAtlasSource base = new EmptyBitmapTextureAtlasSource(
					250, 250);
			ImageTextureBuilder photo = new ImageTextureBuilder(base,
					getPhoto());
			BitmapTextureAtlas texture = new BitmapTextureAtlas(
					engine.getTextureManager(), 256, 256,
					TextureOptions.BILINEAR);
			texture.load();
			face = BitmapTextureAtlasTextureRegionFactory.createFromSource(
					texture,
					photo, 0, 0);
		}
		return face;
	}

	public ITextureRegion getTexturePhoto() {
		return texturePhoto;
	}

	public void setTexturePhoto(ITextureRegion texturePhoto) {
		this.texturePhoto = texturePhoto;
	}

	public EmpathyMapPostitDB getPostitRelated() {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		if (getPhoto() != null) {
			getPhoto().compress(Bitmap.CompressFormat.PNG, 100, stream);
			byte[] byteArray = stream.toByteArray();
			postitRelated.setBitmap(byteArray);
		}
		return postitRelated;
	}

	public void setPostitRelated(EmpathyMapPostitDB postitRelated) {
		this.postitRelated = postitRelated;
	}

}
