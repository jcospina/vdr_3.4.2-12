package com.canvas.vo;

import org.andengine.engine.Engine;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

import android.widget.Toast;

import com.canvas.dataaccess.PostItDB;
import com.canvas.dataaccess.RevenuePostitDB;
import com.canvas.revenueestimator.RevenueEstimatorScene;
import com.canvas.revenueestimator.RevenueManager;
import com.smartsoft.Educanvas.Block;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.CanvasManager;
import com.smartsoft.Educanvas.MainScene;
import com.smartsoft.Educanvas.MaximizedPostIt;
import com.smartsoft.Educanvas.PostIt;
import com.smartsoft.Educanvas.SceneManager;

public class RevenuePostitVO extends PostIt {
	public final static String REFERENCE = "revenue_streams";
	private double revenue;
	private RevenuePostitDB postitRelated;

	public RevenuePostitVO(float pX, float pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, final CanvasActivity canvas, final String title,
			String description, Color color) {
		super(pX, pY, pTextureRegion, CanvasManager.revenueRegionMax,
				pVertexBufferObjectManager, engine, canvas, title, description,
				color, RevenuePostitVO.REFERENCE);
		setOnEditAplicationInvokeListener(new OnEditAplicationInvokeListener() {

			@Override
			public void onEditAplicationInvoke(Object postit) {
				SceneManager.getInstance().setMainSceneAsCurrent(false);
				MainScene.getInstance().onUnloadManagedScene();
				SceneManager.getInstance()
						.showScene(
								new RevenueEstimatorScene(canvas,
										RevenuePostitVO.this));
			}
		});
		setOnPostitChangeListener(new OnPostitChangeLister() {
			@Override
			public void onMinimize(PostIt postit) {
			}

			@Override
			public void onMaximize(MaximizedPostIt maximizedPostit) {
				if (isCanvasEmpty()) {
					canvas.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(
									canvas,
									"Para acceder al estimador de ingresos, a�ade primero elementos al lienzo",
									Toast.LENGTH_LONG).show();
						}
					});
					maximizedPostit.getAplicationInvokeBtn().detachSelf();
				}
			}
		});
	}

	public RevenuePostitVO(float pX, float pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, final CanvasActivity canvas, final String title,
			String description, Color color, PostItDB p) {
		super(pX, pY, pTextureRegion, CanvasManager.revenueRegionMax,
				pVertexBufferObjectManager, engine, canvas, title, description,
				color, RevenuePostitVO.REFERENCE, p);
		setOnEditAplicationInvokeListener(new OnEditAplicationInvokeListener() {

			@Override
			public void onEditAplicationInvoke(Object postit) {
				SceneManager.getInstance().setMainSceneAsCurrent(false);
				MainScene.getInstance().onUnloadManagedScene();
				SceneManager.getInstance()
						.showScene(
								new RevenueEstimatorScene(canvas,
										RevenuePostitVO.this));
			}
		});
		setOnPostitChangeListener(new OnPostitChangeLister() {
			@Override
			public void onMinimize(PostIt postit) {
			}

			@Override
			public void onMaximize(MaximizedPostIt maximizedPostit) {
				if (isCanvasEmpty()) {
					canvas.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(
									canvas,
									"Para acceder al estimador de ingresos, a�ade primero elementos al lienzo",
									Toast.LENGTH_LONG).show();
						}
					});
					maximizedPostit.getAplicationInvokeBtn().detachSelf();
				}
			}
		});
	}

	public double getRevenue() {
		return revenue;
	}

	public void setRevenue(double revenue) {
		this.revenue = revenue;
	}

	public static RevenuePostitVO fromPostIt(Object p, Block<RevenuePostitVO> b) {
		if (p instanceof RevenuePostitVO) {
			return (RevenuePostitVO) p;
		} else if (p instanceof PostIt) {
			PostIt c = (PostIt) p;
			RevenuePostitVO cr;
			if (c.getRelatedDatabaseObject() == null) {
				cr = new RevenuePostitVO(c.getX(), c.getY(),
						CanvasManager.revenueRegionNormal,
						c.getVertexBufferObjectManager(), c.engine, c.canvas,
						c.getTitle(), c.getDescription(), c.getColor());
			} else {
				cr = new RevenuePostitVO(c.getX(), c.getY(),
						CanvasManager.revenueRegionNormal,
						c.getVertexBufferObjectManager(), c.engine, c.canvas,
						c.getTitle(), c.getDescription(), c.getColor(),
						c.getRelatedDatabaseObject());
			}
			cr.setParentBlock(b);
			return cr;
		}
		return null;
	}

	public RevenuePostitDB getPostitRelated() {
		return postitRelated;
	}

	public void setPostitRelatedData() {
		postitRelated.setRevenue(RevenueManager.getInstance().totalRevenue);
		postitRelated
				.setOtherRevenue(RevenueManager.getInstance().actualOtherRevenue);
		postitRelated.setSalesRevenues(RevenueManager.getInstance()
				.getTotalSalesRevenue());
		postitRelated.setSubscriptionRevenue(RevenueManager.getInstance()
				.getTotalSubscriptionRevenue());
		postitRelated.setPayPerUseRevenue(RevenueManager.getInstance()
				.getTotalPayPerUseRevenue());
		postitRelated.setRentalRevenue(RevenueManager.getInstance()
				.getTotalRentalRevenue());
		postitRelated.setTransFeeRevenue(RevenueManager.getInstance()
				.getTotalTransactionFeeRevenue());
		postitRelated.setSalesFeeRevenue(RevenueManager.getInstance()
				.getTotalSalesFeeRevenue());
		postitRelated.setDisplayAdsRevenue(RevenueManager.getInstance()
				.getTotalDisplayAdsRevenue());
		postitRelated.setClickAdsRevenue(RevenueManager.getInstance()
				.getTotalClickAdsRevenue());
		postitRelated.setLicensingRevenue(RevenueManager.getInstance()
				.getTotalLicensingRevenue());
	}

	public void setPostitRelated(RevenuePostitDB postitRelated) {
		this.postitRelated = postitRelated;
	}

	private boolean isCanvasEmpty() {
		if (MainScene.getInstance().customerSegments.postits.size() == 0
				&& MainScene.getInstance().keyActivities.postits.size() == 0
				&& MainScene.getInstance().keyPartnerships.postits.size() == 0
				&& MainScene.getInstance().keyResources.postits.size() == 0
				&& MainScene.getInstance().valuePropositions.postits.size() == 0
				&& MainScene.getInstance().relationships.postits.size() == 0
				&& MainScene.getInstance().channels.postits.size() == 0) {
			return true;
		}
		return false;
	}
}
