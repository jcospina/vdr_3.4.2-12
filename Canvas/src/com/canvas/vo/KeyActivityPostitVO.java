package com.canvas.vo;

import org.andengine.engine.Engine;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

import com.canvas.dataaccess.PostItDB;
import com.smartsoft.Educanvas.Block;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.CanvasManager;
import com.smartsoft.Educanvas.MaximizedPostIt;
import com.smartsoft.Educanvas.PostIt;

public class KeyActivityPostitVO extends PostIt {

	public final static String REFERENCE = "key_activities";

	public KeyActivityPostitVO(float pX, float pY,
			ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, CanvasActivity canvas, String title,
			String description, Color color) {
		super(pX, pY, pTextureRegion,
				CanvasManager.keyActivityRegionMax,
				pVertexBufferObjectManager, engine, canvas, title, description,
				color, KeyActivityPostitVO.REFERENCE);
		setOnEditAplicationInvokeListener(new OnEditAplicationInvokeListener() {
			@Override
			public void onEditAplicationInvoke(Object postit) {
			}
		});

		setOnPostitChangeListener(new OnPostitChangeLister() {
			@Override
			public void onMinimize(PostIt postit) {
			}

			@Override
			public void onMaximize(MaximizedPostIt maximizedPostit) {
				maximizedPostit.getAplicationInvokeBtn().detachSelf();
			}
		});
	}

	public KeyActivityPostitVO(float pX, float pY,
			ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, CanvasActivity canvas, String title,
			String description, Color color, PostItDB p) {
		super(pX, pY, pTextureRegion,
				CanvasManager.keyActivityRegionMax,
				pVertexBufferObjectManager, engine, canvas, title, description,
				color, KeyActivityPostitVO.REFERENCE, p);
		setOnEditAplicationInvokeListener(new OnEditAplicationInvokeListener() {
			@Override
			public void onEditAplicationInvoke(Object postit) {
			}
		});

		setOnPostitChangeListener(new OnPostitChangeLister() {
			@Override
			public void onMinimize(PostIt postit) {
			}

			@Override
			public void onMaximize(MaximizedPostIt maximizedPostit) {
				maximizedPostit.getAplicationInvokeBtn().detachSelf();
			}
		});

	}

	public static KeyActivityPostitVO fromPostIt(Object p,
			Block<KeyActivityPostitVO> b) {
		if (p instanceof KeyActivityPostitVO) {
			return (KeyActivityPostitVO) p;
		} else if (p instanceof PostIt) {
			PostIt c = (PostIt) p;
			KeyActivityPostitVO cr;
			if (c.getRelatedDatabaseObject() == null) {
				cr = new KeyActivityPostitVO(c.getX(), c.getY(),
						CanvasManager.keyActivityRegionNormal, c.getVertexBufferObjectManager(),
						c.engine, c.canvas, c.getTitle(), c.getDescription(),
						c.getColor());
			} else {
				cr = new KeyActivityPostitVO(c.getX(), c.getY(),
						CanvasManager.keyActivityRegionNormal, c.getVertexBufferObjectManager(),
						c.engine, c.canvas, c.getTitle(), c.getDescription(),
						c.getColor(), c.getRelatedDatabaseObject());
			}
			cr.setParentBlock(b);
			return cr;
		}
		return null;
	}

}
