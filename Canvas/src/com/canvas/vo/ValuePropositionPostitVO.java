package com.canvas.vo;

import java.util.List;

import org.andengine.engine.Engine;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

import com.canvas.dataaccess.PostItDB;
import com.canvas.valuepropositions.Pin;
import com.canvas.valuepropositions.ValuePropositionsScene;
import com.smartsoft.Educanvas.Block;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.CanvasManager;
import com.smartsoft.Educanvas.MainScene;
import com.smartsoft.Educanvas.PostIt;
import com.smartsoft.Educanvas.SceneManager;

public class ValuePropositionPostitVO extends PostIt {

	public final static String REFERENCE = "value_propositions";
	private List<Pin> pines;

	public ValuePropositionPostitVO(float pX, float pY,
			ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, final CanvasActivity canvas, String title,
			String description, Color color) {
		super(pX, pY, pTextureRegion,
				CanvasManager.vPropositionPostitRegionMax,
				pVertexBufferObjectManager, engine, canvas, title, description,
				color, ValuePropositionPostitVO.REFERENCE);

		setOnEditAplicationInvokeListener(new OnEditAplicationInvokeListener() {

			@Override
			public void onEditAplicationInvoke(Object postit) {
				SceneManager.getInstance().setMainSceneAsCurrent(false);
				MainScene.getInstance().onUnloadManagedScene();
				SceneManager.getInstance().showScene(
						new ValuePropositionsScene(
								ValuePropositionPostitVO.this, canvas));
			}
		});

	}

	public ValuePropositionPostitVO(float pX, float pY,
			ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, final CanvasActivity canvas, String title,
			String description, Color color, PostItDB p) {
		super(pX, pY, pTextureRegion,
				CanvasManager.vPropositionPostitRegionMax,
				pVertexBufferObjectManager, engine, canvas, title, description,
				color, ValuePropositionPostitVO.REFERENCE, p);

		setOnEditAplicationInvokeListener(new OnEditAplicationInvokeListener() {

			@Override
			public void onEditAplicationInvoke(Object postit) {
				SceneManager.getInstance().setMainSceneAsCurrent(false);
				MainScene.getInstance().onUnloadManagedScene();
				SceneManager.getInstance().showScene(
						new ValuePropositionsScene(
								ValuePropositionPostitVO.this, canvas));
			}
		});

	}

	public static ValuePropositionPostitVO fromPostIt(Object p,
			Block<ValuePropositionPostitVO> b) {
		if (p instanceof ValuePropositionPostitVO) {
			return (ValuePropositionPostitVO) p;
		} else if (p instanceof PostIt) {
			PostIt c = (PostIt) p;
			ValuePropositionPostitVO cr;
			if (c.getRelatedDatabaseObject() == null) {
				cr = new ValuePropositionPostitVO(c.getX(), c.getY(),
						CanvasManager.vPropositionPostitRegionNormal,
						c.getVertexBufferObjectManager(), c.engine, c.canvas,
						c.getTitle(), c.getDescription(), c.getColor());
			} else{
				cr = new ValuePropositionPostitVO(c.getX(), c.getY(),
						CanvasManager.vPropositionPostitRegionNormal,
						c.getVertexBufferObjectManager(), c.engine, c.canvas,
						c.getTitle(), c.getDescription(), c.getColor(), c.getRelatedDatabaseObject());
			}
			cr.setParentBlock(b);
			return cr;
		}
		return null;
	}

	public List<Pin> getPines() {
		return pines;
	}

	public void setPines(List<Pin> pines) {
		this.pines = pines;
	}

}
