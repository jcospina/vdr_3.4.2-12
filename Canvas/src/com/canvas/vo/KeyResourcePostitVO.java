package com.canvas.vo;

import org.andengine.engine.Engine;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

import com.canvas.dataaccess.PostItDB;
import com.smartsoft.Educanvas.Block;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.CanvasManager;
import com.smartsoft.Educanvas.MaximizedPostIt;
import com.smartsoft.Educanvas.PostIt;

public class KeyResourcePostitVO extends PostIt {

	public final static String REFERENCE = "key_resources";

	public KeyResourcePostitVO(float pX, float pY,
			ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, CanvasActivity canvas, String title,
			String description, Color color) {
		super(pX, pY, pTextureRegion,
				CanvasManager.keyResourceRegionMax,
				pVertexBufferObjectManager, engine, canvas, title, description,
				color, KeyResourcePostitVO.REFERENCE);
		setOnEditAplicationInvokeListener(new OnEditAplicationInvokeListener() {
			@Override
			public void onEditAplicationInvoke(Object postit) {
			}
		});

		setOnPostitChangeListener(new OnPostitChangeLister() {
			@Override
			public void onMinimize(PostIt postit) {
			}

			@Override
			public void onMaximize(MaximizedPostIt maximizedPostit) {
				maximizedPostit.getAplicationInvokeBtn().detachSelf();
			}
		});
	}

	public KeyResourcePostitVO(float pX, float pY,
			ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, CanvasActivity canvas, String title,
			String description, Color color, PostItDB p) {
		super(pX, pY, pTextureRegion,
				CanvasManager.keyResourceRegionMax,
				pVertexBufferObjectManager, engine, canvas, title, description,
				color, KeyResourcePostitVO.REFERENCE, p);
		setOnEditAplicationInvokeListener(new OnEditAplicationInvokeListener() {
			@Override
			public void onEditAplicationInvoke(Object postit) {
			}
		});

		setOnPostitChangeListener(new OnPostitChangeLister() {
			@Override
			public void onMinimize(PostIt postit) {
			}

			@Override
			public void onMaximize(MaximizedPostIt maximizedPostit) {
				maximizedPostit.getAplicationInvokeBtn().detachSelf();
			}
		});
	}

	public static KeyResourcePostitVO fromPostIt(Object p,
			Block<KeyResourcePostitVO> b) {
		if (p instanceof KeyResourcePostitVO) {
			return (KeyResourcePostitVO) p;
		} else if (p instanceof PostIt) {
			PostIt c = (PostIt) p;
			KeyResourcePostitVO cr;
			if (c.getRelatedDatabaseObject() == null) {
				cr = new KeyResourcePostitVO(c.getX(), c.getY(),
						CanvasManager.keyResourceRegionNormal, c.getVertexBufferObjectManager(),
						c.engine, c.canvas, c.getTitle(), c.getDescription(),
						c.getColor());
			} else {
				cr = new KeyResourcePostitVO(c.getX(), c.getY(),
						CanvasManager.keyResourceRegionNormal, c.getVertexBufferObjectManager(),
						c.engine, c.canvas, c.getTitle(), c.getDescription(),
						c.getColor(), c.getRelatedDatabaseObject());
			}
			cr.setParentBlock(b);
			return cr;
		}
		return null;
	}

}
