package com.canvas.vo;

import org.andengine.engine.Engine;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

import android.widget.Toast;

import com.canvas.costestimator.CostEstimatorScene;
import com.canvas.costestimator.CostManager;
import com.canvas.dataaccess.CostPostitDB;
import com.canvas.dataaccess.PostItDB;
import com.smartsoft.Educanvas.Block;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.CanvasManager;
import com.smartsoft.Educanvas.MainScene;
import com.smartsoft.Educanvas.MaximizedPostIt;
import com.smartsoft.Educanvas.PostIt;
import com.smartsoft.Educanvas.SceneManager;

public class CostPostitVO extends PostIt {

	public final static String REFERENCE = "costs_structure";

	private double cost;
	private CostPostitDB postitRelated;

	public CostPostitVO(float pX, float pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, final CanvasActivity canvas, final String title,
			String description, Color color) {
		super(pX, pY, pTextureRegion, CanvasManager.costsRegionMax,
				pVertexBufferObjectManager, engine, canvas, title, description,
				color, CostPostitVO.REFERENCE);
		setOnEditAplicationInvokeListener(new OnEditAplicationInvokeListener() {

			@Override
			public void onEditAplicationInvoke(Object postit) {
				SceneManager.getInstance().setMainSceneAsCurrent(false);
				MainScene.getInstance().onUnloadManagedScene();
				SceneManager.getInstance().showScene(
						new CostEstimatorScene(canvas, CostPostitVO.this));
			}
		});

		setOnPostitChangeListener(new OnPostitChangeLister() {
			@Override
			public void onMinimize(PostIt postit) {
			}

			@Override
			public void onMaximize(MaximizedPostIt maximizedPostit) {
				if (isCanvasEmpty()) {
					canvas.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(
									canvas,
									"Para acceder a la estructura de costos, a�ade primero elementos al lienzo",
									Toast.LENGTH_LONG).show();
						}
					});
					maximizedPostit.getAplicationInvokeBtn().detachSelf();
				}
			}
		});
	}

	public CostPostitVO(float pX, float pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, final CanvasActivity canvas, final String title,
			String description, Color color, PostItDB p) {
		super(pX, pY, pTextureRegion, CanvasManager.costsRegionMax,
				pVertexBufferObjectManager, engine, canvas, title, description,
				color, CostPostitVO.REFERENCE, p);
		setOnEditAplicationInvokeListener(new OnEditAplicationInvokeListener() {

			@Override
			public void onEditAplicationInvoke(Object postit) {
				SceneManager.getInstance().setMainSceneAsCurrent(false);
				MainScene.getInstance().onUnloadManagedScene();
				SceneManager.getInstance().showScene(
						new CostEstimatorScene(canvas, CostPostitVO.this));
			}
		});

		setOnPostitChangeListener(new OnPostitChangeLister() {
			@Override
			public void onMinimize(PostIt postit) {
			}

			@Override
			public void onMaximize(MaximizedPostIt maximizedPostit) {
				if (isCanvasEmpty()) {
					canvas.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(
									canvas,
									"Para acceder a la estructura de costos, a�ade primero elementos al lienzo",
									Toast.LENGTH_LONG).show();
						}
					});
					maximizedPostit.getAplicationInvokeBtn().detachSelf();
				}
			}
		});
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public static CostPostitVO fromPostIt(Object p, Block<CostPostitVO> b) {
		if (p instanceof CostPostitVO) {
			return (CostPostitVO) p;
		} else if (p instanceof PostIt) {
			PostIt c = (PostIt) p;
			CostPostitVO cr;
			if (c.getRelatedDatabaseObject() == null) {
				cr = new CostPostitVO(c.getX(), c.getY(),
						CanvasManager.costsRegionNormal,
						c.getVertexBufferObjectManager(), c.engine, c.canvas,
						c.getTitle(), c.getDescription(), c.getColor());
			} else {
				cr = new CostPostitVO(c.getX(), c.getY(),
						CanvasManager.costsRegionNormal,
						c.getVertexBufferObjectManager(), c.engine, c.canvas,
						c.getTitle(), c.getDescription(), c.getColor(),
						c.getRelatedDatabaseObject());
			}
			cr.setParentBlock(b);
			return cr;
		}
		return null;
	}

	public CostPostitDB getPostitRelated() {
		postitRelated.setCost(CostManager.getInstace().getTotalCost());
		postitRelated.setOtherCost(CostManager.getInstace().actualOtherCost);
		postitRelated
				.setVariableCost(CostManager.getInstace().actualVariableCost);
		return postitRelated;
	}

	public void setPostitRelated(CostPostitDB postitRelated) {

		this.postitRelated = postitRelated;
	}

	private boolean isCanvasEmpty() {
		if (MainScene.getInstance().customerSegments.postits.size() == 0
				&& MainScene.getInstance().keyActivities.postits.size() == 0
				&& MainScene.getInstance().keyPartnerships.postits.size() == 0
				&& MainScene.getInstance().keyResources.postits.size() == 0
				&& MainScene.getInstance().valuePropositions.postits.size() == 0
				&& MainScene.getInstance().relationships.postits.size() == 0
				&& MainScene.getInstance().channels.postits.size() == 0) {
			return true;
		}
		return false;
	}

}
