package com.canvas.vo;

import org.andengine.engine.Engine;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

import com.canvas.dataaccess.PostItDB;
import com.canvas.partnerships.KeyPartnershipPostitRelated;
import com.canvas.partnerships.KeyPartnershipsScene;
import com.smartsoft.Educanvas.Block;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.CanvasManager;
import com.smartsoft.Educanvas.MainScene;
import com.smartsoft.Educanvas.MaximizedPostIt;
import com.smartsoft.Educanvas.PostIt;
import com.smartsoft.Educanvas.SceneManager;

public class KeyPartnershipPostitVO extends PostIt {

	public final static String REFERENCE = "key_partnerships";
	private KeyPartnershipPostitRelated postitRelated;

	public KeyPartnershipPostitVO(float pX, float pY,
			ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, final CanvasActivity canvas, String title,
			String description, Color color) {
		super(pX, pY, pTextureRegion,
				CanvasManager.keyPartnerRegionMax,
				pVertexBufferObjectManager, engine, canvas, title, description,
				color, KeyPartnershipPostitVO.REFERENCE);
		setOnEditAplicationInvokeListener(new OnEditAplicationInvokeListener() {

			@Override
			public void onEditAplicationInvoke(Object postit) {
				SceneManager.getInstance().setMainSceneAsCurrent(false);
				MainScene.getInstance().onUnloadManagedScene();
				SceneManager.getInstance()
						.showScene(new KeyPartnershipsScene(canvas));
			}
		});

		setOnPostitChangeListener(new OnPostitChangeLister() {

			@Override
			public void onMinimize(PostIt postit) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onMaximize(MaximizedPostIt maximizedPostit) {
				maximizedPostit.getColorPicker().detachSelf();
			}
		});
		setPostitRelated(null);
	}

	public KeyPartnershipPostitVO(float pX, float pY,
			ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, final CanvasActivity canvas, String title,
			String description, Color color, PostItDB p) {
		super(pX, pY, pTextureRegion,
				CanvasManager.keyPartnerRegionMax,
				pVertexBufferObjectManager, engine, canvas, title, description,
				color, KeyPartnershipPostitVO.REFERENCE, p);
		setOnEditAplicationInvokeListener(new OnEditAplicationInvokeListener() {

			@Override
			public void onEditAplicationInvoke(Object postit) {
				SceneManager.getInstance().setMainSceneAsCurrent(false);
				MainScene.getInstance().onUnloadManagedScene();
				SceneManager.getInstance()
						.showScene(new KeyPartnershipsScene(canvas));
			}
		});

		setOnPostitChangeListener(new OnPostitChangeLister() {

			@Override
			public void onMinimize(PostIt postit) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onMaximize(MaximizedPostIt maximizedPostit) {
				maximizedPostit.getColorPicker().detachSelf();
			}
		});
		setPostitRelated(null);
	}

	public static KeyPartnershipPostitVO fromPostIt(Object p,
			Block<KeyPartnershipPostitVO> b) {
		if (p instanceof KeyPartnershipPostitVO) {
			return (KeyPartnershipPostitVO) p;
		} else if (p instanceof PostIt) {
			PostIt c = (PostIt) p;
			KeyPartnershipPostitVO cr;
			if (c.getRelatedDatabaseObject() == null) {
				cr = new KeyPartnershipPostitVO(c.getX(), c.getY(),
						CanvasManager.keyPartnerRegionNormal, c.getVertexBufferObjectManager(),
						c.engine, c.canvas, c.getTitle(), c.getDescription(),
						c.getColor());
			} else {
				cr = new KeyPartnershipPostitVO(c.getX(), c.getY(),
						CanvasManager.keyPartnerRegionNormal, c.getVertexBufferObjectManager(),
						c.engine, c.canvas, c.getTitle(), c.getDescription(),
						c.getColor(), c.getRelatedDatabaseObject());
			}
			cr.setParentBlock(b);
			return cr;
		}
		return null;
	}

	public KeyPartnershipPostitRelated getPostitRelated() {
		return postitRelated;
	}

	public void setPostitRelated(KeyPartnershipPostitRelated postitRelated) {
		this.postitRelated = postitRelated;
	}

}
