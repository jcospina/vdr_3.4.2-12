package com.canvas.vo;

import java.util.ArrayList;

import org.andengine.engine.Engine;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

import com.canvas.channels.Channel;
import com.canvas.channels.ChannelValueProposition;
import com.canvas.channels.ChannelsScene;
import com.canvas.dataaccess.ChannelPostitDB;
import com.canvas.dataaccess.PostItDB;
import com.smartsoft.Educanvas.Block;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.CanvasManager;
import com.smartsoft.Educanvas.MainScene;
import com.smartsoft.Educanvas.PostIt;
import com.smartsoft.Educanvas.SceneManager;

public class ChannelPostitVO extends PostIt {

	public final static String REFERENCE = "channels";
	public ArrayList<ChannelValueProposition> valuePropositions = new ArrayList<ChannelValueProposition>(
			10);
	public Channel channel;
	public ChannelPostitDB postitRelated;

	public ChannelPostitVO(float pX, float pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, final CanvasActivity canvas, String title,
			String description, Color color) {
		super(pX, pY, pTextureRegion, CanvasManager.channelPostitRegionMax,
				pVertexBufferObjectManager, engine, canvas, title, description,
				color, ChannelPostitVO.REFERENCE);
		setOnEditAplicationInvokeListener(new OnEditAplicationInvokeListener() {

			@Override
			public void onEditAplicationInvoke(Object postit) {
				SceneManager.getInstance().setMainSceneAsCurrent(false);
				MainScene.getInstance().onUnloadManagedScene();
				SceneManager.getInstance().showScene(
						new ChannelsScene(canvas, ChannelPostitVO.this));
			}
		});

	}

	public ChannelPostitVO(float pX, float pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, final CanvasActivity canvas, String title,
			String description, Color color, PostItDB p) {
		super(pX, pY, pTextureRegion, CanvasManager.channelPostitRegionMax,
				pVertexBufferObjectManager, engine, canvas, title, description,
				color, ChannelPostitVO.REFERENCE, p);
		setOnEditAplicationInvokeListener(new OnEditAplicationInvokeListener() {

			@Override
			public void onEditAplicationInvoke(Object postit) {
				SceneManager.getInstance().setMainSceneAsCurrent(false);
				MainScene.getInstance().onUnloadManagedScene();
				SceneManager.getInstance().showScene(
						new ChannelsScene(canvas, ChannelPostitVO.this));
			}
		});

	}

	public static ChannelPostitVO fromPostIt(Object p, Block<ChannelPostitVO> b) {
		if (p instanceof ChannelPostitVO) {
			return (ChannelPostitVO) p;
		} else if (p instanceof PostIt) {
			PostIt c = (PostIt) p;
			ChannelPostitVO cr;
			if (c.getRelatedDatabaseObject() == null) {
				cr = new ChannelPostitVO(c.getX(), c.getY(),
						CanvasManager.channelPostitRegionNormal,
						c.getVertexBufferObjectManager(), c.engine, c.canvas,
						c.getTitle(), c.getDescription(), c.getColor());
			} else{
				cr = new ChannelPostitVO(c.getX(), c.getY(),
						CanvasManager.channelPostitRegionNormal,
						c.getVertexBufferObjectManager(), c.engine, c.canvas,
						c.getTitle(), c.getDescription(), c.getColor(), c.getRelatedDatabaseObject());
			}
			cr.setParentBlock(b);
			return cr;
		}
		return null;
	}

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	public ArrayList<ChannelValueProposition> getValuePropositions() {
		return valuePropositions;
	}

	public void setValuePropositions(
			ArrayList<ChannelValueProposition> valuePropositions) {
		this.valuePropositions = valuePropositions;
	}
	
	public void setPostitRelated(ChannelPostitDB postitRelated) {
		this.postitRelated = postitRelated;
	}
	
	public ChannelPostitDB getPostitRelated() {		
		postitRelated.setChannel(channel.id);
		return postitRelated;
	}
}
