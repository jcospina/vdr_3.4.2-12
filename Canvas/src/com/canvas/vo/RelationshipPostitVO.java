package com.canvas.vo;

import java.util.HashMap;
import java.util.List;

import org.andengine.engine.Engine;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

import com.canvas.dataaccess.PostItDB;
import com.canvas.relationships.RelationshipsScene;
import com.smartsoft.Educanvas.Block;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.CanvasManager;
import com.smartsoft.Educanvas.MainScene;
import com.smartsoft.Educanvas.PostIt;
import com.smartsoft.Educanvas.SceneManager;

public class RelationshipPostitVO extends PostIt {

	public final static String REFERENCE = "relationships";
	private HashMap<ValuePropositionPostitVO, List<CustomerPostitVO>> valuePropositionsMap;

	public RelationshipPostitVO(float pX, float pY,
			ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, final CanvasActivity canvas, String title,
			String description, Color color) {
		super(pX, pY, pTextureRegion,
				CanvasManager.relationshipsRegionMax,
				pVertexBufferObjectManager, engine, canvas, title, description,
				color, RelationshipPostitVO.REFERENCE);

		// valuePropositionsMap = new HashMap<ValuePropositionPostitVO,
		// List<CustomerPostitVO>>();

		setOnEditAplicationInvokeListener(new OnEditAplicationInvokeListener() {

			@Override
			public void onEditAplicationInvoke(Object postit) {
				SceneManager.getInstance().setMainSceneAsCurrent(false);
				MainScene.getInstance().onUnloadManagedScene();
				SceneManager.getInstance().showScene(
						new RelationshipsScene(RelationshipPostitVO.this, canvas));

			}
		});
	}

	public RelationshipPostitVO(float pX, float pY,
			ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, final CanvasActivity canvas, String title,
			String description, Color color, PostItDB p) {
		super(pX, pY, pTextureRegion,
				CanvasManager.relationshipsRegionMax,
				pVertexBufferObjectManager, engine, canvas, title, description,
				color, RelationshipPostitVO.REFERENCE, p);

		// valuePropositionsMap = new HashMap<ValuePropositionPostitVO,
		// List<CustomerPostitVO>>();

		setOnEditAplicationInvokeListener(new OnEditAplicationInvokeListener() {

			@Override
			public void onEditAplicationInvoke(Object postit) {
				SceneManager.getInstance().setMainSceneAsCurrent(false);
				MainScene.getInstance().onUnloadManagedScene();
				SceneManager.getInstance().showScene(
						new RelationshipsScene(RelationshipPostitVO.this, canvas));

			}
		});
	}

	public static RelationshipPostitVO fromPostIt(Object p,
			Block<RelationshipPostitVO> b) {
		if (p instanceof RelationshipPostitVO) {
			return (RelationshipPostitVO) p;
		} else if (p instanceof PostIt) {
			PostIt c = (PostIt) p;
			RelationshipPostitVO cr;
			if (c.getRelatedDatabaseObject() == null) {
				cr = new RelationshipPostitVO(c.getX(), c.getY(),
						CanvasManager.relationshipsRegionNormal, c.getVertexBufferObjectManager(),
						c.engine, c.canvas, c.getTitle(), c.getDescription(),
						c.getColor());
			} else {
				cr = new RelationshipPostitVO(c.getX(), c.getY(),
						CanvasManager.relationshipsRegionNormal, c.getVertexBufferObjectManager(),
						c.engine, c.canvas, c.getTitle(), c.getDescription(),
						c.getColor(), c.getRelatedDatabaseObject());
			}
			cr.setParentBlock(b);
			return cr;
		}
		return null;
	}

	public HashMap<ValuePropositionPostitVO, List<CustomerPostitVO>> getValuePropositionsMap() {
		return valuePropositionsMap;
	}

	public void setValuePropositionsMap(
			HashMap<ValuePropositionPostitVO, List<CustomerPostitVO>> valuePropositionsMap) {
		this.valuePropositionsMap = valuePropositionsMap;
	}

}
