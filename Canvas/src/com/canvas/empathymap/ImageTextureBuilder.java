package com.canvas.empathymap;

import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.bitmap.source.decorator.BaseBitmapTextureAtlasSourceDecorator;

import android.graphics.Bitmap;
import android.graphics.Canvas;

public class ImageTextureBuilder extends BaseBitmapTextureAtlasSourceDecorator {
	
	private Bitmap bitmap;
	
	public ImageTextureBuilder(
			IBitmapTextureAtlasSource pBitmapTextureAtlasSource, Bitmap bitmap) {
		super(pBitmapTextureAtlasSource);
		this.bitmap = bitmap;
	}

	@Override
	public BaseBitmapTextureAtlasSourceDecorator deepCopy() {
		return null;
	}

	@Override
	protected void onDecorateBitmap(Canvas pCanvas) throws Exception {
		pCanvas.drawBitmap(bitmap, 0, 0, this.mPaint);
	}

}
