package com.canvas.empathymap;

import org.andengine.engine.Engine;
import org.andengine.engine.Engine.EngineLock;
import org.andengine.entity.text.AutoWrap;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.HorizontalAlign;
import org.andengine.util.color.Color;

import com.canvas.dataaccess.controller.EmpathyMapNoteController;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.CanvasManager;
import com.smartsoft.Educanvas.MainScene;
import com.smartsoft.Educanvas.PostIt;

public class EmpathyMapPostit extends PostIt {

	public final static String REFERENCE = "none";
	public EmpathyMapScene empathyMap;
	public boolean isNew = false;
	public int relatedId;

	public int getRelatedId() {
		return relatedId;
	}

	public void setRelatedId(int relatedId) {
		this.relatedId = relatedId;
	}

	public EmpathyMapPostit(int relatedId, float pX, float pY,
			ITextureRegion pTextureRegion, ITextureRegion maximizedRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, CanvasActivity canvas, String title,
			String description, Color color, EmpathyMapScene empathyMap) {
		super(pX, pY, pTextureRegion, CanvasManager.bigEMapPostit,
				pVertexBufferObjectManager, engine, canvas, title, description,
				color, EmpathyMapPostit.REFERENCE);
		this.empathyMap = empathyMap;
		this.relatedId = relatedId;
	}

	@SuppressWarnings("unused")
	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
			float pTouchAreaLocalX, float pTouchAreaLocalY) {
		switch (pSceneTouchEvent.getAction()) {
		case TouchEvent.ACTION_DOWN:
			break;

		case TouchEvent.ACTION_MOVE:
			float dX = this.getX() + (this.getWidth() / 2)
					- pSceneTouchEvent.getX();
			float dY = this.getY() + (this.getHeight() / 2)
					- pSceneTouchEvent.getY();

			if (!(dX < getWidth() / 5 && dX > -getWidth() / 5)
					|| !(dY < getHeight() / 5 && dY > -getHeight() / 5)) {
				isMoving = true;
				setPosX(pSceneTouchEvent.getX() - this.getWidth() / 2);
				setPosY(pSceneTouchEvent.getY() - this.getHeight() / 2);
				this.setPosition(getPosX(), getPosY());
			}
			break;

		case TouchEvent.ACTION_UP:
			if (!maximizing) {
				if (isMoving) {
					if (this.collidesWith(empathyMap.deletePostit)) {
						if (this == null) {
							return false;
						}
						final EngineLock engineLock = engine.getEngineLock();
						engineLock.lock();
						/* Now it is save to remove the entity! */
						empathyMap.gameHud.detachChild(this);
						empathyMap.customer.removePostit(this);
						EmpathyMapNoteController controller = new EmpathyMapNoteController(
								CanvasManager.getInstance().context);
						controller.removeEntry(relatedId);
						if (!this.isDisposed()) {
							this.dispose();
						}
						engineLock.unlock();
					} else if (this.collidesWith(empathyMap.addPostit)) {
						this.setPosition(empathyMap.addPostit.getX()
								+ empathyMap.addPostit.getWidth() + 10,
								empathyMap.addPostit.getY());
						this.setPosX(empathyMap.addPostit.getX()
								+ empathyMap.addPostit.getWidth() + 10);
						this.setPosY(empathyMap.addPostit.getY());
					} else if (this.collidesWith(empathyMap.camera)) {
						this.setPosition(empathyMap.camera.getX()
								- empathyMap.camera.getWidth() - 20,
								empathyMap.camera.getY());
						this.setPosX(empathyMap.camera.getX()
								- empathyMap.camera.getWidth() - 20);
						this.setPosY(empathyMap.camera.getY());

					} else if (this.collidesWith(empathyMap.gallery)) {
						this.setPosition(empathyMap.gallery.getX()
								+ empathyMap.gallery.getWidth() + 10,
								empathyMap.gallery.getY());
						this.setPosX(empathyMap.gallery.getX()
								+ empathyMap.gallery.getWidth() + 10);
						this.setPosY(empathyMap.gallery.getY());
					} else {
						this.setPosition(getPosX(), getPosY());
					}
					isMoving = false;
				} else {
					maximizing = true;
					maximizePostit();
				}
			}
			break;

		default:
			break;
		}

		return true;
	}

	private boolean maximizing = false;

	@Override
	public void showExtraInformation() {
		final EmpathyMaximizedPostit mPostit = new EmpathyMaximizedPostit(
				(CanvasActivity.CAMERA_WIDTH - CanvasManager.bigEMapPostit
						.getWidth()) / 2,
				(CanvasActivity.CAMERA_HEIGHT - CanvasManager.bigEMapPostit
						.getHeight()) / 2, CanvasManager.bigEMapPostit, canvas,
				getTitle(), getDescription(), getColor(),
				EmpathyMapPostit.REFERENCE, empathyMap);

		mPostit.setOnActionListener(new OnActionListenerEmpathyMap() {

			@Override
			public void onTitleChange(String title) {
				setTitle(title);
			}

			@Override
			public void onEdit() {
				if (getOnEditAplicationInvokeListener() != null) {
					getOnEditAplicationInvokeListener().onEditAplicationInvoke(
							EmpathyMapPostit.this);
				}
			}

			@Override
			public void onDescriptionChange(String description) {
				setDescription(description);
			}

			@Override
			public void onClose() {
				mPostit.setVisible(false);
				MainScene.getInstance().registerSpriteTouchAreas();
				EmpathyMapPostit.this.setColor(mPostit.getPostitColor());
				EmpathyMapPostit.this.setTitle(mPostit.getTitle());
				EmpathyMapPostit.this.setDescription(mPostit.getDescription());
				EmpathyMapPostit.this.setVisible(true);
				EmpathyMapPostit.this.minimizePostit();				
			}
		});
		empathyMap.unregisterSpriteTouchAreas();
		empathyMap.gameHud.attachChild(mPostit);
		mPostit.draw();

		this.setVisible(false); // Ocultamos el postit actual, la idea es
		// mostrar en la escena un postit grande.

	}

	@Override
	protected void drawTitle() {
		maximizing = false;
		removeAllChildern();
		TextOptions tOptions = new TextOptions(AutoWrap.WORDS, 90,
				HorizontalAlign.CENTER, Text.LEADING_DEFAULT);
		Text text = new Text(5, 0,
				CanvasManager.getInstance().minimizedPostitFont,
				(getTitle() != null) ? getTitle() : "", tOptions,
				canvas.getVertexBufferObjectManager());

		text.setText((getTitle() != null) ? getTitle() : "");
		text.setPosition((getWidth() - text.getWidth()) / 2,
				(getHeight() / 2 - text.getHeight() / 2));
		this.attachChild(text);
	}

}
