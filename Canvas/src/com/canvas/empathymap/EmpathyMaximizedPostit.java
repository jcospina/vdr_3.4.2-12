package com.canvas.empathymap;

import org.andengine.entity.Entity;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.util.color.Color;

import android.graphics.Typeface;

import com.canvas.util.ColorPicker;
import com.canvas.util.InputText;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.CanvasManager;

public class EmpathyMaximizedPostit extends Entity {
	private ITextureRegion backgroundRegion;
	/**
	 * Posici�n X del postit maximizado
	 */
	private float posX;

	/**
	 * Posici�n Y del postit maximizado
	 */
	private float posY;

	/**
	 * Canvas sobre el cual se est� dibujando el postit, necesario para acceder
	 * a recursos de im�genes
	 */
	private CanvasActivity canvas;

	/**
	 * T�tulo del postit
	 */
	private String title;

	/**
	 * Descripci�n del postit
	 */
	private String description;

	/**
	 * Color del postit
	 */
	private Color postitColor;

	/**
	 * Texto de referencia para encontrar la informaci�n de ayuda dentro del
	 * postit
	 */
	protected String postitReference;

	protected OnActionListenerEmpathyMap onActionListener;
	/**
	 * Fuente utilizada para el t�tulo
	 */
	protected Font titleFont;
	protected Font descriptionFont;

	/**
	 * Representa la imagen en background utilizada para representar el postit
	 */
	private Sprite postitBackgroundSprite;

	/**
	 * Representa el selector de color
	 */
	private ColorPicker colorPicker;

	/**
	 * Representa el bot�n de cerrado (minizaci�n del postit)
	 */
	private Sprite closeBtn;

	/**
	 * Representa el mapa de empat�a al cual est� atado
	 */
	private EmpathyMapScene empathyMap;

	public EmpathyMaximizedPostit(float posX, float posY,
			ITextureRegion backgroundRegion, CanvasActivity canvas,
			String title, String description, Color postitColor,
			String postitReference, EmpathyMapScene empathyMap) {
		super();
		this.posX = posX;
		this.posY = posY;
		this.canvas = canvas;
		this.title = title;
		this.description = description;
		this.postitColor = postitColor;
		this.postitReference = postitReference;
		this.backgroundRegion = backgroundRegion;
		this.empathyMap = empathyMap;
		loadFonts();
		// help = new PostItHelp("", "", vbom);
	}

	/**
	 * Carga las fuentes utilizadas en el despliegue del postit
	 */
	private void loadFonts() {
		titleFont = FontFactory.create(canvas.getFontManager(),
				canvas.getTextureManager(), 256, 256,
				Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 32);
		titleFont.load();

		descriptionFont = FontFactory.create(canvas.getFontManager(),
				canvas.getTextureManager(), 256, 256,
				Typeface.create(Typeface.DEFAULT, Typeface.NORMAL), 20);
		descriptionFont.load();
	}

	/**
	 * Inicializa todas las herramientas del postit y las dibuja en la escena.
	 * Al cerrarse se eliminan todos estos elementos evitando que se queden
	 * consumiendo memoria.
	 */
	public void draw() {

		postitBackgroundSprite = new Sprite(posX, posY, backgroundRegion,
				canvas.getVertexBufferObjectManager());

		final InputText titleInputText = new InputText(0, 110, "Titulo",
				"Presione aqui para agregar un t�tulo",
				"Ingresa un titulo para el PostIt", this.getTitle(),
				CanvasManager.getInstance().mTiledTextureRegion, titleFont, 10,
				0, 580, canvas.getVertexBufferObjectManager(), canvas);

		titleInputText
				.setOnTextChangeListener(new InputText.OnTextChangeListener() {

					@Override
					public void onTextChange(String text) {
						if (onActionListener != null) {
							onActionListener.onTitleChange(titleInputText
									.getText());
						}
					}
				});

		final InputText descriptionInputText = new InputText(0, 200,
				"Descripci�n", "Presione aqui para agregar una descripci�n",
				"Ingresa una descripci�n para el PostIt",
				this.getDescription(),
				CanvasManager.getInstance().mTiledTextureRegion,
				descriptionFont, 10, 0, 580,
				canvas.getVertexBufferObjectManager(), canvas);

		descriptionInputText
				.setOnTextChangeListener(new InputText.OnTextChangeListener() {

					@Override
					public void onTextChange(String text) {
						if (onActionListener != null) {
							onActionListener
									.onDescriptionChange(descriptionInputText
											.getText());
						}
					}
				});

		this.setVisible(false); // Ocultamos el postit actual, la idea es
								// mostrar en la escena un postit grande.
		this.setScale(1, 1); //

		// colorPicker = new ColorPicker(postitBackgroundSprite.getWidth()
		// - CanvasManager.getInstance().closeBtn.getWidth()
		// - CanvasManager.getInstance().colorBtnTextureRegion.getWidth() - 20,
		// 20,
		// CanvasManager.getInstance().colorBtnTextureRegion,
		// canvas.getVertexBufferObjectManager(), canvas,
		// this.getPostitColor(), null);

		colorPicker = new ColorPicker(
				CanvasManager.getInstance().colorBtnTextureRegion.getWidth() - 20,
				postitBackgroundSprite.getHeight() - CanvasManager.getInstance().colorBtnTextureRegion.getWidth()*(3.5f),
				CanvasManager.getInstance().colorBtnTextureRegion,
				canvas.getVertexBufferObjectManager(), canvas,
				this.getPostitColor(), null);

		colorPicker
				.setOnColorChangeListener(new ColorPicker.OnColorChangeListener() {

					@Override
					public void onColorChange() {
						// TODO Auto-generated method stub
						EmpathyMaximizedPostit.this.setPostitColor(colorPicker
								.getSelectedColor());
					}
				});

		closeBtn = new Sprite(postitBackgroundSprite.getWidth()
				- CanvasManager.getInstance().closeBtn.getWidth() - 10, 20,
				CanvasManager.getInstance().closeBtn,
				canvas.getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {
				// TODO Auto-generated method stub
				if (pSceneTouchEvent.isActionUp()) {
					setTitle(titleInputText.getText());
					setDescription(descriptionInputText.getText());
					canvas.getEngine().getEngineLock().lock();
					empathyMap.gameHud.detachChild(postitBackgroundSprite);
					postitBackgroundSprite.detachChildren();
					empathyMap.gameHud.unregisterTouchArea(this);
					empathyMap.gameHud.unregisterTouchArea(colorPicker);
					empathyMap.gameHud.unregisterTouchArea(titleInputText);
					empathyMap.gameHud
							.unregisterTouchArea(descriptionInputText);
					if (!colorPicker.isDisposed()) {
						colorPicker.dispose();
					}
					if (!titleInputText.isDisposed()) {
						titleInputText.dispose();
					}
					if (!descriptionInputText.isDisposed()) {
						descriptionInputText.dispose();
					}

					titleFont.unload();
					descriptionFont.unload();
					if (!this.isDisposed()) {
						this.dispose();
					}

					EmpathyMaximizedPostit.this.setVisible(true);

					canvas.getEngine().getEngineLock().unlock();

					if (onActionListener != null) {
						onActionListener.onClose();
					}
				}
				return true;
			}
		};

		postitBackgroundSprite.setColor(postitColor);
		postitBackgroundSprite.attachChild(closeBtn);
		postitBackgroundSprite.attachChild(colorPicker);
		postitBackgroundSprite.attachChild(titleInputText);
		postitBackgroundSprite.attachChild(descriptionInputText);
		this.attachChild(postitBackgroundSprite);
		empathyMap.gameHud.registerTouchArea(closeBtn);
		empathyMap.gameHud.registerTouchArea(colorPicker);
		empathyMap.gameHud.registerTouchArea(titleInputText);
		empathyMap.gameHud.registerTouchArea(descriptionInputText);
		postitBackgroundSprite.setZIndex(2);
		sortChildren();

		this.setVisible(true);

	}

	public float getPosX() {
		return posX;
	}

	public void setPosX(float posX) {
		this.posX = posX;
	}

	public float getPosY() {
		return posY;
	}

	public void setPosY(float posY) {
		this.posY = posY;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Color getPostitColor() {
		return postitColor;
	}

	public void setPostitColor(Color postitColor) {
		this.postitColor = postitColor;
		if (postitBackgroundSprite != null) {
			postitBackgroundSprite.setColor(this.postitColor);
		}
	}

	public ColorPicker getColorPicker() {
		return colorPicker;
	}

	public void setColorPicker(ColorPicker colorPicker) {
		this.colorPicker = colorPicker;
	}

	public Sprite getCloseBtn() {
		return closeBtn;
	}

	public void setCloseBtn(Sprite closeBtn) {
		this.closeBtn = closeBtn;
	}

	public OnActionListenerEmpathyMap getOnActionListener() {
		return onActionListener;
	}

	public void setOnActionListener(OnActionListenerEmpathyMap onActionListener) {
		this.onActionListener = onActionListener;
	}

	public ITextureRegion getBackgroundRegion() {
		return backgroundRegion;
	}

	public void setBackgroundRegion(ITextureRegion backgroundRegion) {
		this.backgroundRegion = backgroundRegion;
	}

};

/**
 * Utilizado para ejecutar un m�todo una vez el postit ha sido cerrado. De esta
 * forma se puede entender desde otra instancia que el postit est� listo para
 * ser minimizado.
 * 
 * @author vgarzon
 * 
 */
interface OnActionListenerEmpathyMap {
	public void onClose();

	public void onEdit();

	public void onTitleChange(String title);

	public void onDescriptionChange(String description);
}
