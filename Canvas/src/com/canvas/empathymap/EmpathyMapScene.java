package com.canvas.empathymap;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.andengine.engine.Engine.EngineLock;
import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.source.EmptyBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.util.color.Color;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import com.canvas.dataaccess.EmpathyMapNoteDB;
import com.canvas.dataaccess.EmpathyMapPostitDB;
import com.canvas.dataaccess.PostItDB;
import com.canvas.dataaccess.controller.EmpathyMapNoteController;
import com.canvas.dataaccess.controller.EmpathyMapPostitController;
import com.canvas.dataaccess.controller.PostitController;
import com.canvas.util.UtilColor;
import com.canvas.vo.CustomerPostitVO;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.CanvasManager;
import com.smartsoft.Educanvas.MainScene;
import com.smartsoft.Educanvas.ManagedScene;

public class EmpathyMapScene extends ManagedScene {

	public HUD gameHud = new HUD();
	private Sprite empathyFace;
	private Sprite frame;
	private Sprite background;
	public Sprite camera;
	public Sprite gallery;
	public Sprite addPostit;
	public Sprite deletePostit;
	private Uri fileUri;
	private CanvasActivity activity;
	private final float CROSS_SCREEN = CanvasActivity.CAMERA_HEIGHT
			- CanvasActivity.CAMERA_HEIGHT / 4;
	public CustomerPostitVO customer;
	private List<BitmapTextureAtlas> textures;

	public EmpathyMapScene(CanvasActivity activity, CustomerPostitVO customer) {
		this.setOnSceneTouchListenerBindingOnActionDownEnabled(true);
		this.setTouchAreaBindingOnActionDownEnabled(true);
		this.setTouchAreaBindingOnActionMoveEnabled(true);
		this.activity = activity;
		this.customer = customer;
		gameHud.setScaleCenter(0, 0);
		gameHud.setScale(1);
	}

	// -----------NO LOADING SCREEN------------------
	@Override
	public Scene onLoadingScreenLoadAndShown() {
		return null;
	}

	@Override
	public void onLoadingScreenUnloadAndHidden() {
	}

	// -----------------------------------------------

	@Override
	public void onLoadScene() {
		textures = CanvasManager.getInstance().loadEmpathyMapResources();
		boolean loadFromDatabase = true;
		EmpathyMapPostitDB emP = null;
		int index = -1;
		EmpathyMapPostitController emController = new EmpathyMapPostitController(
				CanvasManager.getInstance().context);
		if (customer.getRelatedDatabaseObject() != null) {
			emP = emController.getByPostitId(customer
					.getRelatedDatabaseObject().getId());
			index = customer.getRelatedDatabaseObject().getId();
		}
		if (emP == null) {
			emP = new EmpathyMapPostitDB(-1, index, getBitmapFromAsset(
					CanvasManager.getInstance().context,
					"gfx/empathymap/customer.png"));
			loadFromDatabase = false;
		}

		customer.setPostitRelated(emP);
		IBitmapTextureAtlasSource base = new EmptyBitmapTextureAtlasSource(256,
				256);
		Bitmap loadedPhoto;
		if (emP.getBitmap() == null) {
			byte[] data = getBitmapFromAsset(
					CanvasManager.getInstance().context,
					"gfx/empathymap/customer.png");
			loadedPhoto = BitmapFactory.decodeByteArray(data, 0, data.length);
		} else {
			loadedPhoto = BitmapFactory.decodeByteArray(emP.getBitmap(), 0,
					emP.getBitmap().length);
		}
		int index_of = MainScene.getInstance().customerSegments.postits
				.indexOf(customer);
		customer.setPhoto(loadedPhoto);
		MainScene.getInstance().customerSegments.postits.get(index_of)
				.setPhoto(loadedPhoto);
		ImageTextureBuilder photo = new ImageTextureBuilder(base, loadedPhoto);
		CanvasManager.empathyFaceRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromSource(
						CanvasManager.getInstance().empathyMapBitmapTextureAtlas,
						photo, 0, 0);
		Sprite customer_bg = new Sprite(
				(CanvasActivity.CAMERA_WIDTH / 2)
						- (CanvasManager.customerBGTextureRegion.getWidth() / 2)
						+ 21,
				(CROSS_SCREEN / 2)
						- (CanvasManager.customerBGTextureRegion.getHeight() / 2)
						+ 21, CanvasManager.customerBGTextureRegion,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());
		empathyFace = new Sprite((CanvasActivity.CAMERA_WIDTH / 2)
				- (CanvasManager.empathyFaceRegion.getWidth() / 2) + 21,
				(CROSS_SCREEN / 2)
						- (CanvasManager.empathyFaceRegion.getHeight() / 2)
						+ 21, CanvasManager.empathyFaceRegion,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());
		empathyFace.setCullingEnabled(true);
		// Crea los sprites para mostrar en el escenario

		frame = new Sprite(CanvasActivity.CAMERA_WIDTH / 2
				- CanvasManager.empathyFaceRegion.getWidth() / 2, CROSS_SCREEN
				/ 2 - CanvasManager.empathyFaceRegion.getHeight() / 2,
				CanvasManager.frameRegion,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());
		frame.setCullingEnabled(true);
		background = new Sprite(0, 0, CanvasManager.EMBackgroundRegion,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());
		background.setIgnoreUpdate(true);
		background.setCullingEnabled(true);
		camera = new Sprite(CanvasActivity.CAMERA_WIDTH / 2
				- (frame.getWidth() / 2), CROSS_SCREEN / 2
				- CanvasManager.takePictureRegion.getHeight()
				+ frame.getHeight() / 4, CanvasManager.takePictureRegion,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager()) {

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.isActionUp()) {
					takePicture();
				}
				return true;
			}

		};
		camera.setCullingEnabled(true);
		addPostit = new Sprite(CanvasActivity.CAMERA_WIDTH * 0.02f,
				CanvasActivity.CAMERA_WIDTH * 0.02f,
				CanvasManager.emAddPostitBtnTextureRegion,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager()) {

			boolean stillPressed = false;
			EmpathyMapPostit postit = null;

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.isActionMove()) {
					if (!stillPressed) {
						float x = pSceneTouchEvent.getX() - this.getWidth() / 2;
						float y = pSceneTouchEvent.getY() - this.getHeight()
								/ 2;
						postit = addPostIt(x, y);
						postit.setPosX(x);
						postit.setPosY(y);
						stillPressed = true;
					}
					if (postit != null) {
						float x = pSceneTouchEvent.getX() > CanvasActivity.TOOL_BOX_WIDTH ? pSceneTouchEvent
								.getX() - this.getWidth() / 2
								: CanvasActivity.TOOL_BOX_WIDTH;
						float y = pSceneTouchEvent.getY() - this.getHeight()
								/ 2;
						postit.setPosition(x, y);
						postit.setPosX(x);
						postit.setPosY(y);
					}
				}
				if (pSceneTouchEvent.isActionUp()) {
					stillPressed = false;
					if (postit != null) {
						if (postit.collidesWith(deletePostit)
								|| postit.collidesWith(addPostit)) {
							final EngineLock engineLock = CanvasManager
									.getInstance().engine.getEngineLock();
							engineLock.lock();
							/* Now it is save to remove the entity! */
							gameHud.detachChild(this.postit);
							customer.removePostit(postit);
							if (!this.isDisposed()) {
								this.postit.dispose();
								this.postit = null;
							}
							engineLock.unlock();
						}
					}
				}
				return true;
			}
		};
		addPostit.setCullingEnabled(true);
		deletePostit = new Sprite(CanvasActivity.CAMERA_WIDTH * 0.98f
				- CanvasManager.emDdeleteRegion.getWidth(),
				CanvasActivity.CAMERA_WIDTH * 0.02f,
				CanvasManager.emDdeleteRegion,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());
		deletePostit.setCullingEnabled(true);
		gallery = new Sprite(CanvasActivity.CAMERA_WIDTH / 2
				+ (frame.getWidth() / 2) - 20, CROSS_SCREEN / 2
				- CanvasManager.takePictureRegion.getHeight()
				+ frame.getHeight() / 4, CanvasManager.pickFromGalleryRegion,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.isActionUp()) {
					pickFromGallery();
				}
				return true;
			}
		};
		gallery.setCullingEnabled(true);
		frame.setScale(0.8f);
		gameHud.attachChild(background);
		customer_bg.setScale(0.8f);
		gameHud.attachChild(customer_bg);
		if (empathyFace != null) {
			empathyFace.setScale(0.8f);
			gameHud.attachChild(empathyFace);
		}
		gameHud.attachChild(frame);
		gameHud.attachChild(camera);
		gameHud.attachChild(gallery);
		gameHud.attachChild(addPostit);
		gameHud.attachChild(deletePostit);
		registerTouchArea(camera);
		registerTouchArea(gallery);
		registerTouchArea(addPostit);
		if (loadFromDatabase) {
			EmpathyMapNoteController emNController = new EmpathyMapNoteController(
					CanvasManager.getInstance().context);
			ArrayList<EmpathyMapNoteDB> notes = (ArrayList<EmpathyMapNoteDB>) emNController
					.getByEMId(emP.getId());
			if (!notes.isEmpty()) {
				for (EmpathyMapNoteDB note : notes) {
					Log.d("position", "Recuperado: " + note.getTitle() + " - x: " + note.getpX() + " - y: " + note.getpY());
					EmpathyMapPostit EMPostit = new EmpathyMapPostit(
							note.getId(), note.getpX(), note.getpY(),
							CanvasManager.smallEMapPostit,
							CanvasManager.bigEMapPostit,
							CanvasManager.getInstance().engine
									.getVertexBufferObjectManager(),
							CanvasManager.getInstance().engine, activity,
							note.getTitle(), note.getDescription(),
							(note.getColor() != 0 ? UtilColor.fromInt(note
									.getColor()) : new Color(0.99f, 0.84f,
									0.38f)), this);
					EMPostit.setCullingEnabled(true);
					gameHud.attachChild(EMPostit);
					registerTouchArea(EMPostit);
					customer.getCustomerPostIt().add(EMPostit);
				}
			}
		}

	}

	private EmpathyMapPostit addPostIt(final float pX, final float pY) {
		final EmpathyMapPostit postit = new EmpathyMapPostit(-1, pX, pY,
				CanvasManager.smallEMapPostit, CanvasManager.bigEMapPostit,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager(),
				CanvasManager.getInstance().engine, activity, null, null, null,
				this);
		postit.isNew = true;
		postit.setCullingEnabled(true);
		gameHud.attachChild(postit);
		customer.getCustomerPostIt().add(postit);
		registerTouchArea(postit);
		setTouchAreaBindingOnActionDownEnabled(true);
		return postit;
	}

	private void takePicture() {
		Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US)
				.format(new Date());
		fileUri = Uri.fromFile(new File(Environment
				.getExternalStorageDirectory(), "IMG_" + timeStamp + ".jpg"));
		cameraIntent
				.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, fileUri);

		customer.setPhotoUri(fileUri);

		// EmpathyMapScene.EMapManager.instance.setFileUri(fileUri);
		// EmpathyMapScene.EMapManager.instance.setCurrentMap(this);
		cameraIntent.putExtra("return-data", true);
		// start the image capture Intent
		activity.startActivityForResult(cameraIntent,
				CanvasActivity.EMPATHY_REQUEST_CODE);
	}

	private void pickFromGallery() {
		Intent i = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

		activity.startActivityForResult(i, CanvasActivity.PICK_FROM_GALLERY);
	}

	public void updateSprite(Bitmap bitmap) {
		customer.setPhoto(bitmap);
		CanvasManager.getInstance().empathyMapBitmapTextureAtlas
				.clearTextureAtlasSources();
		IBitmapTextureAtlasSource base = new EmptyBitmapTextureAtlasSource(256,
				256);
		ImageTextureBuilder photo = new ImageTextureBuilder(base, bitmap);
		BitmapTextureAtlasTextureRegionFactory.createFromSource(
				CanvasManager.getInstance().empathyMapBitmapTextureAtlas,
				photo, 0, 0);
	}

	public static byte[] getBitmapFromAsset(Context context, String strName) {
		AssetManager assetManager = context.getAssets();

		InputStream istr;
		Bitmap bitmap = null;
		try {
			istr = assetManager.open(strName);
			bitmap = BitmapFactory.decodeStream(istr);
		} catch (IOException e) {
			return null;
		}
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray();
		return byteArray;
	}

	@Override
	public void onShowScene() {
		CanvasManager.getInstance().engine.getCamera().setHUD(gameHud);
	}

	@Override
	public void onHideScene() {
	}

	@Override
	public void onUnloadScene() {
		saveData();
		activity.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				gameHud.detachChildren();
				gameHud.dispose();
				EmpathyMapScene.this.detachChildren();
				EmpathyMapScene.this.clearEntityModifiers();
				EmpathyMapScene.this.clearTouchAreas();
				EmpathyMapScene.this.clearUpdateHandlers();

				CanvasManager.unloadTextures(textures);
			}
		});
	}

	private void saveData() {
		EmpathyMapNoteController emnController = new EmpathyMapNoteController(
				CanvasManager.getInstance().context);
		EmpathyMapPostitController empController = new EmpathyMapPostitController(
				CanvasManager.getInstance().context);
		PostitController pc = new PostitController(
				CanvasManager.getInstance().context);
		if (customer.getRelatedDatabaseObject() == null) {
			PostItDB pdb = PostItDB.fromPostit(customer, -1,
					CanvasActivity.canvasId);
			pdb.setId((int) pc.create(pdb));
			int indexInCollection = MainScene.getInstance().customerSegments.postits
					.indexOf(customer);
			MainScene.getInstance().customerSegments.postits.get(
					indexInCollection).setRelatedDatabaseObject(pdb);
			customer.setRelatedDatabaseObject(pdb);
		}
		long index = customer.getPostitRelated().getId();
		customer.getPostitRelated().setIdPostit(
				customer.getRelatedDatabaseObject().getId());
		if (index == -1) {
			index = empController.create(customer.getPostitRelated());
		} else {
			empController.update(customer.getPostitRelated());
		}
		for (EmpathyMapPostit emp : customer.getCustomerPostIt()) {
			Log.d("position", "Guardado: " + emp.getTitle() + " - x: " + emp.getPosX() + " - y: " + emp.getPosY());
			if (emp.getRelatedId() == -1) {
				emnController.create(new EmpathyMapNoteDB(0, (int) index, emp
						.getPosX(), emp.getPosY(), emp.getTitle(), emp
						.getDescription(), emp.getColor().getARGBPackedInt()));
			} else {
				emnController.update(new EmpathyMapNoteDB(emp.getRelatedId(),
						(int) index, emp.getPosX(), emp.getPosY(), emp
								.getTitle(), emp.getDescription(), emp
								.getColor().getARGBPackedInt()));
			}
		}

	}

	/**
	 * En ocasiones es necesario evitar que los sprites contenidos en la escena
	 * puedan ser "touchables", este m�todo toma cada uno de los objetos en la
	 * escena que sean Sprite y quita el registro TouchArea de los mismos.
	 */
	public void unregisterSpriteTouchAreas() {
		for (int i = 0; i < this.getChildCount(); i++) {
			Object ie = this.getChildByIndex(i);
			if (ie instanceof Sprite) {
				this.unregisterTouchArea((Sprite) ie);
			}
		}
	}

	/**
	 * Cuando se elimina el registro TouchArea de los sprites en la escena, es
	 * necesario que vuelvan a ser registrados para que se pueda interactuar
	 * nuevamente con ellos.
	 */
	public void registerSpriteTouchAreas() {
		for (int i = 0; i < this.getChildCount(); i++) {
			Object ie = this.getChildByIndex(i);
			if (ie instanceof Sprite) {
				this.registerTouchArea((Sprite) ie);
			}
		}
	}
}
