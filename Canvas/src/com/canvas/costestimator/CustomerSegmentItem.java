/**
 * Representación de los segmentos de clientes para el estimador de costos
 */
package com.canvas.costestimator;

public class CustomerSegmentItem{
	private String name;
	private int customerCount = 0;
	private double marketShare = 0;
	private double averagePurchase = 0;
	private int purchasePeriod = 12;
	private double customerSubtotal;
	private int relatedItemId;
	
	public CustomerSegmentItem(){
		
	}
	
	public CustomerSegmentItem(String name, int customerCount,
			double marketShare, float averagePurchase, int purchasePeriod, int relatedItemId) {
		super();
		this.name = name;
		this.customerCount = customerCount;
		this.marketShare = marketShare;
		this.averagePurchase = averagePurchase;
		this.purchasePeriod = purchasePeriod;
		this.relatedItemId = relatedItemId;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCustomerCount() {
		return customerCount;
	}
	public void setCustomerCount(int customerCount) {
		this.customerCount = customerCount;
	}
	public double getMarketShare() {
		return marketShare;
	}
	public void setMarketShare(double marketShare) {
		this.marketShare = marketShare;
	}
	public double getAveragePurchase() {
		return averagePurchase;
	}
	public void setAveragePurchase(double averagePurchase) {
		this.averagePurchase = averagePurchase;
	}
	public int getPurchasePeriod() {
		return purchasePeriod;
	}
	public void setPurchasePeriod(int purchasePeriod) {
		this.purchasePeriod = purchasePeriod;
	}

	public double getCustomerSubtotal() {		
		return customerSubtotal;
	}

	public void setCustomerSubtotal(double customerSubtotal) {
		this.customerSubtotal = customerSubtotal;
	}
	
	public int getRelatedItemId() {
		return relatedItemId;
	}
	
	public void setRelatedItemId(int relatedItemId) {
		this.relatedItemId = relatedItemId;
	}
	
	/**
	 * Calcula el factor multiplicador para un cliente específico
	 */
	public double calculateCustomerFactor(){
		return getAveragePurchase()*getMarketShare()*getPurchasePeriod();
	}		
	
}
