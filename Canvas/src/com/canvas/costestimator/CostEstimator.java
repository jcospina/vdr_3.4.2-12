package com.canvas.costestimator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.canvas.dataaccess.CostPostitDB;
import com.canvas.dataaccess.ItemCostDB;
import com.canvas.dataaccess.PostItDB;
import com.canvas.dataaccess.VariableCostDB;
import com.canvas.dataaccess.controller.CostPostitController;
import com.canvas.dataaccess.controller.ItemCostController;
import com.canvas.dataaccess.controller.PostitController;
import com.canvas.dataaccess.controller.VariableCostController;
import com.canvas.vo.CustomerPostitVO;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.CanvasManager;
import com.smartsoft.Educanvas.MainScene;
import com.smartsoft.Educanvas.PostIt;
import com.smartsoft.Educanvas.R;
import com.smartsoft.Educanvas.SceneManager;

public class CostEstimator extends Activity implements OnItemClickListener,
		View.OnClickListener {

	private ListView costTypeList; // Lista de tipos de costos
	private ViewFlipper flipper; // Flipper para cambiar entre vistas
	private ArrayAdapter<String> listAdapter;// Adaptador para costTypeList
	private ImageButton saveButton; // Bot�n para guardar los datos
	private ImageButton clearButton; // Bot�n para limpiar los datos
	private ExpandableListView fixedCostsExpList;// Lista de costos fijos
	private ExpandableListView variableCostsExpList;// Lista de costos variables
	private ListView customerSegmentCostsList;// Lista de segmento de clientes
	public TextView totalCost; // Costo total
	private TextView costTitle;
	private EditText otherCosts;
	private ProgressDialog savingDialog;
	private PostitController pc;
	private ItemCostController icController;
	private VariableCostController vcController;
	private boolean recalculateFixed = false;
	private boolean recalculateVariable = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cost_estimator);

		fillBlocks();

		saveButton = (ImageButton) findViewById(R.id.saveButton);
		clearButton = (ImageButton) findViewById(R.id.clear);

		saveButton.setOnClickListener(this);
		clearButton.setOnClickListener(this);

		costTypeList = (ListView) findViewById(R.id.costType);
		ArrayList<String> types = new ArrayList<String>();
		types.addAll(Arrays.asList(getResources().getStringArray(
				R.array.typesArray)));
		listAdapter = new ArrayAdapter<String>(this,
				R.layout.cost_estimator_simple_row, types);
		costTypeList.setAdapter(listAdapter);
		costTypeList.setSelection(0);
		costTypeList.setOnItemClickListener(this);

		flipper = (ViewFlipper) findViewById(R.id.costFlipper);
		flipper.setInAnimation(this, R.anim.push_left_in);
		flipper.setOutAnimation(this, R.anim.push_left_out);

		fixedCostsExpList = (ExpandableListView) findViewById(R.id.fixedCostList);
		variableCostsExpList = (ExpandableListView) findViewById(R.id.variableCostList);
		customerSegmentCostsList = (ListView) findViewById(R.id.customerSegmentCostList);

		CostAdapter fixed_adapter = new CostAdapter(this, false);
		fixedCostsExpList.setAdapter(fixed_adapter);
		CostAdapter variable_adapter = new CostAdapter(this, true);
		variableCostsExpList.setAdapter(variable_adapter);
		CustomerSegmentAdapter customerAdapter = new CustomerSegmentAdapter(
				this, R.layout.cost_estimator_customer_seg_layout);
		customerSegmentCostsList.setAdapter(customerAdapter);

		otherCosts = (EditText) findViewById(R.id.otherCosts);
		otherCosts.setText(CostManager.getInstace().actualOtherCost + "");
		otherCosts.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					EditText origin = (EditText) v;
					String text = "0";
					if (origin.getText().toString() != null) {
						text = origin.getText().toString();
					}
					if (!text.isEmpty()) {
						double value = Double.parseDouble(text);
						CostManager.getInstace().calculateOtherCosts(value);
					}
				}

			}
		});
		costTitle = (TextView) findViewById(R.id.costTitle);
		costTitle.setText(CostManager.getInstace().costPostit.getTitle());
		CostManager.getInstace().setActivity(this);
	}

	/**
	 * Cambia el layout que se muestra al seleccionar un tipo de costo en la
	 * lista
	 */
	private void changeView(int position) {
		flipper.setDisplayedChild(position);
	}

	/**
	 * Handler para gestionar los mensajes de actualizaci�n de la interfaz
	 */
	public Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.arg1) {
			case 0:
				double currentCost = (Double) msg.obj;
				updateTotalCost(currentCost);
				break;
			case 2:
				finish();
				startActivity(getIntent());
				overridePendingTransition(R.anim.fadeout, R.anim.fadein);
				break;
			case 3:
				cancelDialog();
				break;
			}
		}
	};

	/**
	 * Actualiza el TextView que muestra el costo total
	 */
	private void updateTotalCost(double newCost) {
		try {
			String currentCost = totalCost.getText().toString();
			String costWithoutSymbol = currentCost.substring(2);
			double actualCost = Double.parseDouble((String) costWithoutSymbol);
			actualCost += newCost;
			CostManager.getInstace().setTotalCost(actualCost);
			totalCost.setText("$ " + actualCost);
		} catch (Exception e) {
			Log.d("COST", "No puede convertir a double");
		}
	}

	/**
	 * Recupera la informaci�n sobre los postit desde el escenario y busca
	 * informaci�n asociada a costos en la base de datos
	 */
	private void fillBlocks() {
		// updateDatabase();

		CostPostitController cpController = new CostPostitController(
				CanvasManager.getInstance().context);
		icController = new ItemCostController(
				CanvasManager.getInstance().context);
		vcController = new VariableCostController(
				CanvasManager.getInstance().context);
		pc = new PostitController(CanvasManager.getInstance().context);
		CostPostitDB cpdb = null;
		int index = -1;
		if (CostManager.getInstace().costPostit.getRelatedDatabaseObject() != null) {
			index = CostManager.getInstace().costPostit
					.getRelatedDatabaseObject().getId();
			cpdb = cpController.getByPostitId(index);
		}
		if (cpdb == null) {
			cpdb = new CostPostitDB(-1, 0, 0, index, 0);
		}
		CostManager.getInstace().actualOtherCost = cpdb.getOtherCost();
		CostManager.getInstace().setTotalCost(cpdb.getCost());
		CostManager.getInstace().costPostit.setPostitRelated(cpdb);
		CostManager.getInstace().actualVariableCost = cpdb.getVariableCost();
		CostManager.getInstace().actualFixedCost = CostManager.getInstace()
				.getTotalCost()
				- CostManager.getInstace().actualVariableCost
				- CostManager.getInstace().actualOtherCost;
		ArrayList<ItemCostDB> icdb = (ArrayList<ItemCostDB>) icController
				.getByCostPostitId(cpdb.getId());// Elementos ItemCostDB
		ArrayList<VariableCostDB> vcdb = (ArrayList<VariableCostDB>) vcController
				.getByCostPostitId(cpdb.getId());// Elementos VariableCostDB

		ArrayList<CostBlock> blocks = new ArrayList<CostBlock>();
		// Se trae la informaci�n de las tablas de costos fijos y variables
		if (!icdb.isEmpty()) {
			// Key Partners
			ArrayList<CostItem> kp_items = iterateDB(
					icController.filterByBlock(icdb, 0),
					MainScene.getInstance().keyPartnerships.postits, 0);
			blocks.add(new CostBlock("Aliados clave", kp_items));
			// Key Activities
			ArrayList<CostItem> ka_items = iterateDB(
					icController.filterByBlock(icdb, 1),
					MainScene.getInstance().keyActivities.postits, 1);
			blocks.add(new CostBlock("Actividades clave", ka_items));
			// Key Resources
			ArrayList<CostItem> kr_items = iterateDB(
					icController.filterByBlock(icdb, 2),
					MainScene.getInstance().keyResources.postits, 2);
			blocks.add(new CostBlock("Recursos clave", kr_items));
			// Value Propositions
			ArrayList<CostItem> vp_items = iterateDB(
					icController.filterByBlock(icdb, 3),
					MainScene.getInstance().valuePropositions.postits, 3);
			blocks.add(new CostBlock("Propuestas de valor", vp_items));
			// Channels
			ArrayList<CostItem> ch_items = iterateDB(
					icController.filterByBlock(icdb, 5),
					MainScene.getInstance().channels.postits, 5);
			blocks.add(new CostBlock("Canales", ch_items));
		} else {
			// Key Partners
			ArrayList<CostItem> kp_items = iterate(
					MainScene.getInstance().keyPartnerships.postits, 0);
			blocks.add(new CostBlock("Aliados clave", kp_items));
			// Key Activities
			ArrayList<CostItem> ka_items = iterate(
					MainScene.getInstance().keyActivities.postits, 1);
			blocks.add(new CostBlock("Actividades clave", ka_items));
			// Key Resources
			ArrayList<CostItem> kr_items = iterate(
					MainScene.getInstance().keyResources.postits, 2);
			blocks.add(new CostBlock("Recursos clave", kr_items));
			// Value Propositions
			ArrayList<CostItem> vp_items = iterate(
					MainScene.getInstance().valuePropositions.postits, 3);
			blocks.add(new CostBlock("Propuestas de valor", vp_items));
			// Channels
			ArrayList<CostItem> ch_items = iterate(
					MainScene.getInstance().channels.postits, 5);
			blocks.add(new CostBlock("Canales", ch_items));
		}
		if (!vcdb.isEmpty()) {
			ArrayList<CustomerSegmentItem> items = new ArrayList<CustomerSegmentItem>();
			ArrayList<Integer> relatedItemIds = new ArrayList<Integer>();
			ArrayList<Integer> ids = new ArrayList<Integer>();
			for (VariableCostDB c : vcdb) {
				if (pc.getById(c.getRelatedItemId()) != null) {
					items.add(new CustomerSegmentItem(c.getCustomerSegment(),
							100, c.getMarketShare(), (float) c
									.getAveragePurchase(), c
									.getPurchasePeriod(), c.getRelatedItemId()));
					relatedItemIds.add(c.getRelatedItemId());
				} else {
					vcController.removeEntry(c.getId());
					recalculateVariable = true;
				}
			}
			for (CustomerPostitVO cpvo : MainScene.getInstance().customerSegments.postits) {
				ids.add(cpvo.getRelatedDatabaseObject().getId());
			}
			ArrayList<Integer> difference = MainScene.getDifference(ids,
					relatedItemIds);
			for (int dbid : difference) {
				PostItDB pdb = pc.getById(dbid);
				items.add(new CustomerSegmentItem(pdb.getTitle(), 1, 0, 0, 0,
						pdb.getId()));
			}
			CostManager.getInstace().setCustomers(items);
		} else {
			ArrayList<CustomerSegmentItem> items = new ArrayList<CustomerSegmentItem>();
			for (CustomerPostitVO c : MainScene.getInstance().customerSegments.postits) {
				items.add(new CustomerSegmentItem(c.getTitle(), c.getCount(),
						0, 0, 0, c.getRelatedDatabaseObject().getId()));
			}
			CostManager.getInstace().setCustomers(items);
		}
		CostManager.getInstace().setBlocks(blocks);
		if (recalculateFixed) {
			double fc = CostManager.getInstace().actualFixedCost;
			CostManager.getInstace().calculateFixedCosts();
			fc = CostManager.getInstace().actualFixedCost - fc;
			updateTotalCost(fc);
		}
		if (recalculateVariable) {
			CostManager.getInstace().calculateVariableCosts();
			CostManager.getInstace().setTotalCost(
					CostManager.getInstace().getTotalCost());
		}
		totalCost = (TextView) findViewById(R.id.costValue);
		totalCost.setText("$ " + CostManager.getInstace().getTotalCost());
	}

	private <T extends PostIt> ArrayList<CostItem> iterate(List<T> titles,
			int block) {
		ArrayList<CostItem> items = new ArrayList<CostItem>();
		for (T p : titles) {
			items.add(new CostItem(p.getTitle(), 0, 0, block, p
					.getRelatedDatabaseObject().getId(), false));
		}
		return items;
	}

	private <T extends PostIt> ArrayList<CostItem> iterateDB(
			List<ItemCostDB> titles, List<T> currentTitles, int block) {
		ArrayList<CostItem> items = new ArrayList<CostItem>();
		ArrayList<Integer> relatedItemIds = new ArrayList<Integer>();
		for (ItemCostDB p : titles) {
			if (pc.getById(p.getRelatedItemId()) != null) {
				items.add(new CostItem(p.getItem(), p.getCost(), p
						.getVariableCost(), p.getBlock(), p.getRelatedItemId(),
						true));
				relatedItemIds.add(p.getRelatedItemId());
			} else {
				icController.removeEntry(p.getId());
				recalculateFixed = true;
			}
		}
		ArrayList<Integer> ids = new ArrayList<Integer>();
		for (T p : currentTitles) {
			ids.add(p.getRelatedDatabaseObject().getId());
		}
		ArrayList<Integer> difference = MainScene.getDifference(ids,
				relatedItemIds);
		for (int dbid : difference) {
			PostItDB pdb = pc.getById(dbid);
			items.add(new CostItem(pdb.getTitle(), 0, 0, block, pdb.getId(),
					false));
		}
		return items;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		changeView(position);
	}

	@Override
	public void onBackPressed() {

	}

	private boolean clear = false;

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.saveButton:
			saveData();
			break;

		case R.id.clear:
			AlertDialog ad = new AlertDialog.Builder(this)
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setTitle("Borrar datos")
					.setMessage("Est� seguro que desea borrar los datos?")
					.setPositiveButton("S�",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									clear = true;
								}

							}).setNegativeButton("No", null).create();
			ad.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialog) {
					if (clear) {
						CostManager.getInstace().clearCosts();
						clear = false;
					}
				}
			});
			ad.show();
			break;
		}
	}

	// Guarda el estado actual del lienzo en la base de datos
	public void updateDatabase() {
		PostitController controller = new PostitController(
				CanvasManager.getInstance().context);
		for (PostIt p : MainScene.getInstance().keyPartnerships.postits) {
			PostItDB pdb = savePostIt(p, controller, 0);
			if (pdb != null) {
				int indexInCollection = MainScene.getInstance().keyPartnerships.postits
						.indexOf(p);
				MainScene.getInstance().keyPartnerships.postits.get(
						indexInCollection).setRelatedDatabaseObject(pdb);
			}
		}
		for (PostIt p : MainScene.getInstance().keyActivities.postits) {
			PostItDB pdb = savePostIt(p, controller, 1);
			if (pdb != null) {
				int indexInCollection = MainScene.getInstance().keyActivities.postits
						.indexOf(p);
				MainScene.getInstance().keyActivities.postits.get(
						indexInCollection).setRelatedDatabaseObject(pdb);
			}
		}
		for (PostIt p : MainScene.getInstance().keyResources.postits) {
			PostItDB pdb = savePostIt(p, controller, 2);
			if (pdb != null) {
				int indexInCollection = MainScene.getInstance().keyResources.postits
						.indexOf(p);
				MainScene.getInstance().keyResources.postits.get(
						indexInCollection).setRelatedDatabaseObject(pdb);
			}
		}
		for (PostIt p : MainScene.getInstance().valuePropositions.postits) {
			PostItDB pdb = savePostIt(p, controller, 3);
			if (pdb != null) {
				int indexInCollection = MainScene.getInstance().valuePropositions.postits
						.indexOf(p);
				MainScene.getInstance().valuePropositions.postits.get(
						indexInCollection).setRelatedDatabaseObject(pdb);
			}
		}
		for (PostIt p : MainScene.getInstance().relationships.postits) {
			PostItDB pdb = savePostIt(p, controller, 4);
			if (pdb != null) {
				int indexInCollection = MainScene.getInstance().relationships.postits
						.indexOf(p);
				MainScene.getInstance().relationships.postits.get(
						indexInCollection).setRelatedDatabaseObject(pdb);
			}
		}
		for (PostIt p : MainScene.getInstance().channels.postits) {
			PostItDB pdb = savePostIt(p, controller, 5);
			if (pdb != null) {
				int indexInCollection = MainScene.getInstance().channels.postits
						.indexOf(p);
				MainScene.getInstance().channels.postits.get(indexInCollection)
						.setRelatedDatabaseObject(pdb);
			}
		}
		for (PostIt p : MainScene.getInstance().customerSegments.postits) {
			PostItDB pdb = savePostIt(p, controller, 6);
			if (pdb != null) {
				int indexInCollection = MainScene.getInstance().customerSegments.postits
						.indexOf(p);
				MainScene.getInstance().customerSegments.postits.get(
						indexInCollection).setRelatedDatabaseObject(pdb);
			}
		}
	}

	private PostItDB savePostIt(PostIt p, PostitController controller, int block) {
		if (p.getRelatedDatabaseObject() == null) {
			PostItDB pdb = PostItDB.fromPostit(p, -1, CanvasActivity.canvasId);
			pdb.setId((int) controller.create(pdb));
			return pdb;
		}
		return null;
	}

	// Guarda los datos de los costos
	private void saveData() {
		showDialog();
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				CostPostitController cpController = new CostPostitController(
						CanvasManager.getInstance().context);
				ItemCostController icController = new ItemCostController(
						CanvasManager.getInstance().context);
				VariableCostController vcController = new VariableCostController(
						CanvasManager.getInstance().context);
				pc = new PostitController(CanvasManager.getInstance().context);
				int indexInCollection = -1;
				if (CostManager.getInstace().costPostit
						.getRelatedDatabaseObject() == null) {
					PostItDB pdb = PostItDB.fromPostit(
							CostManager.getInstace().costPostit, -1,
							CanvasActivity.canvasId);
					pdb.setId((int) pc.create(pdb));
					indexInCollection = MainScene.getInstance().costs.postits
							.indexOf(CostManager.getInstace().costPostit);
					MainScene.getInstance().costs.postits
							.get(indexInCollection).setRelatedDatabaseObject(
									pdb);
					CostManager.getInstace().costPostit
							.setRelatedDatabaseObject(pdb);
				}
				indexInCollection = MainScene.getInstance().costs.postits
						.indexOf(CostManager.getInstace().costPostit);
				MainScene.getInstance().costs.postits.get(indexInCollection)
						.setCost(CostManager.getInstace().getTotalCost());
				long index = CostManager.getInstace().costPostit
						.getPostitRelated().getId();
				CostManager.getInstace().costPostit.getPostitRelated()
						.setIdPostit(
								CostManager.getInstace().costPostit
										.getRelatedDatabaseObject().getId());
				if (index == -1) {
					index = cpController.create(CostManager.getInstace().costPostit
							.getPostitRelated());
				} else {
					cpController.update(CostManager.getInstace().costPostit
							.getPostitRelated());
				}
				icController.removeEntries((int) index);
				vcController.removeEntries((int) index);
				for (CostBlock cb : CostManager.getInstace().getBlocks()) {
					for (CostItem ci : cb.getItems()) {
						icController.create(new ItemCostDB(-1, ci.getName(), ci
								.getBlock(), ci.getCost(), (int) index, ci
								.getVariableCost(), ci.getRelatedItemId()));
					}
				}
				for (CustomerSegmentItem ci : CostManager.getInstace()
						.getCustomers()) {
					vcController.create(new VariableCostDB(-1, ci.getName(), ci
							.getMarketShare(), ci.getAveragePurchase(), ci
							.getPurchasePeriod(), (int) index, ci
							.getRelatedItemId()));
				}
				Message m = new Message();
				m.arg1 = 3;
				handler.sendMessage(m);
			}
		});
		t.start();

	}

	private void showDialog() {
		savingDialog = new ProgressDialog(this);
		savingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		savingDialog.setMessage(getResources().getString(R.string.saving));
		savingDialog.setCancelable(false);
		savingDialog.show();
	}

	private void cancelDialog() {
		savingDialog.dismiss();
		CostManager.getInstace().reset();
		super.onBackPressed();
		SceneManager.getInstance().setMainSceneAsCurrent(true);
		SceneManager.getInstance().showScene(MainScene.getInstance());
	}

}
