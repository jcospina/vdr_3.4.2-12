package com.canvas.costestimator;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.smartsoft.Educanvas.R;


public class CustomerSegmentAdapter extends ArrayAdapter<CustomerSegmentItem> {

	private ArrayList<CustomerSegmentItem> data;
	private Context context;

	public CustomerSegmentAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId, CostManager.getInstace()
				.getCustomers());
		this.data = CostManager.getInstace().getCustomers();
		this.context = context;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		CustomerSegmentItem item = data.get(position);
		CustomerSegmentListener listener = new CustomerSegmentListener(position);
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(
					R.layout.cost_estimator_customer_seg_layout, null);
		}
		TextView customerSegment = (TextView) convertView
				.findViewById(R.id.customerSegment);
		customerSegment.setText(item.getName());
		EditText marketShare = (EditText) convertView
				.findViewById(R.id.marketShare);
		marketShare.setText(item.getMarketShare() + "");
		marketShare.setOnFocusChangeListener(listener);
		EditText averagePurchase = (EditText) convertView
				.findViewById(R.id.averagePurchase);
		averagePurchase.setText(item.getAveragePurchase() + "");
		averagePurchase.setOnFocusChangeListener(listener);
		Spinner purchasePeriod = (Spinner) convertView
				.findViewById(R.id.purchasePeriod);
		ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter
				.createFromResource(context, R.array.purchasePeriod,
						android.R.layout.simple_spinner_item);
		spinnerAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		purchasePeriod.setAdapter(spinnerAdapter);
		purchasePeriod.setOnItemSelectedListener(listener);		
		purchasePeriod.setSelection(getPurchasePeriod(item.getPurchasePeriod()));
		return convertView;
	}
	
	private int getPurchasePeriod(int value){		
		int spinnerPosition = 0;
		switch (value) {
		case 1:
			spinnerPosition = 0;
			break;
		case 2:
			spinnerPosition = 1; 
			break;
		case 4:
			spinnerPosition = 2; 
			break;
		case 6:
			spinnerPosition = 3;
			break;
		case 12:
			spinnerPosition = 4;
			break;
		case 52:
			spinnerPosition = 5;
			break;
		case 365:
			spinnerPosition = 6;
			break;
		default:
			spinnerPosition = 0;
			break;
		}
		
		return spinnerPosition;
	}

}
