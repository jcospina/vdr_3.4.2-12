package com.canvas.costestimator;

import org.andengine.entity.scene.Scene;

import android.content.Context;
import android.content.Intent;

import com.canvas.vo.CostPostitVO;
import com.smartsoft.Educanvas.ManagedScene;

public class CostEstimatorScene extends ManagedScene{
	
	private CostPostitVO costPostit;
	Context context;
	
	public CostEstimatorScene(Context context, CostPostitVO costPostit) {
		super();
		this.costPostit = costPostit;
		this.context = context;
	}

	@Override
	public Scene onLoadingScreenLoadAndShown() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onLoadingScreenUnloadAndHidden() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onLoadScene() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onShowScene() {
		// TODO Auto-generated method stub
		CostManager.getInstace().costPostit = costPostit;
		Intent i = new Intent(context, CostEstimator.class);
		//CostManager.getInstace().setCostPostit(costPostit);
		context.startActivity(i);
	}

	@Override
	public void onHideScene() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUnloadScene() {
		// TODO Auto-generated method stub
		
	}

}
