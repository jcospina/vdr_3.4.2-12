/**
 * Manager donde est� centralizado el c�lculo de los costos, se encarga de calcular costos
 * fijos y variables y de notificar a la interfaz que los costos totales deben ser actualizados
 */
package com.canvas.costestimator;

import java.util.ArrayList;
import java.util.Iterator;

import android.os.Message;

import com.canvas.dataaccess.controller.CostPostitController;
import com.canvas.dataaccess.controller.ItemCostController;
import com.canvas.dataaccess.controller.VariableCostController;
import com.canvas.vo.CostPostitVO;
import com.smartsoft.Educanvas.CanvasManager;

public class CostManager {

	private static CostManager instance;
	private ArrayList<CostBlock> blocks;
	private ArrayList<CustomerSegmentItem> customers;
	private CostEstimator activity;
	public double actualVariableCost = 0;
	public double actualFixedCost = 0;
	public double actualOtherCost = 0;
	private double totalCost = 0;
	public CostPostitVO costPostit;

	public static CostManager getInstace() {
		if (instance == null) {
			instance = new CostManager();
		}
		return instance;
	}

	public void setActivity(CostEstimator activity) {
		this.activity = activity;
	}

	public void setCostPostit(CostPostitVO costPostit) {
		this.costPostit = costPostit;
		instance = new CostManager();
	}

	/**
	 * Calcula los costos fijos
	 */
	public void calculateFixedCosts() {
		double totalFixedCost = calculateItemTotal(false);
		double difference = totalFixedCost - actualFixedCost;
		actualFixedCost = totalFixedCost;
		sendMessage(difference, 0, -1);
	}

	/**
	 * Calcula los costos variables
	 */
	public void calculateVariableCosts() {
		double itemTotal = calculateItemTotal(true);
		double totalVariableCost = itemTotal
				* calculateMultiplyFactor(itemTotal);
		double difference = totalVariableCost - actualVariableCost;
		actualVariableCost = totalVariableCost;
		sendMessage(difference, 0, -1);
	}

	/**
	 * Calcula otros costos
	 */
	public void calculateOtherCosts(double value) {
		double difference = value - actualOtherCost;
		actualOtherCost = value;
		sendMessage(difference, 0, -1);
	}

	/**
	 * Calcula los costos totales de los items en todos los bloques
	 */
	private double calculateItemTotal(boolean isVariable) {
		double itemTotal = 0;
		Iterator<CostBlock> blockIt = blocks.iterator();
		while (blockIt.hasNext()) {
			ArrayList<CostItem> currentItems = blockIt.next().getItems();
			Iterator<CostItem> itemIterator = currentItems.iterator();
			while (itemIterator.hasNext()) {
				CostItem currentItem = itemIterator.next();
				if (isVariable) {
					itemTotal += currentItem.getVariableCost();
				} else {
					itemTotal += currentItem.getCost();
				}
			}
		}
		return itemTotal;
	}

	/**
	 * Calcula el factor multiplicador total para los costos variables este
	 * factor est� dado por la suma de los factores individuales de cada
	 * segmento de clientes, donde un factor individual est� dado por:
	 * marketShare*averagePurchase*purchasePeriod*customerCount
	 */
	private double calculateMultiplyFactor(double itemTotal) {
		double multiplyFactor = 0;
		Iterator<CustomerSegmentItem> customerIt = customers.iterator();
		while (customerIt.hasNext()) {
			CustomerSegmentItem currentCustomer = customerIt.next();
			multiplyFactor += currentCustomer.calculateCustomerFactor();
		}
		return multiplyFactor;
	}

	private void sendMessage(Object obj, int arg1, int arg2) {
		Message m = new Message();
		m.arg1 = arg1;
		m.arg2 = arg2;
		m.obj = obj;
		activity.handler.sendMessage(m);
	}

	public void setBlocks(ArrayList<CostBlock> blocks) {
		this.blocks = blocks;
	}

	public ArrayList<CostBlock> getBlocks() {
		return blocks;
	}

	public void setCustomers(ArrayList<CustomerSegmentItem> customers) {
		this.customers = customers;
	}

	public ArrayList<CustomerSegmentItem> getCustomers() {
		return customers;
	}

	public synchronized void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}

	public synchronized double getTotalCost() {
		return totalCost;
	}

	/**
	 * Limpia los costos en la interfaz
	 */
	public void clearCosts() {
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				Iterator<CostBlock> blockIt = blocks.iterator();
				while (blockIt.hasNext()) {
					ArrayList<CostItem> currentItems = blockIt.next()
							.getItems();
					Iterator<CostItem> itemIterator = currentItems.iterator();
					while (itemIterator.hasNext()) {
						CostItem currentItem = itemIterator.next();
						currentItem.setCost(0);
						currentItem.setVariableCost(0);
					}
				}
				Iterator<CustomerSegmentItem> customerIt = customers.iterator();
				while (customerIt.hasNext()) {
					CustomerSegmentItem currentCustomer = customerIt.next();
					currentCustomer.setAveragePurchase(0);
					currentCustomer.setMarketShare(0);
					currentCustomer.setPurchasePeriod(1);
					currentCustomer.setCustomerSubtotal(0);
				}

				/**
				 * Elimina los registros de la base de datos
				 */
				if (CostManager.getInstace().costPostit
						.getRelatedDatabaseObject() != null) {

					int index_items = CostManager.getInstace().costPostit
							.getPostitRelated().getId();

					CostPostitController cpController = new CostPostitController(
							CanvasManager.getInstance().context);

					ItemCostController icController = new ItemCostController(
							CanvasManager.getInstance().context);
					VariableCostController vcController = new VariableCostController(
							CanvasManager.getInstance().context);

					icController.removeEntries((int) index_items);
					vcController.removeEntries((int) index_items);
					
					actualVariableCost = 0;
					actualFixedCost = 0;
					actualOtherCost = 0;
					totalCost = 0;

					cpController.removeEntry(index_items);
				}

				Message m = new Message();
				m.arg1 = 2;
				activity.handler.sendMessage(m);
			}
		});
		t.start();
	}

	public void reset() {
		blocks.clear();
		customers.clear();
		activity = null;
		actualVariableCost = 0;
		actualFixedCost = 0;
		actualOtherCost = 0;
		totalCost = 0;
		costPostit = null;
		instance = null;
	}

}
