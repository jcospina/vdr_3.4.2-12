/**
 * Adaptador para presentar la lista de bloques con sus respectivos items
 */
package com.canvas.costestimator;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.smartsoft.Educanvas.R;

public class CostAdapter extends BaseExpandableListAdapter {

	private Context context;
	private ArrayList<CostBlock> elements;
	private boolean isVariable;

	public CostAdapter(Context context, boolean isVariable) {
		this.context = context;
		this.elements = CostManager.getInstace().getBlocks();
		this.isVariable = isVariable;
	}

	public void addItem(CostItem item, CostBlock element) {
		if (!elements.contains(element)) {
			elements.add(element);
		}
		int index = elements.indexOf(element);
		ArrayList<CostItem> childs = elements.get(index).getItems();
		childs.add(item);
		elements.get(index).setItems(childs);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		ArrayList<CostItem> childList = elements.get(groupPosition).getItems();
		return childList.get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		CostItem item = (CostItem) getChild(groupPosition, childPosition);
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.cost_estimator_item_layout, null);
		}
		TextView itemName = (TextView) convertView.findViewById(R.id.itemName);
		itemName.setText(item.getName());
		EditText cost = (EditText) convertView.findViewById(R.id.itemCost);
		if(item.isLoadedFromDB()){
			if(isVariable){
				cost.setText(item.getVariableCost() + "");
			} else{
				cost.setText(item.getCost() + "");
			}
		}
		cost.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					v.requestFocus();
				}
				return false;
			}
		});
		CostListener listener = new CostListener(groupPosition, childPosition,
				isVariable);		
		cost.setOnFocusChangeListener(listener);
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		ArrayList<CostItem> childList = elements.get(groupPosition).getItems();
		return childList.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return elements.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return elements.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		CostBlock cost = (CostBlock) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater inf = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inf.inflate(R.layout.cost_estimator_simple_row, null);
		}
		TextView groupLabel = (TextView) convertView
				.findViewById(R.id.rowTextView);
		groupLabel.setText(cost.getBlock());
		ExpandableListView eLV = (ExpandableListView) parent;
		eLV.expandGroup(groupPosition);
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}

}
