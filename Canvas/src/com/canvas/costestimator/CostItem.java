/**
 * Representación de cada uno de los elementos que se encuentra en el escenario para el estimador de costos
 */
package com.canvas.costestimator;

public class CostItem {
	
	private String name;
	private double cost = 0;
	private double variableCost = 0;
	private int block;
	private int relatedItemId;
	private boolean loadedFromDB = false;
	
	public CostItem() {
	}
	
	public CostItem(String name, double cost, double variableCost, int block, int relatedItemId, boolean loadedFromDB) {
		this.name = name;
		this.cost = cost;
		this.variableCost = variableCost;
		this.block = block;
		this.relatedItemId = relatedItemId;
		this.loadedFromDB = loadedFromDB;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}		
	
	public void setVariableCost(double variableCost) {
		this.variableCost = variableCost;
	}
	
	public double getVariableCost() {
		return variableCost;
	}
	
	public int getBlock() {
		return block;
	}
	
	public void setBlock(int block) {
		this.block = block;
	}
	
	public int getRelatedItemId() {
		return relatedItemId;
	}
	
	public void setRelatedItemId(int relatedItemId) {
		this.relatedItemId = relatedItemId;
	}
	
	public void setLoadedFromDB(boolean loadedFromDB) {
		this.loadedFromDB = loadedFromDB;
	}
	
	public boolean isLoadedFromDB() {
		return loadedFromDB;
	}
	
}
