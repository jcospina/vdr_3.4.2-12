package com.canvas.costestimator;


import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;

public class CostListener implements OnFocusChangeListener{

	private int groupPosition;
	private int childPosition;
	private boolean isVariable;
	
	public CostListener(int groupPosition, int childPosition, boolean isVariable) {
		this.groupPosition = groupPosition;
		this.childPosition = childPosition;
		this.isVariable = isVariable;
	}
	
	@Override
	public void onFocusChange(View view, boolean hasFocus) {
		if(!hasFocus){
			EditText origin = (EditText) view;
			String text = "0";
			if (origin.getText().toString() != null) {
				text = origin.getText().toString();
			}
			if (!text.isEmpty()) {
				double value = Double.parseDouble(text);
				if(isVariable){
					CostManager.getInstace().getBlocks().get(groupPosition).getItems().get(childPosition).setVariableCost(value);
					CostManager.getInstace().calculateVariableCosts();
				} else{
					CostManager.getInstace().getBlocks().get(groupPosition).getItems().get(childPosition).setCost(value);
					CostManager.getInstace().calculateFixedCosts();
				}
			}
		}
	}

}
