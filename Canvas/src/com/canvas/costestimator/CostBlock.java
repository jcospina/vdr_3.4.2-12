/**
 * Representación de un bloque del escenario con sus respectivos items
 */
package com.canvas.costestimator;

import java.util.ArrayList;
import java.util.Iterator;

public class CostBlock {
	
	private ArrayList<CostItem> items;
	private double totalCost = 0;
	private String block;
	
	public CostBlock(String name) {
		this.block = name;
	}
	
	public CostBlock(String name, ArrayList<CostItem> items){
		this.items = items;
		this.block = name;
	}
	
	/**
	 * Calcula el costo para el bloque con base en el costo de cada uno de sus elementos 
	 */
	public double estimateCost(){
		Iterator<CostItem> costIterator = items.iterator();
		while(costIterator.hasNext()){
			CostItem current = (CostItem)costIterator.next(); 
			totalCost += current.getCost();
		}
		return totalCost;
	}

	public ArrayList<CostItem> getItems() {
		return items;
	}

	public void setItems(ArrayList<CostItem> items) {
		this.items = items;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}				
	
}
