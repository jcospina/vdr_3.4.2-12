package com.canvas.costestimator;


import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;

import com.smartsoft.Educanvas.R;

public class CustomerSegmentListener implements OnFocusChangeListener,
		OnItemSelectedListener {

	private int position;

	public CustomerSegmentListener(int position) {
		this.position = position;
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		switch (position) {
		case 0:
			CostManager.getInstace().getCustomers().get(this.position).setPurchasePeriod(1);
			recalculate();
			break;
		case 1:
			CostManager.getInstace().getCustomers().get(this.position).setPurchasePeriod(2);
			recalculate();
			break;
		case 2:
			CostManager.getInstace().getCustomers().get(this.position).setPurchasePeriod(4);
			recalculate();
			break;
		case 3:
			CostManager.getInstace().getCustomers().get(this.position).setPurchasePeriod(6);
			recalculate();
			break;
		case 4:
			CostManager.getInstace().getCustomers().get(this.position).setPurchasePeriod(12);
			recalculate();
			break;
		case 5:
			CostManager.getInstace().getCustomers().get(this.position).setPurchasePeriod(52);
			recalculate();
			break;
		case 6:
			CostManager.getInstace().getCustomers().get(this.position).setPurchasePeriod(365);
			recalculate();
			break;
		default:
			break;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {

	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		if (!hasFocus) {
			EditText origin = (EditText) v;
			String text = "0";
			if (origin.getText().toString() != null) {
				text = origin.getText().toString();
			}
			if (!text.isEmpty()) {
				double value = Double.parseDouble(text);
				switch (v.getId()) {
				case R.id.marketShare:
					CostManager.getInstace().getCustomers().get(position).setMarketShare(value);
					recalculate();
					break;
				case R.id.averagePurchase:
					CostManager.getInstace().getCustomers().get(position).setAveragePurchase(value);
					recalculate();
					break;
				default:
					break;
				}
			}
		}
	}
	
	public void recalculate(){
		CostManager.getInstace().calculateVariableCosts();
	}

}
