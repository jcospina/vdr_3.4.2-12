package com.canvas.revenueestimator;

import org.andengine.entity.scene.Scene;

import android.content.Context;
import android.content.Intent;

import com.canvas.vo.RevenuePostitVO;
import com.smartsoft.Educanvas.ManagedScene;

public class RevenueEstimatorScene extends ManagedScene {
	
	private Context context;
	RevenuePostitVO revenuePostit;
	
	public RevenueEstimatorScene(Context context, RevenuePostitVO revenuePostit) {
		this.context = context;
		this.revenuePostit = revenuePostit;
	}
	
	@Override
	public Scene onLoadingScreenLoadAndShown() {
		return null;
	}

	@Override
	public void onLoadingScreenUnloadAndHidden() {

	}

	@Override
	public void onLoadScene() {
		
	}

	@Override
	public void onShowScene() {
		RevenueManager.getInstance().revenuePostit = revenuePostit;
		Intent i = new Intent(context, RevenueEstimator.class);
		context.startActivity(i);
	}

	@Override
	public void onHideScene() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUnloadScene() {
		// TODO Auto-generated method stub

	}

}
