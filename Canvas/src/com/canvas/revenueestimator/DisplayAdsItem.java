package com.canvas.revenueestimator;

public class DisplayAdsItem extends CustomerSegmentRevenue {

	private float averagePageViewsPerUser = 0;
	private float averageAdUnitsPerPage = 0;
	private float averageCPMPerAdUnit = 0;

	public DisplayAdsItem() {
		super();
	}

	public DisplayAdsItem(String name, int customerCount, double marketShare,
			int purchasePeriod, int relatedItemId, float averagePageViewsPerUser,
			float averageAdUnitsPerPage, float averageCPMPerAdUnit) {
		super(name, customerCount, marketShare, purchasePeriod, relatedItemId);
		this.averagePageViewsPerUser = averagePageViewsPerUser;
		this.averageAdUnitsPerPage = averageAdUnitsPerPage;
		this.averageCPMPerAdUnit = averageCPMPerAdUnit;
	}

	public float getAveragePageViewsPerUser() {
		return averagePageViewsPerUser;
	}

	public void setAveragePageViewsPerUser(float averagePageViewsPerUser) {
		this.averagePageViewsPerUser = averagePageViewsPerUser;
	}

	public float getAverageAdUnitsPerPage() {
		return averageAdUnitsPerPage;
	}

	public void setAverageAdUnitsPerPage(float averageAdUnitsPerPage) {
		this.averageAdUnitsPerPage = averageAdUnitsPerPage;
	}

	public float getAverageCPMPerAdUnit() {
		return averageCPMPerAdUnit;
	}

	public void setAverageCPMPerAdUnit(float averageCPMPerAdUnit) {
		this.averageCPMPerAdUnit = averageCPMPerAdUnit;
	}

	@Override
	public double calculateSubTotal() {
		return getMarketShare() * getPurchasePeriod()
				* getAverageAdUnitsPerPage()
				* (getAverageCPMPerAdUnit() / 1000)
				* getAveragePageViewsPerUser();
	}

}
