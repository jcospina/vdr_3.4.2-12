package com.canvas.revenueestimator;

public abstract class CustomerSegmentRevenue {
	
	private String name;
	private int customerCount = 0;
	private double marketShare = 0;
	private int purchasePeriod = 12;		
	private int relatedItemId;
	
	public CustomerSegmentRevenue(){
		
	}
	
	public CustomerSegmentRevenue(String name, int customerCount,
			double marketShare, int purchasePeriod, int relatedItemId) {
		this.name = name;
		this.customerCount = customerCount;
		this.marketShare = marketShare;
		this.purchasePeriod = purchasePeriod;
		this.relatedItemId = relatedItemId;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCustomerCount() {
		return customerCount;
	}
	public void setCustomerCount(int customerCount) {
		this.customerCount = customerCount;
	}
	public double getMarketShare() {
		return marketShare;
	}
	public void setMarketShare(double marketShare) {
		this.marketShare = marketShare;
	}
	public int getPurchasePeriod() {
		return purchasePeriod;
	}
	public void setPurchasePeriod(int purchasePeriod) {
		this.purchasePeriod = purchasePeriod;
	}
	
	public int getRelatedItemId() {
		return relatedItemId;
	}
	
	public void setRelatedItemId(int relatedItemId) {
		this.relatedItemId = relatedItemId;
	}
	
	public abstract double calculateSubTotal();
				
}
