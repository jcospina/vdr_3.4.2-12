package com.canvas.revenueestimator;

public class ClickAdsItem extends CustomerSegmentRevenue {

	private float averagePageViewPerUser = 0;
	private float averageAdsPerPage = 0;
	private float averageCTR = 0;
	private float averageCPC = 0;

	public ClickAdsItem() {
		super();
	}

	public ClickAdsItem(String name, int customerCount, double marketShare,
			int purchasePeriod, int relatedItemId, float averagePageViewPerUser,
			float averageAdsPerPage, float averageCTR, float averageCPC) {
		super(name, customerCount, marketShare, purchasePeriod, relatedItemId);
		this.averagePageViewPerUser = averagePageViewPerUser;
		this.averageAdsPerPage = averageAdsPerPage;
		this.averageCTR = averageCTR;
		this.averageCPC = averageCPC;
	}

	public float getAveragePageViewPerUser() {
		return averagePageViewPerUser;
	}

	public void setAveragePageViewPerUser(float averagePageViewPerUser) {
		this.averagePageViewPerUser = averagePageViewPerUser;
	}

	public float getAverageAdsPerPage() {
		return averageAdsPerPage;
	}

	public void setAverageAdsPerPage(float averageAdsPerPage) {
		this.averageAdsPerPage = averageAdsPerPage;
	}

	public float getAverageCTR() {
		return averageCTR;
	}

	public void setAverageCTR(float averageCTR) {
		this.averageCTR = averageCTR;
	}

	public float getAverageCPC() {
		return averageCPC;
	}

	public void setAverageCPC(float averageCPC) {
		this.averageCPC = averageCPC;
	}

	@Override
	public double calculateSubTotal() {
		return getMarketShare() * getPurchasePeriod() * getAverageAdsPerPage()
				* getAverageCPC() * getAverageCTR()
				* getAveragePageViewPerUser();
	}

}
