package com.canvas.revenueestimator;

import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;

import com.smartsoft.Educanvas.R;

public class RevenueListener implements OnFocusChangeListener,
		OnItemSelectedListener {

	private int groupPosition;
	private int chilPosition;
	private int type;
	private RevenueManager manager;
	private int[] spinnerValues = { 1, 2, 4, 6, 12, 52, 365 };

	public RevenueListener(int groupPosition, int chilPosition, int type) {
		this.groupPosition = groupPosition;
		this.chilPosition = chilPosition;
		this.type = type;
		manager = RevenueManager.getInstance();
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		int spinnerValue = spinnerValues[position];
		switch (type) {
		case 0:
			((SalesItem) manager.getSales().get(groupPosition).getCustomers().get(chilPosition)).setPurchasePeriod(spinnerValue);
			break;
		case 1:
			((SubscriptionItem) manager.getSubscriptions().get(groupPosition).getCustomers().get(chilPosition)).setPurchasePeriod(spinnerValue);
			break;
		case 2:
			((PayPerUseItem) manager.getPayPerUse().get(groupPosition).getCustomers().get(chilPosition)).setPurchasePeriod(spinnerValue);
			break;
		case 3:
			((RentalItem) manager.getRentals().get(groupPosition).getCustomers().get(chilPosition)).setPurchasePeriod(spinnerValue);
			break;
		case 4:
			((TransactionFeeItem) manager.getTransactionFees().get(groupPosition).getCustomers().get(chilPosition)).setPurchasePeriod(spinnerValue);
			break;
		case 5:
			((SalesFeeItem) manager.getSalesFees().get(groupPosition).getCustomers().get(chilPosition)).setPurchasePeriod(spinnerValue);
			break;
		case 6:
			((DisplayAdsItem) manager.getDisplayAds().get(groupPosition).getCustomers().get(chilPosition)).setPurchasePeriod(spinnerValue);
			break;
		case 7:
			((ClickAdsItem) manager.getClickAds().get(groupPosition).getCustomers().get(chilPosition)).setPurchasePeriod(spinnerValue);
			break;
		case 8:
			((LicensingRevenueItem) manager.getLicensing().get(groupPosition).getCustomers().get(chilPosition)).setPurchasePeriod(spinnerValue);
			break;
		default:
			break;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {

	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		if (!hasFocus) {
			EditText origin = (EditText) v;
			String text = "0";
			if (origin.getText().toString() != null) {
				text = origin.getText().toString();
			}
			if (!text.isEmpty()) {
				double value = Double.parseDouble(text);
				evalType(v.getId(), value);
			}
		}
	}

	private void evalType(int id, double value) {
		switch (type) {
		case 0:// Ventas
			switch (id) {
			case R.id.rMarketShare:
				((SalesItem) manager.getSales().get(groupPosition)
						.getCustomers().get(chilPosition))
						.setMarketShare(value);
				break;
			case R.id.field_1:
				((SalesItem) manager.getSales().get(groupPosition)
						.getCustomers().get(chilPosition))
						.setAveragePurchases(value);
				break;
			case R.id.field_2:
				((SalesItem) manager.getSales().get(groupPosition)
						.getCustomers().get(chilPosition))
						.setAveragePrice(value);
				break;
			}
			break;
		case 1:// Subscripcion
			switch (id) {
			case R.id.rMarketShare:
				((SubscriptionItem) manager.getSubscriptions()
						.get(groupPosition).getCustomers().get(chilPosition))
						.setMarketShare(value);
				break;
			case R.id.field_1:
				((SubscriptionItem) manager.getSubscriptions()
						.get(groupPosition).getCustomers().get(chilPosition))
						.setSubscriptionPrice(value);
				break;
			}
			
			break;
		case 2:// Pago por uso
			switch (id) {
			case R.id.rMarketShare:
				((PayPerUseItem) manager.getPayPerUse().get(groupPosition)
						.getCustomers().get(chilPosition))
						.setMarketShare(value);
				break;
			case R.id.field_1:
				((PayPerUseItem) manager.getPayPerUse().get(groupPosition)
						.getCustomers().get(chilPosition))
						.setAverageUnits(value);
				break;
			case R.id.field_2:
				((PayPerUseItem) manager.getPayPerUse().get(groupPosition)
						.getCustomers().get(chilPosition))
						.setAveragePricePerUnit(value);
				break;
			}
			break;
		case 3:// Alquiler
			switch (id) {
			case R.id.rMarketShare:
				((RentalItem) manager.getRentals().get(groupPosition)
						.getCustomers().get(chilPosition))
						.setMarketShare(value);
				break;
			case R.id.field_1:
				((RentalItem) manager.getRentals().get(groupPosition)
						.getCustomers().get(chilPosition))
						.setAverageUnitsRented(value);
				break;
			case R.id.field_2:
				((RentalItem) manager.getRentals().get(groupPosition)
						.getCustomers().get(chilPosition))
						.setAverageRentalFeePerUnit(value);
				break;
			}
			break;
		case 4:// Porcentaje por transacción
			switch (id) {
			case R.id.rMarketShare:
				((TransactionFeeItem) manager.getTransactionFees()
						.get(groupPosition).getCustomers().get(chilPosition))
						.setMarketShare(value);
				break;
			case R.id.field_1:
				((TransactionFeeItem) manager.getTransactionFees()
						.get(groupPosition).getCustomers().get(chilPosition))
						.setAverageTransactionPerCostumer(value);
				break;
			case R.id.field_2:
				((TransactionFeeItem) manager.getTransactionFees()
						.get(groupPosition).getCustomers().get(chilPosition))
						.setAverageTransactionPrice(value);
				break;
			case R.id.field_3:
				((TransactionFeeItem) manager.getTransactionFees()
						.get(groupPosition).getCustomers().get(chilPosition))
						.setAverageTransactionFee(value);
				break;
			}
			break;
		case 5:// Porcentaje por venta
			switch (id) {
			case R.id.rMarketShare:
				((SalesFeeItem) manager.getSalesFees().get(groupPosition)
						.getCustomers().get(chilPosition))
						.setMarketShare(value);
				break;
			case R.id.field_1:
				((SalesFeeItem) manager.getSalesFees().get(groupPosition)
						.getCustomers().get(chilPosition))
						.setAveragePurchases(value);
				break;
			case R.id.field_2:
				((SalesFeeItem) manager.getSalesFees().get(groupPosition)
						.getCustomers().get(chilPosition))
						.setAveragePrice(value);
				break;
			case R.id.field_3:
				((SalesFeeItem) manager.getSalesFees().get(groupPosition)
						.getCustomers().get(chilPosition)).setSalesFee(value);
				break;
			}
			break;
		case 6:// Mostrar publicidad
			switch (id) {
			case R.id.rMarketShare:
				((DisplayAdsItem) manager.getDisplayAds().get(groupPosition)
						.getCustomers().get(chilPosition))
						.setMarketShare(value);
				break;
			case R.id.field_1:
				((DisplayAdsItem) manager.getDisplayAds().get(groupPosition)
						.getCustomers().get(chilPosition))
						.setAveragePageViewsPerUser((float) value);
				break;
			case R.id.field_2:
				((DisplayAdsItem) manager.getDisplayAds().get(groupPosition)
						.getCustomers().get(chilPosition))
						.setAverageAdUnitsPerPage((float) value);
				break;
			case R.id.field_3:
				((DisplayAdsItem) manager.getDisplayAds().get(groupPosition)
						.getCustomers().get(chilPosition))
						.setAverageCPMPerAdUnit((float) value);
				break;
			}
			break;
		case 7:// Click en publicidad
			switch (id) {
			case R.id.rMarketShare:
				((ClickAdsItem) manager.getClickAds().get(groupPosition)
						.getCustomers().get(chilPosition))
						.setMarketShare(value);
				break;
			case R.id.field_1:
				((ClickAdsItem) manager.getClickAds().get(groupPosition)
						.getCustomers().get(chilPosition))
						.setAveragePageViewPerUser((float) value);
				break;
			case R.id.field_2:
				((ClickAdsItem) manager.getClickAds().get(groupPosition)
						.getCustomers().get(chilPosition))
						.setAverageAdsPerPage((float) value);
				break;
			case R.id.field_3:
				((ClickAdsItem) manager.getClickAds().get(groupPosition)
						.getCustomers().get(chilPosition))
						.setAverageCTR((float) value);
				break;
			case R.id.field_4:
				((ClickAdsItem) manager.getClickAds().get(groupPosition)
						.getCustomers().get(chilPosition))
						.setAverageCPC((float) value);
				break;
			}
			break;
		case 8:// Licenciamiento
			switch (id) {
			case R.id.rMarketShare:
				((LicensingRevenueItem) manager.getLicensing()
						.get(groupPosition).getCustomers().get(chilPosition))
						.setMarketShare(value);
				break;
			case R.id.field_1:
				((LicensingRevenueItem) manager.getLicensing()
						.get(groupPosition).getCustomers().get(chilPosition))
						.setAverageLicensesPerCostumer((float) value);
				break;
			case R.id.field_2:
				((LicensingRevenueItem) manager.getLicensing()
						.get(groupPosition).getCustomers().get(chilPosition))
						.setAverageLicensePrice((float) value);
				break;
			}
			break;
		}
		manager.calculateCost(type);
	}	
}
