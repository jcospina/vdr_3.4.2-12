package com.canvas.revenueestimator;

public class SubscriptionItem extends CustomerSegmentRevenue {

	private double subscriptionPrice = 0;

	public SubscriptionItem() {
		super();
	}

	public SubscriptionItem(String name, int customerCount, double marketShare,
			int purchasePeriod, int relatedItemId, double subscriptionPrice) {
		super(name, customerCount, marketShare, purchasePeriod, relatedItemId);
		this.subscriptionPrice = subscriptionPrice;

	}

	public void setSubscriptionPrice(double subscriptionPrice) {
		this.subscriptionPrice = subscriptionPrice;
	}

	public double getSubscriptionPrice() {
		return subscriptionPrice;
	}

	@Override
	public double calculateSubTotal() {
		return getMarketShare() * getPurchasePeriod() * getSubscriptionPrice();
	}

}
