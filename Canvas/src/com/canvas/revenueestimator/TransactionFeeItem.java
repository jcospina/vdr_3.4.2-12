package com.canvas.revenueestimator;

public class TransactionFeeItem extends CustomerSegmentRevenue {

	private double averageTransactionPerCostumer = 0;
	private double averageTransactionPrice = 0;
	private double averageTransactionFee = 0;

	public TransactionFeeItem() {
		super();
	}

	public TransactionFeeItem(String name, int customerCount,
			double marketShare, int purchasePeriod, int relatedItemId,
			double averageTransactionPerCostumer,
			double averageTransactionPrice, double averageTransactionFee) {
		super(name, customerCount, marketShare, purchasePeriod, relatedItemId);
		this.averageTransactionPerCostumer = averageTransactionPerCostumer;
		this.averageTransactionPrice = averageTransactionPrice;
		this.averageTransactionFee = averageTransactionFee;
	}

	public double getAverageTransactionPerCostumer() {
		return averageTransactionPerCostumer;
	}

	public void setAverageTransactionPerCostumer(
			double averageTransactionPerCostumer) {
		this.averageTransactionPerCostumer = averageTransactionPerCostumer;
	}

	public double getAverageTransactionPrice() {
		return averageTransactionPrice;
	}

	public void setAverageTransactionPrice(double averageTransactionPrice) {
		this.averageTransactionPrice = averageTransactionPrice;
	}

	public double getAverageTransactionFee() {
		return averageTransactionFee;
	}

	public void setAverageTransactionFee(double averageTransactionFee) {
		this.averageTransactionFee = averageTransactionFee;
	}

	@Override
	public double calculateSubTotal() {
		return getMarketShare() * getPurchasePeriod()
				* getAverageTransactionFee()
				* getAverageTransactionPerCostumer()
				* getAverageTransactionPrice();
	}

}
