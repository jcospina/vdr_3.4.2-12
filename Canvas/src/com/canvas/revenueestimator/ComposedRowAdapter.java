package com.canvas.revenueestimator;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.smartsoft.Educanvas.R;

public class ComposedRowAdapter extends BaseExpandableListAdapter{
	
	private Context context;
	private ArrayList<ValuePropositionRevenue> elements; 
	private int customerViewId;	
	private int type = 0;
	
	public ComposedRowAdapter(Context context, int type) {
		this.context = context;
		evalType(type);
		this.type = type;
	}
	
	private void evalType(int type){
		switch (type) {
		case 0:
			elements = RevenueManager.getInstance().getSales();
			customerViewId = R.layout.revenue_estimator_two_row;
			break;
		case 1:
			elements = RevenueManager.getInstance().getSubscriptions();
			customerViewId = R.layout.revenue_estimator_one_row;
			break;
		case 2:
			elements = RevenueManager.getInstance().getPayPerUse();
			customerViewId = R.layout.revenue_estimator_two_row;
			break;
		case 3:
			elements = RevenueManager.getInstance().getRentals();
			customerViewId = R.layout.revenue_estimator_two_row;
			break;
		case 4:
			elements = RevenueManager.getInstance().getTransactionFees();
			customerViewId = R.layout.revenue_estimator_three_row;
			break;
		case 5:
			elements = RevenueManager.getInstance().getSalesFees();
			customerViewId = R.layout.revenue_estimator_three_row;
			break;
		case 6:
			elements = RevenueManager.getInstance().getDisplayAds();
			customerViewId = R.layout.revenue_estimator_three_row;
			break;
		case 7:
			elements = RevenueManager.getInstance().getClickAds();	
			customerViewId = R.layout.revenue_estimator_four_row;
			break;
		case 8:
			elements = RevenueManager.getInstance().getLicensing();
			customerViewId = R.layout.revenue_estimator_two_row;
			break;
		default:
			break;
		}
	}
	
	public void addItem(CustomerSegmentRevenue item, ValuePropositionRevenue element) {
		if (!elements.contains(element)) {
			elements.add(element);
		}
		int index = elements.indexOf(element);
		ArrayList<CustomerSegmentRevenue> childs = elements.get(index).getCustomers();
		childs.add(item);
		elements.get(index).setCustomers(childs);
	}
	
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		ArrayList<CustomerSegmentRevenue> childList = elements.get(groupPosition).getCustomers();
		return childList.get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		CustomerSegmentRevenue item = (CustomerSegmentRevenue) getChild(groupPosition, childPosition);
		RevenueListener listener = new RevenueListener(groupPosition, childPosition, type);
		if(convertView == null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(customerViewId, null);
		}
		TextView label = (TextView) convertView.findViewById(R.id.rCustomerSegment);
		label.setText(item.getName());
		EditText marketShare = (EditText) convertView.findViewById(R.id.rMarketShare);
		marketShare.setText(item.getMarketShare() + "");
		marketShare.setOnFocusChangeListener(listener);
		Spinner purchasePeriod = (Spinner) convertView
				.findViewById(R.id.purchasePeriod);
		ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter
				.createFromResource(context, R.array.purchasePeriod,
						android.R.layout.simple_spinner_item);
		spinnerAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		purchasePeriod.setAdapter(spinnerAdapter);
		purchasePeriod.setSelection(getPurchasePeriod(item.getPurchasePeriod()));
		purchasePeriod.setOnItemSelectedListener(listener);
		EditText field_1 = (EditText) convertView.findViewById(R.id.field_1);
		EditText field_2 = (EditText) convertView.findViewById(R.id.field_2);
		EditText field_3 = (EditText) convertView.findViewById(R.id.field_3);
		EditText field_4 = (EditText) convertView.findViewById(R.id.field_4);
		switch (type) {
		case 0://Sales
			SalesItem sItem = (SalesItem) item;
			field_1.setText(sItem.getAveragePurchases() + "");
			field_1.setOnFocusChangeListener(listener);
			field_2.setText(sItem.getAveragePrice() + "");
			field_2.setOnFocusChangeListener(listener);
			break;
		case 1://Subscription
			SubscriptionItem subsItem = (SubscriptionItem) item;
			field_1.setText(subsItem.getSubscriptionPrice() + "");
			field_1.setOnFocusChangeListener(listener);
			break;
		case 2://PayPerUse
			PayPerUseItem ppuItem = (PayPerUseItem) item;
			field_1.setText(ppuItem.getAverageUnits() + "");
			field_1.setOnFocusChangeListener(listener);
			field_2.setText(ppuItem.getAveragePricePerUnit() + "");
			field_2.setOnFocusChangeListener(listener);
			break;
		case 3://Rental
			RentalItem rItem = (RentalItem) item;
			field_1.setText(rItem.getAverageUnitsRented() + "");
			field_1.setOnFocusChangeListener(listener);
			field_2.setText(rItem.getAverageRentalFeePerUnit() + "");
			field_2.setOnFocusChangeListener(listener);
			break;
		case 4://Transaction fee
			TransactionFeeItem tfItem = (TransactionFeeItem) item;
			field_1.setText(tfItem.getAverageTransactionPerCostumer() + "");
			field_1.setOnFocusChangeListener(listener);
			field_2.setText(tfItem.getAverageTransactionPrice() + "");
			field_2.setOnFocusChangeListener(listener);
			field_3.setText(tfItem.getAverageTransactionFee() + "");
			field_3.setOnFocusChangeListener(listener);
			break;
		case 5://Sales fee
			SalesFeeItem sfItem = (SalesFeeItem) item;
			field_1.setText(sfItem.getAveragePurchases() + "");
			field_1.setOnFocusChangeListener(listener);
			field_2.setText(sfItem.getAveragePrice() + "");
			field_2.setOnFocusChangeListener(listener);
			field_3.setText(sfItem.getSalesFee() + "");
			field_3.setOnFocusChangeListener(listener);
			break;
		case 6://Display ads
			DisplayAdsItem daItem = (DisplayAdsItem) item;
			field_1.setText(daItem.getAveragePageViewsPerUser() + "");
			field_1.setOnFocusChangeListener(listener);
			field_2.setText(daItem.getAverageAdUnitsPerPage() + "");
			field_2.setOnFocusChangeListener(listener);
			field_3.setText(daItem.getAverageCPMPerAdUnit() + "");
			field_3.setOnFocusChangeListener(listener);
			break;
		case 7://Click ads
			ClickAdsItem caItem = (ClickAdsItem) item;
			field_1.setText(caItem.getAveragePageViewPerUser() + "");
			field_1.setOnFocusChangeListener(listener);
			field_2.setText(caItem.getAverageAdsPerPage() + "");
			field_2.setOnFocusChangeListener(listener);
			field_3.setText(caItem.getAverageCTR() + "");
			field_3.setOnFocusChangeListener(listener);
			field_4.setText(caItem.getAverageCPC() + "");
			field_4.setOnFocusChangeListener(listener);
			break;
		case 8://Licensing
			LicensingRevenueItem lItem = (LicensingRevenueItem) item;
			field_1.setText(lItem.getAverageLicensesPerCostumer() + "");
			field_1.setOnFocusChangeListener(listener);
			field_2.setText(lItem.getAverageLicensePrice() + "");
			field_2.setOnFocusChangeListener(listener);
			break;
		}
		return convertView;
	}
	
	
	private int getPurchasePeriod(int value){		
		int spinnerPosition = 0;
		switch (value) {
		case 1:
			spinnerPosition = 0;
			break;
		case 2:
			spinnerPosition = 1; 
			break;
		case 4:
			spinnerPosition = 2; 
			break;
		case 6:
			spinnerPosition = 3;
			break;
		case 12:
			spinnerPosition = 4;
			break;
		case 52:
			spinnerPosition = 5;
			break;
		case 365:
			spinnerPosition = 6;
			break;
		default:
			spinnerPosition = 0;
			break;
		}
		
		return spinnerPosition;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		ArrayList<CustomerSegmentRevenue> childList = elements.get(groupPosition).getCustomers();
		return childList.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return elements.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return elements.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		ValuePropositionRevenue vp = (ValuePropositionRevenue) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater inf = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inf.inflate(R.layout.revenue_estimator_simple_row, null);
		}
		TextView groupLabel = (TextView) convertView
				.findViewById(R.id.rowTextView);
		groupLabel.setText(vp.getName());
		ExpandableListView eLV = (ExpandableListView) parent;
		eLV.expandGroup(groupPosition);
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}		

}
