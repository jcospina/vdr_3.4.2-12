package com.canvas.revenueestimator;

import java.util.ArrayList;

import android.content.Context;
import android.os.Message;

import com.canvas.dataaccess.ClickAdsItemDB;
import com.canvas.dataaccess.DisplayAdItemDB;
import com.canvas.dataaccess.LicensingItemDB;
import com.canvas.dataaccess.PayPerUseItemDB;
import com.canvas.dataaccess.PostItDB;
import com.canvas.dataaccess.RentalItemDB;
import com.canvas.dataaccess.RevenueItemDB;
import com.canvas.dataaccess.SalesFeeItemDB;
import com.canvas.dataaccess.SalesItemDB;
import com.canvas.dataaccess.SubscriptionItemDB;
import com.canvas.dataaccess.TransactionFeeItemDB;
import com.canvas.dataaccess.controller.ClickAdsItemController;
import com.canvas.dataaccess.controller.DisplayAdItemController;
import com.canvas.dataaccess.controller.LicensingItemController;
import com.canvas.dataaccess.controller.PayPerUseItemController;
import com.canvas.dataaccess.controller.PostitController;
import com.canvas.dataaccess.controller.RentalItemController;
import com.canvas.dataaccess.controller.RevenueItemController;
import com.canvas.dataaccess.controller.RevenuePostitController;
import com.canvas.dataaccess.controller.SalesFeeItemController;
import com.canvas.dataaccess.controller.SalesItemController;
import com.canvas.dataaccess.controller.SubscriptionItemController;
import com.canvas.dataaccess.controller.TransactionFeeItemController;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.MainScene;

public class PersistRevenue {

	private PostitController postitController;
	private RevenuePostitController rpController;
	private RevenueItemController riController;

	private SalesItemController siController;
	private SubscriptionItemController suiController;
	private PayPerUseItemController ppuController;
	private RentalItemController rController;
	private TransactionFeeItemController tfiController;
	private SalesFeeItemController sfiController;
	private DisplayAdItemController daiController;
	private ClickAdsItemController caiController;
	private LicensingItemController liController;

	private RevenueManager manager;
	private RevenueEstimator activity;

	public PersistRevenue(Context context, RevenueEstimator activity) {

		this.activity = activity;

		postitController = new PostitController(context);
		rpController = new RevenuePostitController(context);
		riController = new RevenueItemController(context);

		siController = new SalesItemController(context);
		suiController = new SubscriptionItemController(context);
		ppuController = new PayPerUseItemController(context);
		rController = new RentalItemController(context);
		tfiController = new TransactionFeeItemController(context);
		sfiController = new SalesFeeItemController(context);
		daiController = new DisplayAdItemController(context);
		caiController = new ClickAdsItemController(context);
		liController = new LicensingItemController(context);
	}

	public void saveData() {
		int indexInCollection = -1;
		if (RevenueManager.getInstance().revenuePostit
				.getRelatedDatabaseObject() == null) {
			PostItDB pdb = PostItDB.fromPostit(
					RevenueManager.getInstance().revenuePostit, -1,
					CanvasActivity.canvasId);
			pdb.setId((int) postitController.create(pdb));
			indexInCollection = MainScene.getInstance().revenues.postits
					.indexOf(RevenueManager.getInstance().revenuePostit);
			MainScene.getInstance().revenues.postits.get(indexInCollection)
					.setRelatedDatabaseObject(pdb);
			RevenueManager.getInstance().revenuePostit
					.setRelatedDatabaseObject(pdb);
		}
		indexInCollection = MainScene.getInstance().revenues.postits
				.indexOf(RevenueManager.getInstance().revenuePostit);
		MainScene.getInstance().revenues.postits.get(indexInCollection)
				.setRevenue(RevenueManager.getInstance().getTotalRevenue());
		long delete_index = RevenueManager.getInstance().revenuePostit
				.getPostitRelated().getId();
		long add_index = 0;
		RevenueManager.getInstance().revenuePostit.getPostitRelated()
				.setIdPostit(
						RevenueManager.getInstance().revenuePostit
								.getRelatedDatabaseObject().getId());
		RevenueManager.getInstance().revenuePostit.setPostitRelatedData();
		// if (index == -1) {
		// index = rpController
		// .create(RevenueManager.getInstance().revenuePostit
		// .getPostitRelated());
		// } else {
		//
		// rpController.update(RevenueManager.getInstance().revenuePostit
		// .getPostitRelated());
		// }
		if (delete_index == -1) {
			add_index = rpController
					.create(RevenueManager.getInstance().revenuePostit
							.getPostitRelated());
			riController.removeEntries((int) delete_index);
		} else {
			rpController.removeEntry((int) delete_index);
			riController.removeEntries((int) delete_index);
			add_index = rpController
					.create(RevenueManager.getInstance().revenuePostit
							.getPostitRelated());
		}
		manager = RevenueManager.getInstance();
		saveSales((int) add_index, (int) delete_index);
		saveSubscriptions((int) add_index, (int) delete_index);
		savePayPerUse((int) add_index, (int) delete_index);
		saveRentals((int) add_index, (int) delete_index);
		saveTransactionsFee((int) add_index, (int) delete_index);
		saveSalesFee((int) add_index, (int) delete_index);
		saveDisplayAds((int) add_index, (int) delete_index);
		saveClickAds((int) add_index, (int) delete_index);
		saveLicensing((int) add_index, (int) delete_index);

		Message m = new Message();
		m.arg1 = 3;
		activity.handler.sendMessage(m);
	}

	/**
	 * Guarda los datos de ventas
	 */
	private void saveSales(int index, int delete_index) {
		siController.removeEntries(delete_index);
		for (ValuePropositionRevenue vpr : manager.getSales()) {
			ArrayList<CustomerSegmentRevenue> sales = vpr.getCustomers();
			for (CustomerSegmentRevenue csr : sales) {
				SalesItem sale = (SalesItem) csr;
				long revenueItemIndex = riController.create(new RevenueItemDB(
						-1, csr.getName(), csr.getMarketShare(), csr
								.getPurchasePeriod(), (int) index, 0, vpr
								.getName(), csr.getRelatedItemId()));
				siController.create(new SalesItemDB(-1, sale
						.getAveragePurchases(), sale.getAveragePrice(), index,
						(int) revenueItemIndex));
			}
		}
	}

	/**
	 * Guarda los datos de subscripciones
	 */
	private void saveSubscriptions(int index, int delete_index) {
		suiController.removeEntries(delete_index);
		for (ValuePropositionRevenue vpr : manager.getSubscriptions()) {
			ArrayList<CustomerSegmentRevenue> subscriptions = vpr
					.getCustomers();
			for (CustomerSegmentRevenue csr : subscriptions) {
				SubscriptionItem subs = (SubscriptionItem) csr;
				int revenueItemIndex = (int) riController
						.create(new RevenueItemDB(-1, csr.getName(), csr
								.getMarketShare(), csr.getPurchasePeriod(),
								(int) index, 1, vpr.getName(), csr
										.getRelatedItemId()));
				suiController
						.create(new SubscriptionItemDB(-1, subs
								.getSubscriptionPrice(), index,
								(int) revenueItemIndex));
			}
		}
	}

	/**
	 * Guarda los datos de pago por uso
	 */
	private void savePayPerUse(int index, int delete_index) {
		ppuController.removeEntries(delete_index);
		for (ValuePropositionRevenue vpr : manager.getPayPerUse()) {
			ArrayList<CustomerSegmentRevenue> payperuse = vpr.getCustomers();
			for (CustomerSegmentRevenue csr : payperuse) {
				PayPerUseItem ppu = (PayPerUseItem) csr;
				int revenueItemIndex = (int) riController
						.create(new RevenueItemDB(-1, csr.getName(), csr
								.getMarketShare(), csr.getPurchasePeriod(),
								(int) index, 2, vpr.getName(), csr
										.getRelatedItemId()));
				ppuController.create(new PayPerUseItemDB(-1, ppu
						.getAverageUnits(), ppu.getAveragePricePerUnit(),
						index, revenueItemIndex));
			}
		}
	}

	/**
	 * Guarda los datos de alquiler
	 */
	private void saveRentals(int index, int delete_index) {
		rController.removeEntries(delete_index);
		for (ValuePropositionRevenue vpr : manager.getRentals()) {
			ArrayList<CustomerSegmentRevenue> payperuse = vpr.getCustomers();
			for (CustomerSegmentRevenue csr : payperuse) {
				RentalItem ri = (RentalItem) csr;
				int revenueItemIndex = (int) riController
						.create(new RevenueItemDB(-1, csr.getName(), csr
								.getMarketShare(), csr.getPurchasePeriod(),
								(int) index, 3, vpr.getName(), csr
										.getRelatedItemId()));
				rController
						.create(new RentalItemDB(-1,
								ri.getAverageUnitsRented(), ri
										.getAverageRentalFeePerUnit(), index,
								revenueItemIndex));
			}
		}
	}

	/**
	 * Guarda los datos de transacciones
	 */
	private void saveTransactionsFee(int index, int delete_index) {
		tfiController.removeEntries(delete_index);
		for (ValuePropositionRevenue vpr : manager.getTransactionFees()) {
			ArrayList<CustomerSegmentRevenue> transfees = vpr.getCustomers();
			for (CustomerSegmentRevenue csr : transfees) {
				TransactionFeeItem tf = (TransactionFeeItem) csr;
				int revenueItemIndex = (int) riController
						.create(new RevenueItemDB(-1, csr.getName(), csr
								.getMarketShare(), csr.getPurchasePeriod(),
								(int) index, 4, vpr.getName(), csr
										.getRelatedItemId()));
				tfiController.create(new TransactionFeeItemDB(-1, tf
						.getAverageTransactionPerCostumer(), tf
						.getAverageTransactionPrice(), tf
						.getAverageTransactionFee(), index, revenueItemIndex));
			}
		}
	}

	/**
	 * Guarda los datos de porcentaje por ventas
	 */
	private void saveSalesFee(int index, int delete_index) {
		sfiController.removeEntries(delete_index);
		for (ValuePropositionRevenue vpr : manager.getSalesFees()) {
			ArrayList<CustomerSegmentRevenue> salesfees = vpr.getCustomers();
			for (CustomerSegmentRevenue csr : salesfees) {
				SalesFeeItem sf = (SalesFeeItem) csr;
				int revenueItemIndex = (int) riController
						.create(new RevenueItemDB(-1, csr.getName(), csr
								.getMarketShare(), csr.getPurchasePeriod(),
								(int) index, 5, vpr.getName(), csr
										.getRelatedItemId()));
				sfiController.create(new SalesFeeItemDB(-1, sf
						.getAveragePurchases(), sf.getAveragePrice(), sf
						.getSalesFee(), index, revenueItemIndex));
			}
		}
	}

	/**
	 * Guarda los datos de mostrar publicidad
	 */
	private void saveDisplayAds(int index, int delete_index) {
		daiController.removeEntries(delete_index);
		for (ValuePropositionRevenue vpr : manager.getDisplayAds()) {
			ArrayList<CustomerSegmentRevenue> salesfees = vpr.getCustomers();
			for (CustomerSegmentRevenue csr : salesfees) {
				DisplayAdsItem dai = (DisplayAdsItem) csr;
				int revenueItemIndex = (int) riController
						.create(new RevenueItemDB(-1, csr.getName(), csr
								.getMarketShare(), csr.getPurchasePeriod(),
								(int) index, 6, vpr.getName(), csr
										.getRelatedItemId()));
				daiController.create(new DisplayAdItemDB(-1, dai
						.getAveragePageViewsPerUser(), dai
						.getAverageAdUnitsPerPage(), dai
						.getAverageCPMPerAdUnit(), index, revenueItemIndex));
			}
		}
	}

	/**
	 * Guarda los datos de dar click en publicidad
	 */
	private void saveClickAds(int index, int delete_index) {
		caiController.removeEntries(delete_index);
		for (ValuePropositionRevenue vpr : manager.getClickAds()) {
			ArrayList<CustomerSegmentRevenue> clickAds = vpr.getCustomers();
			for (CustomerSegmentRevenue csr : clickAds) {
				ClickAdsItem cai = (ClickAdsItem) csr;
				int revenueItemIndex = (int) riController
						.create(new RevenueItemDB(-1, csr.getName(), csr
								.getMarketShare(), csr.getPurchasePeriod(),
								(int) index, 7, vpr.getName(), csr
										.getRelatedItemId()));
				caiController.create(new ClickAdsItemDB(-1, cai
						.getAveragePageViewPerUser(), cai
						.getAverageAdsPerPage(), cai.getAverageCTR(), cai
						.getAverageCPC(), index, revenueItemIndex));
			}
		}
	}

	/**
	 * Guarda los datos de licencias
	 */
	private void saveLicensing(int index, int delete_index) {
		liController.removeEntries(delete_index);
		for (ValuePropositionRevenue vpr : manager.getLicensing()) {
			ArrayList<CustomerSegmentRevenue> license = vpr.getCustomers();
			for (CustomerSegmentRevenue csr : license) {
				LicensingRevenueItem li = (LicensingRevenueItem) csr;
				int revenueItemIndex = (int) riController
						.create(new RevenueItemDB(-1, csr.getName(), csr
								.getMarketShare(), csr.getPurchasePeriod(),
								(int) index, 8, vpr.getName(), csr
										.getRelatedItemId()));
				liController.create(new LicensingItemDB(-1, li
						.getAverageLicensesPerCostumer(), li
						.getAverageLicensePrice(), index, revenueItemIndex));
			}
		}
	}
}
