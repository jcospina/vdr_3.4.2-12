package com.canvas.revenueestimator;

public class SalesFeeItem extends SalesItem{		

	private double salesFee = 0;		
	
	public SalesFeeItem() {
		super();
	}

	public SalesFeeItem(String name, int customerCount, double marketShare,
			int purchasePeriod, int relatedItemId, double averagePurchases, double averagePrice, double salesFee) {
		super(name, customerCount, marketShare, purchasePeriod, relatedItemId, averagePurchases,
				averagePrice);
		this.salesFee = salesFee;
	}

	public void setSalesFee(double salesFee) {
		this.salesFee = salesFee;
	}
	
	public double getSalesFee() {
		return salesFee;
	}
	
	@Override
	public double calculateSubTotal() {
		return super.calculateSubTotal()*getSalesFee();
	}

}
