package com.canvas.revenueestimator;

import java.util.ArrayList;
import java.util.Iterator;

public class ValuePropositionRevenue {
	
	private ArrayList<CustomerSegmentRevenue> customers;
	private String name;
	private int id;
	
	public ValuePropositionRevenue(String name, int id, ArrayList<CustomerSegmentRevenue> customers) {
		this.name = name;
		this.id = id;
		this.customers = customers;
	}
	
	public double calculateValuePropositionCost(){
		double totalCost = 0;
		Iterator<CustomerSegmentRevenue> iterator = customers.iterator();
		while(iterator.hasNext()){
			CustomerSegmentRevenue current = iterator.next();
			totalCost += current.calculateSubTotal();
		}
		return totalCost;
	}
	
	public void setCustomers(ArrayList<CustomerSegmentRevenue> customers) {
		this.customers = customers;
	}
	
	public ArrayList<CustomerSegmentRevenue> getCustomers() {
		return customers;
	}
	
	public String getName() {
		return name;
	}
	
	public int getId() {
		return id;
	}
	
}
