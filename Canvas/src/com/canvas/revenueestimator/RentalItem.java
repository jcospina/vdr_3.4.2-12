package com.canvas.revenueestimator;

public class RentalItem extends CustomerSegmentRevenue {

	private double averageUnitsRented = 0;
	private double averageRentalFeePerUnit = 0;

	public RentalItem() {
		super();
	}

	public RentalItem(String name, int customerCount, double marketShare,
			int purchasePeriod, int relatedItemId, double averageUnitsRented,
			double averageRentalFeePerUnit) {
		super(name, customerCount, marketShare, purchasePeriod, relatedItemId);
		this.averageUnitsRented = averageUnitsRented;
		this.averageRentalFeePerUnit = averageRentalFeePerUnit;
	}

	public double getAverageUnitsRented() {
		return averageUnitsRented;
	}

	public void setAverageUnitsRented(double averageUnitsRented) {
		this.averageUnitsRented = averageUnitsRented;
	}

	public double getAverageRentalFeePerUnit() {
		return averageRentalFeePerUnit;
	}

	public void setAverageRentalFeePerUnit(double averageRentalFeePerUnit) {
		this.averageRentalFeePerUnit = averageRentalFeePerUnit;
	}

	@Override
	public double calculateSubTotal() {
		return getMarketShare() * getPurchasePeriod()
				* getAverageRentalFeePerUnit() * getAverageUnitsRented();
	}

}
