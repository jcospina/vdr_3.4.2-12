package com.canvas.revenueestimator;

public class SalesItem extends CustomerSegmentRevenue {

	private double averagePurchases = 0;
	private double averagePrice = 0;

	public SalesItem() {
		super();
	}

	public SalesItem(String name, int customerCount, double marketShare,
			int purchasePeriod, int relatedItemId, double averagePurchases, double averagePrice) {
		super(name, customerCount, marketShare, purchasePeriod, relatedItemId);
		this.averagePurchases = averagePurchases;
		this.averagePrice = averagePrice;
	}

	public double getAveragePurchases() {
		return averagePurchases;
	}

	public void setAveragePurchases(double averagePurchases) {
		this.averagePurchases = averagePurchases;
	}

	public double getAveragePrice() {
		return averagePrice;
	}

	public void setAveragePrice(double averagePrice) {
		this.averagePrice = averagePrice;
	}

	@Override
	public double calculateSubTotal() {
		return getMarketShare() * getAveragePurchases() * getAveragePrice()
				* getPurchasePeriod();
	}

}
