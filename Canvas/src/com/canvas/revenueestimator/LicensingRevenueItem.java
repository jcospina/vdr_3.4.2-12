package com.canvas.revenueestimator;

public class LicensingRevenueItem extends CustomerSegmentRevenue {

	private float averageLicensesPerCostumer = 0;
	private float averageLicensePrice = 0;

	public LicensingRevenueItem() {
		super();
	}

	public LicensingRevenueItem(String name, int customerCount,
			double marketShare, int purchasePeriod, int relatedItemId,
			float averageLicensesPerCostumer, float averageLicensePrice) {
		super(name, customerCount, marketShare, purchasePeriod, relatedItemId);
		this.averageLicensesPerCostumer = averageLicensesPerCostumer;
		this.averageLicensePrice = averageLicensePrice;
	}

	public float getAverageLicensesPerCostumer() {
		return averageLicensesPerCostumer;
	}

	public void setAverageLicensesPerCostumer(float averageLicensesPerCostumer) {
		this.averageLicensesPerCostumer = averageLicensesPerCostumer;
	}

	public float getAverageLicensePrice() {
		return averageLicensePrice;
	}

	public void setAverageLicensePrice(float averageLicensePrice) {
		this.averageLicensePrice = averageLicensePrice;
	}

	@Override
	public double calculateSubTotal() {
		return getMarketShare() * getPurchasePeriod()
				* getAverageLicensesPerCostumer() * getAverageLicensePrice();
	}

}
