package com.canvas.revenueestimator;

import java.util.ArrayList;
import java.util.Iterator;

import android.os.Message;

import com.canvas.dataaccess.controller.RevenuePostitController;
import com.canvas.vo.RevenuePostitVO;
import com.smartsoft.Educanvas.CanvasManager;

public class RevenueManager {

	private static RevenueManager instance;
	private RevenueEstimator activity;
	private ArrayList<ValuePropositionRevenue> sales;
	public double actualSalesRevenue = 0;
	private ArrayList<ValuePropositionRevenue> subscriptions;
	public double actualSubscriptionRevenue = 0;
	private ArrayList<ValuePropositionRevenue> payPerUse;
	public double actualPayPerUseRevenue = 0;
	private ArrayList<ValuePropositionRevenue> rentals;
	public double actualRentalRevenue = 0;
	private ArrayList<ValuePropositionRevenue> transactionFees;
	public double actualTransFeeRevenue = 0;
	private ArrayList<ValuePropositionRevenue> salesFees;
	public double actualSalesFeeRevenue = 0;
	private ArrayList<ValuePropositionRevenue> displayAds;
	public double actualDisplayAdsRevenue = 0;
	private ArrayList<ValuePropositionRevenue> clickAds;
	public double actualClickAdsRevenue = 0;
	private ArrayList<ValuePropositionRevenue> licensing;
	public double actualLicensingRevenue = 0;
	public double actualOtherRevenue;
	public double totalRevenue;

	public RevenuePostitVO revenuePostit;

	public static RevenueManager getInstance() {
		if (instance == null) {
			instance = new RevenueManager();
		}
		return instance;
	}

	public void setActivity(RevenueEstimator activity) {
		this.activity = activity;
	}

	public void setRevenuePostit(RevenuePostitVO revenuePostit) {
		instance = new RevenueManager();
		instance.revenuePostit = revenuePostit;
	}

	public void setInitialData(double totalRevenue, double otherRevenue,
			double sales, double subscription, double payPerUse, double rental,
			double transFee, double salesFee, double displayAds,
			double clickAds, double licensing) {
		this.totalRevenue = totalRevenue;
		this.actualOtherRevenue = otherRevenue;
		this.actualSalesRevenue = sales;
		this.actualSubscriptionRevenue = subscription;
		this.actualPayPerUseRevenue = payPerUse;
		this.actualRentalRevenue = rental;
		this.actualTransFeeRevenue = transFee;
		this.actualSalesFeeRevenue = salesFee;
		this.actualDisplayAdsRevenue = displayAds;
		this.actualClickAdsRevenue = clickAds;
		this.actualLicensingRevenue = licensing;
	}

	public void setData(ArrayList<ValuePropositionRevenue> sales,
			ArrayList<ValuePropositionRevenue> subscriptions,
			ArrayList<ValuePropositionRevenue> payPerUse,
			ArrayList<ValuePropositionRevenue> rentals,
			ArrayList<ValuePropositionRevenue> transactionFees,
			ArrayList<ValuePropositionRevenue> salesFees,
			ArrayList<ValuePropositionRevenue> displayAds,
			ArrayList<ValuePropositionRevenue> clickAds,
			ArrayList<ValuePropositionRevenue> licensing) {
		this.sales = sales;
		this.subscriptions = subscriptions;
		this.payPerUse = payPerUse;
		this.rentals = rentals;
		this.transactionFees = transactionFees;
		this.salesFees = salesFees;
		this.displayAds = displayAds;
		this.clickAds = clickAds;
		this.licensing = licensing;
	}

	public void setValues() {
		actualSalesRevenue = returnTotal(sales.iterator());
		actualSubscriptionRevenue = returnTotal(subscriptions.iterator());
		actualPayPerUseRevenue = returnTotal(payPerUse.iterator());
		actualRentalRevenue = returnTotal(rentals.iterator());
		actualTransFeeRevenue = returnTotal(transactionFees.iterator());
		actualSalesFeeRevenue = returnTotal(salesFees.iterator());
		actualDisplayAdsRevenue = returnTotal(displayAds.iterator());
		actualClickAdsRevenue = returnTotal(clickAds.iterator());
		actualLicensingRevenue = returnTotal(licensing.iterator());
	}

	public void calculateOtherRevenue(double value) {
		double difference = value - actualOtherRevenue;
		actualOtherRevenue = value;
		sendMessage(difference, 0, -1);
	}

	public void calculateCost(int type) {
		double total = 0;
		double difference = 0;
		Iterator<ValuePropositionRevenue> iterator;
		switch (type) {
		case 0: // Sales
			iterator = sales.iterator();
			total = returnTotal(iterator);
			difference = total - actualSalesRevenue;
			actualSalesRevenue = total;
			break;
		case 1: // Subscriptiom
			iterator = subscriptions.iterator();
			total = returnTotal(iterator);
			difference = total - actualSubscriptionRevenue;
			actualSubscriptionRevenue = total;
			break;
		case 2:// Pay per use
			iterator = payPerUse.iterator();
			total = returnTotal(iterator);
			difference = total - actualPayPerUseRevenue;
			actualPayPerUseRevenue = total;
			break;
		case 3:// Rental
			iterator = rentals.iterator();
			total = returnTotal(iterator);
			difference = total - actualRentalRevenue;
			actualRentalRevenue = total;
			break;
		case 4:// Transaction fee
			iterator = transactionFees.iterator();
			total = returnTotal(iterator);
			difference = total - actualTransFeeRevenue;
			actualTransFeeRevenue = total;
			break;
		case 5:// Sales fee
			iterator = salesFees.iterator();
			total = returnTotal(iterator);
			difference = total - actualSalesFeeRevenue;
			actualSalesFeeRevenue = total;
			break;
		case 6:// Display Ads
			iterator = displayAds.iterator();
			total = returnTotal(iterator);
			difference = total - actualDisplayAdsRevenue;
			actualDisplayAdsRevenue = total;
			break;
		case 7:// Clicks ads
			iterator = clickAds.iterator();
			total = returnTotal(iterator);
			difference = total - actualClickAdsRevenue;
			actualClickAdsRevenue = total;
			break;
		case 8:// Licensing
			iterator = licensing.iterator();
			total = returnTotal(iterator);
			difference = total - actualLicensingRevenue;
			actualLicensingRevenue = total;
			break;
		}
		sendMessage(difference, 0, -1);
	}

	private double returnTotal(Iterator<ValuePropositionRevenue> it) {
		double total = 0;
		while (it.hasNext()) {
			total += it.next().calculateValuePropositionCost();
		}
		return total;
	}

	private void sendMessage(Object obj, int arg1, int arg2) {
		Message m = new Message();
		m.arg1 = arg1;
		m.arg2 = arg2;
		m.obj = obj;
		activity.handler.sendMessage(m);
	}

	public void clearRevenues() {
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				sales.clear();
				subscriptions.clear();
				payPerUse.clear();
				rentals.clear();
				transactionFees.clear();
				salesFees.clear();
				displayAds.clear();
				clickAds.clear();
				licensing.clear();
				if (RevenueManager.getInstance().revenuePostit
						.getRelatedDatabaseObject() != null) {
					int index_items = RevenueManager.getInstance().revenuePostit
							.getRelatedDatabaseObject().getId();

					actualClickAdsRevenue = 0;
					actualDisplayAdsRevenue = 0;
					actualLicensingRevenue = 0;
					actualOtherRevenue = 0;
					actualPayPerUseRevenue = 0;
					actualRentalRevenue = 0;
					actualSalesFeeRevenue = 0;
					actualSalesRevenue = 0;
					actualSubscriptionRevenue = 0;
					actualTransFeeRevenue = 0;

					RevenuePostitController rpController = new RevenuePostitController(
							CanvasManager.getInstance().context);
					rpController.removeAllByPostitId(index_items);

				}
				Message m = new Message();
				m.arg1 = 2;
				activity.handler.sendMessage(m);
			}
		});
		t.start();
	}

	public void setTotalRevenue(double totalRevenue) {
		this.totalRevenue = totalRevenue;
	}

	public double getTotalRevenue() {
		return totalRevenue;
	}

	public ArrayList<ValuePropositionRevenue> getSales() {
		return sales;
	}

	public void setSales(ArrayList<ValuePropositionRevenue> sales) {
		this.sales = sales;
	}

	public ArrayList<ValuePropositionRevenue> getSubscriptions() {
		return subscriptions;
	}

	public void setSubscriptions(
			ArrayList<ValuePropositionRevenue> subscriptions) {
		this.subscriptions = subscriptions;
	}

	public ArrayList<ValuePropositionRevenue> getPayPerUse() {
		return payPerUse;
	}

	public void setPayPerUse(ArrayList<ValuePropositionRevenue> payPerUse) {
		this.payPerUse = payPerUse;
	}

	public ArrayList<ValuePropositionRevenue> getRentals() {
		return rentals;
	}

	public void setRentals(ArrayList<ValuePropositionRevenue> rentals) {
		this.rentals = rentals;
	}

	public ArrayList<ValuePropositionRevenue> getTransactionFees() {
		return transactionFees;
	}

	public void setTransactionFees(
			ArrayList<ValuePropositionRevenue> transactionFees) {
		this.transactionFees = transactionFees;
	}

	public ArrayList<ValuePropositionRevenue> getSalesFees() {
		return salesFees;
	}

	public void setSalesFees(ArrayList<ValuePropositionRevenue> salesFees) {
		this.salesFees = salesFees;
	}

	public ArrayList<ValuePropositionRevenue> getDisplayAds() {
		return displayAds;
	}

	public void setDisplayAds(ArrayList<ValuePropositionRevenue> displayAds) {
		this.displayAds = displayAds;
	}

	public ArrayList<ValuePropositionRevenue> getClickAds() {
		return clickAds;
	}

	public void setClickAds(ArrayList<ValuePropositionRevenue> clickAds) {
		this.clickAds = clickAds;
	}

	public ArrayList<ValuePropositionRevenue> getLicensing() {
		return licensing;
	}

	public void setLicensing(ArrayList<ValuePropositionRevenue> licensing) {
		this.licensing = licensing;
	}

	public double getTotalSalesRevenue() {
		Iterator<ValuePropositionRevenue> iterator = sales.iterator();
		return returnTotal(iterator);
	}

	public double getTotalSubscriptionRevenue() {
		Iterator<ValuePropositionRevenue> iterator = subscriptions.iterator();
		return returnTotal(iterator);
	}

	public double getTotalPayPerUseRevenue() {
		Iterator<ValuePropositionRevenue> iterator = payPerUse.iterator();
		return returnTotal(iterator);
	}

	public double getTotalRentalRevenue() {
		Iterator<ValuePropositionRevenue> iterator = rentals.iterator();
		return returnTotal(iterator);
	}

	public double getTotalTransactionFeeRevenue() {
		Iterator<ValuePropositionRevenue> iterator = transactionFees.iterator();
		return returnTotal(iterator);
	}

	public double getTotalSalesFeeRevenue() {
		Iterator<ValuePropositionRevenue> iterator = salesFees.iterator();
		return returnTotal(iterator);
	}

	public double getTotalDisplayAdsRevenue() {
		Iterator<ValuePropositionRevenue> iterator = displayAds.iterator();
		return returnTotal(iterator);
	}

	public double getTotalClickAdsRevenue() {
		Iterator<ValuePropositionRevenue> iterator = clickAds.iterator();
		return returnTotal(iterator);
	}

	public double getTotalLicensingRevenue() {
		Iterator<ValuePropositionRevenue> iterator = licensing.iterator();
		return returnTotal(iterator);
	}

}
