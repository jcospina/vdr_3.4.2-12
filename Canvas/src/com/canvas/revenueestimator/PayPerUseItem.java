package com.canvas.revenueestimator;

public class PayPerUseItem extends CustomerSegmentRevenue {

	private double averageUnits = 0;
	private double averagePricePerUnit = 0;

	public PayPerUseItem() {
		super();
	}

	public PayPerUseItem(String name, int customerCount, double marketShare,
			int purchasePeriod, int relatedItemId, double averageUnits,
			double averagePricePerUnit) {
		super(name, customerCount, marketShare, purchasePeriod, relatedItemId);
		this.averageUnits = averageUnits;
		this.averagePricePerUnit = averagePricePerUnit;
	}

	public void setAverageUnits(double averageUnits) {
		this.averageUnits = averageUnits;
	}

	public double getAverageUnits() {
		return averageUnits;
	}

	public void setAveragePricePerUnit(double averagePricePerUnit) {
		this.averagePricePerUnit = averagePricePerUnit;
	}

	public double getAveragePricePerUnit() {
		return averagePricePerUnit;
	}

	@Override
	public double calculateSubTotal() {
		return getMarketShare() * getPurchasePeriod() * getAverageUnits()
				* getAveragePricePerUnit();
	}

}
