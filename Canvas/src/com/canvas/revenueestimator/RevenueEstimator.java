package com.canvas.revenueestimator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.canvas.dataaccess.ClickAdsItemDB;
import com.canvas.dataaccess.DisplayAdItemDB;
import com.canvas.dataaccess.LicensingItemDB;
import com.canvas.dataaccess.PayPerUseItemDB;
import com.canvas.dataaccess.PostItDB;
import com.canvas.dataaccess.RentalItemDB;
import com.canvas.dataaccess.RevenueItemDB;
import com.canvas.dataaccess.RevenuePostitDB;
import com.canvas.dataaccess.SalesFeeItemDB;
import com.canvas.dataaccess.SalesItemDB;
import com.canvas.dataaccess.SubscriptionItemDB;
import com.canvas.dataaccess.TransactionFeeItemDB;
import com.canvas.dataaccess.controller.ClickAdsItemController;
import com.canvas.dataaccess.controller.DisplayAdItemController;
import com.canvas.dataaccess.controller.LicensingItemController;
import com.canvas.dataaccess.controller.PayPerUseItemController;
import com.canvas.dataaccess.controller.PostitController;
import com.canvas.dataaccess.controller.RentalItemController;
import com.canvas.dataaccess.controller.RevenueItemController;
import com.canvas.dataaccess.controller.RevenuePostitController;
import com.canvas.dataaccess.controller.SalesFeeItemController;
import com.canvas.dataaccess.controller.SalesItemController;
import com.canvas.dataaccess.controller.SubscriptionItemController;
import com.canvas.dataaccess.controller.TransactionFeeItemController;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.CanvasManager;
import com.smartsoft.Educanvas.MainScene;
import com.smartsoft.Educanvas.PostIt;
import com.smartsoft.Educanvas.R;
import com.smartsoft.Educanvas.SceneManager;

public class RevenueEstimator extends Activity implements OnItemClickListener,
		View.OnClickListener {

	private ListView revenueTypesList;
	private ViewFlipper flipper;
	private ArrayAdapter<String> listAdapter;
	private ImageButton saveButton; // Bot�n para guardar los datos
	private ImageButton clearButton; // Bot�n para limpiar los datos
	private EditText otherRevenues;
	private TextView totalRevenue;
	private TextView revenueTitle;

	private ExpandableListView salesList;
	private ExpandableListView subscriptionList;
	private ExpandableListView payPerUseList;
	private ExpandableListView rentalList;
	private ExpandableListView transaccionFeeList;
	private ExpandableListView salesFee;
	private ExpandableListView displayAdsList;
	private ExpandableListView clickAdsList;
	private ExpandableListView licensingList;
	private ProgressDialog savingDialog;

	private PostitController pc;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.revenue_estimator);

		fillValueProposition();
		// Obtiene la referencia a los botones de guardar y limpiar
		RelativeLayout tools = (RelativeLayout) findViewById(R.id.tools);
		saveButton = (ImageButton) tools.findViewById(R.id.saveButton);
		clearButton = (ImageButton) tools.findViewById(R.id.clear);
		// Fija el listener para los botones
		saveButton.setOnClickListener(this);
		clearButton.setOnClickListener(this);
		// Llena la lista de tipo de ingresos
		revenueTypesList = (ListView) findViewById(R.id.revenueType);
		ArrayList<String> types = new ArrayList<String>();
		types.addAll(Arrays.asList(getResources().getStringArray(
				R.array.revenueTypesArray)));
		listAdapter = new ArrayAdapter<String>(this,
				R.layout.revenue_estimator_simple_row, types);
		revenueTypesList.setAdapter(listAdapter);
		revenueTypesList.setSelection(0);
		revenueTypesList.setOnItemClickListener(this);
		// Obtiene el flipper el cual permite cambiar las vistas para cada tipo
		// de ingreso
		flipper = (ViewFlipper) findViewById(R.id.revenueFlipper);
		flipper.setInAnimation(this, R.anim.push_left_in);
		flipper.setOutAnimation(this, R.anim.push_left_out);
		// Referencia a las listas
		salesList = (ExpandableListView) findViewById(R.id.salesList);
		subscriptionList = (ExpandableListView) findViewById(R.id.subscriptionList);
		payPerUseList = (ExpandableListView) findViewById(R.id.payperuseList);
		rentalList = (ExpandableListView) findViewById(R.id.rentalList);
		transaccionFeeList = (ExpandableListView) findViewById(R.id.transactionFeeList);
		salesFee = (ExpandableListView) findViewById(R.id.salesFeeList);
		displayAdsList = (ExpandableListView) findViewById(R.id.displayAdsList);
		clickAdsList = (ExpandableListView) findViewById(R.id.clickAdsList);
		licensingList = (ExpandableListView) findViewById(R.id.licensingList);
		// Fija adaptadores por las listas

		salesList.setAdapter(new ComposedRowAdapter(this, 0));
		subscriptionList.setAdapter(new ComposedRowAdapter(this, 1));
		payPerUseList.setAdapter(new ComposedRowAdapter(this, 2));
		rentalList.setAdapter(new ComposedRowAdapter(this, 3));
		transaccionFeeList.setAdapter(new ComposedRowAdapter(this, 4));
		salesFee.setAdapter(new ComposedRowAdapter(this, 5));
		displayAdsList.setAdapter(new ComposedRowAdapter(this, 6));
		clickAdsList.setAdapter(new ComposedRowAdapter(this, 7));
		licensingList.setAdapter(new ComposedRowAdapter(this, 8));
		// Otros ingresos
		otherRevenues = (EditText) findViewById(R.id.otherRevenues);
		otherRevenues.setText(RevenueManager.getInstance().actualOtherRevenue
				+ "");
		totalRevenue = (TextView) findViewById(R.id.revenueValue);
		totalRevenue.setText("$ " + RevenueManager.getInstance().totalRevenue);
		// otherCosts.setText(CostManager.getInstace().actualOtherCost + "");
		otherRevenues.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					EditText origin = (EditText) v;
					String text = "0";
					if (origin.getText().toString() != null) {
						text = origin.getText().toString();
					}
					if (!text.isEmpty()) {
						double value = Double.parseDouble(text);
						RevenueManager.getInstance().calculateOtherRevenue(
								value);
					}
				}

			}
		});
		revenueTitle = (TextView) findViewById(R.id.revenueTitle);
		revenueTitle.setText(RevenueManager.getInstance().revenuePostit
				.getTitle());
		RevenueManager.getInstance().setActivity(this);
	}

	private void fillValueProposition() {
		// updateDatabase();
		pc = new PostitController(getApplicationContext());

		List<String> vpNames = getPostItTitles(pc.getByIdCanvasAndBlock(
				CanvasActivity.canvasId, 3));
		List<Customer> customerSegments = new ArrayList<RevenueEstimator.Customer>();
		for (PostItDB c : pc.getByIdCanvasAndBlock(CanvasActivity.canvasId, 6)) {
			customerSegments.add(new Customer(c.getTitle(), 0, c.getId()));
		}

		RevenuePostitController rpc = new RevenuePostitController(
				getApplicationContext());
		RevenuePostitDB rpdb = null;
		int index = -1;
		if (RevenueManager.getInstance().revenuePostit
				.getRelatedDatabaseObject() != null) {
			index = RevenueManager.getInstance().revenuePostit
					.getRelatedDatabaseObject().getId();
			rpdb = rpc.getByPostitId(index);
		}
		if (rpdb == null) {
			rpdb = new RevenuePostitDB(-1, 0, 0, index, 0, 0, 0, 0, 0, 0, 0, 0,
					0);
		}
		// Fija los datos iniciales
		RevenueManager.getInstance().setInitialData(rpdb.getRevenue(),
				rpdb.getOtherRevenue(), rpdb.getSalesRevenues(),
				rpdb.getSubscriptionRevenue(), rpdb.getPayPerUseRevenue(),
				rpdb.getRentalRevenue(), rpdb.getTransFeeRevenue(),
				rpdb.getSalesFeeRevenue(), rpdb.getDisplayAdsRevenue(),
				rpdb.getClickAdsRevenue(), rpdb.getLicensingRevenue());
		RevenueManager.getInstance().revenuePostit.setPostitRelated(rpdb);
		RevenueItemController ric = new RevenueItemController(
				getApplicationContext());
		// Datos ventas
		ArrayList<ValuePropositionRevenue> salesData = getSalesItems(vpNames,
				customerSegments, ric, rpdb);
		// Datos subscripci�n
		ArrayList<ValuePropositionRevenue> subscriptionData = getSubscriptionItems(
				vpNames, customerSegments, ric, rpdb);
		// Datos pago por uso
		ArrayList<ValuePropositionRevenue> payPerUseData = getPayPerUseData(
				vpNames, customerSegments, ric, rpdb);
		// Datos alquiler
		ArrayList<ValuePropositionRevenue> rentalData = getRentalData(vpNames,
				customerSegments, ric, rpdb);
		// Datos porcentaje por transacci�n
		ArrayList<ValuePropositionRevenue> transFeeData = getTransFeeData(
				vpNames, customerSegments, ric, rpdb);
		// Datos porcentaje por ventas
		ArrayList<ValuePropositionRevenue> salesFeeData = getSalesFeeData(
				vpNames, customerSegments, ric, rpdb);
		// Datos desplegar publicidad
		ArrayList<ValuePropositionRevenue> displayAdsData = getDisplayAdsData(
				vpNames, customerSegments, ric, rpdb);
		// Datos click en publicidad
		ArrayList<ValuePropositionRevenue> clickAdsData = getClickAdsData(
				vpNames, customerSegments, ric, rpdb);
		// Datos licenciamiento
		ArrayList<ValuePropositionRevenue> licensingData = getLicensingData(
				vpNames, customerSegments, ric, rpdb);

		RevenueManager.getInstance().setData(salesData, subscriptionData,
				payPerUseData, rentalData, transFeeData, salesFeeData,
				displayAdsData, clickAdsData, licensingData);
		RevenueManager.getInstance().setValues();
	}

	// Datos ventas
	private ArrayList<ValuePropositionRevenue> getSalesItems(
			List<String> vpNames, List<Customer> customerSegments,
			RevenueItemController ric, RevenuePostitDB rpdb) {
		int i = 0;
		ArrayList<ValuePropositionRevenue> salesData = new ArrayList<ValuePropositionRevenue>();

		ArrayList<RevenueItemDB> ridb = (ArrayList<RevenueItemDB>) ric
				.getByRevenuePostitIdAndType(rpdb.getId(), 0);
		if (ridb.isEmpty()) {
			for (String vp : vpNames) {
				ArrayList<CustomerSegmentRevenue> items = new ArrayList<CustomerSegmentRevenue>();
				for (Customer c : customerSegments) {
					items.add(new SalesItem(c.title, c.count, 0, 1,
							c.relatedItemId, 0, 0));
				}
				salesData.add(new ValuePropositionRevenue(vp, i, items));
				i++;
			}
		} else {
			SalesItemController sic = new SalesItemController(
					getApplicationContext());
			ArrayList<Integer> relatedItemIds = new ArrayList<Integer>();
			ArrayList<Integer> ids = new ArrayList<Integer>();
			for (String vp : vpNames) {
				ArrayList<CustomerSegmentRevenue> items = new ArrayList<CustomerSegmentRevenue>();
				for (RevenueItemDB r : ridb) {
					if (pc.getById(r.getRelatedItemId()) != null) {
						if (vp.equals(r.getValueProposition())) {
							SalesItemDB si = sic
									.getByRevenuePostitId(r.getId());
							if (si != null) {
								items.add(new SalesItem(r.getName(), 0, r
										.getMarketShare(), r
										.getPurchasePeriod(), r
										.getRelatedItemId(), si
										.getAveragePurchase(), si
										.getAveragePrice()));
								relatedItemIds.add(r.getRelatedItemId());
							}
						}
					} else {
						ric.removeEntry(r.getId());
						sic.removeEntryByRevItemId(r.getRelatedItemId());
					}
				}
				for (Customer c : customerSegments) {
					ids.add(c.relatedItemId);
				}
				ArrayList<Integer> difference = MainScene.getDifference(ids,
						relatedItemIds);
				for (int dbid : difference) {
					PostItDB pdb = pc.getById(dbid);
					items.add(new SalesItem(pdb.getTitle(), 1, 0, 1, pdb
							.getId(), 0, 0));
				}
				salesData.add(new ValuePropositionRevenue(vp, i, items));
				i++;
			}
		}
		return salesData;
	}

	// Datos subscripci�n
	private ArrayList<ValuePropositionRevenue> getSubscriptionItems(
			List<String> vpNames, List<Customer> customerSegments,
			RevenueItemController ric, RevenuePostitDB rpdb) {
		int i = 0;
		ArrayList<ValuePropositionRevenue> subscriptionData = new ArrayList<ValuePropositionRevenue>();
		ArrayList<RevenueItemDB> ridb = (ArrayList<RevenueItemDB>) ric
				.getByRevenuePostitIdAndType(rpdb.getId(), 1);

		if (ridb.isEmpty()) {
			for (String vp : vpNames) {
				ArrayList<CustomerSegmentRevenue> items = new ArrayList<CustomerSegmentRevenue>();
				for (Customer c : customerSegments) {
					items.add(new SubscriptionItem(c.title, c.count, 0, 1,
							c.relatedItemId, 0));
				}
				subscriptionData.add(new ValuePropositionRevenue(vp, i, items));
				i++;
			}
		} else {
			SubscriptionItemController sic = new SubscriptionItemController(
					getApplicationContext());
			ArrayList<Integer> relatedItemIds = new ArrayList<Integer>();
			ArrayList<Integer> ids = new ArrayList<Integer>();
			for (String vp : vpNames) {
				ArrayList<CustomerSegmentRevenue> items = new ArrayList<CustomerSegmentRevenue>();
				for (RevenueItemDB r : ridb) {
					if (pc.getById(r.getRelatedItemId()) != null) {
						if (vp.equals(r.getValueProposition())) {
							SubscriptionItemDB sidb = sic
									.getByRevenuePostitId(r.getId());
							if (sidb != null) {
								items.add(new SubscriptionItem(r.getName(), 0,
										r.getMarketShare(), r
												.getPurchasePeriod(), r
												.getRelatedItemId(), sidb
												.getSubscriptionPrice()));
								relatedItemIds.add(r.getRelatedItemId());
							}
						}
					} else {
						ric.removeEntry(r.getId());
						sic.removeEntryByRevItemId(r.getRelatedItemId());
					}
				}
				for (Customer c : customerSegments) {
					ids.add(c.relatedItemId);
				}
				ArrayList<Integer> difference = MainScene.getDifference(ids,
						relatedItemIds);
				for (int dbid : difference) {
					PostItDB pdb = pc.getById(dbid);
					items.add(new SubscriptionItem(pdb.getTitle(), 1, 0, 1, pdb
							.getId(), 0));
				}
				subscriptionData.add(new ValuePropositionRevenue(vp, i, items));
				i++;
			}
		}
		return subscriptionData;
	}

	// Datos pago por uso
	private ArrayList<ValuePropositionRevenue> getPayPerUseData(
			List<String> vpNames, List<Customer> customerSegments,
			RevenueItemController ric, RevenuePostitDB rpdb) {
		ArrayList<ValuePropositionRevenue> payPerUseData = new ArrayList<ValuePropositionRevenue>();
		int i = 0;
		ArrayList<RevenueItemDB> ridb = (ArrayList<RevenueItemDB>) ric
				.getByRevenuePostitIdAndType(rpdb.getId(), 2);

		if (ridb.isEmpty()) {
			for (String vp : vpNames) {
				ArrayList<CustomerSegmentRevenue> items = new ArrayList<CustomerSegmentRevenue>();
				for (Customer c : customerSegments) {
					items.add(new PayPerUseItem(c.title, c.count, 0, 1,
							c.relatedItemId, 0, 0));
				}
				payPerUseData.add(new ValuePropositionRevenue(vp, i, items));
				i++;
			}
		} else {
			PayPerUseItemController ppiuc = new PayPerUseItemController(
					getApplicationContext());
			ArrayList<Integer> relatedItemIds = new ArrayList<Integer>();
			ArrayList<Integer> ids = new ArrayList<Integer>();
			for (String vp : vpNames) {
				ArrayList<CustomerSegmentRevenue> items = new ArrayList<CustomerSegmentRevenue>();
				for (RevenueItemDB r : ridb) {
					if (pc.getById(r.getRelatedItemId()) != null) {
						if (vp.equals(r.getValueProposition())) {
							PayPerUseItemDB ppiudb = ppiuc
									.getByRevenuePostitId(r.getId());
							if (ppiudb != null) {
								items.add(new PayPerUseItem(r.getName(), 0, r
										.getMarketShare(), r
										.getPurchasePeriod(), r
										.getRelatedItemId(), ppiudb
										.getAverageUnits(), ppiudb
										.getAverageUnitPrice()));
								relatedItemIds.add(r.getRelatedItemId());
							}
						}
					} else {
						ric.removeEntry(r.getId());
						ppiuc.removeEntryByRevItemId(r.getRelatedItemId());
					}
				}
				for (Customer c : customerSegments) {
					ids.add(c.relatedItemId);
				}
				ArrayList<Integer> difference = MainScene.getDifference(ids,
						relatedItemIds);
				for (int dbid : difference) {
					PostItDB pdb = pc.getById(dbid);
					items.add(new PayPerUseItem(pdb.getTitle(), 1, 0, 1, pdb
							.getId(), 0, 0));
				}
				payPerUseData.add(new ValuePropositionRevenue(vp, i, items));
				i++;
			}
		}
		return payPerUseData;
	}

	// Datos de alquiler
	private ArrayList<ValuePropositionRevenue> getRentalData(
			List<String> vpNames, List<Customer> customerSegments,
			RevenueItemController ric, RevenuePostitDB rpdb) {
		ArrayList<ValuePropositionRevenue> rentalData = new ArrayList<ValuePropositionRevenue>();
		int i = 0;
		ArrayList<RevenueItemDB> ridb = (ArrayList<RevenueItemDB>) ric
				.getByRevenuePostitIdAndType(rpdb.getId(), 3);

		if (ridb.isEmpty()) {
			for (String vp : vpNames) {
				ArrayList<CustomerSegmentRevenue> items = new ArrayList<CustomerSegmentRevenue>();
				for (Customer c : customerSegments) {
					items.add(new RentalItem(c.title, c.count, 0, 1,
							c.relatedItemId, 0, 0));
				}
				rentalData.add(new ValuePropositionRevenue(vp, i, items));
				i++;
			}
		} else {
			RentalItemController reic = new RentalItemController(
					getApplicationContext());
			ArrayList<Integer> relatedItemIds = new ArrayList<Integer>();
			ArrayList<Integer> ids = new ArrayList<Integer>();
			for (String vp : vpNames) {
				ArrayList<CustomerSegmentRevenue> items = new ArrayList<CustomerSegmentRevenue>();
				for (RevenueItemDB r : ridb) {
					if (pc.getById(r.getRelatedItemId()) != null) {
						if (vp.equals(r.getValueProposition())) {
							RentalItemDB reidb = reic.getByRevenuePostitId(r
									.getId());
							if (reidb != null) {
								items.add(new RentalItem(r.getName(), 0, r
										.getMarketShare(), r
										.getPurchasePeriod(), r
										.getRelatedItemId(), reidb
										.getAverageUnitsRented(), reidb
										.getRentalFeePerUnit()));
								relatedItemIds.add(r.getRelatedItemId());
							}
						}
					} else {
						ric.removeEntry(r.getId());
						reic.removeEntryByRevItemId(r.getRelatedItemId());
					}
				}
				for (Customer c : customerSegments) {
					ids.add(c.relatedItemId);
				}
				ArrayList<Integer> difference = MainScene.getDifference(ids,
						relatedItemIds);
				for (int dbid : difference) {
					PostItDB pdb = pc.getById(dbid);
					items.add(new RentalItem(pdb.getTitle(), 1, 0, 1, pdb
							.getId(), 0, 0));
				}
				rentalData.add(new ValuePropositionRevenue(vp, i, items));
				i++;
			}
		}
		return rentalData;
	}

	// Datos porcentaje por transacci�n
	private ArrayList<ValuePropositionRevenue> getTransFeeData(
			List<String> vpNames, List<Customer> customerSegments,
			RevenueItemController ric, RevenuePostitDB rpdb) {
		ArrayList<ValuePropositionRevenue> transFeeData = new ArrayList<ValuePropositionRevenue>();
		int i = 0;
		ArrayList<RevenueItemDB> ridb = (ArrayList<RevenueItemDB>) ric
				.getByRevenuePostitIdAndType(rpdb.getId(), 4);

		if (ridb.isEmpty()) {
			for (String vp : vpNames) {
				ArrayList<CustomerSegmentRevenue> items = new ArrayList<CustomerSegmentRevenue>();
				for (Customer c : customerSegments) {
					items.add(new TransactionFeeItem(c.title, c.count, 0, 1,
							c.relatedItemId, 0, 0, 0));
				}
				transFeeData.add(new ValuePropositionRevenue(vp, i, items));
				i++;
			}
		} else {
			TransactionFeeItemController tfic = new TransactionFeeItemController(
					getApplicationContext());
			ArrayList<Integer> relatedItemIds = new ArrayList<Integer>();
			ArrayList<Integer> ids = new ArrayList<Integer>();
			for (String vp : vpNames) {
				ArrayList<CustomerSegmentRevenue> items = new ArrayList<CustomerSegmentRevenue>();
				for (RevenueItemDB r : ridb) {
					if (pc.getById(r.getRelatedItemId()) != null) {
						if (vp.equals(r.getValueProposition())) {
							TransactionFeeItemDB tfidb = tfic
									.getByRevenuePostitId(r.getId());
							if (tfidb != null) {
								items.add(new TransactionFeeItem(r.getName(),
										0, r.getMarketShare(), r
												.getPurchasePeriod(), r
												.getRelatedItemId(), tfidb
												.getAverageTransPerClient(),
										tfidb.getAveragePricePerTrans(), tfidb
												.getTransFee()));
								relatedItemIds.add(r.getRelatedItemId());
							}
						}
					} else {
						ric.removeEntry(r.getId());
						tfic.removeEntryByRevItemId(r.getRelatedItemId());
					}
				}
				for (Customer c : customerSegments) {
					ids.add(c.relatedItemId);
				}
				ArrayList<Integer> difference = MainScene.getDifference(ids,
						relatedItemIds);
				for (int dbid : difference) {
					PostItDB pdb = pc.getById(dbid);
					items.add(new TransactionFeeItem(pdb.getTitle(), 1, 0, 1,
							pdb.getId(), 0, 0, 0));
				}
				transFeeData.add(new ValuePropositionRevenue(vp, i, items));
				i++;
			}
		}
		return transFeeData;
	}

	// Datos porcentaje por ventas
	private ArrayList<ValuePropositionRevenue> getSalesFeeData(
			List<String> vpNames, List<Customer> customerSegments,
			RevenueItemController ric, RevenuePostitDB rpdb) {
		ArrayList<ValuePropositionRevenue> salesFeeData = new ArrayList<ValuePropositionRevenue>();
		int i = 0;
		ArrayList<RevenueItemDB> ridb = (ArrayList<RevenueItemDB>) ric
				.getByRevenuePostitIdAndType(rpdb.getId(), 5);

		if (ridb.isEmpty()) {
			for (String vp : vpNames) {
				ArrayList<CustomerSegmentRevenue> items = new ArrayList<CustomerSegmentRevenue>();
				for (Customer c : customerSegments) {
					items.add(new SalesFeeItem(c.title, c.count, 0, 1,
							c.relatedItemId, 0, 0, 0));
				}
				salesFeeData.add(new ValuePropositionRevenue(vp, i, items));
				i++;
			}
		} else {
			SalesFeeItemController sfic = new SalesFeeItemController(
					getApplicationContext());
			ArrayList<Integer> relatedItemIds = new ArrayList<Integer>();
			ArrayList<Integer> ids = new ArrayList<Integer>();
			for (String vp : vpNames) {
				ArrayList<CustomerSegmentRevenue> items = new ArrayList<CustomerSegmentRevenue>();
				for (RevenueItemDB r : ridb) {
					if (pc.getById(r.getRelatedItemId()) != null) {
						if (vp.equals(r.getValueProposition())) {
							SalesFeeItemDB sfidb = sfic.getByRevenuePostitId(r
									.getId());
							if (sfidb != null) {
								items.add(new SalesFeeItem(r.getName(), 0, r
										.getMarketShare(), r
										.getPurchasePeriod(), r
										.getRelatedItemId(), sfidb
										.getAveragePurchase(), sfidb
										.getAveragePrice(), sfidb.getSaleFee()));
								relatedItemIds.add(r.getRelatedItemId());
							}
						}
					} else {
						ric.removeEntry(r.getId());
						sfic.removeEntryByRevItemId(r.getRelatedItemId());
					}
				}
				for (Customer c : customerSegments) {
					ids.add(c.relatedItemId);
				}
				ArrayList<Integer> difference = MainScene.getDifference(ids,
						relatedItemIds);
				for (int dbid : difference) {
					PostItDB pdb = pc.getById(dbid);
					items.add(new SalesFeeItem(pdb.getTitle(), 1, 0, 1, pdb
							.getId(), 0, 0, 0));
				}
				salesFeeData.add(new ValuePropositionRevenue(vp, i, items));
				i++;
			}
		}
		return salesFeeData;
	}

	// Datos desplegar publicidad
	private ArrayList<ValuePropositionRevenue> getDisplayAdsData(
			List<String> vpNames, List<Customer> customerSegments,
			RevenueItemController ric, RevenuePostitDB rpdb) {
		ArrayList<ValuePropositionRevenue> displayAdsData = new ArrayList<ValuePropositionRevenue>();
		int i = 0;
		ArrayList<RevenueItemDB> ridb = (ArrayList<RevenueItemDB>) ric
				.getByRevenuePostitIdAndType(rpdb.getId(), 6);

		if (ridb.isEmpty()) {
			for (String vp : vpNames) {
				ArrayList<CustomerSegmentRevenue> items = new ArrayList<CustomerSegmentRevenue>();
				for (Customer c : customerSegments) {
					items.add(new DisplayAdsItem(c.title, c.count, 0, 1,
							c.relatedItemId, 0, 0, 0));
				}
				displayAdsData.add(new ValuePropositionRevenue(vp, i, items));
				i++;
			}
		} else {
			DisplayAdItemController daic = new DisplayAdItemController(
					getApplicationContext());
			ArrayList<Integer> relatedItemIds = new ArrayList<Integer>();
			ArrayList<Integer> ids = new ArrayList<Integer>();
			for (String vp : vpNames) {
				ArrayList<CustomerSegmentRevenue> items = new ArrayList<CustomerSegmentRevenue>();
				for (RevenueItemDB r : ridb) {
					if (pc.getById(r.getRelatedItemId()) != null) {
						if (vp.equals(r.getValueProposition())) {
							DisplayAdItemDB daidb = daic.getByRevenuePostitId(r
									.getId());
							if (daidb != null) {
								items.add(new DisplayAdsItem(r.getName(), 0, r
										.getMarketShare(), r
										.getPurchasePeriod(), r
										.getRelatedItemId(), daidb
										.getAveragePageViewPerUser(), daidb
										.getAverageAdUnitsPerPage(), daidb
										.getAverageCPMPerAdUnit()));
								relatedItemIds.add(r.getRelatedItemId());
							}
						}
					} else {
						ric.removeEntry(r.getId());
						daic.removeEntryByRevItemId(r.getRelatedItemId());
					}
				}
				for (Customer c : customerSegments) {
					ids.add(c.relatedItemId);
				}
				ArrayList<Integer> difference = MainScene.getDifference(ids,
						relatedItemIds);
				for (int dbid : difference) {
					PostItDB pdb = pc.getById(dbid);
					items.add(new DisplayAdsItem(pdb.getTitle(), 1, 0, 1, pdb
							.getId(), 0, 0, 0));
				}
				displayAdsData.add(new ValuePropositionRevenue(vp, i, items));
				i++;
			}
		}
		return displayAdsData;
	}

	// Datos click en publicidad
	private ArrayList<ValuePropositionRevenue> getClickAdsData(
			List<String> vpNames, List<Customer> customerSegments,
			RevenueItemController ric, RevenuePostitDB rpdb) {
		ArrayList<ValuePropositionRevenue> clickAdsData = new ArrayList<ValuePropositionRevenue>();
		int i = 0;
		ArrayList<RevenueItemDB> ridb = (ArrayList<RevenueItemDB>) ric
				.getByRevenuePostitIdAndType(rpdb.getId(), 7);

		if (ridb.isEmpty()) {
			for (String vp : vpNames) {
				ArrayList<CustomerSegmentRevenue> items = new ArrayList<CustomerSegmentRevenue>();
				for (Customer c : customerSegments) {
					items.add(new ClickAdsItem(c.title, c.count, 0, 1,
							c.relatedItemId, 0, 0, 0, 0));
				}
				clickAdsData.add(new ValuePropositionRevenue(vp, i, items));
				i++;
			}
		} else {
			ClickAdsItemController caic = new ClickAdsItemController(
					getApplicationContext());
			ArrayList<Integer> relatedItemIds = new ArrayList<Integer>();
			ArrayList<Integer> ids = new ArrayList<Integer>();
			for (String vp : vpNames) {
				ArrayList<CustomerSegmentRevenue> items = new ArrayList<CustomerSegmentRevenue>();
				for (RevenueItemDB r : ridb) {
					if (pc.getById(r.getRelatedItemId()) != null) {
						if (vp.equals(r.getValueProposition())) {
							ClickAdsItemDB caidb = caic.getByRevenuePostitId(r
									.getId());
							if (caidb != null) {
								items.add(new ClickAdsItem(r.getName(), 0, r
										.getMarketShare(), r
										.getPurchasePeriod(), r
										.getRelatedItemId(), caidb
										.getAveragePageViewPerUser(), caidb
										.getAverageAdsPerPage(), caidb
										.getAverageCTR(), caidb.getAverageCPC()));
								relatedItemIds.add(r.getRelatedItemId());
							}
						}
					} else {
						ric.removeEntry(r.getId());
						caic.removeEntryByRevItemId(r.getRelatedItemId());
					}
				}
				for (Customer c : customerSegments) {
					ids.add(c.relatedItemId);
				}
				ArrayList<Integer> difference = MainScene.getDifference(ids,
						relatedItemIds);
				for (int dbid : difference) {
					PostItDB pdb = pc.getById(dbid);
					items.add(new ClickAdsItem(pdb.getTitle(), 1, 0, 1, pdb
							.getId(), 0, 0, 0, 0));
				}
				clickAdsData.add(new ValuePropositionRevenue(vp, i, items));
				i++;
			}
		}
		return clickAdsData;
	}

	// Datos licenciamiento
	private ArrayList<ValuePropositionRevenue> getLicensingData(
			List<String> vpNames, List<Customer> customerSegments,
			RevenueItemController ric, RevenuePostitDB rpdb) {
		ArrayList<ValuePropositionRevenue> licensingData = new ArrayList<ValuePropositionRevenue>();
		int i = 0;
		ArrayList<RevenueItemDB> ridb = (ArrayList<RevenueItemDB>) ric
				.getByRevenuePostitIdAndType(rpdb.getId(), 8);

		if (ridb.isEmpty()) {
			for (String vp : vpNames) {
				ArrayList<CustomerSegmentRevenue> items = new ArrayList<CustomerSegmentRevenue>();
				for (Customer c : customerSegments) {
					items.add(new LicensingRevenueItem(c.title, c.count, 0, 1,
							c.relatedItemId, 0, 0));
				}
				licensingData.add(new ValuePropositionRevenue(vp, i, items));
				i++;
			}
		} else {
			LicensingItemController lic = new LicensingItemController(
					getApplicationContext());
			ArrayList<Integer> relatedItemIds = new ArrayList<Integer>();
			ArrayList<Integer> ids = new ArrayList<Integer>();
			for (String vp : vpNames) {
				ArrayList<CustomerSegmentRevenue> items = new ArrayList<CustomerSegmentRevenue>();
				for (RevenueItemDB r : ridb) {
					if (pc.getById(r.getRelatedItemId()) != null) {
						if (vp.equals(r.getValueProposition())) {
							LicensingItemDB lidb = lic.getByRevenuePostitId(r
									.getId());
							if (lidb != null) {
								items.add(new LicensingRevenueItem(r.getName(),
										0, r.getMarketShare(), r
												.getPurchasePeriod(), r
												.getRelatedItemId(),
										lidb.getAverageLicensesPerCostumer(),
										lidb.getAverageLicensePrice()));
								relatedItemIds.add(r.getRelatedItemId());
							}
						}
					} else {
						ric.removeEntry(r.getId());
						lic.removeEntryByRevItemId(r.getRelatedItemId());
					}
				}
				for (Customer c : customerSegments) {
					ids.add(c.relatedItemId);
				}
				ArrayList<Integer> difference = MainScene.getDifference(ids,
						relatedItemIds);
				for (int dbid : difference) {
					PostItDB pdb = pc.getById(dbid);
					items.add(new LicensingRevenueItem(pdb.getTitle(), 1, 0, 1,
							pdb.getId(), 0, 0));
				}
				licensingData.add(new ValuePropositionRevenue(vp, i, items));
				i++;
			}
		}
		return licensingData;
	}

	public Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.arg1) {
			case 0:
				double currentRevenue = (Double) msg.obj;
				updateTotalCost(currentRevenue);
				break;
			case 2:
				finish();
				startActivity(getIntent());
				overridePendingTransition(R.anim.fadeout, R.anim.fadein);
				break;
			case 3:
				cancelDialog();
				break;
			}
		}
	};

	private List<String> getPostItTitles(List<PostItDB> items) {
		List<String> titles = new ArrayList<String>();
		for (PostItDB title : items) {
			titles.add(title.getTitle());
		}
		return titles;
	}

	private void updateTotalCost(double newRevenue) {
		try {
			String currentRevenue = totalRevenue.getText().toString();
			String revenueWithoutSymbol = currentRevenue.substring(2);
			double actualRevenue = Double
					.parseDouble((String) revenueWithoutSymbol);
			actualRevenue += newRevenue;
			RevenueManager.getInstance().setTotalRevenue(actualRevenue);
			totalRevenue.setText("$ " + actualRevenue);
		} catch (Exception e) {
			Log.d("COST", "No puede convertir a double");
		}
	}

	private void changeView(int position) {
		flipper.setDisplayedChild(position);
	}

	private boolean clear = false;

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.saveButton:
			// Guarda la informaci�n en la base de datos y reinicia la clase
			saveData();
			break;

		case R.id.clear:
			AlertDialog ad = new AlertDialog.Builder(this)
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setTitle("Borrar datos")
					.setMessage("�Est� seguro que desea borrar los datos?")
					.setPositiveButton("S�",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									clear = true;
								}

							}).setNegativeButton("No", null).create();
			ad.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
					if (clear) {
						RevenueManager.getInstance().clearRevenues();
						clear = false;
					}
				}
			});
			ad.show();
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		changeView(position);
	}

	@Override
	public void onBackPressed() {

	}

	class Customer {
		public String title;
		public int count;
		public int relatedItemId;

		public Customer(String title, int count, int relatedItemId) {
			super();
			this.title = title;
			this.count = count;
			this.relatedItemId = relatedItemId;
		}
	}

	/**
	 * Actualiza la informaci�n de la base de datos con las notas del escenario
	 */
	public void updateDatabase() {
		PostitController controller = new PostitController(
				CanvasManager.getInstance().context);
		for (PostIt p : MainScene.getInstance().valuePropositions.postits) {
			PostItDB pdb = savePostIt(p, controller, 3);
			if (pdb != null) {
				int indexInCollection = MainScene.getInstance().valuePropositions.postits
						.indexOf(p);
				MainScene.getInstance().valuePropositions.postits.get(
						indexInCollection).setRelatedDatabaseObject(pdb);
			}
		}
		for (PostIt p : MainScene.getInstance().customerSegments.postits) {
			PostItDB pdb = savePostIt(p, controller, 6);
			if (pdb != null) {
				int indexInCollection = MainScene.getInstance().customerSegments.postits
						.indexOf(p);
				MainScene.getInstance().customerSegments.postits.get(
						indexInCollection).setRelatedDatabaseObject(pdb);
			}
		}
	}

	private PostItDB savePostIt(PostIt p, PostitController controller, int block) {
		if (p.getRelatedDatabaseObject() == null) {
			PostItDB pdb = PostItDB.fromPostit(p, -1, CanvasActivity.canvasId);
			pdb.setId((int) controller.create(pdb));
			return pdb;
		}
		return null;
	}

	/**
	 * Guarda la informaci�n de ingresos en la base de datos
	 */

	private void saveData() {
		final PersistRevenue persist = new PersistRevenue(
				getApplicationContext(), this);
		showDialog();
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				persist.saveData();
			}
		});
		t.start();
	}

	private void showDialog() {
		savingDialog = new ProgressDialog(this);
		savingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		savingDialog.setMessage(getResources().getString(R.string.saving));
		savingDialog.setCancelable(false);
		savingDialog.show();
	}

	private void cancelDialog() {
		savingDialog.dismiss();
		super.onBackPressed();
		SceneManager.getInstance().setMainSceneAsCurrent(true);
		SceneManager.getInstance().showScene(MainScene.getInstance());
	}

}
