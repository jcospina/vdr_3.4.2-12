package com.canvas.partnerships;

import java.util.List;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.SpriteBackground;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.util.color.Color;

import android.graphics.Typeface;

import com.canvas.dataaccess.KeyPartnerPostitDB;
import com.canvas.dataaccess.PostItDB;
import com.canvas.dataaccess.controller.KeyPartnerPostitController;
import com.canvas.dataaccess.controller.PostitController;
import com.canvas.vo.KeyPartnershipPostitVO;
import com.smartsoft.Educanvas.CanvasActivity;
import com.smartsoft.Educanvas.CanvasManager;
import com.smartsoft.Educanvas.MainScene;
import com.smartsoft.Educanvas.ManagedScene;

public class KeyPartnershipsScene extends ManagedScene {

	public HUD gameHud = new HUD();

	private Color redColor;
	private Color yellowColor;
	private Color greenColor;
	private Sprite redLightSprite;
	private Sprite yellowLightSprite;
	private Sprite greenLightSprite;
	private CanvasActivity canvas;

	float widhtArea = 343;
	Font postitFont;

	private List<BitmapTextureAtlas> textures;

	public KeyPartnershipsScene(CanvasActivity canvas) {
		this.setOnSceneTouchListenerBindingOnActionDownEnabled(true);
		this.setTouchAreaBindingOnActionDownEnabled(true);
		this.setTouchAreaBindingOnActionMoveEnabled(true);
		this.canvas = canvas;
		gameHud.setScaleCenter(0f, 0f);
		gameHud.setScale(1);

	}

	@Override
	public Scene onLoadingScreenLoadAndShown() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onLoadingScreenUnloadAndHidden() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLoadScene() {
		textures = CanvasManager.getInstance().loadKeyPartnershipsResources();

		setBackground(new SpriteBackground(new Sprite(0, 0,
				CanvasManager.keypartnershipsBackground,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager())));

		redColor = new Color(1f, .56f, 0.56f);

		redLightSprite = new Sprite(165, 198, CanvasManager.redLightRegion,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());
		redLightSprite.setCullingEnabled(true);

		yellowColor = new Color(1.0f, 0.79f, 0.41f);

		yellowLightSprite = new Sprite(477, 200,
				CanvasManager.yellowLightRegion,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());
		yellowLightSprite.setCullingEnabled(true);

		greenColor = new Color(0.77f, 1.0f, 0.61f);

		greenLightSprite = new Sprite(752, 240, CanvasManager.greenLightRegion,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());
		greenLightSprite.setCullingEnabled(true);

		Sprite shadowSprite = new Sprite(100, 545,
				CanvasManager.keypartnershipsShadow,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());
		shadowSprite.setCullingEnabled(true);

		postitFont = FontFactory.create(
				CanvasManager.getInstance().engine.getFontManager(),
				CanvasManager.getInstance().engine.getTextureManager(), 256,
				256, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 16);
		postitFont.load();

		gameHud.attachChild(yellowLightSprite);
		gameHud.attachChild(redLightSprite);
		gameHud.attachChild(greenLightSprite);
		gameHud.attachChild(shadowSprite);
	}

	@Override
	public void onShowScene() {
		/*
		 * gameHud.attachChild(redColor); gameHud.attachChild(yellowColor);
		 * gameHud.attachChild(greenColor);
		 */
		drawPostits();
		CanvasManager.getInstance().engine.getCamera().setHUD(gameHud);
	}

	@Override
	public void onHideScene() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUnloadScene() {
		persistKeyPartnerPostitDB();
		canvas.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {

				gameHud.unregisterTouchAreas(new ITouchArea.ITouchAreaMatcher() {
					@Override
					public boolean matches(ITouchArea pObject) {
						return true;
					}
				});

				for (int i = 0; i < gameHud.getChildCount(); i++) {
					if (!(gameHud.getChildByIndex(i) instanceof KeyPartnershipPostitRelated)) {
						if (!gameHud.getChildByIndex(i).isDisposed()) {
							gameHud.getChildByIndex(i).dispose();
						}
					}
				}

				gameHud.detachChildren();
				gameHud.dispose();
				KeyPartnershipsScene.this.detachChildren();
				KeyPartnershipsScene.this.clearEntityModifiers();
				KeyPartnershipsScene.this.clearTouchAreas();
				KeyPartnershipsScene.this.clearUpdateHandlers();
				
				if(postitFont != null){
					postitFont.unload();
				}
				
				CanvasManager.unloadTextures(textures);
			}
		});

	}

	public void drawPostits() {

		loadKeyPartnerPostitDBFromDB();

		int col = -1;
		int index = 0;
		int separationX = 20;
		int separationY = 40;

		for (int i = 0; i < MainScene.getInstance().keyPartnerships.postits
				.size(); i++) {
			KeyPartnershipPostitVO kpvo = MainScene.getInstance().keyPartnerships.postits
					.get(i);
			final KeyPartnershipPostitRelated pvo = kpvo.getPostitRelated() == null ? new KeyPartnershipPostitRelated(
					0, 0, CanvasManager.keyPartnershipPostitRegion,
					CanvasManager.getInstance().engine
							.getVertexBufferObjectManager(), kpvo, postitFont)
					: kpvo.getPostitRelated();

			float posX = CanvasActivity.CAMERA_WIDTH
					+ (col * (pvo.getWidth() + separationX));
			float posY = index * (pvo.getHeight() + separationY) + separationY;

			index++;
			if (index > 4) {
				index = 0;
				col--;
			}

			// si se debe crear un nuevo postit related, fijamos el color y la
			// posici�n
			if (kpvo.getPostitRelated() == null) {
				pvo.setColor(0.5f, 0.5f, 0.5f);
				pvo.setPosition(posX, posY);
			} else {
				defineColor(kpvo.getPostitRelated());
			}
			pvo.drawTitle();
			kpvo.setPostitRelated(pvo);

			pvo.setOnMoveListener(new KeyPartnershipPostitRelated.OnMoveListener() {

				@Override
				public void onMoveStarted() {
					// TODO Auto-generated method stub

				}

				@Override
				public void onMoveFinished() {
					defineColor(pvo);
				}
			});
			pvo.setCullingEnabled(true);
			gameHud.attachChild(pvo);
			registerTouchArea(pvo);
			setTouchAreaBindingOnActionDownEnabled(true);
		}
	}

	public void defineColor(KeyPartnershipPostitRelated pvo) {
		if (pvo.getX() < greenLightSprite.getX() + greenLightSprite.getWidth()
				&& pvo.getX() > redLightSprite.getX()
				&& pvo.getY() >= yellowLightSprite.getY()
				&& pvo.getY() < yellowLightSprite.getY()
						+ yellowLightSprite.getHeight()) {
			if (pvo.getX() >= greenLightSprite.getX()) {
				pvo.setColor(greenColor);
			} else if (pvo.getX() >= yellowLightSprite.getX()) {
				pvo.setColor(yellowColor);
			} else {
				pvo.setColor(redColor);
			}
		} else {
			pvo.setColor(0.8f, 0.8f, 0.8f);
		}
		pvo.getPartnership().setColor(pvo.getColor());
	}

	public void loadKeyPartnerPostitDBFromDB() {
		KeyPartnerPostitController kController = new KeyPartnerPostitController(
				CanvasManager.getInstance().context);
		for (KeyPartnershipPostitVO kpvo : MainScene.getInstance().keyPartnerships.postits) {
			if (kpvo.getRelatedDatabaseObject() != null) {
				KeyPartnerPostitDB kpdb = kController.getByPostitId(kpvo
						.getRelatedDatabaseObject().getId());
				if (kpdb != null) {
					kpvo.setPostitRelated(new KeyPartnershipPostitRelated(kpdb
							.getpSX(), kpdb.getpSY(),
							CanvasManager.keyPartnershipPostitRegion,
							CanvasManager.getInstance().engine
									.getVertexBufferObjectManager(), kpvo,
							postitFont));
				}
			}
		}
	}

	public void persistKeyPartnerPostitDB() {
		KeyPartnerPostitController kController = new KeyPartnerPostitController(
				CanvasManager.getInstance().context);
		PostitController pc = new PostitController(
				CanvasManager.getInstance().context);
		for (KeyPartnershipPostitVO kpvo : MainScene.getInstance().keyPartnerships.postits) {

			if (kpvo.getRelatedDatabaseObject() == null) {
				PostItDB pdb = PostItDB.fromPostit(kpvo, -1,
						CanvasActivity.canvasId);
				pdb.setId((int) pc.create(pdb));
				kpvo.setRelatedDatabaseObject(pdb);
			}

			kpvo.getRelatedDatabaseObject().setColor(
					kpvo.getColor().getARGBPackedInt());
			pc.update(kpvo.getRelatedDatabaseObject()); // Actualizamos el color
														// del postit exterior

			KeyPartnerPostitDB kpdb = kController.getByPostitId(kpvo
					.getRelatedDatabaseObject().getId());
			if (kpdb == null) {
				kpdb = new KeyPartnerPostitDB(-1, kpvo.getPostitRelated()
						.getX(), kpvo.getPostitRelated().getY(), kpvo
						.getRelatedDatabaseObject().getId());
			}
			kpdb.setpSX(kpvo.getPostitRelated().getX());
			kpdb.setpSY(kpvo.getPostitRelated().getY());
			kController.createOrUpdate(kpdb);

		}
	}

}
