package com.canvas.partnerships;

import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.AutoWrap;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.HorizontalAlign;

import com.canvas.vo.KeyPartnershipPostitVO;
import com.smartsoft.Educanvas.CanvasManager;

public class KeyPartnershipPostitRelated extends Sprite {

	private KeyPartnershipPostitVO partnership;
	private OnMoveListener onMoveListener;
	private Font pFont;
	private boolean isMoving;

	public KeyPartnershipPostitRelated(float pX, float pY,
			ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			KeyPartnershipPostitVO partnership, Font pFont) {
		super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
		this.pFont = pFont;
		this.setPartnership(partnership);
		drawTitle();
	}

	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
			float pTouchAreaLocalX, float pTouchAreaLocalY) {

		switch (pSceneTouchEvent.getAction()) {

		case TouchEvent.ACTION_DOWN:
			isMoving = true;
			break;

		case TouchEvent.ACTION_MOVE:
			if (isMoving) {
				this.setPosition(pSceneTouchEvent.getX() - getWidth() / 2,
						pSceneTouchEvent.getY() - getHeight() / 2);
				if (onMoveListener != null) {
					onMoveListener.onMoveStarted();
				}
			}
			break;

		case TouchEvent.ACTION_UP:
			if (onMoveListener != null) {
				onMoveListener.onMoveFinished();
			}
			isMoving = false;
			break;

		default:
			break;

		}

		return true;
	}

	public void drawTitle() {
		this.detachChildren();
		TextOptions tOptions = new TextOptions(AutoWrap.WORDS, 90,
				HorizontalAlign.CENTER, Text.LEADING_DEFAULT);
		Text text = new Text(5, 0, pFont,
				(partnership.getTitle() != null) ? partnership.getTitle() : "",
				tOptions,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());

		// text.setText((this.title != null) ? this.title : "");
		text.setPosition((getWidth() - text.getWidth()) / 2,
				(getHeight() + text.getHeight()) / 4);
		this.attachChild(text);
	}

	public OnMoveListener getOnMoveListener() {
		return onMoveListener;
	}

	public void setOnMoveListener(OnMoveListener onMoveListener) {
		this.onMoveListener = onMoveListener;
	}

	public KeyPartnershipPostitVO getPartnership() {
		return partnership;
	}

	public void setPartnership(KeyPartnershipPostitVO partnership) {
		this.partnership = partnership;
	}

	public interface OnMoveListener {
		public void onMoveStarted();

		public void onMoveFinished();
	}

}
