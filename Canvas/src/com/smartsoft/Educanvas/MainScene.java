package com.smartsoft.Educanvas;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.andengine.engine.Engine.EngineLock;
import org.andengine.entity.Entity;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.SpriteBackground;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.util.ScreenCapture;
import org.andengine.input.touch.TouchEvent;
import org.andengine.util.color.Color;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Message;

import com.canvas.dataaccess.PostItDB;
import com.canvas.dataaccess.controller.PostitController;
import com.canvas.util.BackupGenerator;
import com.canvas.util.ExportImage;
import com.canvas.util.ExportPDF;
import com.canvas.util.UtilColor;
import com.canvas.vo.ChannelPostitVO;
import com.canvas.vo.CostPostitVO;
import com.canvas.vo.CustomerPostitVO;
import com.canvas.vo.GenericPostit;
import com.canvas.vo.KeyActivityPostitVO;
import com.canvas.vo.KeyPartnershipPostitVO;
import com.canvas.vo.KeyResourcePostitVO;
import com.canvas.vo.RelationshipPostitVO;
import com.canvas.vo.RevenuePostitVO;
import com.canvas.vo.ValuePropositionPostitVO;

public class MainScene extends ManagedScene {

	private static MainScene instance;
	private CanvasActivity activity;

	public Block<CustomerPostitVO> customerSegments;
	public Block<CostPostitVO> costs;
	public Block<KeyPartnershipPostitVO> keyPartnerships;
	public Block<KeyActivityPostitVO> keyActivities;
	public Block<KeyResourcePostitVO> keyResources;
	public Block<ValuePropositionPostitVO> valuePropositions;
	public Block<RelationshipPostitVO> relationships;
	public Block<ChannelPostitVO> channels;
	public Block<RevenuePostitVO> revenues;

	public Sprite deleteButton = null;
	public Sprite addPostitBtn = null;
	public Sprite loadButton = null;
	public Sprite exportButton = null;
	public Sprite infoButton = null;

	public static MainScene getInstance() {
		if (instance == null) {
			instance = new MainScene();
		}
		return instance;
	}

	public MainScene() {

	}

	public void setActivity(CanvasActivity activity) {
		this.activity = activity;
	}

	private void createScene() {
		this.setZIndex(0);
		final ScreenCapture screenCapture = new ScreenCapture();
		this.attachChild(screenCapture);
		Sprite bSprite = new Sprite(0, 0,
				CanvasManager.getInstance().backgroundTextureRegion,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());
		this.setBackground(new SpriteBackground(bSprite));
		bSprite.setIgnoreUpdate(true);
		bSprite.setCullingEnabled(true);

		Entity canvasBlocks = new Entity(0, 0);

		keyPartnerships = new Block<KeyPartnershipPostitVO>(
				CanvasActivity.TOOL_BOX_WIDTH, 0f,
				CanvasActivity.CANVAS_WIDTH / 5,
				2.2f * CanvasActivity.CAMERA_HEIGHT / 3,
				CanvasManager.BLOCK_POSTITS_LIMIT,
				Block.BlockNumbers.KEY_PARTNERSHIPS_BN,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());
		keyPartnerships.setCullingEnabled(true);

		keyActivities = new Block<KeyActivityPostitVO>(
				CanvasActivity.TOOL_BOX_WIDTH + CanvasActivity.CANVAS_WIDTH / 5,
				0, CanvasActivity.CANVAS_WIDTH / 5,
				1.1f * CanvasActivity.CAMERA_HEIGHT / 3,
				CanvasManager.BLOCK_POSTITS_LIMIT,
				Block.BlockNumbers.KEY_ACTIVITIES_BN, CanvasManager
						.getInstance().engine.getVertexBufferObjectManager());
		keyActivities.setCullingEnabled(true);

		keyResources = new Block<KeyResourcePostitVO>(
				CanvasActivity.TOOL_BOX_WIDTH + CanvasActivity.CANVAS_WIDTH / 5,
				1.1f * CanvasActivity.CAMERA_HEIGHT / 3,
				CanvasActivity.CANVAS_WIDTH / 5,
				1.1f * CanvasActivity.CAMERA_HEIGHT / 3,
				CanvasManager.BLOCK_POSTITS_LIMIT,
				Block.BlockNumbers.KEY_RESOURCES_BN, CanvasManager
						.getInstance().engine.getVertexBufferObjectManager());
		keyResources.setCullingEnabled(true);
		valuePropositions = new Block<ValuePropositionPostitVO>(
				CanvasActivity.TOOL_BOX_WIDTH
						+ (2 * CanvasActivity.CANVAS_WIDTH / 5), 0,
				CanvasActivity.CANVAS_WIDTH * 0.15f,
				2.2f * CanvasActivity.CAMERA_HEIGHT / 3,
				CanvasManager.BLOCK_POSTITS_LIMIT,
				Block.BlockNumbers.VALUE_PROPOSITIONS_BN,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());
		valuePropositions.setCullingEnabled(true);
		relationships = new Block<RelationshipPostitVO>(
				CanvasActivity.TOOL_BOX_WIDTH
						+ (2 * CanvasActivity.CANVAS_WIDTH / 5)
						+ CanvasActivity.CANVAS_WIDTH * 0.15f, 0,
				CanvasActivity.CANVAS_WIDTH * 0.25f,
				1.1f * CanvasActivity.CAMERA_HEIGHT / 3,
				CanvasManager.BLOCK_POSTITS_LIMIT,
				Block.BlockNumbers.RELATIONSHIPS_BN,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());
		relationships.setCullingEnabled(true);
		channels = new Block<ChannelPostitVO>(CanvasActivity.TOOL_BOX_WIDTH
				+ (2 * CanvasActivity.CANVAS_WIDTH / 5)
				+ CanvasActivity.CANVAS_WIDTH * 0.15f,
				1.1f * CanvasActivity.CAMERA_HEIGHT / 3,
				CanvasActivity.CANVAS_WIDTH * 0.25f,
				1.1f * CanvasActivity.CAMERA_HEIGHT / 3,
				CanvasManager.BLOCK_POSTITS_LIMIT,
				Block.BlockNumbers.CHANNELS_BN,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());
		channels.setCullingEnabled(true);
		customerSegments = new Block<CustomerPostitVO>(
				CanvasActivity.TOOL_BOX_WIDTH
						+ (4 * CanvasActivity.CANVAS_WIDTH / 5), 0,
				CanvasActivity.CANVAS_WIDTH / 5,
				2.2f * CanvasActivity.CAMERA_HEIGHT / 3,
				CanvasManager.BLOCK_POSTITS_LIMIT,
				Block.BlockNumbers.CUSTOMER_SEGMENTS_BN,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());
		customerSegments.setCullingEnabled(true);
		costs = new Block<CostPostitVO>(CanvasActivity.TOOL_BOX_WIDTH,
				2.2f * CanvasActivity.CAMERA_HEIGHT / 3,
				CanvasActivity.CANVAS_WIDTH / 2,
				CanvasActivity.CAMERA_HEIGHT / 3,
				CanvasManager.BLOCK_POSTITS_LIMIT, Block.BlockNumbers.COSTS_BN,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());
		costs.setCullingEnabled(true);
		revenues = new Block<RevenuePostitVO>(CanvasActivity.TOOL_BOX_WIDTH
				+ (CanvasActivity.CANVAS_WIDTH / 2),
				2.2f * CanvasActivity.CAMERA_HEIGHT / 3,
				CanvasActivity.CANVAS_WIDTH / 2,
				CanvasActivity.CAMERA_HEIGHT / 3,
				CanvasManager.BLOCK_POSTITS_LIMIT,
				Block.BlockNumbers.REVENUEW_BN,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());
		revenues.setCullingEnabled(true);

		canvasBlocks.attachChild(keyPartnerships);
		canvasBlocks.attachChild(keyActivities);
		canvasBlocks.attachChild(keyResources);
		canvasBlocks.attachChild(valuePropositions);
		canvasBlocks.attachChild(relationships);
		canvasBlocks.attachChild(channels);
		canvasBlocks.attachChild(customerSegments);
		canvasBlocks.attachChild(costs);
		canvasBlocks.attachChild(revenues);
		canvasBlocks.setCullingEnabled(true);
		// this.attachChild(tools);
		this.attachChild(canvasBlocks);
	}

	private void loadPostitsFromDatabase() {
		PostitController controller = new PostitController(
				CanvasManager.getInstance().context);
		ArrayList<PostItDB> postits = (ArrayList<PostItDB>) controller
				.getByIdCanvas(CanvasActivity.canvasId);
		ArrayList<GenericPostit> keyPartnerPostits = returnRealPostIts(
				controller, postits, 0);
		for (GenericPostit gp : keyPartnerPostits) {
			KeyPartnershipPostitVO keyPartnershipPostitVO = KeyPartnershipPostitVO
					.fromPostIt(gp, keyPartnerships);
			keyPartnershipPostitVO.setCullingEnabled(true);
			this.attachChild(keyPartnershipPostitVO);
			this.registerTouchArea(keyPartnershipPostitVO);
			keyPartnerships.addPostit(keyPartnershipPostitVO);
		}
		ArrayList<GenericPostit> keyActivitiesPostits = returnRealPostIts(
				controller, postits, 1);
		for (GenericPostit gp : keyActivitiesPostits) {
			KeyActivityPostitVO keyActivityPostitVO = KeyActivityPostitVO
					.fromPostIt(gp, keyActivities);
			keyActivityPostitVO.setCullingEnabled(true);
			this.attachChild(keyActivityPostitVO);
			this.registerTouchArea(keyActivityPostitVO);
			keyActivities.addPostit(keyActivityPostitVO);
		}
		ArrayList<GenericPostit> keyResourcesPostits = returnRealPostIts(
				controller, postits, 2);
		for (GenericPostit gp : keyResourcesPostits) {
			KeyResourcePostitVO keyResourcePostitVO = KeyResourcePostitVO
					.fromPostIt(gp, keyResources);
			keyResourcePostitVO.setCullingEnabled(true);
			this.attachChild(keyResourcePostitVO);
			this.registerTouchArea(keyResourcePostitVO);
			keyResources.addPostit(keyResourcePostitVO);
		}
		ArrayList<GenericPostit> valuePropositionPostits = returnRealPostIts(
				controller, postits, 3);
		for (GenericPostit gp : valuePropositionPostits) {
			ValuePropositionPostitVO vPropositionPostitVO = ValuePropositionPostitVO
					.fromPostIt(gp, valuePropositions);
			vPropositionPostitVO.setCullingEnabled(true);
			this.attachChild(vPropositionPostitVO);
			this.registerTouchArea(vPropositionPostitVO);
			valuePropositions.addPostit(vPropositionPostitVO);
		}
		ArrayList<GenericPostit> relationshipsPostits = returnRealPostIts(
				controller, postits, 4);
		for (GenericPostit gp : relationshipsPostits) {
			RelationshipPostitVO relationshipPostitVO = RelationshipPostitVO
					.fromPostIt(gp, relationships);
			relationshipPostitVO.setCullingEnabled(true);
			this.attachChild(relationshipPostitVO);
			this.registerTouchArea(relationshipPostitVO);
			relationships.addPostit(relationshipPostitVO);
		}
		ArrayList<GenericPostit> channelPostits = returnRealPostIts(controller,
				postits, 5);
		for (GenericPostit gp : channelPostits) {
			ChannelPostitVO channelPostitVO = ChannelPostitVO.fromPostIt(gp,
					channels);
			channelPostitVO.setCullingEnabled(true);
			this.attachChild(channelPostitVO);
			this.registerTouchArea(channelPostitVO);
			channels.addPostit(channelPostitVO);
		}
		ArrayList<GenericPostit> customerPostits = returnRealPostIts(
				controller, postits, 6);
		for (GenericPostit gp : customerPostits) {
			CustomerPostitVO customerPostitVO = CustomerPostitVO.fromPostIt(gp,
					customerSegments);
			customerPostitVO.setCullingEnabled(true);
			this.attachChild(customerPostitVO);
			this.registerTouchArea(customerPostitVO);
			customerSegments.addPostit(customerPostitVO);
		}
		ArrayList<GenericPostit> costPostits = returnRealPostIts(controller,
				postits, 7);
		for (GenericPostit gp : costPostits) {
			CostPostitVO costPostitVO = CostPostitVO.fromPostIt(gp, costs);
			costPostitVO.setCullingEnabled(true);
			this.attachChild(costPostitVO);
			this.registerTouchArea(costPostitVO);
			costs.addPostit(costPostitVO);
		}
		ArrayList<GenericPostit> revenuePostits = returnRealPostIts(controller,
				postits, 8);
		for (GenericPostit gp : revenuePostits) {
			RevenuePostitVO revenuePostitVO = RevenuePostitVO.fromPostIt(gp,
					revenues);
			revenuePostitVO.setCullingEnabled(true);
			this.attachChild(revenuePostitVO);
			this.registerTouchArea(revenuePostitVO);
			revenues.addPostit(revenuePostitVO);
		}

	}

	/**
	 * Retorna los postit que se van a mostrar en el escenario para cada bloque
	 * 
	 * @return
	 */
	private ArrayList<GenericPostit> returnRealPostIts(
			PostitController controller, List<PostItDB> postits, int block) {
		ArrayList<GenericPostit> realPostits = new ArrayList<GenericPostit>();
		if (!controller.filterByBlock(postits, block).isEmpty()) {
			ArrayList<PostItDB> filtered = (ArrayList<PostItDB>) controller
					.filterByBlock(postits, block);
			for (PostItDB p : filtered) {
				GenericPostit genericPostit = new GenericPostit(p.getpX(),
						p.getpY(),
						CanvasManager.getInstance().mPostitTextureRegionBG,
						CanvasManager.getInstance().engine
								.getVertexBufferObjectManager(),
						CanvasManager.getInstance().engine, activity,
						p.getTitle(), p.getDescription(),
						(p.getColor() != 0 ? UtilColor.fromInt(p.getColor())
								: new Color(0.99f, 0.84f, 0.38f)), p);
				realPostits.add(genericPostit);
			}
		}
		return realPostits;
	}

	/*
	 * Cuando un postit es cambiado de un bloque a otro sufre un proceso de
	 * transformaci�n en el que deja de ser instanciado por la clase del bloque
	 * en el que se encontraba y empieza a instanciarse por la clase del bloque
	 * actual. Este m�todo elimina la instancia anterior para quedarse
	 * �nicamente con la actual y no malgastar recursos.
	 */
	public void changePostitTypeOnScene(Sprite p, Sprite pVO) {
		if (p != pVO) {
			this.unregisterTouchArea(p);
			this.detachChild(p);
			this.attachChild(pVO);
			this.registerTouchArea(pVO);
			if (!p.isDisposed()) {
				p.dispose();
			}
		}
	}

	/**
	 * En ocasiones es necesario evitar que los sprites contenidos en la escena
	 * puedan ser "touchables", este m�todo toma cada uno de los objetos en la
	 * escena que sean Sprite y quita el registro TouchArea de los mismos.
	 */
	public void unregisterSpriteTouchAreas() {
		for (int i = 0; i < this.getChildCount(); i++) {
			Object ie = this.getChildByIndex(i);
			if (ie instanceof Sprite) {
				this.unregisterTouchArea((Sprite) ie);
			}
		}
	}

	/**
	 * Cuando se elimina el registro TouchArea de los sprites en la escena, es
	 * necesario que vuelvan a ser registrados para que se pueda interactuar
	 * nuevamente con ellos.
	 */
	public void registerSpriteTouchAreas() {
		for (int i = 0; i < this.getChildCount(); i++) {
			Object ie = this.getChildByIndex(i);
			if (ie instanceof Sprite) {
				this.registerTouchArea((Sprite) ie);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void defineBlock(PostIt p, float startX, float startY, float posX, float posY) {
		float pX = p.getPosX();
		float pY = p.getPosY();
		if (pY > (2 * CanvasActivity.CAMERA_HEIGHT / 3)) {
			if (pX < CanvasActivity.CANVAS_WIDTH / 2) {
				if (costs.postits.size() >= costs.getLimit()) {
					if(startX == -1 || startY == -1){
						deletePostIt(p);
					} else{
						p.setPosition(startX, startY);
						p.setPosX(startX);
						p.setPosY(startY);
						//defineBlock(p, -1, -1, startX, startY);
					}
				} else {
					if (p.getParentBlock() != null) {
						p.getParentBlock().removePostit(p);
					}
					p.setPosition(posX, posY);
					p.setPosX(posX);
					p.setPosY(posY);
					CostPostitVO pVO = CostPostitVO.fromPostIt(p, costs);
					changePostitTypeOnScene(p, pVO);
					costs.addPostit(pVO);
					int index = costs.postits.indexOf(pVO);
					if (costs.postits.get(index).getRelatedDatabaseObject() != null) {
						costs.postits.get(index).getRelatedDatabaseObject()
								.setBlock(7);
					}
				}

			} else {
				// RevenuePostitVO pVO = RevenuePostitVO.fromPostIt(p,
				// revenues);
				// changePostitTypeOnScene(p, pVO);
				// if (!revenues.addPostit(pVO)) {
				// deletePostIt(pVO);
				// } else {
				// int index = revenues.postits.indexOf(pVO);
				// if (revenues.postits.get(index).getRelatedDatabaseObject() !=
				// null) {
				// revenues.postits.get(index).getRelatedDatabaseObject()
				// .setBlock(8);
				// }
				// }
				if (revenues.postits.size() >= revenues.getLimit()) {
					if(startX == -1 || startY == -1){
						deletePostIt(p);
					} else{
						p.setPosition(startX, startY);
						p.setPosX(startX);
						p.setPosY(startY);
						//defineBlock(p, -1, -1, startX, startY);
					}
				} else {
					if (p.getParentBlock() != null) {
						p.getParentBlock().removePostit(p);
					}
					p.setPosition(posX, posY);
					p.setPosX(posX);
					p.setPosY(posY);
					RevenuePostitVO pVO = RevenuePostitVO.fromPostIt(p,
							revenues);
					changePostitTypeOnScene(p, pVO);
					revenues.addPostit(pVO);
					int index = revenues.postits.indexOf(pVO);
					if (revenues.postits.get(index).getRelatedDatabaseObject() != null) {
						revenues.postits.get(index).getRelatedDatabaseObject()
								.setBlock(8);
					}
				}

			}
		} else if (pX > CanvasActivity.TOOL_BOX_WIDTH
				+ (4 * CanvasActivity.CANVAS_WIDTH / 5)) {
			if (customerSegments.postits.size() >= customerSegments.getLimit()) {
				if(startX == -1 || startY == -1){
					deletePostIt(p);
				} else{
					p.setPosition(startX, startY);
					p.setPosX(startX);
					p.setPosY(startY);
					//defineBlock(p, -1, -1, startX, startY);
				}
			} else {
				if (p.getParentBlock() != null) {
					p.getParentBlock().removePostit(p);
				}
				p.setPosition(posX, posY);
				p.setPosX(posX);
				p.setPosY(posY);
				CustomerPostitVO pVO = CustomerPostitVO.fromPostIt(p,
						customerSegments);
				changePostitTypeOnScene(p, pVO);
				customerSegments.addPostit(pVO);
				int index = customerSegments.postits.indexOf(pVO);
				if (customerSegments.postits.get(index)
						.getRelatedDatabaseObject() != null) {
					customerSegments.postits.get(index)
							.getRelatedDatabaseObject().setBlock(6);
				}
			}

		} else if (pX > (CanvasActivity.TOOL_BOX_WIDTH
				+ (2 * CanvasActivity.CANVAS_WIDTH / 5) + CanvasActivity.CANVAS_WIDTH * 0.15f)) {
			if (pY > CanvasActivity.CAMERA_HEIGHT / 3) {
				if (channels.postits.size() >= channels.getLimit()) {
					if(startX == -1 || startY == -1){
						deletePostIt(p);
					} else{
						p.setPosition(startX, startY);
						p.setPosX(startX);
						p.setPosY(startY);
						//defineBlock(p, -1, -1, startX, startY);
					}
				} else {
					if (p.getParentBlock() != null) {
						p.getParentBlock().removePostit(p);
					}
					p.setPosition(posX, posY);
					p.setPosX(posX);
					p.setPosY(posY);
					ChannelPostitVO pVO = ChannelPostitVO.fromPostIt(p,
							channels);
					changePostitTypeOnScene(p, pVO);
					channels.addPostit(pVO);
					int index = channels.postits.indexOf(pVO);
					if (channels.postits.get(index).getRelatedDatabaseObject() != null) {
						channels.postits.get(index).getRelatedDatabaseObject()
								.setBlock(5);
					}
				}

			} else {
				if (relationships.postits.size() >= relationships.getLimit()) {
					if(startX == -1 || startY == -1){
						deletePostIt(p);
					} else{
						p.setPosition(startX, startY);
						p.setPosX(startX);
						p.setPosY(startY);
						//defineBlock(p, -1, -1, startX, startY);
					}
				} else {
					if (p.getParentBlock() != null) {
						p.getParentBlock().removePostit(p);
					}
					p.setPosition(posX, posY);
					p.setPosX(posX);
					p.setPosY(posY);
					RelationshipPostitVO pVO = RelationshipPostitVO.fromPostIt(
							p, relationships);
					changePostitTypeOnScene(p, pVO);
					relationships.addPostit(pVO);
					int index = relationships.postits.indexOf(pVO);
					if (relationships.postits.get(index)
							.getRelatedDatabaseObject() != null) {
						relationships.postits.get(index)
								.getRelatedDatabaseObject().setBlock(4);
					}
				}
			}
		} else if (pX > CanvasActivity.TOOL_BOX_WIDTH
				+ (2 * CanvasActivity.CANVAS_WIDTH / 5)) {
			if (valuePropositions.postits.size() >= valuePropositions
					.getLimit()) {
				if(startX == -1 || startY == -1){
					deletePostIt(p);
				} else{
					p.setPosition(startX, startY);
					p.setPosX(startX);
					p.setPosY(startY);
					//defineBlock(p, -1, -1, startX, startY);
				}
			} else {
				if (p.getParentBlock() != null) {
					p.getParentBlock().removePostit(p);
				}
				p.setPosition(posX, posY);
				p.setPosX(posX);
				p.setPosY(posY);
				ValuePropositionPostitVO pVO = ValuePropositionPostitVO
						.fromPostIt(p, valuePropositions);
				changePostitTypeOnScene(p, pVO);
				valuePropositions.addPostit(pVO);
				int index = valuePropositions.postits.indexOf(pVO);
				if (valuePropositions.postits.get(index)
						.getRelatedDatabaseObject() != null) {
					valuePropositions.postits.get(index)
							.getRelatedDatabaseObject().setBlock(3);
				}
			}

		} else if (pX > CanvasActivity.TOOL_BOX_WIDTH
				+ (CanvasActivity.CANVAS_WIDTH / 5)) {
			if (pY > CanvasActivity.CAMERA_HEIGHT / 3) {
				if (keyResources.postits.size() >= keyResources.getLimit()) {
					if(startX == -1 || startY == -1){
						deletePostIt(p);
					} else{
						p.setPosition(startX, startY);
						p.setPosX(startX);
						p.setPosY(startY);
						//defineBlock(p, -1, -1, startX, startY);
					}
				} else {
					if (p.getParentBlock() != null) {
						p.getParentBlock().removePostit(p);
					}
					p.setPosition(posX, posY);
					p.setPosX(posX);
					p.setPosY(posY);
					KeyResourcePostitVO pVO = KeyResourcePostitVO.fromPostIt(p,
							keyResources);
					changePostitTypeOnScene(p, pVO);
					keyResources.addPostit(pVO);
					int index = keyResources.postits.indexOf(pVO);
					if (keyResources.postits.get(index)
							.getRelatedDatabaseObject() != null) {
						keyResources.postits.get(index)
								.getRelatedDatabaseObject().setBlock(2);
					}
				}

			} else {
				if (keyActivities.postits.size() >= keyActivities.getLimit()) {
					if(startX == -1 || startY == -1){
						deletePostIt(p);
					} else{
						p.setPosition(startX, startY);
						p.setPosX(startX);
						p.setPosY(startY);
						//defineBlock(p, -1, -1, startX, startY);
					}
				} else {
					if (p.getParentBlock() != null) {
						p.getParentBlock().removePostit(p);
					}
					p.setPosition(posX, posY);
					p.setPosX(posX);
					p.setPosY(posY);
					KeyActivityPostitVO pVO = KeyActivityPostitVO.fromPostIt(p,
							keyActivities);
					changePostitTypeOnScene(p, pVO);
					keyActivities.addPostit(pVO);
					int index = keyActivities.postits.indexOf(pVO);
					if (keyActivities.postits.get(index)
							.getRelatedDatabaseObject() != null) {
						keyActivities.postits.get(index)
								.getRelatedDatabaseObject().setBlock(1);
					}
				}
			}
		} else {
			if (keyPartnerships.postits.size() >= keyPartnerships.getLimit()) {
				if(startX == -1 || startY == -1){
					deletePostIt(p);
				} else{
					p.setPosition(startX, startY);
					p.setPosX(startX);
					p.setPosY(startY);
					//defineBlock(p, -1, -1, startX, startY);
				}
			} else {
				if (p.getParentBlock() != null) {
					p.getParentBlock().removePostit(p);
				}
				p.setPosition(posX, posY);
				p.setPosX(posX);
				p.setPosY(posY);
				KeyPartnershipPostitVO pVO = KeyPartnershipPostitVO.fromPostIt(
						p, keyPartnerships);
				changePostitTypeOnScene(p, pVO);
				keyPartnerships.addPostit(pVO);
				int index = keyPartnerships.postits.indexOf(pVO);
				if (keyPartnerships.postits.get(index)
						.getRelatedDatabaseObject() != null) {
					keyPartnerships.postits.get(index)
							.getRelatedDatabaseObject().setBlock(0);
				}
			}
		}
		if (pX < CanvasActivity.TOOL_BOX_WIDTH) {

		}
		// final String selectedBlock = message;
		// System.out.println("Block " + message);
		//
		// runOnUiThread(new Runnable() {
		// public void run() {
		// Toast.makeText(CanvasActivity.this, selectedBlock,
		// Toast.LENGTH_SHORT).show();
		// }
		// });

	}

	@Override
	public void onLoadScene() {
		CanvasManager.getInstance().loadMainSceneResources();
		createScene();
		addPostitBtn = new Sprite(CanvasActivity.TOOL_BOX_WIDTH / 2 - 35,
				CanvasActivity.CAMERA_HEIGHT * 0.075f,
				CanvasManager.getInstance().addPostitBtnTextureRegion,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager()) {
			boolean stillPressed = false;
			PostIt postit = null;

			@Override
			public boolean onAreaTouched(final TouchEvent pSceneTouchEvent,
					final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
				if (pSceneTouchEvent.isActionMove()) {
					if (!stillPressed) {
						// float x = pSceneTouchEvent.getX() >
						// CanvasActivity.TOOL_BOX_WIDTH ? pSceneTouchEvent
						// .getX() - this.getWidth() / 2
						// : CanvasActivity.TOOL_BOX_WIDTH;
						float x = pSceneTouchEvent.getX() - this.getWidth() / 2;
						float y = pSceneTouchEvent.getY() - this.getHeight()
								/ 2;
						postit = addPostIt(x, y);
						postit.setPosX(x);
						postit.setPosY(y);
						stillPressed = true;
					}
					if (postit != null) {
						float x = pSceneTouchEvent.getX() - this.getWidth() / 2;
						float y = pSceneTouchEvent.getY() - this.getHeight()
								/ 2;
						postit.setPosition(x, y);
						postit.setPosX(x);
						postit.setPosY(y);
					}
				}
				if (pSceneTouchEvent.isActionUp()) {
					stillPressed = false;
					if (postit != null) {
						if (postit.collidesWith(deleteButton)
								|| postit.collidesWith(this)
								|| postit.getX() < CanvasActivity.TOOL_BOX_WIDTH) {
							final EngineLock engineLock = CanvasManager
									.getInstance().engine.getEngineLock();
							engineLock.lock();
							/* Now it is save to remove the entity! */
							MainScene.getInstance().detachChild(postit);
							if (!postit.isDisposed()) {
								postit.dispose();
							}
							// this.postit = null;
							engineLock.unlock();
						} else {
							defineBlock(postit, -1, -1, postit.getPosX(), postit.getPosY());
						}
					}
				}
				return true;
			}
		};

		deleteButton = new Sprite(CanvasActivity.TOOL_BOX_WIDTH / 2 - 35,
				CanvasActivity.CAMERA_HEIGHT * 0.26f,
				CanvasManager.getInstance().deleteRegion,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager());

		exportButton = new Sprite(CanvasActivity.TOOL_BOX_WIDTH / 2 - 35,
				CanvasActivity.CAMERA_HEIGHT * 0.5f,
				CanvasManager.getInstance().exportTextureRegion,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager()) {

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.isActionUp()) {
					Message m = new Message();
					m.arg1 = 102;
					MainScene.getInstance().activity.handler.sendMessage(m);
					final ExportPDF exportPDF = new ExportPDF(
							CanvasManager.getInstance().context);
					final ExportImage export = new ExportImage(
							CanvasManager.getInstance().context);
					final BackupGenerator backupGenerator = new BackupGenerator(
							CanvasManager.getInstance().context, activity);
					Thread t = new Thread(new Runnable() {

						@Override
						public void run() {
							File imageFile = export.generateReport();
							backupGenerator.generateBackupFile();
							exportPDF.generateReport(imageFile);
						}
					});
					t.start();
				}
				return true;
			}
		};

		loadButton = new Sprite(CanvasActivity.TOOL_BOX_WIDTH / 2 - 35,
				CanvasActivity.CAMERA_HEIGHT * 0.65f,
				CanvasManager.getInstance().loadTextureRegion,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager()) {

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.isActionUp()) {
					try {
						Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
						intent.setType("file/*");
						activity.startActivityForResult(intent,
								CanvasActivity.PICK_FILE);
					} catch (ActivityNotFoundException e) {
						Message m = new Message();
						m.arg1 = 104;
						activity.handler.sendMessage(m);
					}
				}
				return true;
			}
		};

		infoButton = new Sprite(CanvasActivity.TOOL_BOX_WIDTH / 2 - 35,
				CanvasActivity.CAMERA_HEIGHT * 0.80f,
				CanvasManager.getInstance().infoTextureRegion,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager()) {

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.isActionUp()) {
					Message m = new Message();
					m.arg1 = 103;
					MainScene.getInstance().activity.handler.sendMessage(m);
				}
				return true;
			}
		};

		this.attachChild(deleteButton);
		this.attachChild(addPostitBtn);
		this.attachChild(exportButton);
		this.attachChild(loadButton);
		this.attachChild(infoButton);
		this.registerTouchArea(addPostitBtn);
		this.registerTouchArea(exportButton);
		this.registerTouchArea(loadButton);
		this.registerTouchArea(infoButton);
		this.setTouchAreaBindingOnActionDownEnabled(true);
		loadPostitsFromDatabase();
	}

	private PostIt addPostIt(final float pX, final float pY) {
		final GenericPostit postit = new GenericPostit(pX, pY,
				CanvasManager.getInstance().addPostitBtnTextureRegion,
				CanvasManager.getInstance().engine
						.getVertexBufferObjectManager(),
				CanvasManager.getInstance().engine, activity, null, null, null);
		postit.setHasChanged(true);
		this.attachChild(postit);
		this.registerTouchArea(postit);
		this.setTouchAreaBindingOnActionDownEnabled(true);
		return postit;
	}

	private void deletePostIt(PostIt p) {
		unregisterTouchArea(p);
		p.detachSelf();
		if (!p.isDisposed()) {
			p.dispose();
		}
	}

	public static void resetInstace() {
		instance = null;
	}

	@Override
	public void onShowScene() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onHideScene() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUnloadScene() {
		activity.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				for (int i = 0; i < getChildCount(); i++) {
					if (getChildByIndex(i) instanceof ITouchArea)
						unregisterTouchArea((ITouchArea) getChildByIndex(i));
				}
				MainScene.this.detachChildren();
				MainScene.this.clearEntityModifiers();
				MainScene.this.clearTouchAreas();
				MainScene.this.clearUpdateHandlers();
			}
		});

	}

	public static <T> ArrayList<T> getDifference(ArrayList<T> big,
			ArrayList<T> small) {
		ArrayList<T> difference = new ArrayList<T>(big);
		for (T b1 : big) {
			for (T s1 : small) {
				if (b1.equals(s1)) {
					difference.remove(b1);
				}
			}
		}
		return difference;
	}

	@Override
	public Scene onLoadingScreenLoadAndShown() {
		return null;
	}

	@Override
	public void onLoadingScreenUnloadAndHidden() {

	}

	public void cancelExportDialog(File pdf, File image) {
		Message m = new Message();
		Attachment attachment = new Attachment(pdf, image);
		m.arg1 = 101;
		m.obj = attachment;
		MainScene.getInstance().activity.handler.sendMessage(m);
	}

	class Attachment {
		public File pdf;
		public File image;

		public Attachment(File pdf, File image) {
			super();
			this.pdf = pdf;
			this.image = image;
		}
	}

}
