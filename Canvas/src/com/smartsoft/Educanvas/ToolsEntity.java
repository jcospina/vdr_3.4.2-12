package com.smartsoft.Educanvas;

import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public class ToolsEntity extends Entity {

	private float pX;
	private float pY;
	private float width;
	private float height;

	VertexBufferObjectManager vbom;

	public ToolsEntity(float pX, float pY, float width, float height,
			VertexBufferObjectManager vbom) {
		super(pX, pY);
		this.pX = pX;
		this.pY = pY;
		this.width = width;
		this.height = height;
		this.vbom = vbom;
		this.draw(vbom);
	}

	private void draw(VertexBufferObjectManager vbom) {
		Rectangle rect = new Rectangle(pX, pY, width, height, vbom);
		rect.setColor(0.745f, 0.745f, 0.745f);
		
		this.attachChild(rect);	

	}

	public float getpX() {
		return pX;
	}

	public void setpX(float pX) {
		this.pX = pX;
	}

	public float getpY() {
		return pY;
	}

	public void setpY(float pY) {
		this.pY = pY;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

}
