package com.smartsoft.Educanvas;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.andengine.engine.Engine;
import org.andengine.extension.svg.opengl.texture.atlas.bitmap.SVGBitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.EmptyBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.util.debug.Debug;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;

import com.canvas.empathymap.ImageTextureBuilder;

public class CanvasManager {
	public static final int BLOCK_POSTITS_LIMIT = 3;
	private static CanvasManager instance;

	// Game setup
	public Engine engine;
	public Context context;

	// Texturas para cada tipo de postit
	public static ITextureRegion keyPartnerRegionNormal;
	public static ITextureRegion keyPartnerRegionMax;
	public static ITextureRegion keyActivityRegionNormal;
	public static ITextureRegion keyActivityRegionMax;
	public static ITextureRegion keyResourceRegionNormal;
	public static ITextureRegion keyResourceRegionMax;
	public static ITextureRegion vPropositionPostitRegionNormal;
	public static ITextureRegion vPropositionPostitRegionMax;
	public static ITextureRegion relationshipsRegionNormal;
	public static ITextureRegion relationshipsRegionMax;
	public static ITextureRegion channelPostitRegionNormal;
	public static ITextureRegion channelPostitRegionMax;
	public static ITextureRegion customersRegionNormal;
	public static ITextureRegion customersRegionMax;
	public static ITextureRegion costsRegionNormal;
	public static ITextureRegion costsRegionMax;
	public static ITextureRegion revenueRegionNormal;
	public static ITextureRegion revenueRegionMax;

	// Escena principal
	private BuildableBitmapTextureAtlas mBuildableBitmapTextureAtlas;
	public ITextureRegion mPostitTextureRegionBG; // Postit minimizado
	public ITextureRegion mPostitTextureRegionMaxBG; // Background postit
														// maximizado
	public ITextureRegion deleteRegion;
	public ITextureRegion closeBtn;
	public ITextureRegion helpBtn;
	public ITextureRegion editAplicationInvokeBtn;
	public ITextureRegion addPostitBtnTextureRegion;
	public ITextureRegion backgroundTextureRegion;
	public ITextureRegion helpBackgroundTextureRegion;
	public ITextureRegion contentPostitTextureRegion;
	public ITextureRegion exportTextureRegion;
	public ITextureRegion loadTextureRegion;
	public ITextureRegion infoTextureRegion;

	public ITextureRegion homeBtn;
	public ITextureRegion askBtn;
	public ITextureRegion plusBtn;
	public ITextureRegion typesBtn;

	public Font minimizedPostitFont;
	public Font titleFont;
	public Font descriptionFont;

	// public Scene mScene;
	public TiledTextureRegion mTiledTextureRegion;
	public TiledTextureRegion colorBtnTextureRegion;

	// Mapa de empat�a
	public static ITextureRegion empathyFaceRegion;
	public static ImageTextureBuilder faceTexture;
	public static ITextureRegion frameRegion;
	public static ITextureRegion EMBackgroundRegion;
	public static ITextureRegion takePictureRegion;
	public static ITextureRegion pickFromGalleryRegion;
	public static ITextureRegion frameShadowRegion;
	public static ITextureRegion smallEMapPostit;
	public static ITextureRegion bigEMapPostit;
	public static ITextureRegion emDdeleteRegion;
	public static ITextureRegion emAddPostitBtnTextureRegion;
	public static ITextureRegion customerBGTextureRegion;

	public BitmapTextureAtlas empathyMapBitmapTextureAtlas;

	// Propuestas de valor
	public static ITextureRegion vPropositionsCircleRegion;
	public static ITiledTextureRegion pinRegion;

	// Relaciones
	public static ITextureRegion relationshipsBackground;
	public static ITextureRegion relationshipsAppMin;
	public static ITextureRegion relationshipsAppMax;
	public static ITextureRegion userIconRegion;
	public static ITextureRegion closeIconRegion;
	public static ITextureRegion recicleBinRegion;

	// Alianzas clave
	public static ITextureRegion keypartnershipsBackground;
	public static ITextureRegion keypartnershipsShadow;
	public static ITextureRegion redLightRegion;
	public static ITextureRegion yellowLightRegion;
	public static ITextureRegion greenLightRegion;
	public static ITextureRegion keyPartnershipPostitRegion;

	// Canales
	public static ITextureRegion truck;
	public static ITextureRegion airplane;
	public static ITextureRegion ship;
	public static ITextureRegion mail;
	public static ITextureRegion internet;
	public static ITextureRegion other;
	public static ITextureRegion channelBackground;
	public static ITextureRegion[] valueproposition = new ITextureRegion[3];

	BitmapTextureAtlas mainSceneBitmapTextureAtlas;
	BitmapTextureAtlas mainSceneBackgroundAtlas;

	// This variable will be used to revert the TextureFactory's default path
	// when we change it.
	private String mPreviousAssetBasePath = "";

	public static CanvasManager getInstance() {
		if (instance == null) {
			instance = new CanvasManager();
		}
		return instance;
	}

	// Setup the Manager
	public void setup(final Engine pEngine, final Context pContext) {
		engine = pEngine;
		context = pContext;
	}

	// ====================POSTIT TYPES TEXTURE RESOURCES=====================
	public void loadPostitTypesTextureResources() {
		// Store the current asset base path to apply it after we've loaded our
		// textures
		mPreviousAssetBasePath = BitmapTextureAtlasTextureRegionFactory
				.getAssetBasePath();
		// Set our game assets folder to "assets/gfx/game/"
		BitmapTextureAtlasTextureRegionFactory
				.setAssetBasePath("gfx/postittypes/");

		BuildableBitmapTextureAtlas texture = new BuildableBitmapTextureAtlas(
				engine.getTextureManager(), 1024, 1024);
		keyPartnerRegionNormal = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "partner.png");
		keyPartnerRegionMax = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "value_proposition_clip.png");
		keyActivityRegionNormal = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "activity.png");
		keyActivityRegionMax = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "activity_clip.png");
		keyResourceRegionNormal = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "resource.png");
		keyResourceRegionMax = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "resource_clip.png");
		vPropositionPostitRegionNormal = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "value_proposition.png");
		vPropositionPostitRegionMax = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "value_proposition_clip.png");
		relationshipsRegionNormal = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "relationship.png");
		relationshipsRegionMax = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "relationship_clip.png");
		channelPostitRegionNormal = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "channel.png");
		channelPostitRegionMax = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "channel_clip.png");
		customersRegionNormal = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "customer.png");
		customersRegionMax = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "customer_clip.png");
		costsRegionNormal = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "cost.png");
		costsRegionMax = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "cost_clip.png");
		revenueRegionNormal = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "revenue.png");
		revenueRegionMax = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "revenue_clip.png");
		try {
			texture.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
					0, 1, 4));
			texture.load();
		} catch (TextureAtlasBuilderException e) {
			Debug.e(e);
		}
		// Revert the Asset Path.
		BitmapTextureAtlasTextureRegionFactory
				.setAssetBasePath(mPreviousAssetBasePath);
	}

	// ====================MAIN SCENE RESOURCES======================

	public void loadMainSceneResources() {

		if (mainSceneBackgroundAtlas != null
				&& mainSceneBackgroundAtlas.isLoadedToHardware()
				&& mainSceneBackgroundAtlas != null
				&& mainSceneBackgroundAtlas.isLoadedToHardware()
				&& mBuildableBitmapTextureAtlas != null
				&& mBuildableBitmapTextureAtlas.isLoadedToHardware()) {
			return;
		}

		loadPostitTypesTextureResources();
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		SVGBitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		// BitmapTextureAtlas mainSceneBitmapTextureAtlas;
		mainSceneBitmapTextureAtlas = new BitmapTextureAtlas(
				engine.getTextureManager(), 1100, 1600, TextureOptions.BILINEAR);
		this.mBuildableBitmapTextureAtlas = new BuildableBitmapTextureAtlas(
				engine.getTextureManager(), 1500, 1500, TextureOptions.NEAREST);

		this.mPostitTextureRegionMaxBG = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(mainSceneBitmapTextureAtlas, context,
						"ventana_principal.gif", 0, 0); // 633x670 ->633x670

		this.helpBackgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(mainSceneBitmapTextureAtlas, context,
						"ventana_ayuda.gif", 633, 0); // 440x504 ->1073x670

		this.contentPostitTextureRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(mainSceneBitmapTextureAtlas, context,
						"hoja.gif", 0, 670); // 460x506 ->460x670

		this.mTiledTextureRegion = BitmapTextureAtlasTextureRegionFactory
				.createTiledFromAsset(mainSceneBitmapTextureAtlas, context,
						"transparent.png", 460, 670, 1, 1); // 500x60 ->960x730

		addPostitBtnTextureRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(mainSceneBitmapTextureAtlas, context,
						"arrastrar_lienzo.png", 460, 730); // 100x98 ->

		deleteRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				mainSceneBitmapTextureAtlas, context, "eliminar_lienzo.png", 560, 730);// 100x98
																				// ->

		closeBtn = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				mainSceneBitmapTextureAtlas, context, "cerrar.png", 660, 730); // 90x89

		colorBtnTextureRegion = BitmapTextureAtlasTextureRegionFactory
				.createTiledFromAsset(mainSceneBitmapTextureAtlas, context,
						"pintura.png", 750, 730, 1, 1);// 60x86

		helpBtn = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
				mainSceneBitmapTextureAtlas, context, "boton_ir_ayuda.png",
				811, 730, 1, 1);// 43x73

		editAplicationInvokeBtn = BitmapTextureAtlasTextureRegionFactory
				.createTiledFromAsset(mainSceneBitmapTextureAtlas, context,
						"boton_ir_miniapp.png", 853, 730, 1, 1);// 69x68

		plusBtn = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
				mainSceneBitmapTextureAtlas, context, "boton_vermas.png", 922,
				730, 1, 1);// 51x55

		askBtn = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
				mainSceneBitmapTextureAtlas, context, "boton_ayuda.png", 973,
				730, 1, 1);// 51x55

		typesBtn = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
				mainSceneBitmapTextureAtlas, context, "boton_rayitas.png",
				1024, 730, 1, 1);// 51x55

		homeBtn = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
				mainSceneBitmapTextureAtlas, context, "home_icon.png", 460,
				830, 1, 1);// 51x55
		
		loadTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
				mainSceneBitmapTextureAtlas, context, "cargar_lienzo.png", 0,
				1500, 1, 1);// 100x100
		
		exportTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
				mainSceneBitmapTextureAtlas, context, "exportar_lienzo.png", 100,
				1500, 1, 1);// 100x100
		
		infoTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
				mainSceneBitmapTextureAtlas, context, "info_lienzo.png", 200,
				1500, 1, 1);// 100x100

		this.mPostitTextureRegionBG = SVGBitmapTextureAtlasTextureRegionFactory
				.createFromAsset(this.mBuildableBitmapTextureAtlas, context,
						"postitbg.svg", 100, 100); // 64x32

		mainSceneBackgroundAtlas = new BitmapTextureAtlas(
				engine.getTextureManager(), 1300, 800, TextureOptions.BILINEAR);

		backgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(mainSceneBackgroundAtlas, context,
						"canvas_nuevo.GIF", 0, 0);

		try {
			mainSceneBitmapTextureAtlas.load();
			this.mBuildableBitmapTextureAtlas
					.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
							0, 1, 0));
			this.mBuildableBitmapTextureAtlas.load();
			mainSceneBackgroundAtlas.load();
		} catch (final TextureAtlasBuilderException e) {
			Debug.e(e);
		}

		minimizedPostitFont = FontFactory.create(engine.getFontManager(),
				engine.getTextureManager(), 256, 256,
				Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 16);
		minimizedPostitFont.load();

		titleFont = FontFactory.create(engine.getFontManager(),
				engine.getTextureManager(), 256, 256,
				Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 32);
		titleFont.load();

		descriptionFont = FontFactory.create(engine.getFontManager(),
				engine.getTextureManager(), 256, 256,
				Typeface.create(Typeface.DEFAULT, Typeface.NORMAL), 20);
		descriptionFont.load();
	}

	// ====================EMPATHY MAP RESOURCES=====================
	public List<BitmapTextureAtlas> loadEmpathyMapResources() {
		// Store the current asset base path to apply it after we've loaded our
		// textures
		List<BitmapTextureAtlas> textures = new ArrayList<BitmapTextureAtlas>();

		mPreviousAssetBasePath = BitmapTextureAtlasTextureRegionFactory
				.getAssetBasePath();
		// Set our game assets folder to "assets/gfx/game/"
		try {
			BitmapTextureAtlasTextureRegionFactory
					.setAssetBasePath("gfx/empathymap/");
			BitmapTextureAtlas texture = new BitmapTextureAtlas(
					engine.getTextureManager(), 1900, 752,
					TextureOptions.BILINEAR);
			empathyMapBitmapTextureAtlas = new BitmapTextureAtlas(
					engine.getTextureManager(), 256, 256,
					TextureOptions.BILINEAR);
			BitmapTextureAtlas postit_texture = new BitmapTextureAtlas(
					engine.getTextureManager(), 700, 700,
					TextureOptions.BILINEAR);
			Bitmap empathyFace = BitmapFactory.decodeStream(context.getAssets()
					.open("gfx/empathymap/customer.png"));
			IBitmapTextureAtlasSource base = new EmptyBitmapTextureAtlasSource(
					256, 256);
			faceTexture = new ImageTextureBuilder(base, empathyFace);

			EMBackgroundRegion = BitmapTextureAtlasTextureRegionFactory
					.createFromAsset(texture, context,
							"fondo_mapa_empatia.gif", 0, 0);
			frameRegion = BitmapTextureAtlasTextureRegionFactory
					.createFromAsset(texture, context, "marco.png", 1280, 0);
			empathyFaceRegion = BitmapTextureAtlasTextureRegionFactory
					.createFromSource(empathyMapBitmapTextureAtlas,
							faceTexture, 0, 0);
			takePictureRegion = BitmapTextureAtlasTextureRegionFactory
					.createFromAsset(texture, context, "camera.png", 1380, 300);
			pickFromGalleryRegion = BitmapTextureAtlasTextureRegionFactory
					.createFromAsset(texture, context, "gallery.png", 1444, 300);
			frameShadowRegion = BitmapTextureAtlasTextureRegionFactory
					.createFromAsset(texture, context, "sombra.png", 1280, 400);

			smallEMapPostit = BitmapTextureAtlasTextureRegionFactory
					.createFromAsset(postit_texture, context,
							"ventana_principal_small.png", 600, 0);
			bigEMapPostit = BitmapTextureAtlasTextureRegionFactory
					.createFromAsset(postit_texture, context,
							"ventana_principal_sin.gif", 0, 0);
			emDdeleteRegion = BitmapTextureAtlasTextureRegionFactory
					.createFromAsset(texture, context, "borrar.png", 1600, 0);// 100x98
			emAddPostitBtnTextureRegion = BitmapTextureAtlasTextureRegionFactory
					.createFromAsset(texture, context, "arrastrar.png", 1600,
							100); // 100x98 ->
			customerBGTextureRegion = BitmapTextureAtlasTextureRegionFactory
					.createFromAsset(texture, context, "customer_bg.png", 1600,
							200);
			postit_texture.load();
			empathyMapBitmapTextureAtlas.load();
			texture.load();

			textures.add(postit_texture);
			textures.add(empathyMapBitmapTextureAtlas);
			textures.add(texture);
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}
		// empathyFaceRegion =
		// BitmapTextureAtlasTextureRegionFactory.createFromAsset(mBitmapTextureAtlas,
		// context, "empathy_face.png",10,10); // 64x32
		// Revert the Asset Path.
		BitmapTextureAtlasTextureRegionFactory
				.setAssetBasePath(mPreviousAssetBasePath);
		return textures;
	}

	// ====================VALUE PROPOSITIONS RESOURCES=====================
	public List<BitmapTextureAtlas> loadValuePropositionsResources() {
		// Store the current asset base path to apply it after we've loaded our
		// textures
		List<BitmapTextureAtlas> textures = new ArrayList<BitmapTextureAtlas>();
		mPreviousAssetBasePath = BitmapTextureAtlasTextureRegionFactory
				.getAssetBasePath();
		// Set our game assets folder to "assets/gfx/game/"
		BitmapTextureAtlasTextureRegionFactory
				.setAssetBasePath("gfx/valuepropositions/");

		BitmapTextureAtlas texture = new BitmapTextureAtlas(
				engine.getTextureManager(), 1280, 900); // 786x700
		vPropositionsCircleRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context,
						"value_propositions_circle.gif", 0, 0);
		pinRegion = BitmapTextureAtlasTextureRegionFactory
				.createTiledFromAsset(texture, context, "pin_sprite.png", 0,
						800, 2, 1);// 200x100
		try {
			texture.load();
			textures.add(texture);
		} catch (Exception ex) {
		}

		// Revert the Asset Path.
		BitmapTextureAtlasTextureRegionFactory
				.setAssetBasePath(mPreviousAssetBasePath);

		return textures;
	}

	// ====================RELATIONSHIPS RESOURCES=====================
	public List<BitmapTextureAtlas> loadRelationshipsResources() {
		// Store the current asset base path to apply it after we've loaded our
		// textures

		List<BitmapTextureAtlas> textures = new ArrayList<BitmapTextureAtlas>();

		mPreviousAssetBasePath = BitmapTextureAtlasTextureRegionFactory
				.getAssetBasePath();
		// Set our game assets folder to "assets/gfx/game/"
		BitmapTextureAtlasTextureRegionFactory
				.setAssetBasePath("gfx/relationships/");

		BitmapTextureAtlas texture = new BitmapTextureAtlas(
				engine.getTextureManager(), 1280, 1312);

		relationshipsAppMax = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "corazon.gif", 0, 0);// 588x560

		closeIconRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "boton_cerrar.png", 600, 300);// 200x100

		relationshipsBackground = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "fondo_relaciones.png", 0,
						560);// 1280x752

		relationshipsAppMin = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "pila.png", 600, 0);// 150x150

		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

		recicleBinRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "eliminar_lienzo.png", 600, 400);// 200x100

		BitmapTextureAtlasTextureRegionFactory
				.setAssetBasePath("gfx/postittypes/");

		userIconRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "customer.png", 600, 150);// 200x100

		try {

			texture.load();
			textures.add(texture);
		} catch (Exception ex) {
		}

		// Revert the Asset Path.
		BitmapTextureAtlasTextureRegionFactory
				.setAssetBasePath(mPreviousAssetBasePath);
		return textures;
	}

	// ====================CHANNEL RESOURCES=====================
	public List<BitmapTextureAtlas> loadChannelsResources() {
		// Store the current asset base path to apply it after we've loaded our
		// textures
		List<BitmapTextureAtlas> textures = new ArrayList<BitmapTextureAtlas>();

		mPreviousAssetBasePath = BitmapTextureAtlasTextureRegionFactory
				.getAssetBasePath();
		// Set our game assets folder to "assets/gfx/game/"
		BitmapTextureAtlasTextureRegionFactory
				.setAssetBasePath("gfx/channels/");

		BitmapTextureAtlas texture = new BitmapTextureAtlas(
				engine.getTextureManager(), 2400, 1200);
		truck = BitmapTextureAtlasTextureRegionFactory.createFromAsset(texture,
				context, "truck_big.gif", 0, 0);
		airplane = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				texture, context, "plane_big.gif", 800, 0);
		ship = BitmapTextureAtlasTextureRegionFactory.createFromAsset(texture,
				context, "ship_big.gif", 0, 500);
		mail = BitmapTextureAtlasTextureRegionFactory.createFromAsset(texture,
				context, "mail_big.gif", 800, 500);
		other = BitmapTextureAtlasTextureRegionFactory.createFromAsset(texture,
				context, "other_big.gif", 1600, 0);
		internet = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				texture, context, "internet_big.gif", 1600, 500);
		for (int i = 0; i < valueproposition.length; i++) {
			valueproposition[i] = BitmapTextureAtlasTextureRegionFactory
					.createFromAsset(texture, context, "nota_" + i + ".png",
							200 * i, 1000);
		}
		BitmapTextureAtlas bgTexture = new BitmapTextureAtlas(
				engine.getTextureManager(), 1280, 752);
		channelBackground = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(bgTexture, context, "fondo.gif", 0, 0);
		try {
			texture.load();
			bgTexture.load();
			textures.add(texture);
			textures.add(bgTexture);
		} catch (Exception e) {

		}
		// Revert the Asset Path.
		BitmapTextureAtlasTextureRegionFactory
				.setAssetBasePath(mPreviousAssetBasePath);
		return textures;
	}

	// ====================KEY PARTNERSHIPS RESOURCES=====================
	public List<BitmapTextureAtlas> loadKeyPartnershipsResources() {

		// Store the current asset base path to apply it after we've loaded our
		// textures
		List<BitmapTextureAtlas> textures = new ArrayList<BitmapTextureAtlas>();

		mPreviousAssetBasePath = BitmapTextureAtlasTextureRegionFactory
				.getAssetBasePath();
		// Set our game assets folder to "assets/gfx/game/"
		BitmapTextureAtlasTextureRegionFactory
				.setAssetBasePath("gfx/partnerships/");

		BitmapTextureAtlas texture = new BitmapTextureAtlas(
				engine.getTextureManager(), 1280, 1163);

		keypartnershipsBackground = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "fondo_semaforo.gif", 0, 0);// 1280x752

		redLightRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "color_rojo.gif", 0, 752);// 369x368
		yellowLightRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "color_naranja.gif", 369,
						752);// 346x366
		greenLightRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "color_verde.gif", 715, 752);// 355x326

		keypartnershipsShadow = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "sombra.png", 0, 1120); // 1081x43

		BitmapTextureAtlasTextureRegionFactory
				.setAssetBasePath("gfx/postittypes/");

		keyPartnershipPostitRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texture, context, "partner.png", 1070, 752); // 100x100

		try {

			texture.load();
			textures.add(texture);
		} catch (Exception ex) {
		}

		// Revert the Asset Path.
		BitmapTextureAtlasTextureRegionFactory
				.setAssetBasePath(mPreviousAssetBasePath);
		return textures;
	}

	public static void resetInstace() {
		instance = null;
	}

	public static void unloadTextures(List<BitmapTextureAtlas> textures) {
		for (BitmapTextureAtlas b : textures) {
			try {
				b.unload();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

}
