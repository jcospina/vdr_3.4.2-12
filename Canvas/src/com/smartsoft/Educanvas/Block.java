/**
 * Representa cada uno de los bloques del modelo Canvas. Es una clase gen�rica y al ser
 * instanciada es recomendable indicar expl�citamente que tipo de PostIt almacenar�, en
 * su listado, dicho tipo de PostIt es principalmente un objeto que herede de la clase
 * PostIt.
 */

package com.smartsoft.Educanvas;

import java.util.ArrayList;
import java.util.List;

import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Line;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

public class Block<T> extends Entity {
	/**
	 * Representa el n�mero del bloque para guardarlo en la base de datos
	 */
	private int blockNumber;
	/**
	 * Representa el valor m�ximo de postits que puede contener el bloque
	 */
	private int limit;
	/**
	 * Representa la posici�n en X donde se ubicar� el bloque
	 */
	private float pX;

	/**
	 * Representa la posici�n en Y donde se ubicar� el bloque
	 */
	private float pY;

	/**
	 * Representa el ancho del bloque
	 */
	private float width;

	/**
	 * Representa el alto del bloque
	 */
	private float height;

	/**
	 * Listado donde se almacenar�n los PostIts incluidos dentro del bloque
	 */
	public List<T> postits;

	VertexBufferObjectManager vbom;

	public Block(float pX, float pY, float width, float height, int limit,
			int blockNumber, VertexBufferObjectManager vbom) {
		super(pX, pY);
		this.pX = pX;
		this.pY = pY;
		this.width = width;
		this.height = height;
		this.vbom = vbom;
		this.limit = limit;
		this.blockNumber = blockNumber;
		//this.drawBorder(vbom);
		this.postits = new ArrayList<T>();
	}

	/**
	 * M�todo para dibujar el bloque en la escena, se basa en los campos pX, pY,
	 * width y height
	 * 
	 * @param vbom
	 */
	public void drawBorder(VertexBufferObjectManager vbom) {
		Line topLine = new Line(0, 0, this.width, 0, vbom);
		topLine.setColor(Color.BLACK);

		Line bottomLine = new Line(0, this.height, this.width, this.height,
				vbom);
		bottomLine.setColor(Color.BLACK);

		Line leftLine = new Line(0, 0, 0, this.height, vbom);
		leftLine.setColor(Color.BLACK);

		Line rightLine = new Line(this.width, 0, this.width, this.height, vbom);
		rightLine.setColor(Color.BLACK);

		this.attachChild(topLine);
		this.attachChild(rightLine);
		this.attachChild(bottomLine);
		this.attachChild(leftLine);
	}

	public boolean addPostit(T p) {
		if (postits.size() >= limit) {
			return false;
		}

		this.postits.add(p);
		return true;

	}

	public boolean removePostit(T p) {
		this.postits.remove(p);
		return true;
	}

	public float getpX() {
		return pX;
	}

	public void setpX(float pX) {
		this.pX = pX;
	}

	public float getpY() {
		return pY;
	}

	public void setpY(float pY) {
		this.pY = pY;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getBlockNumber() {
		return blockNumber;
	}

	public void setBlockNumber(int blockNumber) {
		this.blockNumber = blockNumber;
	}

	public class BlockNumbers {
		public static final int KEY_PARTNERSHIPS_BN = 0;
		public static final int KEY_ACTIVITIES_BN = 1;
		public static final int KEY_RESOURCES_BN = 2;
		public static final int VALUE_PROPOSITIONS_BN = 3;
		public static final int RELATIONSHIPS_BN = 4;
		public static final int CHANNELS_BN = 5;
		public static final int CUSTOMER_SEGMENTS_BN = 6;
		public static final int COSTS_BN = 7;
		public static final int REVENUEW_BN = 8;
	}
}
