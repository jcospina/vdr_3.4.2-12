package com.smartsoft.Educanvas;

import java.util.List;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.canvas.dataaccess.CanvasDB;
import com.canvas.dataaccess.controller.CanvasController;
import com.canvas.util.CoverFlow;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class MainActivity extends Activity {

	private List<CanvasDB> canvasList;
	private CoverFlow coverFlow;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_canvas);

		coverFlow = (CoverFlow) findViewById(R.id.coverFlow);
		AdView adView = (AdView) findViewById(R.id.adView);
		adView.loadAd(new AdRequest());
		populateCoverFlow();

		// Use this if you want to use XML layout file

		// coverFlow = (CoverFlow) findViewById(R.id.coverFlow);
		// setContentView(coverFlow);
	}

	@SuppressWarnings("deprecation")
	private void populateCoverFlow() {
		CanvasController cc = new CanvasController(this);
		canvasList = cc.getAll();

		CanvasAdapter coverImageAdapter = new CanvasAdapter(this, canvasList);

		// coverImageAdapter.createReflectedImages();

		// coverImageAdapter.createReflectedImages();
		coverFlow.setAdapter(coverImageAdapter);
		// coverFlow.setSpacing(-15);
		coverFlow.setSpacing(-200);

		ImageButton deleteCanvasBtn = (ImageButton) findViewById(R.id.deleteCanvasBtn);
		if (canvasList != null && !canvasList.isEmpty()) {
			coverFlow.setSelection(0, true);
			deleteCanvasBtn.setEnabled(true);
		} else {
			deleteCanvasBtn.setEnabled(false);
		}
		coverFlow.setAnimationDuration(1000);
	}

	public void setCurrentCanvas(int position) {
	}

	public void showCanvas(View v) {
		if (coverFlow.getSelectedItemPosition() != AdapterView.INVALID_POSITION) {
			Intent i = new Intent(this, CanvasActivity.class);
			i.putExtra("canvasId",
					canvasList.get(coverFlow.getSelectedItemPosition()).getId());
			startActivity(i);
			overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
		}
	}

	@SuppressWarnings("deprecation")
	public void showCredits(View v) {

		final ImageButton creditsBtn = (ImageButton) findViewById(R.id.creditsBtn);
		if (!creditsBtn.isEnabled()) {
			return;
		}

		final int w = getWindowManager().getDefaultDisplay().getWidth() - 100;
		final int h = (int) (2.75f * w);

		creditsBtn.setEnabled(false);
		Animation anim = AnimationUtils.loadAnimation(this, R.anim.fadein);
		anim.setDuration(1000);
		final RelativeLayout blackBackground = new RelativeLayout(this);
		LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, h);
		blackBackground.setBackgroundResource(R.color.transparent_black);
		blackBackground.setLayoutParams(params);
		final RelativeLayout rootView = (RelativeLayout) findViewById(R.id.rootView);
		rootView.addView(blackBackground);
		blackBackground.clearAnimation();
		blackBackground.setAnimation(anim);
		blackBackground.startAnimation(anim);
		
		anim.setAnimationListener(new Animation.AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				blackBackground.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						hideCredits(blackBackground);
					}
				});
			}
		});
		
		

		LinearLayout ll = new LinearLayout(MainActivity.this);
		ll.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, h));
		ll.setGravity(Gravity.CENTER_HORIZONTAL);

		final ImageView creditsImg = new ImageView(MainActivity.this);
		creditsImg.setImageResource(R.drawable.credits);

		Animation creditsAnimation = AnimationUtils.loadAnimation(
				MainActivity.this, R.anim.push_credits_bottom_in);

		creditsImg.setLayoutParams(new LayoutParams(w, h));
		ll.addView(creditsImg);
		blackBackground.addView(ll);
		creditsImg.clearAnimation();
		creditsImg.setAnimation(creditsAnimation);
		creditsImg.startAnimation(creditsAnimation);

		creditsAnimation
				.setAnimationListener(new Animation.AnimationListener() {

					@Override
					public void onAnimationStart(Animation animation) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onAnimationRepeat(Animation animation) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onAnimationEnd(Animation animation) {
						creditsImg.setAlpha(0);
						
						hideCredits(blackBackground);
					}
				});

	}

	public void hideCredits(final View v) {
		Animation anim = AnimationUtils.loadAnimation(this, R.anim.fadeout);
		v.setAnimation(anim);
		v.clearAnimation();
		v.setAnimation(anim);
		v.startAnimation(anim);

		anim.setAnimationListener(new Animation.AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				RelativeLayout rl = (RelativeLayout) v.getParent();
				rl.removeView(v);
				((ImageButton) findViewById(R.id.creditsBtn)).setEnabled(true);
			}
		});
	}

	public void createNewCanvas(View v) {
		/**/

		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		// Get the layout inflater
		LayoutInflater inflater = getLayoutInflater();

		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the dialog layout

		final View dialogView = inflater.inflate(R.layout.new_canvas_dialog,
				null);

		builder.setView(dialogView)
				// Add action buttons
				.setPositiveButton(R.string.create,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								CanvasController cc = new CanvasController(
										MainActivity.this);
								CanvasDB c = new CanvasDB();
								EditText et = (EditText) dialogView
										.findViewById(R.id.newCanvasNameEditText);
								String name = (et != null && !et.getText()
										.toString().trim().equals("")) ? et
										.getText().toString().trim() : null;
								EditText autoret = (EditText) dialogView
										.findViewById(R.id.canvasAutorEditText);
								String autor = (autoret != null && !autoret
										.getText().toString().trim().equals("")) ? autoret
										.getText().toString().trim()
										: null;
								if (name != null) {
									c.setName(name);
									c.setAutor(autor);
									int nCid = (int) cc.create(c);
									Intent i = new Intent(MainActivity.this,
											CanvasActivity.class);
									i.putExtra("canvasId", nCid);
									startActivity(i);
								} else {
									Toast.makeText(MainActivity.this,
											R.string.empty_name,
											Toast.LENGTH_SHORT).show();
								}
							}
						})
				.setNegativeButton(R.string.cancel,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
							}
						});

		builder.create();
		builder.show();
	}

	public void removeCurrentCanvas(View v) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		if (canvasList == null || canvasList.isEmpty()) {
			return;
		}

		final CanvasDB currentCanvas = canvasList.get(coverFlow
				.getSelectedItemPosition());
		builder.setTitle(R.string.RemoveCanvasBtnLabel);
		builder.setMessage(getResources()
				.getString(R.string.removeConfirmation)
				+ " "
				+ currentCanvas.getName() + "?");

		builder.setPositiveButton(R.string.RemoveCanvasBtnLabel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						CanvasController cc = new CanvasController(
								MainActivity.this);

						if (cc.removeCanvas(currentCanvas.getId())) {
							populateCoverFlow();
							Toast.makeText(MainActivity.this,
									R.string.remove_canvas_successfully,
									Toast.LENGTH_SHORT).show();
						} else {
							Toast.makeText(MainActivity.this,
									R.string.remove_canvas_error,
									Toast.LENGTH_SHORT).show();
						}
					}
				}).setNegativeButton(R.string.cancel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});

		builder.create();
		builder.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		populateCoverFlow();
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		populateCoverFlow();
		super.onResume();
	}
}
