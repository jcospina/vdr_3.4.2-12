//Activity principal del lienzo parece que si la felicid�

package com.smartsoft.Educanvas;

import java.io.ByteArrayOutputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.RelativeResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.util.FPSLogger;
import org.andengine.entity.util.ScreenGrabber;
import org.andengine.opengl.view.RenderSurfaceView;
import org.andengine.ui.activity.BaseGameActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.canvas.channels.ChannelsScene;
import com.canvas.dataaccess.CanvasDB;
import com.canvas.dataaccess.PostItDB;
import com.canvas.dataaccess.controller.CanvasController;
import com.canvas.dataaccess.controller.PostitController;
import com.canvas.empathymap.EmpathyMapScene;
import com.canvas.partnerships.KeyPartnershipsScene;
import com.canvas.relationships.RelationshipsScene;
import com.canvas.util.LoadBackup;
import com.canvas.valuepropositions.ValuePropositionsScene;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.smartsoft.Educanvas.MainScene.Attachment;

public class CanvasActivity extends BaseGameActivity {

	// ===========================================================
	// Constants
	// ===========================================================
	public static final int CAMERA_WIDTH = 1280;
	public static final int CAMERA_HEIGHT = 752;
	public static final float TOOL_BOX_WIDTH = (float) (0.1 * CAMERA_WIDTH);
	public static final float CANVAS_WIDTH = (float) (CAMERA_WIDTH - TOOL_BOX_WIDTH);
	// public static final float CANVAS_WIDTH = (float) (CAMERA_WIDTH);
	public static final int EMPATHY_REQUEST_CODE = 1100;
	public static final int PIC_CROP = 1200;
	public static final int PICK_FROM_GALLERY = 1300;
	public static final int PICK_FILE = 1400;

	public static int canvasId = 0;
	private ProgressDialog exportDialog;

	@SuppressWarnings("deprecation")
	@Override
	protected void onSetContentView() {
		this.mRenderSurfaceView = new RenderSurfaceView(this);
		this.mRenderSurfaceView.setRenderer(this.mEngine, this);
		final android.widget.FrameLayout.LayoutParams surfaceViewLayoutParams = new FrameLayout.LayoutParams(
				super.createSurfaceViewLayoutParams());

		// Creating the banner view.
		AdView adView = new AdView(this, AdSize.IAB_BANNER, "a1519ababad2403");
		AdRequest ar = new AdRequest();
		ar.addTestDevice("0123456789ABCDEF");
		//adView.loadAd(ar);
		final FrameLayout.LayoutParams adViewLayoutParams = new FrameLayout.LayoutParams(
				FrameLayout.LayoutParams.WRAP_CONTENT,
				FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.BOTTOM
						| Gravity.CENTER_HORIZONTAL);

		final FrameLayout frameLayout = new FrameLayout(this);
		final FrameLayout.LayoutParams frameLayoutLayoutParams = new FrameLayout.LayoutParams(
				FrameLayout.LayoutParams.FILL_PARENT,
				FrameLayout.LayoutParams.FILL_PARENT);

		frameLayout.addView(this.mRenderSurfaceView, surfaceViewLayoutParams);
		frameLayout.addView(adView, adViewLayoutParams);
		this.setContentView(frameLayout, frameLayoutLayoutParams);
	}

	public Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// Exportar
			if (msg.arg1 == 101) {
				exportDialog.dismiss();
				Attachment attachment = (Attachment) msg.obj;
				createExportOptionsDialog(attachment);
			}
			if (msg.arg1 == 102) {
				exportDialog = new ProgressDialog(CanvasActivity.this);
				exportDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				exportDialog.setMessage("Generando reporte...");
				exportDialog.setCancelable(false);
				exportDialog.show();
			}
			// Info
			if (msg.arg1 == 103) {
				createInfoDialog();
			}
			if (msg.arg1 == 104) {
				AlertDialog.Builder builder = new AlertDialog.Builder(CanvasActivity.this);
				builder.setTitle("No se encuentra la aplicaci�n necesaria");
				builder.setMessage("�Descargarla desde el Google Play?");
				builder.setPositiveButton("S�, por favor",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								Intent marketIntent = new Intent(
										Intent.ACTION_VIEW);
								marketIntent.setData(Uri
										.parse("market://details?id=com.metago.astro"));
								startActivity(marketIntent);
							}
						});
				builder.setNegativeButton("No, gracias", null);
				builder.create().show();
			}
		}
	};

	private void createInfoDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		LayoutInflater inflater = getLayoutInflater();
		View dialogLayout = inflater.inflate(R.layout.info_dialog, null);
		TextView canvas = (TextView) dialogLayout.findViewById(R.id.CanvasInfo);
		TextView autor = (TextView) dialogLayout.findViewById(R.id.CanvasAutor);
		CanvasController cc = new CanvasController(this);
		CanvasDB cbd = cc.getById(CanvasActivity.canvasId);
		canvas.setText(cbd.getName());
		autor.setText(cbd.getAutor());
		builder.setView(dialogLayout);
		builder.create();
		builder.show();
	}

	private void createExportOptionsDialog(final Attachment attachment) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Reporte generado")
				.setPositiveButton("Abrir",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								Intent intent = new Intent(Intent.ACTION_VIEW);
								Uri filepath = Uri.fromFile(attachment.image);
								intent.setDataAndType(filepath, "image/*");
								intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								try {
									startActivity(intent);
								} catch (ActivityNotFoundException e) {
									Toast.makeText(
											CanvasActivity.this,
											"no application available to open the image",
											Toast.LENGTH_SHORT).show();
								}
								dialog.dismiss();
							}
						})
				.setNeutralButton("Compartir",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								Intent sharingIntent = new Intent(
										android.content.Intent.ACTION_SEND_MULTIPLE);
								ArrayList<Uri> uris = new ArrayList<Uri>();
								uris.add(Uri.fromFile(attachment.image));
								uris.add(Uri.fromFile(attachment.pdf));
								sharingIntent.setType("text/plain");
								sharingIntent.putExtra(
										android.content.Intent.EXTRA_SUBJECT,
										"Modelo Canvas");
								sharingIntent.putExtra(
										android.content.Intent.EXTRA_STREAM,
										uris);
								startActivity(Intent.createChooser(
										sharingIntent, "Compartir usando..."));
								dialog.dismiss();
							}
						})
				.setNegativeButton("Cerrar",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
							}
						});
		builder.show();
	}

	@Override
	public boolean onKeyDown(final int keyCode, final KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK
				&& event.getAction() == KeyEvent.ACTION_DOWN) {
			if (CanvasManager.getInstance().engine != null) {

				if (SceneManager.getInstance().isMainScene()) {
					updateDatabase();
					return super.onKeyDown(keyCode, event);
				}

				// Poner condicion para evaluar la escena actual y la escena a
				// la cual se deber� volver al presionar
				// Atr�s
				if (SceneManager.getInstance().mCurrentScene.getClass().equals(
						EmpathyMapScene.class)) {
					SceneManager.getInstance().setMainSceneAsCurrent(true);
					// CanvasManager.getInstance().loadMainSceneResources();
					SceneManager.getInstance().showScene(
							MainScene.getInstance());
				}

				// si se encuentra en propuestas de valor
				if (SceneManager.getInstance().mCurrentScene.getClass().equals(
						ValuePropositionsScene.class)) {
					SceneManager.getInstance().setMainSceneAsCurrent(true);
					// CanvasManager.getInstance().loadMainSceneResources();
					SceneManager.getInstance().showScene(
							MainScene.getInstance());
				}

				// si se encuentra en relaciones
				if (SceneManager.getInstance().mCurrentScene.getClass().equals(
						RelationshipsScene.class)) {
					SceneManager.getInstance().setMainSceneAsCurrent(true);
					// CanvasManager.getInstance().loadMainSceneResources();
					SceneManager.getInstance().showScene(
							MainScene.getInstance());
				}
				// si se encuentra en canales
				if (SceneManager.getInstance().mCurrentScene.getClass().equals(
						ChannelsScene.class)) {
					SceneManager.getInstance().setMainSceneAsCurrent(true);
					// CanvasManager.getInstance().loadMainSceneResources();
					SceneManager.getInstance().showScene(
							MainScene.getInstance());
				}
				// si se encuentra en alianzas clave
				if (SceneManager.getInstance().mCurrentScene.getClass().equals(
						KeyPartnershipsScene.class)) {
					SceneManager.getInstance().setMainSceneAsCurrent(true);
					// CanvasManager.getInstance().loadMainSceneResources();
					SceneManager.getInstance().showScene(
							MainScene.getInstance());
				}
			}
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	@Override
	public EngineOptions onCreateEngineOptions() {
		Intent i = getIntent();
		canvasId = i.getIntExtra("canvasId", 0);
		final Camera camera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);

		EngineOptions engineOptions = new EngineOptions(true,
				ScreenOrientation.LANDSCAPE_SENSOR,
				new RelativeResolutionPolicy(1, 0.95f), camera);
		engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);
		engineOptions.getRenderOptions().setDithering(true);
		// Return the engineOptions object, passing it to the engine
		return engineOptions;

	}

	@Override
	public void onCreateResources(
			OnCreateResourcesCallback pOnCreateResourcesCallback)
			throws Exception {
		CanvasManager.getInstance().setup(this.getEngine(),
				this.getApplicationContext());
		MainScene.getInstance().setActivity(this);
		pOnCreateResourcesCallback.onCreateResourcesFinished();

	}

	@Override
	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback)
			throws Exception {
		this.mEngine.registerUpdateHandler(new FPSLogger());
		MainScene.getInstance().setActivity(this);
		SceneManager.getInstance().showScene(MainScene.getInstance());
		SceneManager.getInstance().setMainSceneAsCurrent(true);
		pOnCreateSceneCallback.onCreateSceneFinished(MainScene.getInstance());
	}

	@Override
	public void onPopulateScene(
			Scene pScene,
			org.andengine.ui.IGameInterface.OnPopulateSceneCallback pOnPopulateSceneCallback)
			throws Exception {

		pOnPopulateSceneCallback.onPopulateSceneFinished();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			final Intent data) {
		if (resultCode != RESULT_CANCELED) {
			switch (requestCode) {
			case CanvasActivity.EMPATHY_REQUEST_CODE:
				// Image captured and saved to fileUri specified in the Intent
				cropImage(((EmpathyMapScene) SceneManager.getInstance().mCurrentScene).customer
						.getPhotoUri());
				break;
			case CanvasActivity.PIC_CROP:
				Bundle extras = data.getExtras();
				// get the cropped bitmap
				Bitmap selectedBitmap = extras.getParcelable("data");
				// EmpathyMapScene.EMapManager.instance.getCurrentMap().updateSprite(selectedBitmap);
				((EmpathyMapScene) SceneManager.getInstance().mCurrentScene)
						.updateSprite(selectedBitmap);
				break;
			case CanvasActivity.PICK_FROM_GALLERY:
				Uri selectedImage = data.getData();
				cropImage(selectedImage);
				break;
			case CanvasActivity.PICK_FILE:
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle("Advertencia")
						.setMessage(
								"Se sobreescribir�n los datos del lienzo actual �desea continuar?")
						.setPositiveButton("S�",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										Uri uri = data.getData();
										String path = "";
										try {
											path = CanvasActivity.getPath(
													CanvasActivity.this, uri);
										} catch (URISyntaxException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										LoadBackup lbu = new LoadBackup(
												CanvasActivity.this, path);
										lbu.loadFile();
									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										dialog.dismiss();
									}
								});
				builder.create();
				builder.show();

				break;
			default:
				break;
			}
		}
	}

	private void cropImage(Uri picUri) {
		try {

			Intent cropIntent = new Intent("com.android.camera.action.CROP");
			// indicate image type and Uri
			cropIntent.setDataAndType(picUri, "image/*");
			// set crop properties
			cropIntent.putExtra("crop", "true");
			// indicate aspect of desired crop
			cropIntent.putExtra("aspectX", 1);
			cropIntent.putExtra("aspectY", 1);
			// indicate output X and Y
			cropIntent.putExtra("outputX", 256);
			cropIntent.putExtra("outputY", 256);
			cropIntent.putExtra("scale", true);
			cropIntent.putExtra("scaleUpIfNeeded", true);
			// retrieve data on return
			cropIntent.putExtra("return-data", true);
			// start the activity - we handle returning in onActivityResult
			startActivityForResult(cropIntent, PIC_CROP);
		}
		// respond to users whose devices do not support the crop action
		catch (ActivityNotFoundException anfe) {
			// display an error message
			String errorMessage = "Whoops - your device doesn't support the crop action!";
			Toast toast = Toast
					.makeText(this, errorMessage, Toast.LENGTH_SHORT);
			toast.show();
		}
	}

	@Override
	public void onBackPressed() {
		// getScreenShot();
		captureScreen();
		MainScene.getInstance().onUnloadManagedScene();
		super.onBackPressed();
	}

	public void updateImage(Bitmap bmp) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray();
		CanvasController cc = new CanvasController(CanvasActivity.this);
		CanvasDB c = cc.getById(canvasId);
		c.setImage(byteArray);
		cc.update(c);
	}

	public void captureScreen() {
		ScreenGrabber screenCapture;

		screenCapture = new ScreenGrabber();

		SceneManager.getInstance().mCurrentScene.attachChild(screenCapture);
		final int viewWidth = CanvasActivity.CAMERA_WIDTH;
		final int viewHeight = CanvasActivity.CAMERA_HEIGHT;

		final int hWidth = 600;
		final int hHeight = 376;

		screenCapture.grab(0, 0, viewWidth, viewHeight,
				new ScreenGrabber.IScreenGrabberCallback() {

					@Override
					public void onScreenGrabbed(Bitmap pBitmap) {
						// TODO Auto-generated method stub
						Bitmap b2 = getResizedBitmap(pBitmap, hHeight, hWidth);
						try {

							ByteArrayOutputStream bytes = new ByteArrayOutputStream();
							b2.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
							updateImage(b2);
						} catch (final Exception e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onScreenGrabFailed(Exception pException) {

					}
				});

	}

	public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {

		int width = bm.getWidth();
		int height = bm.getHeight();
		float scaleWidth = ((float) newWidth) / width;
		float scaleHeight = ((float) newHeight) / height;

		Matrix matrix = new Matrix();
		matrix.postScale(scaleWidth, scaleHeight);
		Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
				matrix, false);

		return resizedBitmap;

	}

	public void updateDatabase() {
		PostitController controller = new PostitController(
				CanvasManager.getInstance().context);
		for (PostIt p : MainScene.getInstance().keyPartnerships.postits) {
			if (p.hasChanged()) {
				controller.update(p.getRelatedDatabaseObject());
			}
			PostItDB pdb = savePostIt(p, controller, 0);
			if (pdb != null) {
				int indexInCollection = MainScene.getInstance().keyPartnerships.postits
						.indexOf(p);
				MainScene.getInstance().keyPartnerships.postits.get(
						indexInCollection).setRelatedDatabaseObject(pdb);
			}
		}
		for (PostIt p : MainScene.getInstance().keyActivities.postits) {
			if (p.hasChanged()) {
				controller.update(p.getRelatedDatabaseObject());
			}
			PostItDB pdb = savePostIt(p, controller, 1);
			if (pdb != null) {
				int indexInCollection = MainScene.getInstance().keyActivities.postits
						.indexOf(p);
				MainScene.getInstance().keyActivities.postits.get(
						indexInCollection).setRelatedDatabaseObject(pdb);
			}
		}
		for (PostIt p : MainScene.getInstance().keyResources.postits) {
			if (p.hasChanged()) {
				controller.update(p.getRelatedDatabaseObject());
			}
			PostItDB pdb = savePostIt(p, controller, 2);
			if (pdb != null) {
				int indexInCollection = MainScene.getInstance().keyResources.postits
						.indexOf(p);
				MainScene.getInstance().keyResources.postits.get(
						indexInCollection).setRelatedDatabaseObject(pdb);
			}
		}
		for (PostIt p : MainScene.getInstance().valuePropositions.postits) {
			if (p.hasChanged()) {
				controller.update(p.getRelatedDatabaseObject());
			}
			PostItDB pdb = savePostIt(p, controller, 3);
			if (pdb != null) {
				int indexInCollection = MainScene.getInstance().valuePropositions.postits
						.indexOf(p);
				MainScene.getInstance().valuePropositions.postits.get(
						indexInCollection).setRelatedDatabaseObject(pdb);
			}
		}
		for (PostIt p : MainScene.getInstance().relationships.postits) {
			if (p.hasChanged()) {
				controller.update(p.getRelatedDatabaseObject());
			}
			PostItDB pdb = savePostIt(p, controller, 4);
			if (pdb != null) {
				int indexInCollection = MainScene.getInstance().relationships.postits
						.indexOf(p);
				MainScene.getInstance().relationships.postits.get(
						indexInCollection).setRelatedDatabaseObject(pdb);
			}
		}
		for (PostIt p : MainScene.getInstance().channels.postits) {
			if (p.hasChanged()) {
				controller.update(p.getRelatedDatabaseObject());
			}
			PostItDB pdb = savePostIt(p, controller, 5);
			if (pdb != null) {
				int indexInCollection = MainScene.getInstance().channels.postits
						.indexOf(p);
				MainScene.getInstance().channels.postits.get(indexInCollection)
						.setRelatedDatabaseObject(pdb);
			}
		}
		for (PostIt p : MainScene.getInstance().customerSegments.postits) {
			if (p.hasChanged()) {
				controller.update(p.getRelatedDatabaseObject());
			}
			PostItDB pdb = savePostIt(p, controller, 6);
			if (pdb != null) {
				int indexInCollection = MainScene.getInstance().customerSegments.postits
						.indexOf(p);
				MainScene.getInstance().customerSegments.postits.get(
						indexInCollection).setRelatedDatabaseObject(pdb);
			}
		}
		for (PostIt p : MainScene.getInstance().costs.postits) {
			if (p.hasChanged()) {
				controller.update(p.getRelatedDatabaseObject());
			}
			PostItDB pdb = savePostIt(p, controller, 7);
			if (pdb != null) {
				int indexInCollection = MainScene.getInstance().costs.postits
						.indexOf(p);
				MainScene.getInstance().costs.postits.get(indexInCollection)
						.setRelatedDatabaseObject(pdb);
			}
		}
		for (PostIt p : MainScene.getInstance().revenues.postits) {
			if (p.hasChanged()) {
				controller.update(p.getRelatedDatabaseObject());
			}
			PostItDB pdb = savePostIt(p, controller, 8);
			if (pdb != null) {
				int indexInCollection = MainScene.getInstance().revenues.postits
						.indexOf(p);
				MainScene.getInstance().revenues.postits.get(indexInCollection)
						.setRelatedDatabaseObject(pdb);
			}
		}
	}

	private PostItDB savePostIt(PostIt p, PostitController controller, int block) {
		if (p.getRelatedDatabaseObject() == null) {
			PostItDB pdb = PostItDB.fromPostit(p, -1, CanvasActivity.canvasId);
			pdb.setId((int) controller.create(pdb));
			return pdb;
		}
		return null;
	}

	public static String getPath(Context context, Uri uri)
			throws URISyntaxException {
		if ("content".equalsIgnoreCase(uri.getScheme())) {
			String[] projection = { "_data" };
			Cursor cursor = null;

			try {
				cursor = context.getContentResolver().query(uri, projection,
						null, null, null);
				int column_index = cursor.getColumnIndexOrThrow("_data");
				if (cursor.moveToFirst()) {
					return cursor.getString(column_index);
				}
			} catch (Exception e) {
				// Eat it
			}
		}

		else if ("file".equalsIgnoreCase(uri.getScheme())) {
			return uri.getPath();
		}

		return null;
	}

	@Override
	protected void onDestroy() {
		updateDatabase();
		super.onDestroy();
	}

}
