/**
 * Clase abstracta que representa cada uno de los postits puestos en el escenario y entrega
 * los m�todos necesarios para maximizar y minimizar el postit.  
 */

package com.smartsoft.Educanvas;

import org.andengine.engine.Engine;
import org.andengine.engine.Engine.EngineLock;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.LoopEntityModifier.ILoopEntityModifierListener;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.AutoWrap;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.HorizontalAlign;
import org.andengine.util.color.Color;
import org.andengine.util.modifier.IModifier;
import org.andengine.util.modifier.LoopModifier;
import org.andengine.util.modifier.ease.EaseLinear;

import com.canvas.dataaccess.PostItDB;
import com.canvas.dataaccess.controller.PostitController;

public abstract class PostIt extends Sprite {
	
	public final static int HIDE_TYPE_ANIMATION = 0;
	public final static int SHOW_TYPE_ANIMATION = 1;

	public Engine engine;
	public CanvasActivity canvas;
	private boolean hasChanged;

	/**
	 * Representa el objeto de la base de datos relacionado con un PostIt
	 * espec�fico
	 */
	private PostItDB relatedDatabaseObject;

	/**
	 * Representa el bloque donde se encuentra el postit
	 */
	@SuppressWarnings("rawtypes")
	private Block parentBlock;

	protected boolean isMoving;

	/**
	 * Representa la posici�n X del centro del postit
	 */
	private float posX;
	/**
	 * Representa la posici�n Y del centro del postit
	 */
	private float posY;

	/**
	 * Palabra clave que permite identificar la informaci�n de ayuda del postit
	 * dentro de los recursos.
	 */
	private String postitReference;

	private ITextureRegion maximizedRegion;

	/**
	 * Esta interfaz permite acceder a los datos del postit cuando ha sido
	 * maximizado o minimazado.
	 */
	protected OnPostitChangeLister onPostitChangeListener;

	private OnEditAplicationInvokeListener onEditAplicationInvokeListener;

	/**
	 * T�tulo del postit
	 */
	private String title;

	/**
	 * Descripci�n del postit
	 */
	private String description;

	public boolean hasChanged() {
		return this.hasChanged;
	}

	public void setHasChanged(boolean hasChanged) {
		this.hasChanged = hasChanged;
	}

	public PostItDB getRelatedDatabaseObject() {
		return relatedDatabaseObject;
	}

	public void setRelatedDatabaseObject(PostItDB relatedDatabaseObject) {
		this.relatedDatabaseObject = relatedDatabaseObject;
	}

	public float getPosX() {
		return posX;
	}

	public void setPosX(float posX) {
		if (this.relatedDatabaseObject != null) {
			if (getPosX() != posX) {
				hasChanged = true;
				this.relatedDatabaseObject.setpX(posX);
				updateBlock();
			}
		}
		this.posX = posX;
	}

	public float getPosY() {
		return posY;
	}

	public void setPosY(float posY) {
		if (this.relatedDatabaseObject != null) {
			if (getPosY() != posY) {
				hasChanged = true;
				this.relatedDatabaseObject.setpX(posY);
				updateBlock();
			}
		}
		this.posY = posY;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		if (this.relatedDatabaseObject != null) {
			if (getTitle() != title) {
				hasChanged = true;
				this.relatedDatabaseObject.setTitle(title);
				this.title = title;
				updateBlock();
			}
		} else{
			this.title = title;
		}		 
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if (this.relatedDatabaseObject != null) {
			if (getDescription() != description) {
				hasChanged = true;
				this.relatedDatabaseObject.setDescription(description);
				this.description = description;
				updateBlock();
			}
		} else{
			this.description = description;
		}
		
	}

	@Override
	public void setColor(Color pColor) {
		if (this.relatedDatabaseObject != null) {
			if (getColor() != pColor) {
				hasChanged = true;
				this.relatedDatabaseObject.setColor(pColor.getARGBPackedInt());
				updateBlock();
			}
		}
		super.setColor(pColor);
	}

	public PostIt(float pX, float pY, ITextureRegion pTextureRegion,
			ITextureRegion maximizedRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, CanvasActivity canvas, String title,
			String description, Color color, String postitReference) {
		super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
		this.engine = engine;
		this.canvas = canvas;
		posX = pX;
		posY = pY;

		this.maximizedRegion = maximizedRegion;
		this.postitReference = postitReference;

		this.title = title != null ? title : "";
		this.description = description != null ? description : "";
		this.setColor(color != null ? color : new Color(0.99f, 0.84f, 0.38f));
		init();
	}

	public PostIt(float pX, float pY, ITextureRegion pTextureRegion,
			ITextureRegion maximizedRegion,
			VertexBufferObjectManager pVertexBufferObjectManager,
			Engine engine, CanvasActivity canvas, String title,
			String description, Color color, String postitReference,
			PostItDB relatedDatabaseObject) {
		super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
		this.relatedDatabaseObject = relatedDatabaseObject;
		this.engine = engine;
		this.canvas = canvas;
		posX = pX;
		posY = pY;

		this.maximizedRegion = maximizedRegion;
		this.postitReference = postitReference;

		this.title = title != null ? title : "";
		this.description = description != null ? description : "";
		this.setColor(color != null ? color : new Color(0.99f, 0.84f, 0.38f));
		init();
	}

	/**
	 * Inicializa los elementos utilizados en la creaci�n del postit
	 */
	private void init() {
		// loadPFont();
		drawTitle();
	}

	/**
	 * Dibuja el t�tulo en el postit cuando se encuentra minizado. Generalmente
	 * es llamado cada vez que se termina la minimizaci�n del postit, de esta
	 * forma se evita que se logre ver el titulo distorsionado mientras se
	 * ejecuta la animaci�n de minimzaci�n o maximizaci�n. Inicialmente se
	 * eliminan todos los elementos contenidos dentro del postit, con ello se
	 * evita que aparezcan t�tulos superpuestos cuando se actualice el mismo.
	 */
	protected void drawTitle() {
		removeAllChildern();
		TextOptions tOptions = new TextOptions(AutoWrap.WORDS, 90,
				HorizontalAlign.CENTER, Text.LEADING_DEFAULT);
		Text text = new Text(5, 0,
				CanvasManager.getInstance().minimizedPostitFont,
				(this.title != null) ? this.title : "", tOptions,
				canvas.getVertexBufferObjectManager());

		text.setText((this.title != null) ? this.title : "");
		text.setPosition((getWidth() - text.getWidth()) / 2,
				(getHeight() + text.getHeight() / 4));
		this.attachChild(text);
	}

	/**
	 * Remueve y elimina todos los elementos contenidos dentro del postit. �til
	 * para actualizar la informaci�n que contiene y evitar que se superponga
	 * con la nueva.
	 */
	protected void removeAllChildern() {
		for (int i = 0; i < this.getChildCount(); i++) {
			IEntity ie = this.getChildByIndex(i);
			this.detachChild(ie);
			if (!ie.isDisposed()) {
				ie.dispose();
			}
		}
	}

	private float startX = 0;
	private float startY = 0;

	@SuppressWarnings({ "unchecked" })
	@Override
	public boolean onAreaTouched(final TouchEvent pSceneTouchEvent,
			final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
		switch (pSceneTouchEvent.getAction()) {
		case TouchEvent.ACTION_DOWN:
			startX = this.getX();
			startY = this.getY();
			break;

		case TouchEvent.ACTION_MOVE:
			float dX = this.getX() + (this.getWidth() / 2)
					- pSceneTouchEvent.getX();
			float dY = this.getY() + (this.getHeight() / 2)
					- pSceneTouchEvent.getY();

			if (!(dX < getWidth() / 4 && dX > -getWidth() / 4)
					|| !(dY < getHeight() / 4 && dY > -getHeight() / 4)) {
				isMoving = true;
				float tPosX = pSceneTouchEvent.getX() - this.getWidth() / 2;
				float tPosY = pSceneTouchEvent.getY() - this.getHeight() / 2;
				this.setPosition(tPosX, tPosY);
				this.setPosX(tPosX);
				this.setPosY(tPosY);
			}
			break;

		case TouchEvent.ACTION_UP:
			if (isMoving) {
				if (parentBlock != null) {
					if (this.collidesWith(MainScene.getInstance().deleteButton)) {
						if (this == null) {
							return false;
						}
						final EngineLock engineLock = engine.getEngineLock();
						engineLock.lock();
						/* Now it is save to remove the entity! */
						PostitController controller = new PostitController(
								CanvasManager.getInstance().context);
						if (getRelatedDatabaseObject() != null) {
							controller.removeEntry(getRelatedDatabaseObject()
									.getId());
						}
						MainScene.getInstance().detachChild(this);
						this.getParentBlock().removePostit(this);
						if (!this.isDisposed()) {
							this.dispose();
						}
						engineLock.unlock();
					} else if (this
							.collidesWith(MainScene.getInstance().addPostitBtn)) {
						this.setPosition(
								MainScene.getInstance().addPostitBtn.getX()
										+ MainScene.getInstance().addPostitBtn
												.getWidth() + 10,
								MainScene.getInstance().addPostitBtn.getY());
						this.setPosX(MainScene.getInstance().addPostitBtn
								.getX()
								+ MainScene.getInstance().addPostitBtn
										.getWidth() + 10);
						this.setPosY(MainScene.getInstance().addPostitBtn
								.getY());
					} else if (this.getX() < CanvasActivity.TOOL_BOX_WIDTH) {
						this.setPosition(startX, startY);
					}
					// } else if (this.getX() < (parentBlock.getpX())
					// || this.getX() > (parentBlock.getpX()
					// + parentBlock.getWidth() - this.getWidth() / 2)
					// || this.getY() < (parentBlock.getpY() - this
					// .getHeight() / 2)
					// || this.getY() > (parentBlock.getpY()
					// + parentBlock.getHeight() - this
					// .getHeight())) {
					// this.setPosition(startX, startY);
					// } else {
					// posX = pSceneTouchEvent.getX() - this.getWidth() / 2;
					// this.setPosition(posX, this.getPosY());
					// }
					else if (this.getX() < CanvasActivity.TOOL_BOX_WIDTH) {
						this.setPosition(startX, startY);
					} else {
						posX = pSceneTouchEvent.getX() - this.getWidth() / 2;
						//this.setPosition(posX, this.getPosY());
						MainScene.getInstance().defineBlock(this, startX, startY, posX, this.getPosY());
					}
					isMoving = false;
				}
			} else {
				maximizePostit();
			}
			break;

		default:
			break;
		}

		return true;
	}

	/**
	 * Metodo que representa la maximizaci�n del postit. La maximizaci�n se
	 * realiza ejecutando el m�todo animatePostit.
	 */
	protected void maximizePostit() {
		removeAllChildern();
		animatePostit(0.15f, 1, 6, posX,
				CanvasActivity.CAMERA_WIDTH / 2 - this.getWidthScaled() / 2
						+ 100, posY,
				CanvasActivity.CAMERA_HEIGHT / 2 - this.getHeightScaled() / 2,
				PostIt.SHOW_TYPE_ANIMATION);
	}

	/**
	 * Metodo que representa la minimizaci�n del postit. La maximizaci�n se
	 * realiza ejecutando el m�todo animatePostit.
	 */
	protected void minimizePostit() {
		removeAllChildern();
		animatePostit(0.3f, 6, 1, this.getX(), posX, this.getY(), posY,
				PostIt.HIDE_TYPE_ANIMATION);
	}

	/**
	 * Este m�todo representa la animaci�n del postit ya sea para maximizarlo o
	 * minimizarlo. Es una combinaci�n de una animaci�n de escala y una
	 * animaci�n de desplazamiento.
	 * 
	 * @param duration
	 *            : Duraci�n en segundos de la animaci�n
	 * @param fromScale
	 *            : Escala a partir de la cual inicia la animaci�n. (tama�o del
	 *            postit al iniciar la animaci�n)
	 * @param toScale
	 *            : Escala final (tama�o del postit al terminar la animaci�n) de
	 *            la animaci�n
	 * @param fromX
	 *            : Posici�n X de inicio.
	 * @param toX
	 *            : Posici�n X de finalizaci�n.
	 * @param fromY
	 *            : Posici�n Y de inicio.
	 * @param toY
	 *            : Posici�n Y de finalizaci�n.
	 * @param animationType
	 *            : Indica expl�citamente si es una animaci�n de maximizaci�n o
	 *            de minimizaci�n
	 * 
	 *            Cuando es una animaci�n de maximizaci�n, el m�todo
	 *            showExtraInformation es ejecutado al finalizar la animaci�n.
	 *            Cuando es una minimizaci�n, es posible que haya sido
	 *            actualizada la informaci�n del postit y por tanto se ejecuta
	 *            el m�todo drawTitle para actualizar el t�tulo que ser�
	 *            desplegado y ademas se ejecuta el m�todo onMinimize de la
	 *            interfaz onPostitChangeListener.
	 */
	private void animatePostit(float duration, float fromScale, float toScale,
			float fromX, float toX, float fromY, float toY,
			final int animationType) {
		final LoopEntityModifier entityModifier = new LoopEntityModifier(
				new IEntityModifierListener() {
					@Override
					public void onModifierStarted(
							final IModifier<IEntity> pModifier,
							final IEntity pItem) {
						canvas.runOnUiThread(new Runnable() {
							@Override
							public void run() {
							}
						});
					}

					@Override
					public void onModifierFinished(
							final IModifier<IEntity> pEntityModifier,
							final IEntity pEntity) {
						canvas.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								if (animationType == PostIt.SHOW_TYPE_ANIMATION) {
									showExtraInformation();
								} else {
									drawTitle();
									if (onPostitChangeListener != null) {
										onPostitChangeListener
												.onMinimize(PostIt.this);
									}
								}
							}
						});
					}
				}, 1, new ILoopEntityModifierListener() {

					@Override
					public void onLoopFinished(
							final LoopModifier<IEntity> pLoopModifier,
							final int pLoop, final int pLoopCount) {
						canvas.runOnUiThread(new Runnable() {
							@Override
							public void run() {
							}
						});
					}

					@Override
					public void onLoopStarted(
							LoopModifier<IEntity> pLoopModifier, int pLoop,
							int pLoopCount) {
						// TODO Auto-generated method stub

					}
				}, new SequenceEntityModifier(new ParallelEntityModifier(
						new ScaleModifier(duration, fromScale, toScale),
						new MoveModifier(duration, fromX, toX, fromY, toY,
								EaseLinear.getInstance()))));

		this.registerEntityModifier(entityModifier);
	}

	/**
	 * Este m�todo se encarga de presentar la informaci�n extra del postit.
	 * Dicha informaci�n es el t�tulo, mas la descripci�n. Adem�s presenta las
	 * herramientas utilizadas para lanzar ocultar de nuevo el postit, o lanzar
	 * la miniaplicaci�n para su edici�n. Esta informaci�n se encuentra incluida
	 * dentro de un elemento de tipo MaximizedPostit. Al finalizar la carga de
	 * esta informaci�n el m�todo onMaximized de la interfaz
	 * onPostitChangeListener es ejecutado. Antes de cargar toda la informaci�n
	 * extra, se quitan los registros TouchArea de todos los sprites en la
	 * escena, con ello se evita que mientras se est� trabajando sobre el postit
	 * maximizado puedan moverse los demas postits o puedan agregarse nuevos
	 * postits a la escena. Al finalizar y minimizar el postit nuevamente se
	 * registran los TouchArea de los Sprites.
	 */
	public void showExtraInformation() {

		final MaximizedPostIt mPostit = new MaximizedPostIt(
				90 + (CanvasActivity.CAMERA_WIDTH - CanvasManager.getInstance().mPostitTextureRegionMaxBG
						.getWidth()) / 2,
				(CanvasActivity.CAMERA_HEIGHT - CanvasManager.getInstance().mPostitTextureRegionMaxBG
						.getHeight()) / 2, maximizedRegion, canvas, getTitle(),
				getDescription(), getColor(), getVertexBufferObjectManager(),
				postitReference);

		mPostit.setOnActionListener(new OnActionListener() {

			@Override
			public void onClose() {
				mPostit.setVisible(false);
				MainScene.getInstance().registerSpriteTouchAreas();
				PostIt.this.setColor(mPostit.getPostitColor());
				PostIt.this.setTitle(mPostit.getTitle());
				PostIt.this.setDescription(mPostit.getDescription());
				PostIt.this.setVisible(true);
				PostIt.this.minimizePostit();
			}

			@Override
			public void onEdit() {
				if (onEditAplicationInvokeListener != null) {
					canvas.updateDatabase();
					onEditAplicationInvokeListener
							.onEditAplicationInvoke(PostIt.this);
				}
			}

			@Override
			public void onTitleChange(String title) {
				setTitle(title);
			}

			@Override
			public void onDescriptionChange(String description) {
				setDescription(description);
			}

			@Override
			public void onColorChange(Color c) {
				PostIt.this.setColor(c);
			}
		});

		MainScene.getInstance().unregisterSpriteTouchAreas();
		MainScene.getInstance().attachChild(mPostit);
		mPostit.draw();

		this.setVisible(false); // Ocultamos el postit actual, la idea es
		// mostrar en la escena un postit grande.

		if (onPostitChangeListener != null) {
			onPostitChangeListener.onMaximize(mPostit);
		}
	}

	@SuppressWarnings("rawtypes")
	public Block getParentBlock() {
		return parentBlock;
	}

	@SuppressWarnings("rawtypes")
	public void setParentBlock(Block parentBlock) {
		this.parentBlock = parentBlock;
	}

	public OnPostitChangeLister getOnPostitChangeListener() {
		return onPostitChangeListener;
	}

	public void setOnPostitChangeListener(
			OnPostitChangeLister onPostitChangeListener) {
		this.onPostitChangeListener = onPostitChangeListener;
	}

	public OnEditAplicationInvokeListener getOnEditAplicationInvokeListener() {
		return onEditAplicationInvokeListener;
	}

	public void setOnEditAplicationInvokeListener(
			OnEditAplicationInvokeListener onEditAplicationInvokeListener) {
		this.onEditAplicationInvokeListener = onEditAplicationInvokeListener;
	}

	public ITextureRegion getMaximizedRegion() {
		return maximizedRegion;
	}

	public void setMaximizedRegion(ITextureRegion maximizedRegion) {
		this.maximizedRegion = maximizedRegion;
	}

	private void updateBlock() {
		if (getParentBlock() != null && getRelatedDatabaseObject() != null) {
			int index = getParentBlock().postits.indexOf(this);
			try {
				if (getParentBlock().postits.get(index) != null) {
					((PostIt) getParentBlock().postits.get(index))
							.getRelatedDatabaseObject().setpX(getPosX());
					((PostIt) getParentBlock().postits.get(index))
							.getRelatedDatabaseObject().setpY(getPosY());
					((PostIt) getParentBlock().postits.get(index))
							.getRelatedDatabaseObject().setTitle(getTitle());
					((PostIt) getParentBlock().postits.get(index))
							.getRelatedDatabaseObject().setDescription(
									getDescription());
					((PostIt) getParentBlock().postits.get(index))
							.getRelatedDatabaseObject().setColor(
									this.getColor().getARGBPackedInt());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	/*
	 * public T getPostitContext() { return postitContext; }
	 * 
	 * public void setPostitContext(T postitContext) { this.postitContext =
	 * postitContext; }
	 */
	/**
	 * Interfaz que puede ser utilizada para ejecutar m�todos extra cuando el
	 * postit a terminado de ser animado es decir al finalizar una maximizaci�n,
	 * o al finalizar una minimizaci�n.
	 * 
	 * @author vgarzon
	 */
	public interface OnPostitChangeLister {

		public void onMaximize(MaximizedPostIt maximizedPostit);

		public void onMinimize(PostIt postit);

	};

	/**
	 * Interfaz que permite detectar cuando el bot�n de edici�n mediante la
	 * miniaplicaci�n ha sido presionado.
	 * 
	 * @author vgarzon
	 * 
	 */
	public interface OnEditAplicationInvokeListener {
		public void onEditAplicationInvoke(Object postit);
	};
};
