package com.smartsoft.Educanvas;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.canvas.util.CoverFlow;

public class CanvasAdapter extends BaseAdapter {
	int mGalleryItemBackground;
	private Context mContext;
	private List<com.canvas.dataaccess.CanvasDB> canvasList;
//	private FileInputStream fis;

	/*
	 * private Integer[] mImageIds = { R.drawable.kasabian_kasabian,
	 * R.drawable.starssailor_silence_is_easy, R.drawable.killers_day_and_age,
	 * R.drawable.garbage_bleed_like_me,
	 * R.drawable.death_cub_for_cutie_the_photo_album,
	 * R.drawable.kasabian_kasabian, R.drawable.massive_attack_collected,
	 * R.drawable.muse_the_resistance, R.drawable.starssailor_silence_is_easy };
	 */

//	private ImageView[] mImages;

	public CanvasAdapter(Context c,
			List<com.canvas.dataaccess.CanvasDB> canvasList) {
		mContext = c;
		// mImages = new ImageView[mImageIds.length];
		this.canvasList = canvasList;
//		mImages = new ImageView[canvasList.size()];
		// createReflectedImages();
	}

	@SuppressWarnings("deprecation")
	public ImageView createReflectedImage(Bitmap originalImage) {
		// The gap we want between the reflection and the original image
		final int reflectionGap = 4;

		int width = originalImage.getWidth();
		int height = originalImage.getHeight();

		// This will not scale but will flip on the Y axis
		Matrix matrix = new Matrix();
		matrix.preScale(1, -1);

		// Create a Bitmap with the flip matrix applied to it.
		// We only want the bottom half of the image
		Bitmap reflectionImage = Bitmap.createBitmap(originalImage, 0,
				height / 2, width, height / 2, matrix, false);

		// Create a new bitmap with same width but taller to fit reflection
		Bitmap bitmapWithReflection = Bitmap.createBitmap(width,
				(height + height / 2), Config.ARGB_8888);

		// Create a new Canvas with the bitmap that's big enough for
		// the image plus gap plus reflection
		Canvas canvas = new Canvas(bitmapWithReflection);
		// Draw in the original image
		//canvas.drawBitmap(originalImage, 0, 0, null);
		// Draw in the gap
		//Paint deafaultPaint = new Paint();
		//canvas.drawRect(0, height, width, ref, deafaultPaint);
		// Draw in the reflection
		canvas.drawBitmap(reflectionImage, 0, reflectionGap, null);

		// Create a shader that is a linear gradient that covers the
		// reflection
		Paint paint = new Paint();
		LinearGradient shader = new LinearGradient(0,
				originalImage.getHeight(), 0, bitmapWithReflection.getHeight()
						+ reflectionGap, 0x70ffffff, 0x00ffffff, TileMode.CLAMP);
		// Set the paint to use this shader (linear gradient)
		paint.setShader(shader);
		// Set the Transfer mode to be porter duff and destination in
		paint.setXfermode(new PorterDuffXfermode(Mode.DST_IN));
		// Draw a rectangle using the paint with our linear gradient
		canvas.drawRect(0, height, width, bitmapWithReflection.getHeight()
				+ reflectionGap, paint);

		ImageView imageView = new ImageView(mContext);
		imageView.setImageBitmap(bitmapWithReflection);
		imageView.setLayoutParams(new CoverFlow.LayoutParams(600, 376));
		imageView.setScaleType(ScaleType.MATRIX);
		return imageView;

	}

	public int getCount() {
		return canvasList.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	@SuppressWarnings("deprecation")
	public View getView(int position, View convertView, ViewGroup parent) {

		// Use this code if you want to load from resources
		LinearLayout ll = new LinearLayout(mContext);
		ll.setOrientation(LinearLayout.VERTICAL);
		ImageView i = new ImageView(mContext);
		i.setTag("image");
		// i.setImageResource(mImageIds[position]);
		byte[] imageByte = canvasList.get(position).getImage();
		if(imageByte == null){
			 try {
				java.io.InputStream is = mContext.getAssets().open("gfx/canvas_nuevo.GIF");
				imageByte = new byte[is.available()];
				is.read(imageByte);
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		Bitmap bmp = BitmapFactory.decodeByteArray(imageByte, 0,
				imageByte.length);
		i.setImageBitmap(bmp);
		i.setLayoutParams(new CoverFlow.LayoutParams(600, 376));
		i.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

		// Make sure we set anti-aliasing otherwise we get jaggies
		BitmapDrawable drawable = (BitmapDrawable) i.getDrawable();
		drawable.setAntiAlias(true);


		ll.addView(i);
		//ll.addView(createReflectedImage(bmp));
		TextView tv = new TextView(mContext);
		tv.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT));
		tv.setGravity(Gravity.CENTER_HORIZONTAL);
		tv.setTextSize(20f);
		tv.setText(canvasList.get(position).getName());
		ll.addView(tv);
		return ll;

		// return mImages[position];
	}

	/**
	 * Returns the size (0.0f to 1.0f) of the views depending on the 'offset' to
	 * the center.
	 */
	public float getScale(boolean focused, int offset) {
		/* Formula: 1 / (2 ^ offset) */
		return Math.max(0, 1.0f / (float) Math.pow(2, Math.abs(offset)));
	}

	public List<com.canvas.dataaccess.CanvasDB> getCanvasList() {
		return canvasList;
	}

	public void setCanvasList(List<com.canvas.dataaccess.CanvasDB> canvasList) {
		this.canvasList = canvasList;
	}

}
