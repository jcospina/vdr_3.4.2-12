/**
 * Representa un postit en estado maximizado y presenta todas las herramientas necesarias
 * para su edici�n.
 */

package com.smartsoft.Educanvas;

import org.andengine.entity.Entity;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

import com.canvas.util.ColorPicker;
import com.canvas.util.InputText;

public class MaximizedPostIt extends Entity {

	private ITextureRegion backgroundRegion;
	/**
	 * Indica si la informaci�n de ayuda se encuentra desplegada
	 */
	private boolean helpShowing;

	/**
	 * Posici�n X del postit maximizado
	 */
	private float posX;

	/**
	 * Posici�n Y del postit maximizado
	 */
	private float posY;

	private VertexBufferObjectManager vbom;

	/**
	 * Canvas sobre el cual se est� dibujando el postit, necesario para acceder
	 * a recursos de im�genes
	 */
	private CanvasActivity canvas;

	/**
	 * T�tulo del postit
	 */
	private String title;

	/**
	 * Descripci�n del postit
	 */
	private String description;

	/**
	 * Color del postit
	 */
	private Color postitColor;

	/**
	 * Texto de referencia para encontrar la informaci�n de ayuda dentro del
	 * postit
	 */
	protected String postitReference;

	protected OnActionListener onActionListener;

	/**
	 * Representa la imagen en background utilizada para representar el postit
	 */
	private Sprite postitTypeSprite;
	private Sprite postItSheet;

	/**
	 * Representa el bot�n de ayuda
	 */
	private Sprite helpBtn;

	/**
	 * Representa el selector de color
	 */
	private ColorPicker colorPicker;

	/**
	 * Representa el bot�n de cerrado (minizaci�n del postit)
	 */
	private Sprite closeBtn;

	/**
	 * Representa el elemento donde se despliega la ayuda referente al postit
	 */
	private PostItHelp helpEntity;

	/**
	 * Representa el bot�n para invocar a la miniaplicaci�n que permitir� editar
	 * el postit
	 */
	private Sprite aplicationInvokeBtn;

	public MaximizedPostIt(float posX, float posY,
			ITextureRegion backgroundRegion, CanvasActivity canvas,
			String title, String description, Color postitColor,
			VertexBufferObjectManager vbom, String postitReference) {
		super();
		this.posX = posX;
		this.posY = posY;
		this.vbom = vbom;
		this.canvas = canvas;
		this.title = title;
		this.description = description;
		this.postitColor = postitColor;
		this.postitReference = postitReference;
		this.backgroundRegion = backgroundRegion;
		helpShowing = false;
		// help = new PostItHelp("", "", vbom);
	}

	

	/**
	 * Inicializa todas las herramientas del postit y las dibuja en la escena.
	 * Al cerrarse se eliminan todos estos elementos evitando que se queden
	 * consumiendo memoria.
	 */
	public void draw() {

		final Sprite postitBackgroundSprite = new Sprite(posX, posY,
				CanvasManager.getInstance().mPostitTextureRegionMaxBG,
				canvas.getVertexBufferObjectManager());

		postItSheet = new Sprite(79, 43,
				CanvasManager.getInstance().contentPostitTextureRegion,
				canvas.getVertexBufferObjectManager());

		postitTypeSprite = new Sprite(
				CanvasManager.getInstance().mPostitTextureRegionMaxBG.getWidth()
						- backgroundRegion.getWidth() - 40, 22,
				backgroundRegion, canvas.getVertexBufferObjectManager());

		helpEntity = new PostItHelp(375, 43, postitReference, vbom, canvas);

		final InputText titleInputText = new InputText(24, 70, "Titulo",
				"Presione aqui para agregar un t�tulo",
				"Ingresa un titulo para el PostIt", this.getTitle(),
				CanvasManager.getInstance().mTiledTextureRegion, CanvasManager.getInstance().titleFont, 10,
				0, 388, canvas.getVertexBufferObjectManager(), canvas);

		titleInputText
				.setOnTextChangeListener(new InputText.OnTextChangeListener() {

					@Override
					public void onTextChange(String text) {
						if (onActionListener != null) {
							onActionListener.onTitleChange(titleInputText
									.getText());
						}
					}
				});

		final InputText descriptionInputText = new InputText(24, 160,
				"Descripci�n", "Presione aqui para agregar una descripci�n",
				"Ingresa una descripci�n para el PostIt",
				this.getDescription(),
				CanvasManager.getInstance().mTiledTextureRegion,
				CanvasManager.getInstance().descriptionFont, 10, 0, 388,
				canvas.getVertexBufferObjectManager(), canvas);

		descriptionInputText
				.setOnTextChangeListener(new InputText.OnTextChangeListener() {

					@Override
					public void onTextChange(String text) {
						if (onActionListener != null) {
							onActionListener
									.onDescriptionChange(descriptionInputText
											.getText());
						}
					}
				});
		
		this.setVisible(false); // Ocultamos el postit actual, la idea es
								// mostrar en la escena un postit grande.
		this.setScale(1, 1); //

		colorPicker = new ColorPicker(103, 425,
				CanvasManager.getInstance().colorBtnTextureRegion,
				canvas.getVertexBufferObjectManager(), canvas,
				this.getPostitColor(), null);
		colorPicker
				.setOnColorChangeListener(new ColorPicker.OnColorChangeListener() {

					@Override
					public void onColorChange() {
						// TODO Auto-generated method stub
						MaximizedPostIt.this.setPostitColor(colorPicker
								.getSelectedColor());
						if(onActionListener != null){
							onActionListener.onColorChange(colorPicker.getSelectedColor());
						}
					}
				});

		helpBtn = new Sprite(30, 108, CanvasManager.getInstance().helpBtn,
				canvas.getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {
				// TODO Auto-generated method stub
				if (pSceneTouchEvent.isActionDown()) {

					if (isHelpShowing()) {
						helpEntity.hide();
						setHelpShowing(false);
					} else {
						helpEntity.show();
						setHelpShowing(true);
					}
				}
				return true;
			}
		};

		closeBtn = new Sprite(40, -4, CanvasManager.getInstance().closeBtn,
				canvas.getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {
				// TODO Auto-generated method stub
				if (pSceneTouchEvent.isActionDown()) {
					setTitle(titleInputText.getText());
					setDescription(descriptionInputText.getText());
					canvas.getEngine().getEngineLock().lock();
					MainScene.getInstance().detachChild(postitBackgroundSprite);
					helpEntity.hide();
					postitBackgroundSprite.detachChildren();
					MainScene.getInstance().unregisterTouchArea(this);
					MainScene.getInstance().unregisterTouchArea(colorPicker);
					MainScene.getInstance().unregisterTouchArea(titleInputText);
					MainScene.getInstance().unregisterTouchArea(
							descriptionInputText);
					MainScene.getInstance().unregisterTouchArea(
							aplicationInvokeBtn);
					MainScene.getInstance().unregisterTouchArea(helpBtn);
					if (!colorPicker.isDisposed()) {
						colorPicker.dispose();
					}
					if (!titleInputText.isDisposed()) {
						titleInputText.dispose();
					}
					if (!descriptionInputText.isDisposed()) {
						descriptionInputText.dispose();
					}
					if (!helpBtn.isDisposed()) {
						helpBtn.dispose();
					}
					if (!helpEntity.isDisposed()) {
						helpEntity.dispose();
					}
					if (!aplicationInvokeBtn.isDisposed()) {
						aplicationInvokeBtn.dispose();
					}
					//titleFont.unload();
					//descriptionFont.unload();
					if(!this.isDisposed()){
						this.dispose();
					}
					MaximizedPostIt.this.setVisible(true);

					canvas.getEngine().getEngineLock().unlock();

					if (onActionListener != null) {
						onActionListener.onClose();
					}
				}
				return true;
			}
		};

		aplicationInvokeBtn = new Sprite(530, 400,
				CanvasManager.getInstance().editAplicationInvokeBtn,
				canvas.getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {
				// TODO Auto-generated method stub
				if (pSceneTouchEvent.isActionDown()) {
					if (onActionListener != null) {						
						MainScene.getInstance().unregisterTouchArea(closeBtn);
						MainScene.getInstance()
								.unregisterTouchArea(colorPicker);
						MainScene.getInstance().unregisterTouchArea(
								aplicationInvokeBtn);
						MainScene.getInstance().unregisterTouchArea(
								titleInputText);
						MainScene.getInstance().unregisterTouchArea(
								descriptionInputText);
						MainScene.getInstance().unregisterTouchArea(helpBtn);
						postitBackgroundSprite.detachChildren();
						postitBackgroundSprite.detachSelf();
						onActionListener.onEdit();
					}
				}
				return true;
			}
		};

		// postitBackgroundSprite.setColor(postitColor);
		postitBackgroundSprite.attachChild(postItSheet);
		postitBackgroundSprite.attachChild(postitTypeSprite);
		postitBackgroundSprite.attachChild(closeBtn);
		postitBackgroundSprite.attachChild(helpBtn);
		postitBackgroundSprite.attachChild(colorPicker);
		postitBackgroundSprite.attachChild(aplicationInvokeBtn);
		postItSheet.setColor(postitColor);
		postItSheet.attachChild(titleInputText);
		postItSheet.attachChild(descriptionInputText);
		this.attachChild(postitBackgroundSprite);
		MainScene.getInstance().registerTouchArea(closeBtn);
		MainScene.getInstance().registerTouchArea(colorPicker);
		MainScene.getInstance().registerTouchArea(aplicationInvokeBtn);
		MainScene.getInstance().registerTouchArea(titleInputText);
		MainScene.getInstance().registerTouchArea(descriptionInputText);
		MainScene.getInstance().registerTouchArea(helpBtn);
		postitBackgroundSprite.setZIndex(2);
		helpEntity.setZIndex(1);
		attachChild(helpEntity);
		sortChildren();

		this.setVisible(true);

	}

	public boolean isHelpShowing() {
		return helpShowing;
	}

	public void setHelpShowing(boolean helpShowing) {
		this.helpShowing = helpShowing;
	}

	public float getPosX() {
		return posX;
	}

	public void setPosX(float posX) {
		this.posX = posX;
	}

	public float getPosY() {
		return posY;
	}

	public void setPosY(float posY) {
		this.posY = posY;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Color getPostitColor() {
		return postitColor;
	}

	public void setPostitColor(Color postitColor) {
		this.postitColor = postitColor;
		if (postItSheet != null) {
			// postitTypeSprite.setColor(this.postitColor);
			postItSheet.setColor(this.postitColor);
		}
	}

	public Sprite getHelpBtn() {
		return helpBtn;
	}

	public void setHelpBtn(Sprite helpBtn) {
		this.helpBtn = helpBtn;
	}

	public ColorPicker getColorPicker() {
		return colorPicker;
	}

	public void setColorPicker(ColorPicker colorPicker) {
		this.colorPicker = colorPicker;
	}

	public Sprite getCloseBtn() {
		return closeBtn;
	}

	public void setCloseBtn(Sprite closeBtn) {
		this.closeBtn = closeBtn;
	}

	public PostItHelp getHelpEntity() {
		return helpEntity;
	}

	public void setHelpEntity(PostItHelp helpEntity) {
		this.helpEntity = helpEntity;
	}

	public Sprite getAplicationInvokeBtn() {
		return aplicationInvokeBtn;
	}

	public void setAplicationInvokeBtn(Sprite aplicationInvokeBtn) {
		this.aplicationInvokeBtn = aplicationInvokeBtn;
	}

	public OnActionListener getOnActionListener() {
		return onActionListener;
	}

	public void setOnActionListener(OnActionListener onActionListener) {
		this.onActionListener = onActionListener;
	}

	public ITextureRegion getBackgroundRegion() {
		return backgroundRegion;
	}

	public void setBackgroundRegion(ITextureRegion backgroundRegion) {
		this.backgroundRegion = backgroundRegion;
	}

};

/**
 * Utilizado para ejecutar un m�todo una vez el postit ha sido cerrado. De esta
 * forma se puede entender desde otra instancia que el postit est� listo para
 * ser minimizado.
 * 
 * @author vgarzon
 * 
 */
interface OnActionListener {
	public void onClose();

	public void onEdit();

	public void onTitleChange(String title);

	public void onDescriptionChange(String description);
	
	public void onColorChange(Color c);
}
