/**
 * Representa el campo de información de ayuda del postit y cuenta con los botones necesarios
 * para la navegación a través de la misma. Para cargar la información utiliza el texto postitReference
 * a partir del cual ubica los datos en los recursos de la aplicación.
 */
package com.smartsoft.Educanvas;

import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.LoopEntityModifier.ILoopEntityModifierListener;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.AutoWrap;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.HorizontalAlign;
import org.andengine.util.color.Color;
import org.andengine.util.modifier.IModifier;
import org.andengine.util.modifier.LoopModifier;
import org.andengine.util.modifier.ease.EaseLinear;

import android.graphics.Typeface;

public class PostItHelp extends Entity {
	private String postitReference;
	private float posX;
	private float posY;
	private CanvasActivity context;
	VertexBufferObjectManager vbom;

	Font titleFont;
	Font descriptionFont;
	Sprite spriteBackground;

	Sprite[] buttons;
	Sprite lastBtn;
	
	Text titleText;
	Text descriptionText;

	int titleId;
	int descriptionId;

	public PostItHelp(float x, float y, String postitReference,
			VertexBufferObjectManager vbom, CanvasActivity context) {
		super(x, y);
		this.posX = x;
		this.posY = y;
		this.postitReference = postitReference;
		this.vbom = vbom;
		this.context = context;

		buttons = new Sprite[4];
		loadButtons();
		init();
		draw();
		drawButtons();
		updateText();
		setVisible(false);
	}

	private void init() {
		
		spriteBackground = new Sprite(0, 50, CanvasManager.getInstance().helpBackgroundTextureRegion, vbom);
		
		this.titleFont = FontFactory.create(context.getFontManager(),
				context.getTextureManager(), 256, 256,
				Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 18,
				Color.BLACK.getARGBPackedInt());
		this.titleFont.load();

		this.descriptionFont = FontFactory.create(context.getFontManager(),
				context.getTextureManager(), 256, 256,
				Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 14,
				Color.BLACK.getARGBPackedInt());
		this.descriptionFont.load();

		titleId = context.getResources().getIdentifier(
				postitReference + "_title", "string", context.getPackageName());
		descriptionId = context.getResources().getIdentifier(
				postitReference + "_description", "string",
				context.getPackageName());
		TextOptions tOptions = new TextOptions(AutoWrap.WORDS, 280,
				HorizontalAlign.LEFT, Text.LEADING_DEFAULT);
		titleText = new Text(100, 160, titleFont, "test", tOptions, vbom);
		descriptionText = new Text(100, 190, descriptionFont, "test",
				tOptions, vbom);
		
		spriteBackground.attachChild(titleText);
		spriteBackground.attachChild(descriptionText);
		
	}

	public void draw() {
		//spriteBackground.setColor(new Color(0, 0, 0, 50));
		this.attachChild(spriteBackground);

		Text referenceText = new Text(60, 50, titleFont, context.getResources()
				.getString(R.string.reference), vbom);
		spriteBackground.attachChild(referenceText);
	}
	
	private void updateText(){
		spriteBackground.detachChild(titleText);
		spriteBackground.detachChild(descriptionText);
		
		TextOptions tOptions = new TextOptions(AutoWrap.WORDS, 280,
				HorizontalAlign.LEFT, Text.LEADING_DEFAULT);
		
		String title = titleId != 0 ? context.getResources().getString(titleId)
				: "title not found...";
		String description = descriptionId != 0 ? context.getResources()
				.getString(descriptionId) : "description not found...";
		
		titleText = new Text(60, 90, titleFont, title, tOptions, vbom);
		descriptionText = new Text(60, 120, descriptionFont, description,
				tOptions, vbom);
		
		spriteBackground.attachChild(titleText);
		spriteBackground.attachChild(descriptionText);
	}

	private void drawButtons() {
		for (int i = 0; i < buttons.length; i++) {
			if (buttons[i] != lastBtn) {
				spriteBackground.attachChild(buttons[i]);
			}
		}
	}

	private void registerTouchButtons() {
		for (int i = 0; i < buttons.length; i++) {
			if (buttons[i] != lastBtn) {
				MainScene.getInstance().registerTouchArea(buttons[i]);
			}
		}
	}

	private void unregisterTouchButtons() {
		for (int i = 0; i < buttons.length; i++) {
			MainScene.getInstance().unregisterTouchArea(buttons[i]);
		}
	}

	public void loadButtons() {
		buttons[0] = new Sprite(40, 432, CanvasManager.getInstance().plusBtn, vbom) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {

				if (pSceneTouchEvent.isActionDown()) {
					lastBtn.setPosition(this.getX(), this.getY());
					titleId = context.getResources().getIdentifier(
							postitReference + "_plus_title", "string", context.getPackageName());
					descriptionId = context.getResources().getIdentifier(
							postitReference + "_plus_description", "string",
							context.getPackageName());
					changeButton(0.1f, this, lastBtn);
				}

				return true;
			}
		};
		buttons[1] = new Sprite(173, 432, CanvasManager.getInstance().askBtn, vbom) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {

				if (pSceneTouchEvent.isActionDown()) {
					lastBtn.setPosition(this.getX(), this.getY());
					titleId = context.getResources().getIdentifier("ask_title", "string", context.getPackageName());
					descriptionId = context.getResources().getIdentifier(
							postitReference + "_ask_description", "string",
							context.getPackageName());
					changeButton(0.1f, this, lastBtn);
				}

				return true;
			}
		};
		;
		buttons[2] = new Sprite(306, 432, CanvasManager.getInstance().typesBtn, vbom) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {

				if (pSceneTouchEvent.isActionDown()) {
					lastBtn.setPosition(this.getX(), this.getY());
					titleId = context.getResources().getIdentifier(
							postitReference + "_types_title", "string", context.getPackageName());
					descriptionId = context.getResources().getIdentifier(
							postitReference + "_types_description", "string",
							context.getPackageName());
					changeButton(0.1f, this, lastBtn);
				}

				return true;
			}
		};
		buttons[3] = new Sprite(0, 432, CanvasManager.getInstance().homeBtn, vbom) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {

				if (pSceneTouchEvent.isActionDown()) {
					lastBtn.setPosition(this.getX(), this.getY());
					titleId = context.getResources().getIdentifier(
							postitReference + "_title", "string", context.getPackageName());
					descriptionId = context.getResources().getIdentifier(
							postitReference + "_description", "string",
							context.getPackageName());
					changeButton(0.1f, this, lastBtn);
				}

				return true;
			}
		};
		lastBtn = buttons[3];
	}

	public void show() {
		setVisible(true);
		animate(0.2f, posX, posX - 300, posY, posY, 0, 150, false);
		registerTouchButtons();
	}

	public void hide() {
		animate(0.2f, posX - 300, posX, posY, posY, 0, 150, true);
		unregisterTouchButtons();
	}

	private void changeButton(float duration, final Sprite buttonOut,
			final Sprite buttonIn) {

		spriteBackground.attachChild(buttonIn);

		buttonOut.registerEntityModifier(new AlphaModifier(duration, 1, 0));

		ParallelEntityModifier pem = new ParallelEntityModifier(
				new AlphaModifier(duration, 0, 1), new MoveModifier(duration,
						buttonIn.getX(), buttonIn.getX(), buttonIn.getY() + 20,
						buttonIn.getY()));

		pem.addModifierListener(new IEntityModifierListener() {

			@Override
			public void onModifierStarted(IModifier<IEntity> pModifier,
					IEntity pItem) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onModifierFinished(IModifier<IEntity> pModifier,
					IEntity pItem) {
				MainScene.getInstance().unregisterTouchArea(buttonOut);
				MainScene.getInstance().registerTouchArea(buttonIn);
				lastBtn = buttonOut;
				spriteBackground.detachChild(lastBtn);
				lastBtn.setAlpha(1);
				updateText();
			}

		});

		buttonIn.registerEntityModifier(pem);
	}

	private void animate(float duration, float fromX, float toX, float fromY,
			float toY, int fromAlpha, int toAlpha, final boolean hideOnFinish) {

		final LoopEntityModifier entityModifier = new LoopEntityModifier(
				new IEntityModifierListener() {

					@Override
					public void onModifierStarted(IModifier<IEntity> pModifier,
							IEntity pItem) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onModifierFinished(
							IModifier<IEntity> pModifier, IEntity pItem) {
						setVisible(!hideOnFinish);
					}
				}, 1, new ILoopEntityModifierListener() {

					@Override
					public void onLoopStarted(
							LoopModifier<IEntity> pLoopModifier, int pLoop,
							int pLoopCount) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onLoopFinished(
							LoopModifier<IEntity> pLoopModifier, int pLoop,
							int pLoopCount) {
						// TODO Auto-generated method stub

					}
				}, new SequenceEntityModifier(new ParallelEntityModifier(
						new AlphaModifier(duration, 0, 0.5f), new MoveModifier(
								duration, fromX, toX, fromY, toY,
								EaseLinear.getInstance()))));

		this.registerEntityModifier(entityModifier);
	}

}
