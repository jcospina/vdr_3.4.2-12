//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright © 2011-2013 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;

/// <summary>
/// Simple example script of how a button can be scaled visibly when the mouse hovers over it or it gets pressed.
/// </summary>

[AddComponentMenu("NGUI/Interaction/Button Scale")]
public class UIButtonScale2 : MonoBehaviour
{
	public Transform tweenTarget;
	public Vector3 hover = new Vector3(1.1f, 1.1f, 1.1f);
	public Vector3 pressed = new Vector3(1.05f, 1.05f, 1.05f);
	public float duration = 0.2f;

	Vector3 mScale;
	bool mStarted = false;
	bool mHighlighted = false;



	private double tiempo;
	private bool iniciarTiempo = true;


	private bool presionado = false;
	private string nombreObjeto = "";
	
	private GameObject objetoTemporal;


	private bool objetivoCompleto = false;

	void Start ()
	{
		this.gameObject.transform.GetChild (0).GetComponent<UISprite> ().color = Color.white;
		if (!mStarted)
		{
			mStarted = true;
			if (tweenTarget == null) tweenTarget = transform;
			mScale = tweenTarget.localScale;
		}
	}






	void Update ()
	{

		if ((GestorMenu.TagBoton == "BotonesMenu" || GestorMenu.TagBoton == "Teclado") && !presionado) 
		{

				objetoTemporal = GameObject.Find(GestorMenu.BotonPresionado);
				//objetoTemporal.GetComponent<UIButtonScale2>().OnHover(true);
				tiempo = Time.time;
				iniciarTiempo = false;
			    presionado = true;
				
		}


		if(GestorMenu.TagBoton == "" && presionado )
		{
			this.gameObject.transform.GetChild (0).GetComponent<UISprite> ().color = Color.white;
			objetoTemporal.GetComponent<UIButtonScale2>().OnHover(false);
			objetoTemporal.GetComponent<UIButtonScale2>().OnPress(false);
			this.gameObject.GetComponent<UIButtonScale2>().enabled = false;

			iniciarTiempo = true;
			presionado = false;
			//buscarArreglo = true;
		}



		if(!iniciarTiempo)
		{

			double tempo = Time.time - tiempo;

			if(tempo >= 2f)
			{

				if(GestorMenu.TagBoton == "Teclado")
				{

					//this.gameObject.GetComponent<UIButtonSound1>().Invoke("OnPress",0);

					GameObject camaraNGUI = GameObject.Find("CameraNGUI");
					camaraNGUI.GetComponent<TitilarLetra>().Invoke("SiguienteLetra",0);
					camaraNGUI.GetComponent<TitilarLetra>().CancelInvoke("CambioAlfaLetra");

					if(GestorMenu.PosicionInicial >= 3)
					GestorMenu.PosicionInicial = 0;
					else
					GestorMenu.PosicionInicial = GestorMenu.PosicionInicial+1;

					this.gameObject.GetComponent<AudioSource>().Play();
				}

				if(GestorMenu.BotonPresionado == "BotonBorrar")
				{

					//this.gameObject.GetComponent<UIButtonSound1>().Invoke("OnPress",0);

					if(GestorMenu.PosicionInicial > 0)
					GestorMenu.PosicionInicial = GestorMenu.PosicionInicial - 1;
					else
					GestorMenu.PosicionInicial = 3;


					GameObject objetoCamaraNGUI = GameObject.Find("CameraNGUI");

					objetoCamaraNGUI.GetComponent<TitilarLetra>().Invoke("SiguienteLetra",0);
					objetoCamaraNGUI.GetComponent<TitilarLetra>().CancelInvoke("CambioAlfaLetra");

					this.gameObject.GetComponent<AudioSource>().Play();

				}



				if(GestorMenu.BotonPresionado == "BotonOk")
				{

					//this.gameObject.GetComponent<UIButtonSound1>().Invoke("OnPress",0);

					GameObject[] inicialesDatos = GameObject.FindGameObjectsWithTag ("Record");
					

					GameObject objetoCamaraNGUI = GameObject.Find("CameraNGUI");
					objetoCamaraNGUI.GetComponent<TitilarLetra>().CancelInvoke("CambioAlfaLetra");

					ObtenerNuevasIniciales();
					this.gameObject.GetComponent<AudioSource>().Play();
				}


				if(GestorMenu.BotonPresionado == "BotonSalir")
				{
					this.gameObject.GetComponent<AudioSource>().Play();
					Application.Quit();
				}

				if(GestorMenu.BotonPresionado == "BotonRecords")
				{

					this.gameObject.GetComponent<AudioSource>().Play();
					Application.LoadLevel("Esc_Records");
				}

				if(GestorMenu.BotonPresionado == "BotonJugar")
				{
					this.gameObject.GetComponent<AudioSource>().Play();
					Application.LoadLevel("Esc_Principal");
				}


				if(GestorMenu.BotonPresionado == "BotonRegresar")
				{
					this.gameObject.GetComponent<AudioSource>().Play();
					Application.LoadLevel("Esc_Intro");
				}




				//SE DEFINE LOS BOTONES DEL MENUS PAUSA
				if(GestorMenu.BotonPresionado == "BotonContinuar")//Boton continuar con la partida
				{
					this.gameObject.GetComponent<AudioSource>().Play();
					Application.LoadLevel("Esc_Principal");

				}


				if(GestorMenu.BotonPresionado == "BotonReiniciar" || GestorMenu.BotonPresionado == "BotonSalirPartida" )//Boton para reiniciar la partida
				{

					this.gameObject.GetComponent<AudioSource>().Play();
					//Se inicializan los parametros del juego
					GestorJuego.JuegoPausado = false;
					GestorJuego.Puntaje = 0;//Se reinicia el marcador 
					GestorJuego.Combustible = 0;//Se reiniciar el combustible del avion
					GestorJuego.PosicionAvion = Vector3.zero;//se resetea la posicion del avion
					GestorJuego.TagDestino = "Objetivo1";//Se coloca la tag por defecto

					if(GestorMenu.BotonPresionado == "BotonReiniciar")
					Application.LoadLevel("Esc_Principal");
					else
					Application.LoadLevel("Esc_Intro");

				}


				//SE DEFINE LOS BOTONES DEL MENU DE TEXTO INFORMATIVO
				if(GestorMenu.BotonPresionado.Equals("BotonSalirTexto"))
				{
					this.gameObject.GetComponent<AudioSource>().Play();
					Application.LoadLevel("Esc_Principal");
				}


				iniciarTiempo = true;

			}//TERMINA LA CONDICION "tempo >= 2"


		}//TERMINA LA CONDICION "!iniciarTiempo"


	}
	//-------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>TERMINA EL METODO "UPDATE"





	//-------------------------------<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<METODO PARA OBTENER LAS NUEVAS INICIALES QUE SE COLOCARAN EN LA PANTALLA DE RECORDS
	void ObtenerNuevasIniciales()
	{
		GameObject[] objetoIniciales = GameObject.FindGameObjectsWithTag ("Iniciales"); 

		int d = 0;
		while(d < objetoIniciales.Length)
		{

			for(int i = 0; i < objetoIniciales.Length; ++i)
			{
				if(objetoIniciales[i].name.Equals("Inicial"+d))
				GestorPuntaje.NuevoNombre = GestorPuntaje.NuevoNombre + objetoIniciales[i].GetComponent<UILabel>().text;
			}
			d = d + 1;
		}

		Camera.main.GetComponent<CargarPuntuaciones>().enabled = true;
		//Camera.main.GetComponent<CargarPuntuaciones>().Invoke("MostrarInfoPantalla",0);
	}
	//-------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>TERMINA EL METODO "ObtenerNuevasIniciales"






	//--------------------------------<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<METODO REINICIAR PARAMETROS
	/*void ReiniciarParametros()
	{

		GestorPuntuacion.NuevoNombre = "";
		GestorPuntuacion.MostrarEscenaRecord = false;

		GestorMenuJuego.MovimientoMenuPuntajes = false;


		GestorJuego.MarcadorTotal = 0;
		GestorJuego.GameOver = false;
		GestorJuego.Nivel = 0;
		GestorJuego.TiempoTotal = GestorJuego.SetPointTiempo;
		GestorJuego.TiempoCumplido = false;
		GestorJuego.PlayGame = false;
	}*/
	//------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>TERMINA EL METODO "ReiniciarParametros"




	/*
	private string[] arregloLetras;
	private string nombreRecord = "";
	private string letraBuscar = "";
	private int posicionArreglo = 0;
	private bool buscarArreglo = true;
	//------------------------------<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<METODO PARA BUSCAR EN EL ARREGLO DE CARACTERES LA POSICION DEL CARACTER
	int UbicacionLetra(int dato,string letra)
	{
		int i = 0;
		bool paso = true;
		while(paso && i < GestorPuntuacion.ArregloCaracteres.Length)
		{
			if(GestorPuntuacion.ArregloCaracteres[i].Equals(letra))
			{
				paso = false;
				dato = i;
			}
			i = i + 1;
		}

		return dato;
	}
	//----------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>TERMINA EL METODO "UbicacionLetra"





	//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<METODO PARA MODIFICAR LOS CARACTERES DE LAS INICIALES DEL JUGADOR
	void ModificarInicialesJugador(int signo)
	{

		if(buscarArreglo)
		{
			
			nombreRecord = GestorPuntuacion.NombresJugadores[GestorPuntuacion.PosicionY];
			
			letraBuscar = nombreRecord.Substring(GestorPuntuacion.PosicionX,1);
			
			posicionArreglo = UbicacionLetra(posicionArreglo,letraBuscar);
			buscarArreglo = false;
			
			arregloLetras = new string[GestorPuntuacion.NombresJugadores[GestorPuntuacion.PosicionY].Length];
			
			for(int f = 0; f < arregloLetras.Length ; ++f)
			{
				arregloLetras[f] = GestorPuntuacion.NombresJugadores[GestorPuntuacion.PosicionY].Substring(f,1);
			}
			
		}
		
		
		posicionArreglo = posicionArreglo + signo;
		
		if(posicionArreglo < 0 && signo < 0)
		{
			posicionArreglo = GestorPuntuacion.ArregloCaracteres.Length  - 1;
		}
		

		if(posicionArreglo > (GestorPuntuacion.ArregloCaracteres.Length  - 1) && signo > 0)
		{
			posicionArreglo = 0;
		}
		
		
		string a = GestorPuntuacion.ArregloCaracteres[posicionArreglo];//Nuevo caracter
		string inicialesJugador = "";
		arregloLetras[GestorPuntuacion.PosicionX] = a; //Se cambia el caracter del arreglo
		
		for(int g = 0; g < arregloLetras.Length ; ++ g)
		{
			inicialesJugador = inicialesJugador + arregloLetras[g];
		}
		
		
		GameObject cambioInfoNombre = GameObject.Find("LetreroNombre"+(GestorPuntuacion.PosicionY+1)+"");
		cambioInfoNombre.GetComponent<UILabel>().text = inicialesJugador;
		
		GestorPuntuacion.NombresJugadores[GestorPuntuacion.PosicionY] = inicialesJugador;
	
	}
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>TERMINA EL METODO "ModificarInicialesJugador"

*/

	//Metodo cuando inicia la aplicacion
	void OnEnable () { 
		if (mStarted && mHighlighted) OnHover(UICamera.IsHighlighted(gameObject)); 
	}

	//Metodo cuando termina la aplicacion
	void OnDisable ()
	{
		if (mStarted && tweenTarget != null)
		{
			TweenScale tc = tweenTarget.GetComponent<TweenScale>();

			if (tc != null)
			{
				tc.scale = mScale;
				tc.enabled = false;
			}
		}
	}

	void OnPress (bool isPressed)
	{


		if (enabled)
		{
			if (!mStarted) Start();
			TweenScale.Begin(tweenTarget.gameObject, duration, isPressed ? Vector3.Scale(mScale, pressed) :
				(UICamera.IsHighlighted(gameObject) ? Vector3.Scale(mScale, hover) : mScale)).method = UITweener.Method.EaseInOut;
		}
	}

	void OnHover (bool isOver)
	{

		if (enabled)
		{

			if (!mStarted)
			{
				Start();
			}
			TweenScale.Begin(tweenTarget.gameObject, duration, isOver ? Vector3.Scale(mScale, hover) : mScale).method = UITweener.Method.EaseInOut;
			mHighlighted = isOver;
		}
	}
}
