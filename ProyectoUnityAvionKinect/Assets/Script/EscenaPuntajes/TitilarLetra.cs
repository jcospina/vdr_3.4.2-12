﻿using UnityEngine;
using System.Collections;

public class TitilarLetra : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	private bool llamadoMetodo = true;
	private GameObject letraActual;
	void Update () {
	
		if(llamadoMetodo)
		{

			letraActual = GameObject.Find("Inicial"+GestorMenu.PosicionInicial);
			InvokeRepeating("CambioAlfaLetra",0,0.5f);
			llamadoMetodo = false;
		}


	}


	//--------------------------------------------------<<<<<<<<<<<<<<<<METODO ENCARGADO DE CAMBIAR EL COLOR ALFA DE LA LETRA DE LA INICIAL ACTUAL
	private bool activarAlfa = true;
	private Color kolor;
	void CambioAlfaLetra()
	{

		kolor = letraActual.GetComponent<UILabel> ().color;
				if (activarAlfa)
						kolor.a = 0;
				else
						kolor.a = 1;

		letraActual.GetComponent<UILabel> ().color = kolor;
		activarAlfa = !activarAlfa;
	}
	//-------------------------------------------------->>>>>>>>>>>>>>>>>TERMINA EL METODO "CambioAlfaLetra" 


	void SiguienteLetra()
	{
		kolor.a = 1;
		letraActual.GetComponent<UILabel> ().color = kolor;
		llamadoMetodo = true;
	}

}
