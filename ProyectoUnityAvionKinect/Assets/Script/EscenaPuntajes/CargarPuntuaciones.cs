﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml;

public class CargarPuntuaciones : MonoBehaviour {




	#region ATRIBUTOS
	private bool llamarFuncion = true;
	private bool cargarDatosPantalla = false;
	#endregion
	
	#region METODOS
	//----------------------------------<<<<<<<<<<<<<<<<<<<<<<<<<SE DEFINE LA FUNCION DE LAZO CERRADO
	void Update () {


		if (llamarFuncion) 
		{
			llamarFuncion = false;
			CargarXML();
		}
	}
	//---------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>TERMINA EL METODO "Update"



	//----------------------------------<<<<<<<<<<<<<<<<<<<<<<<<<SE DEFINE LA FUNCION PARA CARGAR EL ARCHIVO XML 
	void CargarXML()
	{


		string dirCarpeta = System.IO.Path.Combine (Application.dataPath+"/Resources/","XML");//se carga la direccion del folder "CarpetaXML"
		string dirArchivoXML = dirCarpeta+"/Puntuaciones.xml";
		
		
		if(!System.IO.Directory.Exists(dirCarpeta))
		{
			System.IO.Directory.CreateDirectory (dirCarpeta);//se crea el folder
		}


		if(!System.IO.File.Exists(dirArchivoXML))
		{
			CrearXML(dirArchivoXML,true);//Se llama al metodo encargado de crear el archivo XML
		}
		else
		{
			XmlTextReader	xmlTextoLectura = new XmlTextReader (dirArchivoXML);
			LecturaXML(xmlTextoLectura,dirArchivoXML);//Se llama al metodo de lectura
		}

	}
	//----------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>TERMINA EL METODO "CargarXML"







	//------------------------------------<<<<<<<<<<<<<<<<<<<<<<<<SE DEFINE EL METODO ENCARGADO DE CREAR EL ARCHIVO XML
	void CrearXML(string ruta, bool lectura)
	{
		XmlDocument documento = new XmlDocument ();//Se crea el objeto documento del tipo XmlDocument
		
		XmlElement raiz = documento.CreateElement ("Jugadores");//Se crea el nodo principal del archivo XML "Jugadores"
		documento.AppendChild (raiz);//Se agrega el nodo raiz al documento


		//Se crean los nodos que hacen parte de la raiz o nodo principal
		
		XmlElement jugador;
		XmlElement nombreJugador;
		XmlElement kilometrajeJugador;


		for(int i = 0; i < GestorPuntaje.ArregloJugadores.Count; ++i)
		{
			jugador = documento.CreateElement("Jugador");//Se crea el nodo Jugador
			raiz.AppendChild(jugador);//Se agrega el nodo jugador al nodo raiz
			
			nombreJugador = documento.CreateElement("Nombre");//Se crea el nodo Nombre del jugador
			nombreJugador.AppendChild(documento.CreateTextNode((string)GestorPuntaje.ArregloJugadores[i]));//Se agrega el atributo al nodo nombreJugador
			jugador.AppendChild(nombreJugador);//Se agrega el nodo nombreJugador al nodo jugador
			
			kilometrajeJugador = documento.CreateElement("Kilometros");//Se crea el nodo Puntaje del jugador
			kilometrajeJugador.AppendChild(documento.CreateTextNode((string)GestorPuntaje.ArregloKilometrosMaximos[i]));//Se carga el atributo al nodo puntajeJugador
			jugador.AppendChild(kilometrajeJugador);//Se agrega el nodo puntajeJugador al nodo jugador

		}

		documento.Save (ruta);//Se guarda el archivo en la ruta

		if (lectura)
		{
			XmlTextReader	xmlTextoLectura = new XmlTextReader (ruta);
			LecturaXML (xmlTextoLectura,ruta);
		}

	}
	//------------------------------------->>>>>>>>>>>>>>>>>>>>>>>>TERMINA EL METODO "CrearXML" 





	//-------------------------------------<<<<<<<<<<<<<<<<<<<<<<<<SE DEFINE EL METODO ENCARGADO DE LA LECTURA DEL ARCHIVO XML
	void LecturaXML(XmlTextReader xmlTextoLectura, string directorio)
	{
		ArrayList listTempJugadores = new ArrayList();
		ArrayList listTempKilometros = new ArrayList();


		cargarDatosPantalla = true;




		try
		{
			bool guardarNombre = false;
			bool guardarKilometraje = false;
			
			
			
			while(xmlTextoLectura.Read())
			{
				if(xmlTextoLectura.NodeType == XmlNodeType.Element)
				{
					
					if(xmlTextoLectura.Name == "Nombre" && !guardarNombre)
					{
						guardarNombre = true;
					}
					
					if(xmlTextoLectura.Name == "Kilometros" && !guardarKilometraje)
					{
						guardarKilometraje = true;
					}
					
				}
				
				if(xmlTextoLectura.NodeType == XmlNodeType.Text)
				{
					if(guardarNombre)
					{
						//GestorPuntuacion.ArregloJugadores.Add(xmlTextoLectura.ReadContentAsString());
						listTempJugadores.Add(xmlTextoLectura.ReadContentAsString());
						guardarNombre = false;
					}
					
					if(guardarKilometraje)
					{
						//GestorPuntuacion.ArregloMaximosPuntajes.Add(xmlTextoLectura.ReadContentAsString());
						listTempKilometros.Add(xmlTextoLectura.ReadContentAsString());
						guardarKilometraje = false;
					}
					
				}
				
			}//FIN DEL CICLO WHILE
			

			GestorPuntaje.ArregloJugadores.Clear();//Se elimina el contenido del arreglo tipo lista
			GestorPuntaje.ArregloKilometrosMaximos.Clear();//Se elimina el contenido del arreglo de tipo lista
					
			GestorPuntaje.ArregloJugadores = listTempJugadores;
			GestorPuntaje.ArregloKilometrosMaximos = listTempKilometros;

			if(cargarDatosPantalla)//condicion para mostrar los datos en la pantalla de Record
			{
				xmlTextoLectura.Close();//Se cierra la lectura del archivo
				ActualizarPantallaRecord(directorio);
				cargarDatosPantalla = false;
			}
						

			if(Application.loadedLevelName.Equals("Esc_Principal"))
				this.transform.gameObject.GetComponent<CargarPuntuaciones>().enabled = false;
			else
				Camera.main.GetComponent<CargarPuntuaciones>().enabled = false;
			llamarFuncion = true;
			
			
			
		}//FIN DEL CICLO TRY 
		catch(XmlException e)
		{
			xmlTextoLectura.Close();//Se cierra la lectura del archivo
			
			if(System.IO.File.Exists(@""+directorio))//Condicion para determinar si el archivo existe
			{
				// Use a try block to catch IOExceptions, to
				// handle the case of the file already being
				// opened by another process.
				try
				{
					System.IO.File.Delete(@""+directorio);
				}
				catch (System.IO.IOException e1)
				{
					Debug.LogError(e1.Message);
					return;
				}
			}
			
			
			CargarXML();
		}


	}
	//--------------------------------------->>>>>>>>>>>>>>>>>>>>>>>TERMINA EL METODO "LecturaXML"






	//---------------------------------------<<<<<<<<<<<<<<<<<<<<SE DEFINE UN METODO ENCARGADO DE ACTUALIZAR LAS MAXIMAS PUNTUACIONES
	void ActualizarPantallaRecord(string dir)
	{
		if(Application.loadedLevelName.Equals("Esc_IngresarIniciales"))//Condicion para determinar si se esta en la escena de ingreso de iniciales
		{
			//Se actualiza el contenido del archivo XML con los datos del nuevo registro o record
			int q = 0;//variable para cambiar de posicion en el arraylist de Puntajes 
			bool insertar = false;
			
			while(GestorJuego.Puntaje < System.Int32.Parse((string)GestorPuntaje.ArregloKilometrosMaximos[q])	&&	q < GestorPuntaje.ArregloKilometrosMaximos.Count - 1)
			{
				q = q+1;
			}
			
			if(GestorJuego.Puntaje >= System.Int32.Parse((string)GestorPuntaje.ArregloKilometrosMaximos[q]))
				insertar = true;
			
			
			if(insertar)
			{
				
				int[] arregloTemporalPuntaje = new int[GestorPuntaje.ArregloKilometrosMaximos.Count]; 
				string[] arregloTemporalNombres = new string[GestorPuntaje.ArregloKilometrosMaximos.Count];
				
				for(int n = 0; n < arregloTemporalPuntaje.Length; ++n)
				{
					if(n < q)
					{
						arregloTemporalPuntaje[n] = System.Int32.Parse((string)GestorPuntaje.ArregloKilometrosMaximos[n]);
						arregloTemporalNombres[n] = (string)GestorPuntaje.ArregloJugadores[n];
					}
					
					if(n == q)
					{
						arregloTemporalPuntaje[n] = (int)GestorJuego.Puntaje;
						arregloTemporalNombres[n] = GestorPuntaje.NuevoNombre;
					}
					
					if(n > q)
					{
						arregloTemporalPuntaje[n] = System.Int32.Parse((string)GestorPuntaje.ArregloKilometrosMaximos[n-1]);
						arregloTemporalNombres[n] = (string)GestorPuntaje.ArregloJugadores[n-1];
					}
				}
				
				
				GestorPuntaje.ArregloJugadores.Clear();
				GestorPuntaje.ArregloKilometrosMaximos.Clear();
				
				for(int m = 0; m < arregloTemporalPuntaje.Length; ++m)
				{
					
					GestorPuntaje.ArregloKilometrosMaximos.Add(arregloTemporalPuntaje[m]+"");
					GestorPuntaje.ArregloJugadores.Add(arregloTemporalNombres[m]);
					
				}
				
				
				if(System.IO.File.Exists(@""+dir))//Condicion para determinar si el archivo existe
				{
					// Use a try block to catch IOExceptions, to
					// handle the case of the file already being
					// opened by another process.
					try
					{
						System.IO.File.Delete(@""+dir);
					}
					catch (System.IO.IOException e1)
					{
						Debug.LogError(e1.Message);
						return;
					}
				}
				
				CrearXML(dir,false);
				
			}//TERMINA CONDICION IF PARA "insertar"
		
			Application.LoadLevel ("Esc_Records");

		}//TERMINA CONDICION IF PARA ESCENA "Esc_IngresarIniciales"
		else
		{

			//se muestra en la pantalla de record los registros que se encuentran en el documento XML
			GameObject[] elementosPantallaRecord = GameObject.FindGameObjectsWithTag ("Record");
			int contador = 0;
			while(contador < elementosPantallaRecord.Length)
			{
				for(int i = 0; i < elementosPantallaRecord.Length; ++i)
				{
					if(elementosPantallaRecord[i].name.Equals("InicialesLugar"+contador))
					{
						elementosPantallaRecord[i].GetComponent<UILabel>().text = (contador+1)+"."+(string)GestorPuntaje.ArregloJugadores[contador];
						
						for(int j = 0; j < elementosPantallaRecord.Length; ++j)
						{
							
							if(elementosPantallaRecord[j].name.Equals("PuntajeLugar"+contador))
							{
								elementosPantallaRecord[j].GetComponent<UILabel>().text = (string)GestorPuntaje.ArregloKilometrosMaximos[contador]+"m";
							}
							
						}//TERMINA EL CICLO PARA "j"
						
					}
					
				}//TERMINA EL CICLO PARA "i"
				contador = contador + 1;
				
			}//TERMINA EL CICLO WHILE PARA "contador"


		}






	}
	//---------------------------------------->>>>>>>>>>>>>>>>>>>>TERMINA EL METODO "ActualizarPantallaRecord" 
	#endregion 
}
