//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright © 2011-2013 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;

/// <summary>
/// Similar to UIButtonColor, but adds a 'disabled' state based on whether the collider is enabled or not.
/// </summary>

//[AddComponentMenu("NGUI/Interaction/Button")]
public class UIButton2 : UIButtonColor
{
	/// <summary>
	/// Color that will be applied when the button is disabled.
	/// </summary>

	public Color disabledColor = Color.grey;


	/// <summary>
	/// If the collider is disabled, assume the disabled color.
	/// </summary>


	private double tiempo;
	private bool iniciarTiempo = true;
	private bool presionado = false;

	private string nombreObjeto;


	void Update()
	{





		if ( (GestorMenu.TagBoton == "BotonesMenu" || GestorMenu.TagBoton == "Teclado") && !presionado) 
		{
			GameObject temporal = GameObject.Find(GestorMenu.BotonPresionado);
			nombreObjeto = GestorMenu.BotonPresionado;
			temporal.GetComponent<UIButton2>().OnHover(true);


			if(GestorMenu.TagBoton == "Teclado")
			{

				if(GestorMenu.PosicionInicial == 0)
				{
					GameObject inicial1 = GameObject.Find("Inicial0");
					inicial1.GetComponent<UILabel>().text = GestorMenu.BotonPresionado+"";
				}
				
				if(GestorMenu.PosicionInicial == 1)
				{
					GameObject inicial2 = GameObject.Find("Inicial1");
					inicial2.GetComponent<UILabel>().text = GestorMenu.BotonPresionado+"";
				}
				
				if(GestorMenu.PosicionInicial == 2)
				{
					GameObject inicial3 = GameObject.Find("Inicial2");
					inicial3.GetComponent<UILabel>().text = GestorMenu.BotonPresionado+"";
				}
				
				if(GestorMenu.PosicionInicial == 3)
				{
					GameObject inicial4 = GameObject.Find("Inicial3");
					inicial4.GetComponent<UILabel>().text = GestorMenu.BotonPresionado+"";
				}
			}


			if(GestorMenu.TagBoton.Equals("BotonesMenu"))
			{
				GameObject temporal2 = GameObject.Find(GestorMenu.BotonPresionado);
				nombreObjeto = GestorMenu.BotonPresionado;
				temporal2.GetComponent<UIButton2>().OnHover(true);
			}



			tiempo = Time.time;
			iniciarTiempo = false;
			presionado = true;
		}
		
		
		if(GestorMenu.TagBoton == "" && presionado )
		{
			GameObject temporal = GameObject.Find(nombreObjeto);
			temporal.GetComponent<UIButton2>().OnHover(false);
			iniciarTiempo = true;
			presionado = false;
			this.gameObject.GetComponent<UIButton2>().enabled = false;
		}



		if(!iniciarTiempo )
		{
			double tempo = Time.time - tiempo;



			if(tempo >= 2)
			{
				GameObject temporal = GameObject.Find(nombreObjeto);
				temporal.GetComponent<UIButton2>().OnPress(true);

				iniciarTiempo = true;
			}
		}

	}






	protected override void OnEnable ()
	{
		if (isEnabled) base.OnEnable();
		else UpdateColor(false, true);
	}

	public override void OnHover (bool isOver) 
	{ 
		if (isEnabled) base.OnHover(isOver); 
	}

	public override void OnPress (bool isPressed) 
	{ 
		if (isEnabled) base.OnPress(isPressed); 
	}

	/// <summary>
	/// Whether the button should be enabled.
	/// </summary>

	public bool isEnabled
	{

		get
		{
			Collider col = collider;
			return col && col.enabled;
		}
		set
		{
			Collider col = collider;
			if (!col) return;

			if (col.enabled != value)
			{
				col.enabled = value;
				UpdateColor(value, false);
			}
		}
	}

	/// <summary>
	/// Update the button's color to either enabled or disabled state.
	/// </summary>

	public void UpdateColor (bool shouldBeEnabled, bool immediate)
	{



		if (tweenTarget)
		{
			if (!mStarted)
			{
				mStarted = true;
				Init();
			}

			Color c = shouldBeEnabled ? defaultColor : disabledColor;
			TweenColor tc = TweenColor.Begin(tweenTarget, 0.15f, c);

			if (immediate)
			{
				tc.color = c;
				tc.enabled = false;
			}
		}
	}

}
