﻿using UnityEngine;
using System.Collections;

public static class GestorPuntaje  {

	#region ATRIBUTOS

	private static ArrayList arregloKilometrosMaximos = new ArrayList(){"15","14","13","12","10","5"};
	private static ArrayList arregloJugadores= new ArrayList(){"POLA","POLO","LOLA","LALO","NANO","YOLA"};
	private static string nuevoNombre = "";


	#endregion

	#region PROPIEDADES

	public static ArrayList ArregloKilometrosMaximos {
		get {
			return arregloKilometrosMaximos;
		}
		set {
			arregloKilometrosMaximos = value;
		}
	}



	public static ArrayList ArregloJugadores {
		get {
			return GestorPuntaje.arregloJugadores;
		}
		set {
			GestorPuntaje.arregloJugadores = value;
		}
	}

	public static string NuevoNombre {
		get {
			return GestorPuntaje.nuevoNombre;
		}
		set {
			 GestorPuntaje.nuevoNombre = value;
		}
	}
	#endregion


}
