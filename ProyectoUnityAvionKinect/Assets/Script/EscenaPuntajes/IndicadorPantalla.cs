﻿using UnityEngine;
using System.Collections;

public class IndicadorPantalla : MonoBehaviour {

	#region ATRIBUTOS
	public Texture2D imagenPuntero;
	public Transform objetivoPuntero;


	private Vector3 screenPos;//vector para obtener la posicion en pantalla
	
	private Vector3 dirRayo;
	private RaycastHit rayo;

	private GameObject objetoCapturado;

	private bool colision;
	#endregion


	#region METODOS
	// Use this for initialization
	void Start () {

		dirRayo = new Vector3 (0,0,200);

		rayo = new RaycastHit ();

		colision = false;



			if(Application.loadedLevelName.Equals("Esc_Pausa"))
			{
				GameObject letreroMenu = GameObject.Find("TextoMenu");
				

				if(GestorJuego.Combustible < 0)
				{
					
				letreroMenu.GetComponent<UILabel>().text = "GAME OVER";
				GameObject anclaBotonReiniciar = GameObject.Find("AnchorBotonReiniciar");
				GameObject anclaBotonSalirPartida = GameObject.Find("AnchorBotonSalirPartida");
				
				anclaBotonReiniciar.GetComponent<UIAnchor>().relativeOffset = new Vector2(-0.15f,-0.1f);
				anclaBotonReiniciar.transform.FindChild("BotonReiniciar").GetComponent<BoxCollider>().enabled = true;
				anclaBotonReiniciar.transform.FindChild("BotonReiniciar").GetComponent<UIPanel>().alpha = 1;
				
				
				anclaBotonSalirPartida.GetComponent<UIAnchor>().relativeOffset = new Vector2(0.15f,-0.1f);
				anclaBotonSalirPartida.transform.FindChild("BotonSalirPartida").GetComponent<BoxCollider>().enabled = true;
				anclaBotonSalirPartida.transform.FindChild("BotonSalirPartida").GetComponent<UIPanel>().alpha = 1;

					
				}
				else
				{

				letreroMenu.GetComponent<UILabel>().text = "PAUSA";	
					
				GameObject anclaBotonContinuar	= GameObject.Find("AnchorBotonContinuar");
				GameObject anclaBotonReiniciar = GameObject.Find("AnchorBotonReiniciar");
				GameObject anclaBotonSalirPartida = GameObject.Find("AnchorBotonSalirPartida");


				anclaBotonContinuar.GetComponent<UIAnchor>().relativeOffset = new Vector2(-0.28f,-0.1f);
				anclaBotonContinuar.transform.FindChild("BotonContinuar").GetComponent<BoxCollider>().enabled = true;
				anclaBotonContinuar.transform.FindChild("BotonContinuar").GetComponent<UIPanel>().alpha = 1;


				anclaBotonReiniciar.GetComponent<UIAnchor>().relativeOffset = new Vector2(0f,-0.1f);
				anclaBotonReiniciar.transform.FindChild("BotonReiniciar").GetComponent<BoxCollider>().enabled = true;
				anclaBotonReiniciar.transform.FindChild("BotonReiniciar").GetComponent<UIPanel>().alpha = 1;
				
				
				anclaBotonSalirPartida.GetComponent<UIAnchor>().relativeOffset = new Vector2(0.28f,-0.1f);
				anclaBotonSalirPartida.transform.FindChild("BotonSalirPartida").GetComponent<BoxCollider>().enabled = true;
				anclaBotonSalirPartida.transform.FindChild("BotonSalirPartida").GetComponent<UIPanel>().alpha = 1;

				}
					
			}



		}


	// Update is called once per frame
	void OnGUI () {
		//Debug.LogError (Screen.width*0.64f);



		if (Physics.Raycast (objetivoPuntero.gameObject.transform.position, dirRayo, out rayo) ) //Physics.Raycast (this.transform.position, dirRayo, out rayo)
		{

			if(rayo.collider.tag == "BotonesMenu" || rayo.collider.tag == "Teclado" )
			{
				GestorMenu.BotonPresionado = rayo.collider.name;
				GestorMenu.TagBoton = rayo.collider.tag;

				if(!colision)
				{
					objetoCapturado = rayo.collider.gameObject;
					objetoCapturado.GetComponent<UIButton2>().enabled = true;
					objetoCapturado.GetComponent<UIButtonScale2>().enabled = true;
					colision = true;
				}

			}

			
		}//Termina condicion raycast para "objetivoPuntero"
		else
		{
			colision = false;
			GestorMenu.BotonPresionado ="";
			GestorMenu.TagBoton = "";
		}


		screenPos = camera.WorldToScreenPoint(objetivoPuntero.transform.position);
		//screenPos = Camera.main.WorldToScreenPoint(this.transform.position);

		float xDerecha = screenPos.x - Screen.width/15;

		float yDerecha = -screenPos.y + Screen.height; 

		if(screenPos.x >=0 && screenPos.x <= Screen.width && screenPos.y >= 0 && screenPos.y <= Screen.height)
		GUI.DrawTexture(new Rect(xDerecha,yDerecha,Screen.width*0.125f,Screen.height*0.15f),imagenPuntero);

		if(screenPos.x < 0)
		{

			if(screenPos.y >= 0 && screenPos.y <= Screen.height)
			//GUI.DrawTexture(new Rect(- Screen.width/6,yDerecha,Screen.width*0.19f,Screen.height*0.19f),imagenPuntero); ajuste mano Izquierda
				GUI.DrawTexture(new Rect(- Screen.width/50,yDerecha,Screen.width*0.125f,Screen.height*0.15f),imagenPuntero);
			else
			PosicionPuntero(true);

		}


		if(screenPos.x > Screen.width)
		{

			if(screenPos.y >= 0 && screenPos.y <= Screen.height)
			//GUI.DrawTexture(new Rect(Screen.width*0.82f,yDerecha,Screen.width*0.19f,Screen.height*0.19f),imagenPuntero); ajuste mano Izquierda
				GUI.DrawTexture(new Rect(Screen.width*0.99f,yDerecha,Screen.width*0.125f,Screen.height*0.15f),imagenPuntero);
			else
			PosicionPuntero(false);

		}

		if(screenPos.y < 0 && screenPos.x >= 0 && screenPos.x <= Screen.width)
			GUI.DrawTexture(new Rect(xDerecha,Screen.height*0.97f,Screen.width*0.125f,Screen.height*0.15f),imagenPuntero);

		if(screenPos.y > Screen.height && screenPos.x >= 0 && screenPos.x <= Screen.width)
			GUI.DrawTexture(new Rect(xDerecha,0,Screen.width*0.125f,Screen.height*0.15f),imagenPuntero);
		

		}
	//------------------------------------------------------>>>>>>>>>>>>>>>>>>>>>>>TERMINA EL METODO ONGUI






	//--------------------------------------------------------<<<<<<<<<<<<<<<<<<<<METODO ENCARGADO DE LIMITAR LA POSICION DEL PUNTERO EN PANTALLA
	void PosicionPuntero(bool dir)
	{
		float cordenada = 0;
		if (dir)
		{
			//cordenada = - Screen.width/6; Ajuste mano Izquierda
			cordenada = - Screen.width/50;
		}
		else
		{
			//cordenada = Screen.width*0.82f; Ajuste mano Izquierda
			cordenada = Screen.width*0.99f;
		}

		if(screenPos.y < 0)
			GUI.DrawTexture(new Rect(cordenada,Screen.height*0.97f,Screen.width*0.125f,Screen.height*0.15f),imagenPuntero);
		
		if(screenPos.y > Screen.height)
			GUI.DrawTexture(new Rect(cordenada,0,Screen.width*0.125f,Screen.height*0.15f),imagenPuntero);

	}
	//--------------------------------------------------------->>>>>>>>>>>>>>>>>>TERMINA EL METODO "PosicionPuntero"


	#endregion
}
