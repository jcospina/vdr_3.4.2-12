﻿using UnityEngine;
using System.Collections;

public static class GestorJuego{

	#region ATRIBUTOS

	private static float velocidad = 1;//Velocidad del avion
	private static bool aterrisar = false;//Variable que indica si el avion debe aterrisar
	private static float altura = 50;//altura a la cual se encuentra el avion
	private static float combustible = 0 ;//combustible del avion

	private static float puntaje = 0;

	private static bool inicializarMenu = true;//Inicializa el menu con la info del lugar de llegada

	private static Vector3 anguloCamara = Vector3.zero;
	private static Vector3 anguloAvion 	= Vector3.zero;


	private static bool juegoPausado = false;

	private static int totalObjetivos = 13;

	private static string letreroInfo = "TextoMenuInfo1";

	private static string tagDestino = "Objetivo1";

	private static Vector3 posicionAvion = Vector3.zero;

	private static Vector3 rotacionAvion = Vector3.zero;



	#endregion


	#region PROPIEDADES



	public static float Velocidad {
		get {
			return GestorJuego.velocidad;
		}
		set {
			GestorJuego.velocidad = value;
		}
	}

	public static bool Aterrisar {
		get {
			return  GestorJuego.aterrisar;
		}
		set {
			GestorJuego.aterrisar = value;
		}
	}



	public static float Altura {
		get {
			return GestorJuego.altura;
		}
		set {
			GestorJuego.altura = value;
		}
	}



	public static float Combustible {
		get {
			return GestorJuego.combustible;
		}
		set {
			GestorJuego.combustible = value;
		}
	}

	public static float Puntaje {
		get {
			return GestorJuego.puntaje;
		}
		set {
			GestorJuego.puntaje = value;
		}
	}



	public static bool InicializarMenu {
		get {
			return GestorJuego.inicializarMenu;
		}
		set {
			GestorJuego.inicializarMenu = value;
		}
	}



	public static Vector3 AnguloCamara {
		get {
			return GestorJuego.anguloCamara;
		}
		set {
			GestorJuego.anguloCamara = value;
		}
	}

	public static Vector3 AnguloAvion {
		get {
			return GestorJuego.anguloAvion;
		}
		set {
			GestorJuego.anguloAvion = value;
		}
	}


	public static bool JuegoPausado {
		get {
			return GestorJuego.juegoPausado;
		}
		set {
			GestorJuego.juegoPausado = value;
		}
	}


	public static int TotalObjetivos {
		get {
			return GestorJuego.totalObjetivos;
		}
		set {
			GestorJuego.totalObjetivos = value;
		}
	}


	public static string LetreroInfo {
		get {
			return GestorJuego.letreroInfo;
		}
		set {
			GestorJuego.letreroInfo = value;
		}
	}


	public static string TagDestino {
		get {
			return GestorJuego.tagDestino;
		}
		set {
			GestorJuego.tagDestino = value;
		}
	}



	public static Vector3 PosicionAvion {
		get {
			return GestorJuego.posicionAvion;
		}
		set {
			GestorJuego.posicionAvion = value;
		}
	}


	public static Vector3 RotacionAvion {
		get {
			return GestorJuego.rotacionAvion;
		}
		set {
			GestorJuego.rotacionAvion = value;
		}
	}


	#endregion


}
