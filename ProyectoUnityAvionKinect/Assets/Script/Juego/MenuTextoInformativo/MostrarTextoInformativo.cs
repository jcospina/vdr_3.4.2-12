﻿using UnityEngine;
using System.Collections;

public class MostrarTextoInformativo : MonoBehaviour {

	private bool llamarFuncion = true;

	private float posicionInicial;
	private float posicionFinal; 

	// Update is called once per frame
	GameObject avion;
	void Update () {
	
		if (llamarFuncion)
		{
			//avion = GameObject.FindGameObjectWithTag("Avion");
			//avion.transform.position = new Vector3(0,0,25);
			//avion.transform.rotation = Quaternion.Euler(0,0,0);
			CargarCordenadas();
			llamarFuncion = false;
		}

		this.transform.Translate (0, Time.deltaTime * 0.2f , 0);//Time.deltaTime * 0.1f


		if(this.transform.localPosition.y > posicionFinal)
		{

			this.transform.GetComponent<MostrarTextoInformativo>().enabled = false;
			this.transform.GetComponent<UILabel>().enabled = false;
			this.transform.localPosition = new Vector3(0,posicionInicial,0);
			llamarFuncion = true;

			Application.LoadLevel("Esc_Principal");
			/*
			this.transform.GetComponent<MostrarTextoInformativo>().enabled = false;
			this.transform.GetComponent<UILabel>().enabled = false;

			this.transform.localPosition = new Vector3(0,posicionInicial,0);
			llamarFuncion = true;

			GameObject camaraMenuTextoInformativo = GameObject.Find("CameraMenuTextoObjetivos");
			camaraMenuTextoInformativo.GetComponent<Camera>().enabled = false;
			camaraMenuTextoInformativo.GetComponent<IndicadorPantalla>().enabled = false;

			GameObject camaraRadar = GameObject.Find("CamaraRadar");
			camaraRadar.GetComponent<Camera>().enabled = true;




			avion.gameObject.GetComponent<LanzamientoAvion>().enabled = true;	

			GestorJuego.LetreroInfo = "";

			GestorJuego.Velocidad = 1;
			GestorJuego.Aterrisar = false;
			*/
		}

	}


	void CargarCordenadas()
	{
		switch (this.gameObject.name)
		{

		case "TextoMenuInfo1":
			posicionInicial = -1765;//posicionInicial = -710;
			posicionFinal = 1765;//posicionFinal = 710;

			break;

		case "TextoMenuInfo2":
			posicionInicial = -1360;//posicionInicial = -560;
			posicionFinal = 1360;//posicionFinal = 560;

			break;

		case "TextoMenuInfo3":
			posicionInicial = -1640;//posicionInicial = -670;
			posicionFinal =1640;//posicionFinal = 670;
			break;

		case "TextoMenuInfo4":
			posicionInicial = -1700;//posicionInicial = -685;
			posicionFinal = 1700;//posicionFinal = 685;
			break;

		case "TextoMenuInfo5":
			posicionInicial = -1810;//posicionInicial = -735;
			posicionFinal = 1810;//posicionFinal = 740;
			break;

		case "TextoMenuInfo6":
			posicionInicial = -1490;//posicionInicial = -600;
			posicionFinal = 1490;//posicionFinal = 600;
			break;

		case "TextoMenuInfo7":
			posicionInicial = -1770;//posicionInicial = -715;
			posicionFinal = 1770;//posicionFinal = 715;
			break;
		
		case "TextoMenuInfo8":
			posicionInicial = -1645;//posicionInicial = -665;
			posicionFinal = 1645;//posicionFinal = 665;
			break;


		case "TextoMenuInfo9":
			posicionInicial = -1625;//posicionInicial = -773;
			posicionFinal = 1625;//posicionFinal = 773;
			break;

		case "TextoMenuInfo10":
			posicionInicial = -1520;//posicionInicial = -595;
			posicionFinal = 1520;//posicionFinal = 595;
			break;

		case "TextoMenuInfo11":
			posicionInicial = -1510;//posicionInicial = -611;
			posicionFinal = 1510;//posicionFinal = 611;
			break;
		
		case "TextoMenuInfo12":
			posicionInicial = -1640;//posicionInicial = -660;
			posicionFinal = 1640;//posicionFinal = 660;
			break;

		case "TextoMenuInfo13":
			posicionInicial = -1745;//posicionInicial = -700;
			posicionFinal = 1745;//posicionFinal = 700;
			break;
		}
	}

}
