﻿using UnityEngine;
using System.Collections;

public class ArrojarAvion : MonoBehaviour {


	private GameObject munheca;
	private GameObject codo;
	private GameObject hombro;
	
	private GameObject cadera;

	private bool lanzamiento = false;
	private double tiempo;

	private RaycastHit rayo;
	// Use this for initialization
	void Start () {
	
		munheca = GameObject.Find ("12_Wrist_Left");
		codo	= GameObject.Find ("11_Elbow_Left");
		hombro	= GameObject.Find ("10_Shoulder_Left");
		
		cadera = GameObject.Find ("00_Hip_Center");

	}
	
	// Update is called once per frame
	void Update () {
	
		float posicion = -14*Mathf.Cos (munheca.transform.position.z);
		Vector3 destino = new Vector3 (0,posicion,20);

		float distanciaY = Mathf.Abs(codo.transform.position.y - hombro.transform.position.y);
		
		float distanciaX = Mathf.Abs (hombro.transform.position.x - munheca.transform.position.x);


		if(distanciaY > 0 && distanciaY < 0.25f && distanciaX >0 && distanciaX < 0.50f)
		{
			if(munheca.transform.position.z < 1.5f )
			this.gameObject.transform.position = Vector3.Lerp (this.transform.position, destino, Time.deltaTime * 5f);

			float distanciaZ = (munheca.transform.position.z - hombro.transform.position.z);
			
			if(distanciaZ < 0.1f)
			{
				tiempo = Time.time;
				lanzamiento = true;
			}


			if(Physics.Linecast(hombro.transform.position,munheca.transform.position,out rayo) && lanzamiento)
			{
				if(rayo.collider.name == codo.collider.name)
				{
					double dato = Time.time - tiempo;
					
					if(dato < 0.2f && dato > 0)
					{
						double combustible = 0;
						float num =  (float)(dato / 0.45f);
						combustible = 1 - Mathf.Pow(num,3);
						
						
						GestorJuego.Velocidad = 1;
						GestorJuego.Combustible = (float)(combustible);
						
						this.transform.GetComponent<MecanicasAvion>().enabled = true;
						this.transform.GetComponent<LanzamientoAvion>().enabled = false;
					}
					
					lanzamiento = false;
				}
			}



		}
	}
}
