﻿using UnityEngine;
using System.Collections;

public class MvtoAvion : MonoBehaviour {

	#region ATRIBUTOS
	private GameObject hombroDE;
	private GameObject hombroIZ;

	
	private GameObject munhecaDE;
	private GameObject munhecaIZ;
	
	
	private GameObject hombroCEN;
	private GameObject espina;


	private bool brazoDerechoExtendido = false;
	private bool brazoIzquierdoExtendido = false;

	private bool rotando = false;
	private bool lateral = false;
	#endregion




	#region METODOS

	//----------------------------------------<<<<<<<<<<<<<<<<<<<SE DEFINE EL METODO DE INCIALIZACION
	void Start () {

		//Se cargan los recursos necesarios
		hombroDE 	= GameObject.FindGameObjectWithTag ("HombroDerecho");
		
		munhecaDE 	= GameObject.FindGameObjectWithTag ("MunhecaDerecha");
		
		
		hombroIZ 	= GameObject.FindGameObjectWithTag ("HombroIzquierdo");
		
		munhecaIZ	= GameObject.FindGameObjectWithTag ("MunhecaIzquierda");
		
		
		hombroCEN 	= GameObject.FindGameObjectWithTag ("Hombro");
		
		espina		= GameObject.FindGameObjectWithTag ("Espina");


	}
	//----------------------------------------->>>>>>>>>>>>>>>>>TERMINA EL METODO "Start"


	//----------------------------------------<<<<<<<<<<<<<<<<<<<<SE DEFINE EL METODO DE LAZO CERRADO
	void Update () {
	



		brazoDerechoExtendido	=	ExtenderBrazo (hombroDE,munhecaDE,brazoDerechoExtendido);
		brazoIzquierdoExtendido	=	ExtenderBrazo (hombroIZ,munhecaIZ,brazoIzquierdoExtendido);

		float xRHE = 0;//Distancia entre el hombro y la espina ejeX



		if(brazoDerechoExtendido && brazoIzquierdoExtendido && !GestorJuego.Aterrisar)
		{
			float xIHom = 0;//distancia hombro ejeX
			float xFEsp = 0;//distancia espina ejeX
			float xTHE = 0;//Resultante entre el hombro y la espina
			
			
			//Se calcula la distancia entre el hombroCentral y la espina en el ejex
			xIHom = hombroCEN.transform.position.x;
			xFEsp = espina.transform.position.x;
			
			xRHE = xFEsp - xIHom;
			
			xTHE = Mathf.Sqrt(Mathf.Pow(xRHE,2));


			if(xTHE >= 0.06f && !rotando)
			{
				lateral = true;
			}
			else
			{
				xRHE = 0;
				//tiltAroundY = 0;
				lateral = false;
			}



			float xHomDerecho 	= hombroDE.transform.position.x;//posicion en el ejeX
			float xHomIzquierdo = hombroIZ.transform.position.x;//de los hombros del usuario
			
			float zHomDerecho	= hombroDE.transform.position.z;//posicion en el ejeZ
			float zHomIzquierdo	= hombroIZ.transform.position.z;//de los hombros del usuario
			


		

		}//TERMINA CONDICION  brazoDerechoExtendido && brazoIzquierdoExtendido && !GestorJuego.Aterrisar
	
	



		/*if(GestorJuego.Aterrisar || GestorJuego.Combustible <= 0)//Condicion que detecta si el avion aterrisa en el objetivo o se queda sin combustible
		{
			Debug.LogError("Entrada");
		}
		else*/
		{
			//this.transform.Translate (xRHE*1.5f,0,0,Space.World);//movimiento lateral nave
			//this.transform.Rotate(0,0,0.1f,Space.Self);//rotacion en el eje local
		}

		if( hombroIZ.transform.localPosition.z < hombroCEN.transform.localPosition.z)
		{
			float longitud = (hombroIZ.transform.localPosition.z - hombroCEN.transform.localPosition.z);
			
			if(longitud <= -0.05f)
			{
				this.transform.Rotate(0,0,longitud*2,Space.Self);//rotacion en el eje local
			}
			
		}
		
		
		if( hombroDE.transform.localPosition.z < hombroCEN.transform.localPosition.z)
		{
			float longitud = (hombroIZ.transform.localPosition.z - hombroCEN.transform.localPosition.z);
			if(longitud >= 0.06f)
			{
				this.transform.Rotate(0,0,longitud*2,Space.Self);//rotacion en el eje local
			}
			
		}

		this.transform.Translate (0,-0.1f,0,Space.World);//Desplazamiento nave
	
	}
	//---------------------------------------->>>>>>>>>>>>>>>>>>>TERMINA EL METODO "Update"







	//----------------------------------------<<<<<<<<<<<<<<<<<<<SE DEFINE UN METODO ENCARGADO DE DETECTAR SI EL BRAZO SE ENCUENTRA EXTENDIDO
	bool ExtenderBrazo(GameObject origen,GameObject destino,bool rta)
	{

		RaycastHit rayo = new RaycastHit ();
		
		if(Physics.Linecast(origen.transform.position,destino.transform.position, out rayo))
		{
			if(origen.tag == "HombroDerecho" && rayo.collider.tag == "CodoDerecho")
			{
				return true;
			}
			
			if(origen.tag == "HombroIzquierdo" && rayo.collider.tag == "CodoIzquierdo")
			{
				return true;
			}
		}
		
		
		return false;
		
	}
	//---------------------------------------->>>>>>>>>>>>>>>>>TERMINA EL METODO "ExtenderBrazo"

	#endregion

}//FIN DE LA CLASE PRINCIPAL

