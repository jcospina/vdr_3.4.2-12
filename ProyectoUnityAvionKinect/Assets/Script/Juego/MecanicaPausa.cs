﻿using UnityEngine;
using System.Collections;

public class MecanicaPausa : MonoBehaviour {

	private bool brazoDerechoExtendido;
	private bool brazoIzquierdoExtendido;

	public GameObject hombroDE;
	public GameObject hombroIZ;

	public  GameObject munhecaDE;
	public GameObject munhecaIZ;

	void Update()
	{
	
		if(!GestorJuego.JuegoPausado)
		{
		brazoDerechoExtendido	=	ExtenderBrazo (hombroDE,munhecaDE,brazoDerechoExtendido);
		brazoIzquierdoExtendido	=	ExtenderBrazo (hombroIZ,munhecaIZ,brazoIzquierdoExtendido);
		}
	
	}



	void OnCollisionEnter(Collision colision)
	{
		if(colision.gameObject.name.Equals("13_Hand_Left") && !GestorJuego.JuegoPausado && brazoDerechoExtendido )
		{
			Debug.LogError("Pausado");
			GameObject objetoAvion = GameObject.FindGameObjectWithTag("Avion");
			GestorJuego.PosicionAvion = objetoAvion.transform.position;
			GestorJuego.RotacionAvion = objetoAvion.transform.rotation.eulerAngles;

			GestorJuego.JuegoPausado = true;
			Application.LoadLevel("Esc_Pausa");
		}

	}


	bool ExtenderBrazo(GameObject origen,GameObject destino,bool rta)
	{
		RaycastHit rayo = new RaycastHit ();
		
		if(Physics.Linecast(origen.transform.position,destino.transform.position, out rayo))
		{
			if(origen.tag == "HombroDerecho" && rayo.collider.tag == "CodoDerecho")
			{
				return true;
			}
			
			if(origen.tag == "HombroIzquierdo" && rayo.collider.tag == "CodoIzquierdo")
			{
				return true;
			}
		}
		
		
		return false;
		
	}


}
