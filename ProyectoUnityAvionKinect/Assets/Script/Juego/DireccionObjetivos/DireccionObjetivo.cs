﻿using UnityEngine;
using System.Collections;

public class DireccionObjetivo : MonoBehaviour {





	private GameObject objetivo;//objeto al que se apunta con el indicador

	private float smooth = 2.0F;//velocidad de giro

	private float distanciaObjetos;//distancia entre los objetivos

	Color32 color = Color.green;//Color del indicador
	Transform[] obj;

	private RaycastHit rayoAvion;//rayo para detectar que se ha llegado al objetivo
	private Vector3 pos;//Direccion del rayo

	public string tagObjetivo;

	// Use this for initialization
	void Start () {
	

		obj = new Transform[this.transform.GetComponentsInChildren<Transform>().Length]; 
		obj = this.transform.GetComponentsInChildren<Transform> ();

		rayoAvion = new RaycastHit ();
		pos = new Vector3 (0,0,150);

		tagObjetivo = "Objetivo1";
		choque = false;

	}




	public bool choque;

	// Update is called once per frame
	void Update () {


		float pendiente = 0;
		float anguloObjetivo = 0;
		Quaternion target = Quaternion.Euler(0,0,anguloObjetivo);


		//objetivo = GameObject.FindGameObjectWithTag (tagObjetivo);

		objetivo = GameObject.Find (GestorJuego.TagDestino);


		if(choque)
		{
			obj [1].gameObject.renderer.material.color = Color.green;
			choque = false;
		}


		if (objetivo == null) 
		{

			transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);
			obj [1].gameObject.renderer.material.color = Color.green;
			return;
		}
		else
		{
		
			if( (objetivo.transform.position.x - this.transform.position.x) != 0 )
			{
				float numerador 	= objetivo.transform.position.y - this.transform.position.y;
				float denominador	= objetivo.transform.position.x - this.transform.position.x;
				
				pendiente = numerador / denominador;
				
				anguloObjetivo = Mathf.Atan(pendiente);
				
				anguloObjetivo = anguloObjetivo * Mathf.Rad2Deg; 
				
				if(denominador < 0)
				{
					anguloObjetivo = 90 + anguloObjetivo;
				}
				else
				{
					anguloObjetivo = -90 + anguloObjetivo;
				}
				
				distanciaObjetos = Mathf.Abs(denominador);
			}
			
			target = Quaternion.Euler(0,0,anguloObjetivo);



			obj [1].gameObject.renderer.material.color = color;
			
			if (distanciaObjetos <= 255 && distanciaObjetos > 0) 
			{
				color.r = (byte)(255- distanciaObjetos);
				color.g = (byte)(distanciaObjetos);
				color.a = 255;
			}


		}




		transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);








		/*
		if(girar)
		{

		
		transform.Rotate(0,0,0.5f);

		float w = -0.01745f;
		float amplitud = 200;
			float x = amplitud * Mathf.Cos(w*this.transform.localRotation.eulerAngles.y);
			float y = amplitud * Mathf.Sin(w*this.transform.localRotation.eulerAngles.y);

		posicionLinea = new Vector3 (x,y,0);

		}
		if(Physics.Raycast(this.transform.position,posicionLinea,out rayo))
		{
			if(rayo.collider.tag == "Objetivo")
			{
				Debug.LogError("Colision");
				girar = false;
			}
			else 
			{
				 girar = true;
			}
		}
		*/
	}


	void OnDrawGizmos()
	{

		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position,pos);
	}




}
