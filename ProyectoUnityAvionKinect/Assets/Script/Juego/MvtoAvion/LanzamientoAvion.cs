﻿using UnityEngine;
using System.Collections;

public class LanzamientoAvion : MonoBehaviour {


	private GameObject munheca;
	private GameObject codo;
	private GameObject hombro;

	private GameObject cadera;

	private RaycastHit rayo;



	// Use this for initialization
	void Start () {

		munheca = GameObject.Find ("12_Wrist_Left");
		codo	= GameObject.Find ("11_Elbow_Left");
		hombro	= GameObject.Find ("10_Shoulder_Left");

		cadera = GameObject.Find ("00_Hip_Center");

		if (GestorJuego.JuegoPausado) 
		{
			GestorJuego.JuegoPausado = false;

			this.transform.position = GestorJuego.PosicionAvion;

			this.transform.rotation = Quaternion.Euler(GestorJuego.RotacionAvion);

			if(GestorJuego.Puntaje > 0)
			{
			this.transform.GetComponent<MecanicasAvion>().enabled = true;
			this.transform.GetComponent<LanzamientoAvion>().enabled = false;
			}

		}
		else
		{
			this.transform.GetComponent<MecanicasAvion>().enabled = false;
			this.transform.GetComponent<LanzamientoAvion>().enabled = true;
		}


		//Seccion de codigo para mostrar en pantalla y radar el objetivo del avion
		GameObject activarDestinoAvion = GameObject.Find(GestorJuego.TagDestino);
		activarDestinoAvion.transform.FindChild ("Indicador").GetComponent<SpriteRenderer> ().enabled = true;


		this.gameObject.GetComponent<CargarPuntuaciones> ().enabled = true;//Se llama al script de puntuaciones



	}
	
	// Update is called once per frame
	private bool lanzamiento = false;
	private double tiempo;




	void Update () {



		if(!GestorJuego.JuegoPausado)
		{

			float posicion = -14*Mathf.Cos (munheca.transform.position.z);
			
			
			Vector3 destino = new Vector3 (0/*+Camera.main.transform.position.x*/,posicion/*+Camera.main.transform.position.y*/,24);
			
			
			float distanciaY = Mathf.Abs(codo.transform.position.y - hombro.transform.position.y);
			
			float distanciaX = Mathf.Abs (hombro.transform.position.x - munheca.transform.position.x);
			
			
			
			if(distanciaY > 0 && distanciaY < 0.15f && distanciaX >0 && distanciaX < 0.25f)
			{
				
				
				if(munheca.transform.position.z < 3f )
				{
					//Vector3 nuevoVector = Vector3.Lerp (this.transform.position, destino, Time.deltaTime * 7f);
					//this.transform.localPosition = transform.InverseTransformDirection(0,nuevoVector.y,25);
					this.transform.position = Vector3.Lerp (this.transform.position, destino, Time.deltaTime * 7f);
				}
				
				
				
				
				float distanciaZ = (munheca.transform.position.z - hombro.transform.position.z);
				
				
				
				if(distanciaZ < 0.1f)
				{
					tiempo = Time.time;
					lanzamiento = true;
				}
				
				if(Physics.Linecast(hombro.transform.position,munheca.transform.position,out rayo) && lanzamiento)
				{
					if(rayo.collider.name == codo.collider.name)
					{
						double dato = Time.time - tiempo;
						
						if(dato <= 0.3f && dato > 0)
						{
							double combustible = 0;
							float num =  (float)(0.1f/dato);

							combustible = 1.2f + num;//Mathf.Pow(num,6);

							GestorJuego.Velocidad = 1.5f;
							GestorJuego.Combustible = (float)(combustible);
							
							this.transform.GetComponent<MecanicasAvion>().enabled = true;
							this.transform.GetComponent<LanzamientoAvion>().enabled = false;
						}
						
						lanzamiento = false;
					}
				}
				
			}//TERMINA CONDICIOBN IF PARA LA CENTENCIA "distanciaY > 0 && distanciaY < 0.15f && distanciaX >0 && distanciaX < 0.25f"

		}//TERMINA LA CONDICION PARA "!GestorJuego.JuegoPausado" 


		 




	}
	//------------------------------------------->>>>>>>>>>>>>>>>>>TERMINA EL METODO "Update"





}
