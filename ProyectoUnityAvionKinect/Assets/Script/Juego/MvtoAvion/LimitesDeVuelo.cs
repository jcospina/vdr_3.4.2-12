﻿using UnityEngine;
using System.Collections;

public class LimitesDeVuelo : MonoBehaviour {

	void OnCollisionEnter(Collision kolision)
	{
		if (kolision.collider.gameObject.tag.Equals ("Avion")) 
		{
			GameObject objetoChocado = kolision.collider.gameObject;

			if(this.gameObject.name.Equals("LimiteNorte"))
			objetoChocado.transform.rotation = Quaternion.Euler(0,0,180);

			if(this.gameObject.name.Equals("LimiteSur"))
			objetoChocado.transform.rotation = Quaternion.Euler(0,0,0);

			if(this.gameObject.name.Equals("LimiteDerecha"))
			objetoChocado.transform.rotation = Quaternion.Euler(0,0,-90);

			if(this.gameObject.name.Equals("LimiteIzquierda"))
			objetoChocado.transform.rotation = Quaternion.Euler(0,0,90);

		}
	}



}
