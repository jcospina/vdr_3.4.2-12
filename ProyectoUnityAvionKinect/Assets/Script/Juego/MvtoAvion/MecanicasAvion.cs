﻿using UnityEngine;
using System.Collections;

public class MecanicasAvion : MonoBehaviour {

	#region ATRIBUTOS
	private GameObject hombroDE;
	private GameObject hombroIZ;


	private GameObject munhecaDE;
	private GameObject munhecaIZ;


	private GameObject hombroCEN;
	private GameObject espina;


	private GameObject objetoPivotCamera;

	private Transform[] objetosAvion;


	private bool brazoDerechoExtendido = false;
	private bool brazoIzquierdoExtendido = false;
	
	private bool rotando = false;
	private bool lateral = false;


	public Sprite avionCentro;
	public Sprite avionDerecha;
	public Sprite avionIzquierda;
	#endregion

	#region METODOS
	// Use this for initialization
	void Start () {
	
		//tagObjetivo = "Objetivo1";
		//numb = 1;

		//Se cargan los recursos necesarios
		hombroDE 	= GameObject.FindGameObjectWithTag ("HombroDerecho");

		munhecaDE 	= GameObject.FindGameObjectWithTag ("MunhecaDerecha");


		hombroIZ 	= GameObject.FindGameObjectWithTag ("HombroIzquierdo");

		munhecaIZ	= GameObject.FindGameObjectWithTag ("MunhecaIzquierda");


		hombroCEN 	= GameObject.FindGameObjectWithTag ("Hombro");

		espina		= GameObject.FindGameObjectWithTag ("Espina");


		objetosAvion = new Transform[this.gameObject.GetComponentsInChildren<Transform>().Length];
		objetosAvion = this.gameObject.GetComponentsInChildren<Transform> ();


		objetoPivotCamera = GameObject.Find("PivonMainCamera");	


	}//------------------------------------------>>>>>>>>>>>>>>>>>>>TERMINA EL METODO START









	// Update is called once per frame

	private bool instanciar = false;

	void Update () {
	

		if(!GestorJuego.JuegoPausado)
		{

			brazoDerechoExtendido	=	ExtenderBrazo (hombroDE,munhecaDE,brazoDerechoExtendido);
			brazoIzquierdoExtendido	=	ExtenderBrazo (hombroIZ,munhecaIZ,brazoIzquierdoExtendido);
			
			float smooth = 2.0F;
			float tiltAngle = 300.0F;
			float tiltAroundY = 0;
			float tiltAroundZ = 0;

			float xRHE = 0;//Distancia entre el hombro y la espina ejeX


			if(brazoDerechoExtendido && brazoIzquierdoExtendido && !GestorJuego.Aterrisar)
			{
				float xIHom = 0;//distancia hombro ejeX
				float xFEsp = 0;//distancia espina ejeX
				float xTHE = 0;//Resultante entre el hombro y la espina
				
				
				//Se calcula la distancia entre el hombroCentral y la espina en el ejex
				xIHom = hombroCEN.transform.position.x;
				xFEsp = espina.transform.position.x;
				
				xRHE = xFEsp - xIHom;
				
				xTHE = Mathf.Sqrt(Mathf.Pow(xRHE,2));
				
				if(xTHE >= 0.04f && !rotando)
				{
					lateral = true;

					if(xRHE>0)
						this.gameObject.GetComponent<SpriteRenderer>().sprite = avionIzquierda;
					else
						this.gameObject.GetComponent<SpriteRenderer>().sprite = avionDerecha;
				}
				else
				{
					xRHE = 0;
					tiltAroundY = 0;
					lateral = false;
					this.gameObject.GetComponent<SpriteRenderer>().sprite = avionCentro;
				}
				
				
				
				float xHomDerecho 	= hombroDE.transform.position.x;//posicion en el ejeX
				float xHomIzquierdo = hombroIZ.transform.position.x;//de los hombros del usuario
				
				float zHomDerecho	= hombroDE.transform.position.z;//posicion en el ejeZ
				float zHomIzquierdo	= hombroIZ.transform.position.z;//de los hombros del usuario
				
				float pendiente = 0;//pendiente que se forma entre el hombro derecho e izquierdo
				
				if((xHomDerecho - xHomIzquierdo) != 0 && !lateral)
				{
					pendiente = (zHomDerecho - zHomIzquierdo)/(xHomDerecho - xHomIzquierdo);
					float anguloRotacion = Mathf.Atan(pendiente);
					
					if(Mathf.Abs(anguloRotacion*Mathf.Rad2Deg) >= 5 )
					{
						rotando = true;
						tiltAroundZ = anguloRotacion;
					}
					else
					{
						tiltAroundZ = 0;
						rotando = false;
					}
					
					
					
					
				}
				else
				{
					tiltAroundZ = 0;
				}
				
			}//Condicion que detecta si los brazos estan EXTENDIDOS




					
			if (/*GestorJuego.Aterrisar ||*/ GestorJuego.Combustible <= 0) 
			{

				int datoComparado = System.Convert.ToInt32(GestorPuntaje.ArregloKilometrosMaximos[GestorPuntaje.ArregloKilometrosMaximos.Count-1]);

				int dato = System.Convert.ToInt32(GestorJuego.Puntaje * 100);

				if(dato > datoComparado)
				{
					Application.LoadLevel("Esc_IngresarIniciales");
					GestorJuego.Puntaje = dato;
				}
				else
				{
					Debug.LogError("Por aqui");
					Application.LoadLevel("Esc_Pausa");
				}



				this.gameObject.GetComponent<SpriteRenderer>().sprite = avionCentro;

				
				if(GestorJuego.Velocidad >=0)
				{
					GestorJuego.Velocidad -=0.002f;
				}

				
			}//TERMINA CONDICION "GestorJuego.Aterrisar"
			else
			{
				
				this.transform.Translate (-xRHE*10,0,0);//movimiento lateral nave
				objetoPivotCamera.transform.Translate(-xRHE*10,0,0);//movimiento lateral nave
				
				
				this.transform.Rotate (0,0,Time.deltaTime * 15f * (tiltAroundZ));//Rotacion del avion
				
				Quaternion target1 = Quaternion.Euler(270,tiltAngle*xRHE,0);
				objetosAvion[1].transform.localRotation = Quaternion.Slerp(objetosAvion[1].transform.localRotation, target1, Time.deltaTime * smooth);
				
			}




			if (GestorJuego.Velocidad >= 0) {
				
				this.transform.Translate (0, GestorJuego.Velocidad,0 );//Desplazamiento nave
				
				objetoPivotCamera.transform.Translate (0,GestorJuego.Velocidad,0);//desplazamiento camara
				instanciar = false;
				GestorJuego.InicializarMenu = true;
				
				
				
			}//TERMINA CONDICION PARA "GestorJuego.Velocidad >= 0"
			/*else
			{

				if(GestorJuego.Combustible <= 0)
				{
					GestorJuego.JuegoPausado = true;
					GameObject menuPausa = GameObject.Find("CameraMenuPausa");
					
					GameObject camaraRadar = GameObject.Find("CamaraRadar");
					
					GameObject textoPause = GameObject.Find("TextoMenuPausa");
					
					//menuPausa.GetComponent<Camera>().enabled = true;
					menuPausa.GetComponent<Camera>().cullingMask = 256;
					
					
					camaraRadar.GetComponent<Camera>().enabled = false;
					
					menuPausa.GetComponent<IndicadorPantalla>().enabled = true;
					
					textoPause.GetComponent<UILabel>().text = "GAME OVER";
				}

			}*/

			//Colision rayo para los objetivos 
			RaycastHit rayoAvion = new RaycastHit ();
			Vector3 pos = new Vector3 (0,0,150);


			if(Physics.Raycast(this.transform.position,pos,out rayoAvion))
			{

				
				if(rayoAvion.collider.name.Equals(GestorJuego.TagDestino) && !choque)
				{

					GestorJuego.Combustible = GestorJuego.Combustible + 0.6f;

					//Se obtiene la posicion y la rotacion del Avion
					GameObject objetoAvion = GameObject.FindGameObjectWithTag("Avion");
					GestorJuego.PosicionAvion = objetoAvion.transform.position;
					GestorJuego.RotacionAvion = objetoAvion.transform.rotation.eulerAngles;



					choque = true;

					//GestorJuego.TagDestino = "Objetivo"+numb;


					switch (rayoAvion.collider.name)
					{


					case "Objetivo1":
						GestorJuego.LetreroInfo = "TextoMenuInfo1";
						GestorJuego.TagDestino = "Objetivo2";
						break;

					case "Objetivo2":
						GestorJuego.LetreroInfo = "TextoMenuInfo2";
						GestorJuego.TagDestino = "Objetivo3";
						break;

					case "Objetivo3":
						GestorJuego.LetreroInfo = "TextoMenuInfo3";
						GestorJuego.TagDestino = "Objetivo4";
						break;

					case "Objetivo4":
						GestorJuego.LetreroInfo = "TextoMenuInfo4";
						GestorJuego.TagDestino = "Objetivo5";
						break;

					case "Objetivo5":
						GestorJuego.LetreroInfo = "TextoMenuInfo5";
						GestorJuego.TagDestino = "Objetivo6";
						break;

					case "Objetivo6":
						GestorJuego.LetreroInfo = "TextoMenuInfo6";
						GestorJuego.TagDestino = "Objetivo7";
						break;

					case "Objetivo7":
						GestorJuego.LetreroInfo = "TextoMenuInfo7";
						GestorJuego.TagDestino = "Objetivo8";
						break;

					case "Objetivo8":
						GestorJuego.LetreroInfo = "TextoMenuInfo8";
						GestorJuego.TagDestino = "Objetivo9";
						break;

					case "Objetivo9":
						GestorJuego.LetreroInfo = "TextoMenuInfo9";
						GestorJuego.TagDestino = "Objetivo10";
						break;

					case "Objetivo10":
						GestorJuego.LetreroInfo = "TextoMenuInfo10";
						GestorJuego.TagDestino = "Objetivo11";
						break;

					case "Objetivo11":
						GestorJuego.LetreroInfo = "TextoMenuInfo11";
						GestorJuego.TagDestino = "Objetivo12";
						break;

					case "Objetivo12":
						GestorJuego.LetreroInfo = "TextoMenuInfo12";
						GestorJuego.TagDestino = "Objetivo13";
						break;

					case "Objetivo13":
						GestorJuego.LetreroInfo = "TextoMenuInfo13";
						GestorJuego.TagDestino = "Objetivo1";
						break;
					}


					Application.LoadLevel("Esc_TextoObjetivo");
					
				}
				else
				{
					choque = false;
				}
			}





		}//TERMINA CONDICION PARA !GestorJuego.JuegoPausado 




	}//---------------------------------------->>>>>>>>>>>>>>>>>>TERMINA EL METODO UPDATE


	private bool choque = false;
	//string  tagObjetivo;

	//-----------------------------------------<<<<<<<<<<<<<<<<<SE DEFINE EL METODO ENCARGADO DE OBTENER EL ANGULO QUE SE FORMA ENTRE LA CADERA, MUNHECA Y HOMBRO 
	float ObtenerAngulo(GameObject objetoA,GameObject objetoB,GameObject objetoC,float anguloResultante)
	{


		

		float ladoA = 0;
		float ladoB = 0;
		float ladoC = 0;

		//Distancia objetoB <-> objetoC (Lado A)
		float xBC = objetoB.transform.position.x - objetoC.transform.position.x;

		float yBC = objetoB.transform.position.y - objetoC.transform.position.y;

		float zBC = objetoB.transform.position.z - objetoC.transform.position.z;

		ladoA = Mathf.Sqrt (Mathf.Pow(xBC,2) + Mathf.Pow(yBC,2) + Mathf.Pow(zBC,2) );
		
		
		//Distancia objetoC <-> objetoA (Lado B)
		float xCA = objetoC.transform.position.x - objetoA.transform.position.x;

		float yCA = objetoC.transform.position.y - objetoA.transform.position.y;

		float zCA = objetoC.transform.position.z - objetoA.transform.position.z;

		ladoB = Mathf.Sqrt (Mathf.Pow(xCA,2) + Mathf.Pow(yCA,2) + Mathf.Pow(zCA,2));
		
		
		//Distancia objetoA <-> objetoB (Lado C)
		float xAB = objetoA.transform.position.x - objetoB.transform.position.x;

		float yAB = objetoA.transform.position.y - objetoB.transform.position.y;

		float zAB = objetoA.transform.position.z - objetoB.transform.position.z;

		ladoC = Mathf.Sqrt (Mathf.Pow(xAB,2) + Mathf.Pow(yAB,2) + Mathf.Pow(zAB,2));



		if (ladoB > 0 && ladoC > 0) 
		{
			float numerador		= Mathf.Pow(ladoB,2) + Mathf.Pow(ladoC,2) - Mathf.Pow(ladoA,2);
			float denominador 	= 2*ladoB*ladoC;
			anguloResultante = Mathf.Acos(numerador/denominador);
		}
		else
		{
			anguloResultante = 0;
		}
		
		//anguloResultante = Mathf.PI - anguloA1;
		
		anguloResultante = anguloResultante * Mathf.Rad2Deg;


		return anguloResultante;
	}
	//------------------------------------------>>>>>>>>>>>>>>>>TERMINA EL METODO "ObtenerAngulo"





	//------------------------------------------>>>>>>>>>>>>>>>Se define un metodo encargado de detectar si el brazo se encuentra extendido
	bool ExtenderBrazo(GameObject origen,GameObject destino,bool rta)
	{
		RaycastHit rayo = new RaycastHit ();

		if(Physics.Linecast(origen.transform.position,destino.transform.position, out rayo))
		{
			if(origen.tag == "HombroDerecho" && rayo.collider.tag == "CodoDerecho")
			{
				//Debug.LogError("BrazoDerechoExtendido");
				return true;
			}

			if(origen.tag == "HombroIzquierdo" && rayo.collider.tag == "CodoIzquierdo")
			{
				//Debug.LogError("BrazoIzquierdoExtendido");
				return true;
			}
		}


		return false;

	}








	//----------------------------------SECCION DE CODIGO PARA MOSTRAR EN PANTALLA LOS INDICADORES DE COMBUSTIBLE Y DINERO RECOLECTADO
	public Texture2D imagenCombustible;
	private Rect posicionCombustible;

	public Texture2D imagenNumeros;
	public Vector2 cantidadNumeros = Vector2.zero;
	private Vector2 tamanhoSprite;
	private Rect posicionNumeros;


	public Texture2D imagenPuntaje;
	public Vector2 cantidadPuntaje = Vector2.zero;
	private Vector2 tamanhoSpritePuntaje;
	private Rect posicionPuntaje;


	public GUIStyle estiloLetra;
	public GUIStyle estiloLetra2;

	void OnGUI()
	{


		if(!GestorJuego.JuegoPausado && GestorJuego.Combustible > 0)
		{

			tamanhoSprite = new Vector2(1 / cantidadNumeros.x, 1 / cantidadNumeros.y);
			posicionCombustible = new Rect (Screen.width*0.84f,0,Screen.width*0.15f,Screen.height*0.18f);
			GUI.DrawTexture (posicionCombustible, imagenCombustible);

			float combustibleActual = GestorJuego.Combustible * 100;
			combustibleActual = (int)combustibleActual;
			GUI.Label (new Rect(Screen.width*0.86f,Screen.width * 0.025f,Screen.width*0.1f,Screen.height*0.1f),combustibleActual+"",estiloLetra2);

			int espacioRecorrido = System.Convert.ToInt32 (GestorJuego.Puntaje*100);//(decimal)GestorJuego.Puntaje * 100; 
			
			GUI.Label (new Rect(Screen.width*0.44f,0,Screen.width*0.1f,Screen.height*0.1f),espacioRecorrido+"m",estiloLetra);

		}//TERMINA LA CONDICION "!GestorJuego.JuegoPausado"



	}






	//--------------------------------<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<METODO ENCARGADO DE SEPARAR EL VALOR OBTENIDO TIEMPO
	private static int[] arregloCombustible = new int[4]{0,0,0,0};
	private static int[] arregloPuntaje =  new int[4]{0,0,0,0};


	 int[] SepararCifras(int[] vec , int tT)
	{
		
		string temp;
		temp = tT+"";
		int longitud;
		longitud = vec.Length - temp.Length;
		
		
		
		if (longitud>0)
		{
			for (int j = 0; j < longitud; ++j)
			{
				temp = "0"+temp;
			}
			
			
		}
		
		for (int i = 0; i < temp.Length; ++i)
		{
			vec[i] = int.Parse(temp.Substring(i, 1));
		}
		
		
		
		return vec;
	}
	
	//----------------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>TERMINA EL METODO SEPARAR-CIFRAS








	#endregion
}
