﻿using UnityEngine;
using System.Collections;

public class Prueba : MonoBehaviour {


	#region ATRIBUTOS
	private RaycastHit rayoAvion;
	private Vector3 posicionRayo;

	private float alturaFinal = 0;
	private bool colision = false;

	#endregion



	// Use this for initialization
	void Start () {
		rayoAvion = new RaycastHit ();
		posicionRayo = new Vector3 (0,0,100);
	}

	// Update is called once per frame
	private float velocidad = 1;
	void Update () {

		if(Physics.Raycast(this.transform.position,posicionRayo,out rayoAvion))
		{
			if(rayoAvion.collider.tag == "Objetivo1" && !colision )
			{
				alturaFinal 	= rayoAvion.collider.transform.position.z;
				colision = true;
			}
		}//Condicion para detectar la colision del rayo a un objetivo


		if(colision)
		{

			if(alturaFinal*0.98f >= this.transform.position.z)
			{
				this.transform.Translate(0,-0.05f,0);

			}

			if(velocidad >=0)
			{
				velocidad -=0.002f;
			}


		}


		/*float rotar = Input.GetAxis ("Horizontal");

		transform.Rotate(Vector3.up * Time.deltaTime * 10f * rotar);*/
		//transform.Rotate(Vector3.up * Time.deltaTime, Space.World);

		if (velocidad >= 0) {
						this.transform.Translate (new Vector3 (0, 0, velocidad));
						Camera.main.transform.Translate (new Vector3 (0, velocidad, 0));
				}
	}



	void OnDrawGizmos()
	{
		Gizmos.color = Color.yellow;
		Gizmos.DrawRay (this.transform.position, posicionRayo);
	}

}
