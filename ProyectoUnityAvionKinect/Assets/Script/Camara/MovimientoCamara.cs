﻿using UnityEngine;
using System.Collections;

public class MovimientoCamara : MonoBehaviour {

	private GameObject objetivoCamara;


	// Use this for initialization
	void Start () {
		objetivoCamara = GameObject.FindGameObjectWithTag ("Avion");
	}
	
	// Update is called once per frame
	void Update () {
			
		if (!objetivoCamara) return;
		
		float anguloPersonaje = objetivoCamara.transform.eulerAngles.z;

		


		Quaternion target = Quaternion.Euler(0,0,anguloPersonaje);
		transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * 15);

		this.transform.position = new Vector3 (objetivoCamara.transform.position.x,objetivoCamara.transform.position.y,-5);

	}
}
