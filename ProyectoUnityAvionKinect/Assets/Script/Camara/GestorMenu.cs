﻿using UnityEngine;
using System.Collections;

public static class GestorMenu 
{
	#region ATRIBUTOS
	private static bool bt_Aceptar = false;
	private static bool bt_Adelante = false;

	private static string tagBoton;
	private static string botonPresionado;

	private static int posicionInicial = 0;

	#endregion 

	#region PROPIEDADES

	public static bool Bt_Aceptar {
		get {
			return GestorMenu.bt_Aceptar;
		}
		set {
			GestorMenu.bt_Aceptar = value;
		}
	}

	public static bool Bt_Adelante {
		get {
			return GestorMenu.bt_Adelante;
		}
		set {
			GestorMenu.bt_Adelante = value;
		}
	}


	public static string BotonPresionado {
		get {
			return GestorMenu.botonPresionado;
		}
		set {
			GestorMenu.botonPresionado = value;
		}
	}
	
	public static string TagBoton {
		get {
			return GestorMenu.tagBoton;
		}
		set {
			GestorMenu.tagBoton = value;
		}
	}

	public static int PosicionInicial {
		get {
			return GestorMenu.posicionInicial;
		}
		set {
			GestorMenu.posicionInicial = value;
		}
	}
	#endregion


}
