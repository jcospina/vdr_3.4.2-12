﻿using UnityEngine;
using System.Collections;

public class Brujula : MonoBehaviour {

	// Use this for initialization

	private float smooth = 2.0f;


	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	    
		Quaternion target = Quaternion.Euler(0,0,1);
		transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);
	}
}
