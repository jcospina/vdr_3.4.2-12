using UnityEngine;
using System.Collections;

public class MenuPuntoDeLlegada : MonoBehaviour{



	public Texture2D imagenPuntero;
	public GameObject objetoCodoDer;
	public GameObject objetoManoDer;

	public Transform target;//variable encargada de almacenar el objeto que se desea obtener la posicion respecto a la pantalla en la escena

	private Vector3 screenPos;//vector para obtener la posicion en pantalla


	private Vector3 dirRayo;
	private RaycastHit rayo;

	private string[] texto;
	private int mensaje = 0;



	private GameObject[] objetosMenu;

	private bool conteo = false;

	private Transform[] elementosHijo;

	void Inicializar()
	{

		elementosHijo = new Transform[this.gameObject.GetComponentsInChildren<Transform>().Length];
		elementosHijo = this.gameObject.GetComponentsInChildren<Transform> ();

		elementosHijo [1].transform.localPosition = new Vector3(0,0,0);//Se cambia la posicion de la camara para facilitar el movimiento del puntero en el menu


		mensaje = 0;
		conteo = false;

		texto = new string[3];
		texto[0] = "Nikola Tesla , fue un inventor, ingeniero mecanico, ingeniero electricista y fisico de origen serbio y el promotor mas importante del nacimiento de la electricidad comercial.";
		texto[1] = "Se le conoce, sobre todo, por sus numerosas y revolucionarias invenciones en el campo del electromagnetismo, desarrolladas a finales del siglo XIX y principios del siglo XX. ";
		texto[2] = "Las patentes de Tesla y su trabajo teorico formaron las bases de los sistemas modernos de potencia electrica por corriente alterna (CA)";	




		objetosMenu = new GameObject[4];

		objetosMenu [0] = GameObject.FindGameObjectWithTag ("B_Aceptar");//Se busca el objeto aceptar
		objetosMenu [1] = GameObject.FindGameObjectWithTag ("B_Atras");//Se busca el objeto atras
		objetosMenu [2] = GameObject.FindGameObjectWithTag ("B_Adelante");//Se busca el objeto adelante
		objetosMenu [3] = GameObject.FindGameObjectWithTag ("TextoInfo");//Se busca el objeto adelante


		objetosMenu[3].GetComponent<UILabel>().text = texto[0];
		rayo = new RaycastHit ();
		dirRayo = new Vector3 (0,0,100);
	}







	void OnGUI(){



		if(GestorJuego.InicializarMenu)
		{

			Inicializar();
			GestorJuego.InicializarMenu = false;
		}



		bool choque = false;
		if(Physics.Raycast(target.transform.position,dirRayo,out rayo))
		{


			if(rayo.collider.tag == "B_Aceptar" && mensaje == objetosMenu.Length -2 )
			{

				choque = true;
				if(tiempo == 0)
				tiempo = Time.time;

				objetosMenu[0].GetComponent<UIButton1>().activarBoton = true;
				bool rta = false;
				//rta = PresionarBoton(rta);

				//if(rta)
				{
					double tActual;
					tActual = Time.time - tiempo;


				//	GUI.Label (new Rect (Screen.width * 0.5f, 0, Screen.width * 0.2f, Screen.height * 0.2f), ("TiempoAceptar:" +(float) tActual),estilo);
				//	print ("Tiempo Aceptar:"+tActual);

					if(tActual >= 1.15f )
					{
						GameObject xOb = GameObject.FindGameObjectWithTag("MenuLlegada");
						Destroy (xOb);
						
						GameObject avion = GameObject.FindGameObjectWithTag("Avion");
						//avion.transform.position = new Vector3(avion.transform.position.x,avion.transform.position.y,25);
						//Camera.main.transform.position = new Vector3(avion.transform.position.x,avion.transform.position.y,-5);
						//Camera.main.transform.rotation = Quaternion.Euler(GestorJuego.AnguloCamara);


						elementosHijo [1].transform.localPosition = new Vector3(0,16,0);

						avion.transform.rotation = Quaternion.Euler (0,0,0);
						avion.transform.position = new Vector3(0,0,25);



						//Camera.main.transform.position = new Vector3(avion.transform.position.x,avion.transform.position.y,-5);
						//Camera.main.transform.rotation = Quaternion.Euler(Vector3.zero);

						GameObject esqueleto = GameObject.Find("KinectPointMan");
						esqueleto.transform.position = new Vector3(0,0,-5);
						esqueleto.transform.localScale = Vector3.one;



						GestorJuego.Velocidad = 1;
						GestorJuego.Aterrisar = false;
										
						avion.gameObject.GetComponent<LanzamientoAvion>().enabled = true;	
						this.gameObject.GetComponent<MovimientoCamara>().enabled = true;
						this.gameObject.GetComponent<MenuPuntoDeLlegada>().enabled = false;//Se desactiva el script
						tiempo = 0;
						return;
					}

				}

			}
			else
			{
				objetosMenu[0].GetComponent<UIButton1>().activarBoton = false;
			}


			if(rayo.collider.tag == "B_Atras")
			{
				choque = true;
				if(tiempo == 0)
				tiempo = Time.time;


				objetosMenu[1].GetComponent<UIButton1>().activarBoton = true;


				double tActual;
				tActual = Time.time - tiempo;


					//GUI.Label (new Rect (Screen.width * 0.5f, 0, Screen.width * 0.2f, Screen.height * 0.2f), ("TiempoAtras:" +(float) tActual),estilo);
					//print ("Tiempo Atras:"+tActual);
					

					if(tActual >=1.15f)
					{
						if(mensaje != 0 && !conteo)
							mensaje -= 1;
						else
							mensaje = 0;
						
						objetosMenu[3].GetComponent<UILabel>().text = texto[mensaje];

					tiempo = 0;
					}




			
			}
			else
			{
				objetosMenu[1].GetComponent<UIButton1>().activarBoton = false;
			}


			if(rayo.collider.tag == "B_Adelante")
			{
				choque = true;
				if(tiempo == 0)
				tiempo = Time.time;


				objetosMenu[2].GetComponent<UIButton1>().activarBoton = true;

					
					

					double tActual;
					tActual = Time.time - tiempo;


						if(tActual >=1.15f)
						{
							if(mensaje < texto.Length - 1 && !conteo)
								mensaje += 1;
							else
								mensaje = texto.Length - 1;
							
							
							objetosMenu[3].GetComponent<UILabel>().text = texto[mensaje];
							tiempo = 0;
						}


					
					
			}
			else
			{
					objetosMenu[2].GetComponent<UIButton1>().activarBoton = false;
			}


			if(!choque)//Condicion para reiniciar el cronometro
			tiempo = 0;


		}
		else
		{
			conteo = false;
		}

	
		screenPos = elementosHijo [1].camera.WorldToScreenPoint (target.position);



		GUI.DrawTexture(new Rect(screenPos.x - Screen.width*0.07f,-screenPos.y + Screen.height*0.94f,Screen.width*0.11f,Screen.height*0.11f),imagenPuntero);



	}



	/*bool PresionarBoton(bool rta)
	{

		RaycastHit rayoMuMa = new RaycastHit();
		
		float ladoA 	= 0;
		float ladoB 	= 0;
		float ladoC 	= 0;
		float angulo	= 0;
		
		Physics.Linecast(objetoCodoDer.transform.position,target.transform.position,out rayoMuMa);
		ladoA = rayoMuMa.distance;				
		Physics.Linecast(target.transform.position,objetoManoDer.transform.position,out rayoMuMa);
		ladoB = rayoMuMa.distance;
		Physics.Linecast(objetoManoDer.transform.position,objetoCodoDer.transform.position,out rayoMuMa);
		ladoC = rayoMuMa.distance;
		
		if(ladoA != 0 && ladoB != 0)
		{
			float numerador 	= Mathf.Pow(ladoA,2)+Mathf.Pow(ladoB,2)-Mathf.Pow(ladoC,2);
			float denomimador	= 2*ladoA*ladoB;
			angulo = Mathf.Acos(numerador/denomimador);
			angulo = angulo * Mathf.Rad2Deg;
		}





		if (angulo >= 75 && angulo <= 80) 
		{

			conteo = true;	
			return true;	
		}

		return false;
	}
*/

	private double tiempo = 0;




}