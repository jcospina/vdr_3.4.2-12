﻿using UnityEngine;
using System.Collections;

public class CargarTexto : MonoBehaviour {

	// Use this for initialization
	void Start () {

		GestorJuego.JuegoPausado = true;

		GameObject habilitarTexto = GameObject.Find (GestorJuego.LetreroInfo);
		habilitarTexto.GetComponent<MostrarTextoInformativo>().enabled = true;
		habilitarTexto.GetComponent<UILabel> ().enabled = true;
	}
	

}
