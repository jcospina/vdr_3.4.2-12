function mezclar(respuestas,numeros){
	   var m= respuestas.length-1;
	   
	   for (var i=m;i>1;i--){ 
	      alea=Math.floor(i*Math.random()); 
	      temp=respuestas[i];
	      temp2=numeros[i];
	      respuestas[i]=respuestas[alea];
	      numeros[i]=numeros[alea];
	      respuestas[alea]=temp;
	      numeros[alea]=temp2;
	      
	    }
	  
}

function Pregunta() {
           this.pregunta = '';
           this.respuestas = []; 
           this.respuestaCorrecta = [];
}


preguntasManejoBasuraRural=[];

var objPregunta1 = new Pregunta();
objPregunta1.pregunta='Aunque las peque\u00f1as comunidades no generan muchos desechos:';
objPregunta1.respuestas[0]='Sus efectos son ben\xe9ficos al ser humano.';
objPregunta1.respuestas[1]='Sus efectos pueden causar da\u00f1o a las personas y el medio ambiente.';
objPregunta1.respuestas[2]='Las peque\u00f1as comunidades no generan residuos.';
objPregunta1.respuestas[3]='Las peque\u00f1as comunidades generan m\xe1s residuos que en las ciudades.';
objPregunta1.respuestaCorrecta=[0,1,0,0];
mezclar (objPregunta1.respuestas,objPregunta1.respuestaCorrecta);
preguntasManejoBasuraRural[preguntasManejoBasuraRural.length] = objPregunta1;

var objPregunta2 = new Pregunta();
objPregunta2.pregunta='\xbfPor qu\xe9 es necesario desarrollar un manejo adecuado de los residuos en el campo?';
objPregunta2.respuestas[0]='Permite disminuir el impacto ambiental y el da\u00f1o a la salud humana.';
objPregunta2.respuestas[1]='Porque permite contralar la cantidad de basura generada.';
objPregunta2.respuestas[2]='El campo no necesita de un plan de manejo de desechos.';
objPregunta2.respuestas[3]='Porque la ciudad deja sus desechos en el campo.';
objPregunta2.respuestaCorrecta=[1,0,0,0];
mezclar (objPregunta2.respuestas,objPregunta2.respuestaCorrecta);
preguntasManejoBasuraRural[preguntasManejoBasuraRural.length] = objPregunta2;

var objPregunta3 = new Pregunta();
objPregunta3.pregunta='Para que las comunidades hagan un buen manejo de los residuos:';
objPregunta3.respuestas[0]='Es necesario invertir en la salud p\xfablica.';
objPregunta3.respuestas[1]='Deben acceder a la educaci\xf3n superior.';
objPregunta3.respuestas[2]='Es necesario invertir recursos, impartiendo a las comunidades, los conocimientos necesarios sobre el manejo de los residuos.';
objPregunta3.respuestas[3]='';
objPregunta3.respuestaCorrecta=[0,0,1,0];
mezclar (objPregunta3.respuestas,objPregunta3.respuestaCorrecta);
preguntasManejoBasuraRural[preguntasManejoBasuraRural.length] = objPregunta3;

var objPregunta4 = new Pregunta();
objPregunta4.pregunta='un residuo es:';
objPregunta4.respuestas[0]='Lo que sobra en las f\xe1bricas.';
objPregunta4.respuestas[1]='Aquello que no contamina ni causa da\u00f1o al medio ambiente.';
objPregunta4.respuestas[2]='Es cualquier objeto, material, sustancia o elemento resultante del consumo o uso de un bien.';
objPregunta4.respuestas[3]='Es un alimento.';
objPregunta4.respuestaCorrecta=[0,0,1,0];
mezclar (objPregunta4.respuestas,objPregunta4.respuestaCorrecta);
preguntasManejoBasuraRural[preguntasManejoBasuraRural.length] = objPregunta4;


var objPregunta5 = new Pregunta();
objPregunta5.pregunta='La composici\xf3n de los desechos est\xe1 ligada a:';
objPregunta5.respuestas[0]='Lo que consumen las f\xe1bricas.';
objPregunta5.respuestas[1]='Lo que se encuentra en el medio ambiente.';
objPregunta5.respuestas[2]='El factor clim\xe1tico.';
objPregunta5.respuestas[3]='El desarrollo tecnol\xf3gico y las relaciones sociales.';
objPregunta5.respuestaCorrecta=[0,0,0,1];
mezclar (objPregunta5.respuestas,objPregunta5.respuestaCorrecta);
preguntasManejoBasuraRural[preguntasManejoBasuraRural.length] = objPregunta5;

var objPregunta6 = new Pregunta();
objPregunta6.pregunta='En el texto "\xbfQu\xe9 es un residuo s\xf3lido?" se afirma que:';
objPregunta6.respuestas[0]='La acumulaci\xf3n de residuos constituye un problema que crece debido al consumismo.';
objPregunta6.respuestas[1]='Los residuos no tienen un valor que pueda producir beneficios.';
objPregunta6.respuestas[2]='Los residuos deben permanecer en el mismo lugar que los genera.';
objPregunta6.respuestas[3]='Los procesos de urbanizaci\xf3n ayudan a disminuir la producci\xf3n de desechos.';
objPregunta6.respuestaCorrecta=[1,0,0,0];
mezclar (objPregunta6.respuestas,objPregunta6.respuestaCorrecta);
preguntasManejoBasuraRural[preguntasManejoBasuraRural.length] = objPregunta6;

var objPregunta7 = new Pregunta();
objPregunta7.pregunta='las tres formas de clasificar los residuos son:';
objPregunta7.respuestas[0]='Su origen, su composici\xf3n y el tipo de manejo.';
objPregunta7.respuestas[1]='Su procedencia, forma y peso.';
objPregunta7.respuestas[2]='Olor, tama\u00f1o y composici\xf3n.';
objPregunta7.respuestas[3]='Su origen, tipo de manejo y color.';
objPregunta7.respuestaCorrecta=[1,0,0,0];
mezclar (objPregunta7.respuestas,objPregunta7.respuestaCorrecta);
preguntasManejoBasuraRural[preguntasManejoBasuraRural.length] = objPregunta7;


var objPregunta8 = new Pregunta();
objPregunta8.pregunta='Al decir: Todo desecho de origen biol\xf3gico, que alguna vez estuvo vivo o fue parte de un ser vivo. Nos referimos a:';
objPregunta8.respuestas[0]='Desechos industriales.';
objPregunta8.respuestas[1]='Composta.';
objPregunta8.respuestas[2]='Desechos org\xe1nicos.';
objPregunta8.respuestas[3]='Partes de un botadero de basura.';
objPregunta8.respuestaCorrecta=[0,0,1,0];
mezclar (objPregunta8.respuestas,objPregunta8.respuestaCorrecta);
preguntasManejoBasuraRural[preguntasManejoBasuraRural.length] = objPregunta8;

var objPregunta9 = new Pregunta();
objPregunta9.pregunta='Residuos t\xf3xicos, inertes y pat\xf3geno, son nombres de la clasificaci\xf3n:';
objPregunta9.respuestas[0]='De residuos inorg\xe1nicos.';
objPregunta9.respuestas[1]='Por tipo de manejo.';
objPregunta9.respuestas[2]='Por composici\xf3n.';
objPregunta9.respuestas[3]='De sustancias s\xf3lidas.';
objPregunta9.respuestaCorrecta=[0,1,0,0];
mezclar (objPregunta9.respuestas,objPregunta9.respuestaCorrecta);
preguntasManejoBasuraRural[preguntasManejoBasuraRural.length] = objPregunta9;

var objPregunta10 = new Pregunta();
objPregunta10.pregunta='la falta de conocimiento de los componentes de los residuos y su posterior tratamiento:';
objPregunta10.respuestas[0]='Ayuda a evitar cambios ambientales.';
objPregunta10.respuestas[1]='Permite saber clasificarla.';
objPregunta10.respuestas[2]='Causa pocos da\u00f1os ambientales.';
objPregunta10.respuestas[3]='Nos permite seleccionar el mejor sistema de tratamiento y disminuir impactos ambientales.';
objPregunta10.respuestaCorrecta=[0,0,0,1];
mezclar (objPregunta10.respuestas,objPregunta10.respuestaCorrecta);
preguntasManejoBasuraRural[preguntasManejoBasuraRural.length] = objPregunta10;


preguntasMalManejoResiduosSolidos=[];

var objPregunta11 = new Pregunta();
objPregunta11.pregunta='Cu\xe1les son las causas ligadas al mal manejo de los residuos s\xf3lidos:';
objPregunta11.respuestas[0]='Desconocimiento de los beneficios y las t\xe9cnicas del uso adecuado de los desperdicios.';
objPregunta11.respuestas[1]='Falta de recursos y espacios para manejar los desperdicios.';
objPregunta11.respuestas[2]='El inexistente compromiso con el medio ambiente.';
objPregunta11.respuestas[3]='El amplio conocimiento que tiene las comunidades sobre c\xf3mo generar desperdicios.';
objPregunta11.respuestaCorrecta=[1,0,0,0];
mezclar (objPregunta11.respuestas,objPregunta11.respuestaCorrecta);
preguntasMalManejoResiduosSolidos[preguntasMalManejoResiduosSolidos.length] = objPregunta11;


var objPregunta12 = new Pregunta();
objPregunta12.pregunta='Debido a la falta de conocimiento sobre el manejo de los desperdicios, los especialistas dicen que:';
objPregunta12.respuestas[0]='La comunidad ha bajado la producci\xf3n de residuos s\xf3lidos.';
objPregunta12.respuestas[1]='No hay manera de bajar los \xcdndices de desperdicios.';
objPregunta12.respuestas[2]='Es m\xe1s econ\xf3mico botar que reutilizar los desperdicios.';
objPregunta12.respuestas[3]='Se produce m\xe1s basura en los campos que en la ciudad.';
objPregunta12.respuestaCorrecta=[0,0,1,0];
mezclar (objPregunta12.respuestas,objPregunta12.respuestaCorrecta);
preguntasMalManejoResiduosSolidos[preguntasMalManejoResiduosSolidos.length] = objPregunta12;


var objPregunta13 = new Pregunta();
objPregunta13.pregunta='Del texto "Consecuencias del mal manejo de residuos solidos" podemos concluir que:';
objPregunta13.respuestas[0]='Sin importar como se manejen los desechos, estos no causan da\u00f1o al ambiente ni a los seres vivos.';
objPregunta13.respuestas[1]='Los productos qu\xcdmicos en el ambiente deterioran los suelos, haci\xe9ndolos menos productivos.';
objPregunta13.respuestas[2]='No existe un manejo adecuado de residuos para evitar da\u00f1os a la salud humana.';
objPregunta13.respuestas[3]='Adem\xe1s de los impactos ambientales y de salubridad, el mal manejo de residuos hace que se reduzcan la cantidad de tierras productivas.';
objPregunta13.respuestaCorrecta=[0,0,0,1];
mezclar (objPregunta13.respuestas,objPregunta13.respuestaCorrecta);
preguntasMalManejoResiduosSolidos[preguntasMalManejoResiduosSolidos.length] = objPregunta13;



var objPregunta14 = new Pregunta();
objPregunta14.pregunta='La contaminaci\xf3n antropog\xe9nica es la causada por:';
objPregunta14.respuestas[0]='Factores naturales como volcanes e inundaciones.';
objPregunta14.respuestas[1]='El paso del ser humano por un territorio o ecosistema.';
objPregunta14.respuestas[2]='La falta de cuidado a los animales.';
objPregunta14.respuestas[3]='Bacterias y virus presentes en el medioambiente.';
objPregunta14.respuestaCorrecta=[0,1,0,0];
mezclar (objPregunta14.respuestas,objPregunta14.respuestaCorrecta);
preguntasMalManejoResiduosSolidos[preguntasMalManejoResiduosSolidos.length] = objPregunta14;


var objPregunta15 = new Pregunta();
objPregunta15.pregunta='Al decir que, transformamos materiales residuales en objetos nuevos que pueden servir a la comunidad y disminuyen la generaci\xf3n de residuos contaminantes, estamos diciendo que:';
objPregunta15.respuestas[0]='Estamos desperdiciando muchos productos innecesariamente.';
objPregunta15.respuestas[1]='Estamos poniendo en pr\xe1ctica la actividad del reciclaje, ayudando al medioambiente.';
objPregunta15.respuestas[2]='Estamos logrando un cambio en el sistema econ\xf3mico consumista.';
objPregunta15.respuestas[3]='El planeta est\xe1 perdiendo recursos r\xe1pidamente y no podemos malgastar.';
objPregunta15.respuestaCorrecta=[0,0,1,0];
mezclar (objPregunta15.respuestas,objPregunta15.respuestaCorrecta);
preguntasMalManejoResiduosSolidos[preguntasMalManejoResiduosSolidos.length] = objPregunta15;


var objPregunta16 = new Pregunta();
objPregunta16.pregunta='La selecci\xf3n domiciliaria de los desechos antes de ser llevados al botadero p\xfablico es necesaria porque:';
objPregunta16.respuestas[0]='Reduce el tiempo de descomposici\xf3n, permitiendo mayor aprovechamiento energ\xe9tico.';
objPregunta16.respuestas[1]='Ayuda a evitar gastos de selecci\xf3n, limpieza y lavado de los subproductos, obteniendo una mejor calidad de materia recuperada con residuos limpios y clasificados.';
objPregunta16.respuestas[2]='Disminuye el volumen de los desperdicios, reduciendo los costos de tratamiento y reutilizaci\xf3n.';
objPregunta16.respuestas[3]='Genera mejores ingresos para los recolectores, permiti\xe9ndoles un mayor aprovechamiento del tiempo de trabajo.';
objPregunta16.respuestaCorrecta=[0,1,0,0];
mezclar (objPregunta16.respuestas,objPregunta16.respuestaCorrecta);
preguntasMalManejoResiduosSolidos[preguntasMalManejoResiduosSolidos.length] = objPregunta16;


var objPregunta17 = new Pregunta();
objPregunta17.pregunta='Cu\xe1les son los pasos que componen el ciclo de reciclaje:';
objPregunta17.respuestas[0]='Separado, incineraci\xf3n y compra de productos reciclados.';
objPregunta17.respuestas[1]='Lavado, procesado e incinerado de s\xf3lidos.';
objPregunta17.respuestas[2]='Reciclado y separaci\xf3n, transformaci\xf3n y fabricaci\xf3n, compra y venta de los productos resultantes.';
objPregunta17.respuestas[3]='Separado de los s\xf3lidos, distribuci\xf3n de los nuevos productos y reciclado de los productos viejos.';
objPregunta17.respuestaCorrecta=[0,0,1,0];
mezclar (objPregunta17.respuestas,objPregunta17.respuestaCorrecta);
preguntasMalManejoResiduosSolidos[preguntasMalManejoResiduosSolidos.length] = objPregunta17;


var objPregunta18 = new Pregunta();
objPregunta18.pregunta='En que consiste el m\xe9todo de las 3R:';
objPregunta18.respuestas[0]='Reutilizar, aprovechar la m\xe1xima utilidad a los objetos sin la necesidad de destruirlos; Reducir, evitando todo aquello que de una u otra forma genera un desperdicio innecesario; Reciclar, utilizar los mismos materiales una y otra vez, reintegr\xe1ndolos a otro proceso natural o industrial para hacer del mismo o un nuevo producto.';
objPregunta18.respuestas[1]='Reutilizar, aprovechar la m\xe1xima utilidad a los objetos sin la necesidad de destruirlos; Reducir, evitando todo aquello que de una u otra forma genera un desperdicio innecesario; Reinventar, hacer nuevos productos de los desechos.';
objPregunta18.respuestas[2]='Reducir, evitando todo aquello que de una u otra forma genera un desperdicio innecesario; Reinventar, hacer nuevos productos de los desechos; Reconstruir, remplazando las partes usadas de los productos.';
objPregunta18.respuestas[3]='Redise\u00f1ar, dando un nuevo uso al objeto antes de desecharlo por completo; Reducir, evitando todo aquello que de una u otra forma genera un desperdicio innecesario; Reciclar, utilizando los mismos materiales una y otra vez, reintegr\xe1ndolos a un proceso natural o industrial para hacer del mismo un nuevo producto.';
objPregunta18.respuestaCorrecta=[1,0,0,0];
mezclar (objPregunta18.respuestas,objPregunta18.respuestaCorrecta);
preguntasMalManejoResiduosSolidos[preguntasMalManejoResiduosSolidos.length] = objPregunta18;


var objPregunta19 = new Pregunta();
objPregunta19.pregunta='Cada comunidad debe presentar un programa reciclaje que incluya las siguientes evaluaciones:';
objPregunta19.respuestas[0]='Costos de manejo y de procesamiento, as\xcd como ganancias finales.';
objPregunta19.respuestas[1]='La calidad del material, la infraestructura de recolecci\xf3n, el mercado de recuperaci\xf3n y los costos totales.';
objPregunta19.respuestas[2]='Las caracter\xcdsticas y cantidades de los subproductos que componen los desechos s\xf3lidos de la comunidad.';
objPregunta19.respuestas[3]='Los procesos y costos de la transformaci\xf3n de los residuos.';
objPregunta19.respuestaCorrecta=[1,0,0,0];
mezclar (objPregunta19.respuestas,objPregunta19.respuestaCorrecta);
preguntasMalManejoResiduosSolidos[preguntasMalManejoResiduosSolidos.length] = objPregunta19;

var objPregunta20 = new Pregunta();
objPregunta20.pregunta='Cu\xe1l es la clasificaci\xf3n de los residuos aprovechables que toda comunidad debe conocer para un adecuado m\xe9todo de reciclaje, que disminuya los impactos ambientales y genere beneficios productivos:';
objPregunta20.respuestas[0]='Rechazados, nocivos, recuperables.';
objPregunta20.respuestas[1]='Transformables, inertes, nocivos y aprovechables.';
objPregunta20.respuestas[2]='Recuperables, no recuperables nocivos, no recuperables inertes, transformables.';
objPregunta20.respuestas[3]='Transformables, peligrosos, no recuperables y no reciclables.';
objPregunta20.respuestaCorrecta=[0,0,1,0];
mezclar (objPregunta20.respuestas,objPregunta20.respuestaCorrecta);
preguntasMalManejoResiduosSolidos[preguntasMalManejoResiduosSolidos.length] = objPregunta20;



preguntasRellenoSanitario=[];

var objPregunta21 = new Pregunta();
objPregunta21.pregunta='Un relleno sanitario es:';
objPregunta21.respuestas[0]='Una t\xe9cnica de mal manejo de los residuos o basuras.';
objPregunta21.respuestas[1]='Una t\xe9cnica de disposici\xf3n final de los residuos en el suelo que no causa peligro para la salud o la seguridad p\xfablica ni ambiental.';
objPregunta21.respuestas[2]='Un lugar donde se clasifican las basuras de las comunidades para su respectiva reutilizaci\xf3n y aprovechamiento.';
objPregunta21.respuestas[3]='Un m\xe9todo poco aconsejable debido a la acumulaci\xf3n de basura.';
objPregunta21.respuestaCorrecta=[0,1,0,0];
mezclar (objPregunta21.respuestas,objPregunta21.respuestaCorrecta);
preguntasRellenoSanitario[preguntasRellenoSanitario.length] = objPregunta21;


var objPregunta22 = new Pregunta();
objPregunta22.pregunta='Para la construcci\xf3n de un relleno sanitario debe tenerse en cuenta una serie de reglas y condiciones, esas son:';
objPregunta22.respuestas[0]='Estar dentro de las ciudades o poblaciones.';
objPregunta22.respuestas[1]='Estar ubicados lejos de las zonas h\xfamedas de la regi\xf3n.';
objPregunta22.respuestas[2]='Un terreno amplio, adecuado para tratar la acumulacion de gases y liquidos lixiviados.';
objPregunta22.respuestas[3]='Ser construidos en zonas altas que no contaminen el espacio p\xfablico.';
objPregunta22.respuestaCorrecta=[0,0,1,0];
mezclar (objPregunta22.respuestas,objPregunta22.respuestaCorrecta);
preguntasRellenoSanitario[preguntasRellenoSanitario.length] = objPregunta22;

var objPregunta23 = new Pregunta();
objPregunta23.pregunta='Aunque se cumplan todas las normas para la construcci\xf3n de un relleno sanitario, lo m\xe1s importante a tener en cuenta a la hora iniciar es:';
objPregunta23.respuestas[0]='La opini\xf3n p\xfablica para saber si se est\xe1 o no satisfaciendo las necesidades de la comunidad, es decir, la consulta previa.';
objPregunta23.respuestas[1]='El manejo de aguas residuales, ya que estos pueden deteriorar el ambiente.';
objPregunta23.respuestas[2]='Los costos de construcci\xf3n del relleno y los sistemas de procesamiento de los residuos.';
objPregunta23.respuestas[3]='No es necesario que cumpla las normas de construcci\xf3n si se tiene en cuenta la opini\xf3n p\xfablica.';
objPregunta23.respuestaCorrecta=[1,0,0,0];
mezclar (objPregunta23.respuestas,objPregunta23.respuestaCorrecta);
preguntasRellenoSanitario[preguntasRellenoSanitario.length] = objPregunta23;


var objPregunta24 = new Pregunta();
objPregunta24.pregunta='Cu\xe1les son los factores a determinar a la hora de construir un relleno sanitario:';
objPregunta24.respuestas[0]='La altura del terreno y el costo del predio.';
objPregunta24.respuestas[1]='La calidad del terreno al cumplir el ciclo de vida del relleno.';
objPregunta24.respuestas[2]='El tama\u00f1o del terreno y la ubicaci\xf3n del mismo, la direcci\xf3n del viento y el uso futuro del terreno.';
objPregunta24.respuestas[3]='Su cercan\xcda a los centros urbanos o rurales y la cantidad de lluvias al a\u00f1o.';
objPregunta24.respuestaCorrecta=[0,0,1,0];
mezclar (objPregunta24.respuestas,objPregunta24.respuestaCorrecta);
preguntasRellenoSanitario[preguntasRellenoSanitario.length] = objPregunta24;



var objPregunta25 = new Pregunta();
objPregunta25.pregunta='Qu\xe9 sucede con el relleno sanitario al terminar su vida \xfatil:';
objPregunta25.respuestas[0]='Debe ser tratado con qu\xcdmicos especiales para devolverle su productividad.';
objPregunta25.respuestas[1]='Debe ser integrado al ambiente natural, transform\xe1ndolo en una zona verde, \xe1rea deportiva, jard\xcdn, vivero o en un bosque.';
objPregunta25.respuestas[2]='Debe ser transformado en zona de cultivo.';
objPregunta25.respuestas[3]='Debe dejarse deshabitado debido a los contaminantes.';
objPregunta25.respuestaCorrecta=[0,1,0,0];
mezclar (objPregunta25.respuestas,objPregunta25.respuestaCorrecta);
preguntasRellenoSanitario[preguntasRellenoSanitario.length] = objPregunta25;

/*faltan 5 preguntas del 3 tema*/

preguntasManejoResiduosSolidosZonaRural=[];

var objPregunta31 = new Pregunta();
objPregunta31.pregunta='Seg\xfan el texto, la causa principal de la falta de manejo adecuado de los desechos en las zonas rurales es:';
objPregunta31.respuestas[0]='La generaci\xf3n de demasiados desechos.';
objPregunta31.respuestas[1]='Son tan pocos los desechos que no necesitan ser tratados.';
objPregunta31.respuestas[2]='Los sobrecostos que demanda el tratamiento.';
objPregunta31.respuestas[3]='La falta de capacitaci\xf3n de los pobladores.';
objPregunta31.respuestaCorrecta=[0,0,0,1];
mezclar (objPregunta31.respuestas,objPregunta31.respuestaCorrecta);
preguntasManejoResiduosSolidosZonaRural[preguntasManejoResiduosSolidosZonaRural.length] = objPregunta31;

var objPregunta32 = new Pregunta();
objPregunta32.pregunta='Del texto podemos inferir que, debido a la falta de manejo de los residuos en las zonas rurales, los pobladores deben enfrentar los problemas que esto presenta:';
objPregunta32.respuestas[0]='da\u00f1o a la salud de las personas y a la naturaleza, deterioro de las propiedades f\xcdsico-qu\xcdmicas del agua consumida cotidianamente.';
objPregunta32.respuestas[1]='Perdida de terrenos productivos y escases de alimentos.';
objPregunta32.respuestas[2]='Frecuentes enfermedades provenientes de la contaminaci\xf3n de las basuras.';
objPregunta32.respuestas[3]='Da\u00f1o en los equipos de agricultura y en los pocos productos cosechados.';
objPregunta32.respuestaCorrecta=[1,0,0,0];
mezclar (objPregunta32.respuestas,objPregunta32.respuestaCorrecta);
preguntasManejoResiduosSolidosZonaRural[preguntasManejoResiduosSolidosZonaRural.length] = objPregunta32;

var objPregunta33 = new Pregunta();
objPregunta33.pregunta='Por qu\xe9 en el texto se dice que es necesario separar los residuos de esta forma: residuos putrescibles, residuos no putrescibles y residuos t\xf3xicos.';
objPregunta33.respuestas[0]='Por la cantidad desmesurada de productos consumidos y desechados.';
objPregunta33.respuestas[1]='Por el poco espacio que se maneja en los rellenos de las zonas rurales.';
objPregunta33.respuestas[2]='Porque en el campo, el uso de insecticidas y qu\xcdmicos es muy constante y estos residuos pueden afectar la vida de los habitantes.';
objPregunta33.respuestas[3]='Da\u00f1o en los equipos de agricultura y en los pocos productos cosechados.';
objPregunta33.respuestaCorrecta=[0,0,1,0];
mezclar (objPregunta33.respuestas,objPregunta33.respuestaCorrecta);
preguntasManejoResiduosSolidosZonaRural[preguntasManejoResiduosSolidosZonaRural.length] = objPregunta33;

var objPregunta34 = new Pregunta();
objPregunta34.pregunta='Qu\xe9 residuos no pueden ser enterrados ni reaprovechados en el hogar ya que pueden contaminar las fuentes h\xcddricas:';
objPregunta34.respuestas[0]='Residuos org\xe1nicos.';
objPregunta34.respuestas[1]='Residuos s\xf3lidos.';
objPregunta34.respuestas[2]='Residuos putrescibles.';
objPregunta34.respuestas[3]='Residuos t\xf3xicos.';
objPregunta34.respuestaCorrecta=[0,0,0,1];
mezclar (objPregunta34.respuestas,objPregunta34.respuestaCorrecta);
preguntasManejoResiduosSolidosZonaRural[preguntasManejoResiduosSolidosZonaRural.length] = objPregunta34;


var objPregunta35 = new Pregunta();
objPregunta35.pregunta='Los residuos org\xe1nicos sirven para producir un compuesto que puede alimentar la tierra como abono, c\xf3mo se le llama a este compuesto:';
objPregunta35.respuestas[0]='Abono qu\xcdmico.';
objPregunta35.respuestas[1]='Compost.';
objPregunta35.respuestas[2]='Residuo s\xf3lido.';
objPregunta35.respuestas[3]='Insecticidas.';
objPregunta35.respuestaCorrecta=[0,1,0,0];
mezclar (objPregunta35.respuestas,objPregunta35.respuestaCorrecta);
preguntasManejoResiduosSolidosZonaRural[preguntasManejoResiduosSolidosZonaRural.length] = objPregunta35;


/*faltan 5 preguntas del 4 tema*/

preguntasTratamientoResiduosSolidos=[];

var objPregunta41 = new Pregunta();
objPregunta41.pregunta='La atracci\xf3n de par\xe1sitos, la generaci\xf3n de l\xcdquidos lixiviados, la fuga de gases, los problemas de mal olor y la muerte de la vegetaci\xf3n de la superficie, es resultado de:';
objPregunta41.respuestas[0]='Mal manejo de las v\xcdas de desecho.';
objPregunta41.respuestas[1]='Falta de capacitaci\xf3n.';
objPregunta41.respuestas[2]='Poco espacio para el vertedero.';
objPregunta41.respuestas[3]='Mala administraci\xf3n y construcci\xf3n de los vertederos.';
objPregunta41.respuestaCorrecta=[0,0,0,1];
mezclar (objPregunta41.respuestas,objPregunta41.respuestaCorrecta);
preguntasTratamientoResiduosSolidos[preguntasTratamientoResiduosSolidos.length] = objPregunta41;


var objPregunta42 = new Pregunta();
objPregunta42.pregunta='Una posible soluci\xf3n para el problema de la pregunta anterior podr\xcda ser:';
objPregunta42.respuestas[0]='La compactaci\xf3n de los desechos y la utilizaci\xf3n de los gases para producir electricidad.';
objPregunta42.respuestas[1]='Construir un vertedero m\xe1s amplio.';
objPregunta42.respuestas[2]='No llevar basuras a los rellenos sanitarios.';
objPregunta42.respuestas[3]='No compactar los desechos y dejarlos sobre la superficie.';
objPregunta42.respuestaCorrecta=[1,0,0,0];
mezclar (objPregunta42.respuestas,objPregunta42.respuestaCorrecta);
preguntasTratamientoResiduosSolidos[preguntasTratamientoResiduosSolidos.length] = objPregunta42;


var objPregunta43 = new Pregunta();
objPregunta43.pregunta='Aunque la incineraci\xf3n de desechos es utilizado com\xfanmente en los campos, esta pr\xe1ctica trae pros y contras:';
objPregunta43.respuestas[0]='Perdida de materiales reciclables pero beneficio a la salud.';
objPregunta43.respuestas[1]='Ganancias econ\xf3micas pero deterioro del medioambiente.';
objPregunta43.respuestas[2]='Es un m\xe9todo pr\xe1ctico y econ\xf3mico pero genera gases contaminantes.';
objPregunta43.respuestas[3]='Es falso que en los campos se realice incineraci\xf3n de desechos, esto s\xf3lo se realiza en las ciudades.';
objPregunta43.respuestaCorrecta=[0,0,1,0];
mezclar (objPregunta43.respuestas,objPregunta43.respuestaCorrecta);
preguntasTratamientoResiduosSolidos[preguntasTratamientoResiduosSolidos.length] = objPregunta43;


var objPregunta44 = new Pregunta();
objPregunta44.pregunta='El compost es el producto resultante de la descomposici\xf3n de materiales org\xe1nicos que es utilizado para nutrir o abonar las tierras de cultivo. Una definici\xf3n m\xe1s t\xe9cnica del compostaje podr\xcda ser:';
objPregunta44.respuestas[0]='Descomposici\xf3n de elementos no utilizados por completo.';
objPregunta44.respuestas[1]='Enterrar los residuos en fosas construidas de hormig\xf3n, las cuales permiten el aislamiento de los compuestos t\xf3xicos y previenen cat\xe1strofes ambientales.';
objPregunta44.respuestas[2]='El compost no es una t\xe9cnica de procesamiento de residuos ya que los productos resultantes contaminan a\xfan m\xe1s.';
objPregunta44.respuestas[3]='Un proceso que modifica las caracter\xcdsticas f\xcdsicas, qu\xcdmicas y biol\xf3gicas de los residuos para aprovecharlos, estabilizarlos o reducir su volumen antes de la disposici\xf3n final.';
objPregunta44.respuestaCorrecta=[0,0,0,1];
mezclar (objPregunta44.respuestas,objPregunta44.respuestaCorrecta);
preguntasTratamientoResiduosSolidos[preguntasTratamientoResiduosSolidos.length] = objPregunta44;


var objPregunta45 = new Pregunta();
objPregunta45.pregunta='Para que el compost est\xe9 listo lo m\xe1s pronto posible deben seguirse los siguientes pasos:';
objPregunta45.respuestas[0]='Regular la proporci\xf3n de carbono y nitr\xf3geno, la humedad, el tama\u00f1o de las part\xcdculas, la porosidad del material, la temperatura y el flujo de ox\xcdgeno.';
objPregunta45.respuestas[1]='Mantener bien h\xfamedo el terreno y aplicar muchos qu\xcdmicos que descompongan el material r\xe1pidamente.';
objPregunta45.respuestas[2]='Dejar que los residuos se sequen al sol para luego procesarlos con abono inorg\xe1nico.';
objPregunta45.respuestas[3]='Cubrir los desechos, cambiar la ubicaci\xf3n constantemente, mantener los desechos totalmente secos, no agregar qu\xcdmicos.'; 
objPregunta45.respuestaCorrecta=[0,0,0,1];
mezclar (objPregunta45.respuestas,objPregunta45.respuestaCorrecta);
preguntasTratamientoResiduosSolidos[preguntasTratamientoResiduosSolidos.length] = objPregunta45;


var objPregunta46 = new Pregunta();
objPregunta46.pregunta='El principal beneficio del compostaje por lombricultara es:';
objPregunta46.respuestas[0]='Es una forma muy econ\xf3mica de producir abonos organicos.';
objPregunta46.respuestas[1]='Tarda poco tiempo en producirse el abono.';
objPregunta46.respuestas[2]='Permite utilizar todos los desechos org\xe1nicos producidos y puede ser realizado por cualquier persona sin consumir recursos econ\xf3micos.';
objPregunta46.respuestas[3]='Mantiene un equilibrio entre los desechos y su aprovechamiento.'; 
objPregunta46.respuestaCorrecta=[0,0,1,0];
mezclar (objPregunta46.respuestas,objPregunta46.respuestaCorrecta);
preguntasTratamientoResiduosSolidos[preguntasTratamientoResiduosSolidos.length] = objPregunta46;


var objPregunta47 = new Pregunta();
objPregunta47.pregunta='C\xf3mo se produce el compost por lombricultura:';
objPregunta47.respuestas[0]='Las lombrices consumen todo el alimento y al morir sus cuerpos se transforman en compost.';
objPregunta47.respuestas[1]='Las lombrices se alimentan de los desechos org\xe1nicos, y los desechos que producen en su digesti\xf3n es el material o compost resultante.';
objPregunta47.respuestas[2]='Las lombrices comen el abono no org\xe1nico y lo transforman en org\xe1nico.';
objPregunta47.respuestas[3]='Es un proceso biol\xf3gico que no necesita de las lombrices para producirse.'; 
objPregunta47.respuestaCorrecta=[0,1,0,0];
mezclar (objPregunta47.respuestas,objPregunta47.respuestaCorrecta);
preguntasTratamientoResiduosSolidos[preguntasTratamientoResiduosSolidos.length] = objPregunta47;


var objPregunta48 = new Pregunta();
objPregunta48.pregunta='Adem\xe1s del compost de alta calidad, cual es el otro producto que genera la lombricultura.';
objPregunta48.respuestas[0]='La carne de lombriz rica en prote\xcdnas.';
objPregunta48.respuestas[1]='Un compost de baja calidad.';
objPregunta48.respuestas[2]='Desechos inservibles para la agricultura.';
objPregunta48.respuestas[3]='Qu\xcdmicos necesarios para el control de plagas.'; 
objPregunta48.respuestaCorrecta=[1,0,0,0];
mezclar (objPregunta48.respuestas,objPregunta48.respuestaCorrecta);
preguntasTratamientoResiduosSolidos[preguntasTratamientoResiduosSolidos.length] = objPregunta48;


var objPregunta49 = new Pregunta();
objPregunta49.pregunta='La mejor opci\xf3n en las zonas rurales para la disposici\xf3n final de los residuos es el botadero porque:';
objPregunta49.respuestas[0]='Permite que el sol descomponga los residuos f\xe1cilmente.';
objPregunta49.respuestas[1]='Genera mayor contaminaci\xf3n pero igual est\xe1 lejos de los hogares y los campos de cultivo.';
objPregunta49.respuestas[2]='Un botadero no es la mejor opci\xf3n, lo indicado es utilizar el relleno sanitario.';
objPregunta49.respuestas[3]='Permite obtener mayores beneficios ambientales, protegiendo la salud tanto de los seres humanos como de los animales.'; 
objPregunta49.respuestaCorrecta=[0,0,1,0];
mezclar (objPregunta49.respuestas,objPregunta49.respuestaCorrecta);
preguntasTratamientoResiduosSolidos[preguntasTratamientoResiduosSolidos.length] = objPregunta49;


var objPregunta50 = new Pregunta();
objPregunta50.pregunta='El beneficio adicional del reciclaje de desechos s\xf3lidos es:';
objPregunta50.respuestas[0]='Poder mantener limpia la regi\xf3n y los hogares.';
objPregunta50.respuestas[1]='La reutilizaci\xf3n de elementos con los cuales se puede elaborar o inventar productos que soluciones necesidades de la comunidad.';
objPregunta50.respuestas[2]='Reducir el tiempo invertido en el trabajo de los campos.';
objPregunta50.respuestas[3]='Ense\u00f1ar sobre c\xf3mo debe tratarse la basura para poder cultivar la tierra.'; 
objPregunta50.respuestaCorrecta=[0,1,0,0];
mezclar (objPregunta50.respuestas,objPregunta50.respuestaCorrecta);
preguntasTratamientoResiduosSolidos[preguntasTratamientoResiduosSolidos.length] = objPregunta50;


sabiasque=['La cantidad de desechos rurales, aunque menor que lo producidos en la ciudad, a largo plazo causa enormes da\u00f1os al medio ambiente, generando da\u00f1o a la salud de los seres vivos debido al mal manejo de estos desperdicios y la cantidad de pesticidas utilizados en la agricultura.',
            'Un plan de manejo de los residuos s\xf3lidos bien administrado, que capacite a la comunidad sobre las maneras de tratar y reaprovechar los residuos, puede disminuir dr\xe1sticamente el impacto ambiental causado por las basuras producidas en cada regi\xf3n.',
            'La composici\xf3n de las basuras est\xe1 ligado al desarrollo humano y a sus avances tanto tecnol\xf3gicos como industriales.',
            'Entre las basuras generadas en la ciudad o el campo, se encuentran componentes que pueden ser reutilizados, generando ganancias tanto econ\xf3micas como ambientales si realiza el manejo adecuado.',
            'El mal manejos de los desechos y el no separar las basuras en el lugar de origen, puede causar graves problemas de salud a los trabajadores que se encargan de la recolecci\xf3n y manejo final de estos desechos, pues estos est\xe1n expuestos directamente a los factores contaminantes de las basuras.',
            'El efecto invernadero es el causante del cambio clim\xe1tico que ha hecho que los polos se derritan, causando aumento en el nivel de los mares y a su vez estragos clim\xe1ticos.',
            'Tambi\xe9n existe la contaminaci\xf3n por ruido. Cuando los decibeles producidos superan los niveles soportables por el ser humano se dice que es contaminaci\xf3n por ruido y, estos sonidos pueden causar trastornos f\xcdsicos y psicol\xf3gicos en las personas deteriorando incluso la calidad de vida.',
            'Reciclar, es una cadena que transforma los desechos y los convierte en productos reutilizables y consumibles que disminuyen los niveles de contaminaci\xf3n al evitar el consumo masivo de elementos contaminantes, generando energ\xcda, recursos y empleos.', 
            'El m\xe9todo de las 3 R permite reconocer cuando los productos en el mercado poseen elementos reutilizables y/o, contienen elementos reciclados en su elaboraci\xf3n, por lo tanto es un producto reciclable y ecol\xf3gico.', 
            'La necesidad de un relleno sanitario en una regi\xf3n indica que la poblaci\xf3n est\xe1 creciendo de manera tan r\xe1pida que es necesario un r\xe1pido y delicado manejo sus desechos, de forma que su acumulaci\xf3n no genere plagas de insecto o roedores ni deteriore el medioambiente cercano a la zona poblada de donde provienen los residuos.', 
            'Debes evitar que los desechos acumulados entren en contacto con zonas de nacientes de agua, ya que estos desechos producen unos l\xcdquidos llamados Lixiviados o Percolados, los cuales son extremadamente contaminantes y nocivos para la salud de los seres vivos.',
            'Aunque la incineraci\xf3n de desechos s\xf3lidos es una pr\xe1ctica c\xf3moda y r\xe1pida, no es la mejor para el tratamiento de las basuras, ya que, aunque permite ocuparse de grandes cantidades de desechos, genera gases y sustancias qu\xcdmicas que inundan el ambiente causando enfermedades respiratorias y da\u00f1o a la capa de ozono.',
            'El compostaje por lombricultura utiliza una especie de lombriz llamada, Lombriz Californana, la cual es mucho m\xe1s grande y resistente que la lombriz com\xfan, adem\xe1s se reproduce de forma m\xe1s constante, produciendo mayor cantidad de crias que transforman los desechos en compost de muy alta calidad. Y que su carne es adem\xe1s rica en prote\xcdnas, haci\xe9ndola adecuada como sustituto alimenticio.', 
            'Aprender a manejar los residuos y reutilizar los elementos reciclables no s\xf3lo disminuye el impacto ambiental, sino que produce beneficios a la comunidad como, nuevos empleos, espacios libres de contaminaci\xf3n y mejor calidad de vida.'];



function Preguntas() {
    this.preguntasManejoBasuraRural = preguntasManejoBasuraRural;
    this.preguntasMalManejoResiduosSolidos = preguntasMalManejoResiduosSolidos; 
    this.preguntasRellenoSanitario = preguntasRellenoSanitario;
    this.preguntasManejoResiduosSolidosZonaRural = preguntasManejoResiduosSolidosZonaRural;
    this.preguntasTratamientoResiduosSolidos = preguntasTratamientoResiduosSolidos;
    this.sabiasque=sabiasque;
    
}
