﻿using UnityEngine;
using System.Collections;

public class FloatPlatform : MonoBehaviour {

	public float floatingSpeed = 1f;
	public float objectHeight = 0.5f;
	public int option = 0;

	private float currentPosX, currentPosY;

	private JoystickInput jInput;

	void Start () {

		//jInput = GetComponent<JoystickInput>();
		jInput = GameObject.FindWithTag ("Player").GetComponent<JoystickInput>();

		currentPosX = transform.position.x;
		currentPosY = transform.position.y;
	}
	
	void Update (){

		if (option == 0)
			transform.position = new Vector2 (currentPosX, currentPosY + ((Mathf.Cos((Time.time) * floatingSpeed)+1)/2) * objectHeight); 	

		if (option == 1)
			transform.position = new Vector2 (currentPosX, currentPosY + ((Mathf.Cos((Time.time) * floatingSpeed)+1)/2) * objectHeight * -1); 

		if (option == 2)
			transform.position = new Vector2 (currentPosX, currentPosY + ((Mathf.Cos((Time.time) * floatingSpeed)+1)/2) * objectHeight); 
	}




	void OnCollisionEnter2D(Collision2D other){

		if (option <= 1){
			
			if(other.gameObject.tag == "Player")
			{
				if (jInput.groundedPlatform || jInput.groundedPlatform2)
					other.gameObject.transform.parent = transform;
				else
					other.gameObject.transform.parent = null;
			}
			
		}


		if (option == 2) {
				
			if(other.gameObject.tag == "Player")
			{
				if (jInput.groundedPlatform || jInput.groundedPlatform2)
					other.gameObject.transform.parent = transform;
				else
					other.gameObject.transform.parent = null;
			}

		}

		if (option == 3) {

			StartCoroutine (Destroyer());

			if(other.gameObject.tag == "Player")
			{
				if (jInput.groundedPlatform || jInput.groundedPlatform2)
					other.gameObject.transform.parent = transform;
				else
					other.gameObject.transform.parent = null;
			}
			
		}

		if (option == 4) {

			StartCoroutine (Destroyer3());
			
			if(other.gameObject.tag == "Player")
			{
				if (jInput.groundedPlatform || jInput.groundedPlatform2)
					other.gameObject.transform.parent = transform;
				else
					other.gameObject.transform.parent = null;
			}
			
		}

		if (option >= 5) {


			StartCoroutine (Destroyer2());

			if(other.gameObject.tag == "Player")
			{
				if (jInput.groundedPlatform || jInput.groundedPlatform2)
					other.gameObject.transform.parent = transform;
				else
					other.gameObject.transform.parent = null;
			}
			
		}
	}



	IEnumerator Destroyer (){
		yield return new WaitForSeconds(0.35f);
		transform.rigidbody2D.isKinematic = false;
	}

	IEnumerator Destroyer2 (){
		yield return new WaitForSeconds(0.15f);
		transform.rigidbody2D.isKinematic = false;
	}

	IEnumerator Destroyer3 (){
		yield return new WaitForSeconds(3.0f);
		transform.rigidbody2D.isKinematic = false;
	}


}
