﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {
	
	public GameObject buttonNoSound;
	private bool noSoundActive = false;
	
	void OnClick (){
		Debug.Log ("HA HECHO CLICK");
		
		if (!noSoundActive){
			Debug.Log ("AQUI");
			buttonNoSound.SetActive (true);
			noSoundActive = true;
			AudioListener.pause = true;
		}
		
		else if (noSoundActive){
			buttonNoSound.SetActive (false);
			noSoundActive = false;
			AudioListener.pause = false;
		}
	}
}