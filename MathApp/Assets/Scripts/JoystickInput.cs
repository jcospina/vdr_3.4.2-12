﻿using UnityEngine;
using System.Collections;

public class JoystickInput : MonoBehaviour {
	
	public Joystick joystick;           // Reference to joystick prefab
	public float movSpeed = 1.0f;             // Movement speed
	public float jumpForce= 200.0f;
	public bool useAxisInput = true;   // Use Input Axis or Joystick
	public Transform groundCheck;
	public bool grounded = false;
	public bool groundedPlatform = false;
	public bool groundedPlatform2 = false;

	private float h = 0;         // Horizontal and Vertical values
	private bool doJump;
	private bool doJump2;
	private float screenWidth;

	// Animation Parameters
	private Animator anim;
	public float jumpDelay = 1.0f;
	private float jumpTime;
	private bool jumped;
	
	public static bool isWin = false;
	public static bool isEndLevel = false;
	//public static bool isJoystickAnim = false;

	public GameObject NGUILogrosCamera;
	public GameObject logro7;
	public GameObject logro8;
	public GameObject logro9;

	void Awake (){

		TutorControl.tutoCont = 1;
		TutorControl.tutoMov = false;
		TutorControl.tutoJump = false;
		TutorControl.tutoCollect = false;
		TutorControl.tutoPause = false;
		TutorControl.tutoOperation = false;
	
	}

	void Start (){

		//Control de Logros ******

		//if (PlayerScorer.p1Chosen == true && PlayerScorer.p3Chosen == true && PlayerScorer.p5Chosen == true && PlayerScorer.p7Chosen == true) {
		if (	PlayerPrefs.GetInt("p1Chosen") == 1 && PlayerPrefs.GetInt("p3Chosen") == 1 && PlayerPrefs.GetInt("p5Chosen") == 1 && PlayerPrefs.GetInt("p7Chosen")== 1 ){
			if (PlayerPrefs.GetInt ("logro7Obtenido") == 0){
				Debug.Log ("Has obtenido Dije Multicultural!!!");
				//ControlLogros.logro7Obtenido = true;	
				PlayerPrefs.SetInt ("logro7Obtenido",1);
				NGUILogrosCamera.SetActive (true);
				logro7.SetActive (true);
			}
		}

		if (PlayerPrefs.GetInt("p2Chosen") == 1 && PlayerPrefs.GetInt("p4Chosen") == 1 && PlayerPrefs.GetInt("p6Chosen") == 1 && PlayerPrefs.GetInt("p8Chosen")== 1 ){

			if (PlayerPrefs.GetInt ("logro7Obtenido") == 0){
				Debug.Log ("Has obtenido Dije Multicultural!!!");
				//ControlLogros.logro7Obtenido = true;	
				PlayerPrefs.SetInt ("logro7Obtenido",1);
				NGUILogrosCamera.SetActive (true);
				logro7.SetActive (true);
			}
		}

		if (PlayerScorer.perseverancia == 3) {
			if (PlayerPrefs.GetInt ("logro8Obtenido") == 0){
				Debug.Log ("Has obtenido el Tambor de Perseverancia!!!");	
				//ControlLogros.logro8Obtenido = true;		
				PlayerPrefs.SetInt ("logro8Obtenido",1);
				NGUILogrosCamera.SetActive (true);
				logro8.SetActive (true);
			}
		}

		if (PlayerScorer.kilometros == 100) {
			if (PlayerPrefs.GetInt ("logro9Obtenido") == 0){
				Debug.Log ("Has obtenido Las Alpargatas de Juan Tama!!!");	
				//ControlLogros.logro9Obtenido = true;	
				PlayerPrefs.SetInt ("logro9Obtenido",1);
				NGUILogrosCamera.SetActive (true);
				logro9.SetActive (true);
			}
		}
		// Fin Control de Logros *****

		PlayerScorer.numbersCollected = 0;
		NumberCreator.numberList.Clear();
		NumberCreator.tNumbers = 0;
		NumberCreator2.numberList.Clear();
		NumberCreator2.tNumbers = 0;
		NumberCreator3.numberList.Clear();
		NumberCreator3.tNumbers = 0;
		NumberCreator4.numberList.Clear();
		NumberCreator4.tNumbers = 0;
		NumberCreator4.tempDiv = 1;

		anim = GetComponent<Animator>();
		screenWidth = Screen.width;
		isWin = false;
		isEndLevel = false;
		Time.timeScale = 1.0f;
	}

	// Update is called once per frame
	void FixedUpdate () {

		TouchControl ();
		Movement ();
		LineCasting ();
		h = 0;

		if (isWin){
			anim.SetTrigger ("Win");
		}

		if (isEndLevel) {
			useAxisInput = true;
		}
	
//		if (isJoystickAnim){
//			anim.SetTrigger ("JOn");
//		}

//		if (!isJoystickAnim){
//			anim.SetFloat ("PlayerMov",0.0f);
//		}


		//Debug.Log ("Fuerza Player= "+gameObject.rigidbody2D.velocity);
	}
	void LineCasting (){
		Debug.DrawLine(this.transform.position, groundCheck.position, Color.green);
		grounded = Physics2D.Linecast(this.transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
		groundedPlatform = Physics2D.Linecast(this.transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("GroundPlatform"));
		groundedPlatform2 = Physics2D.Linecast(this.transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("GroundPlatform2"));
	}

	void TouchControl (){

		// Get horizontal and vertical input values from either axis or the joystick.
		if (!useAxisInput) {
			
			if (Input.touchCount > 0){
				
				Touch t1 = Input.GetTouch(0);
				
				if (t1.position.x < screenWidth/2){	
					
					h = joystick.position.x;	
					TutorControl.tutoMov = true;
				}
				
				if (t1.position.x > screenWidth/2 && grounded == true){
					doJump = true;
					TutorControl.tutoJump = true;
				}

				if (t1.position.x > screenWidth/2 && groundedPlatform == true){
					doJump2 = true;
				}

				if (t1.position.x > screenWidth/2 && groundedPlatform2 == true){
					doJump = true;
				}
				
			}
			if (Input.touchCount>1 && Input.GetTouch(1).position.x>screenWidth/2 && grounded == true){
				doJump = true;
			}

			if (Input.touchCount>1 && Input.GetTouch(1).position.x>screenWidth/2 && groundedPlatform == true){
				doJump2 = true;
			}

			if (Input.touchCount>1 && Input.GetTouch(1).position.x>screenWidth/2 && groundedPlatform2 == true){
				doJump = true;
			}
			
		}
		else {

			h = Input.GetAxis ("Horizontal");

			if(Input.GetKey(KeyCode.Space) && grounded || Input.GetKey(KeyCode.Space) && groundedPlatform2){	
				
				//Debug.Log ("ESPACIO");
				doJump = true;
			}

			if (Input.GetKey(KeyCode.Space) && groundedPlatform){
				doJump2 = true;
			}
		}
	}
	void Movement (){

		if (grounded || groundedPlatform || groundedPlatform2 ){
			anim.SetFloat ("PlayerSpeed", Mathf.Abs(h));
			anim.SetBool ("isJump", false);
		}
		else {
			anim.SetFloat ("PlayerSpeed",0.0f);
		}

		// Rigth Movement
		if (h>0) {
			transform.Translate(Vector2.right * movSpeed * Time.deltaTime);
			transform.eulerAngles = new Vector2(0,0);
		}
		
		// Left Movement
		if (h<0){
			transform.Translate(Vector2.right * movSpeed * Time.deltaTime);
			transform.eulerAngles = new Vector2(0,180);
		}

		// Jump Movement
		if (doJump){
			//*****Deja de estar pegado a la plataforma
			GameObject.FindWithTag ("Player").transform.parent  = null;
			rigidbody2D.AddForce (Vector2.up * jumpForce);
			jumpTime = jumpDelay;
			anim.SetFloat ("PlayerSpeed", 0.0f);
			anim.SetTrigger ("Jump");
			anim.SetBool ("isJump", true);
			jumped = true;
			doJump = false;
		}

		// Jump 2 Movement
		if (doJump2){
			//*****Deja de estar pegado a la plataforma
			GameObject.FindWithTag ("Player").transform.parent = null;
			rigidbody2D.AddForce (Vector2.up * jumpForce * 0.75f);
			jumpTime = jumpDelay;
			anim.SetFloat ("PlayerSpeed", 0.0f);
			anim.SetBool ("isJump", true);
			anim.SetTrigger ("Jump");
			jumped = true;
			doJump2 = false;
		}

		jumpTime -= Time.deltaTime;

		if (jumpTime<=0 && jumped){
			anim.SetBool ("isJump", false);
			anim.SetTrigger ("Land");
		}

		if (groundedPlatform || groundedPlatform2 ){
			anim.SetTrigger ("Land");
		}
	}

}