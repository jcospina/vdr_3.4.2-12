﻿using UnityEngine;
using System.Collections;

public class FloatHorizontal : MonoBehaviour {

	public float floatingSpeed = 1f;
	public float objectHeight = 0.5f;
	public int option = 0;
	
	private float currentPosX, currentPosY;
	private JoystickInput jInput;
	
	
	void Start () {

		jInput = GameObject.FindWithTag ("Player").GetComponent<JoystickInput>();
		currentPosX = transform.position.x;
		currentPosY = transform.position.y;
	}
	
	void Update (){
		if (option == 0)
			transform.position = new Vector2 (currentPosX + ((Mathf.Cos((Time.time) * floatingSpeed)+1)/2) * objectHeight , currentPosY); 	
		
		if (option == 1)
			transform.position = new Vector2 (currentPosX + ((Mathf.Cos((Time.time) * floatingSpeed)+1)/2) * objectHeight * -1, currentPosY); 

//		if (jInput.groundedPlatform){		
//			Debug.Log ("Esta en una plataforma");
//			GameObject.FindWithTag ("Player").transform.parent = transform;
//		}
//		else if (jInput.grounded) {
//			Debug.Log ("Esta NO es una plataforma");
//			//GameObject.FindWithTag ("Player").transform.parent = GameObject.Find ("Players").transform;
//		}
	}

	void OnCollisionEnter2D(Collision2D other){
		
		if (option == 0){
			
			if(other.gameObject.tag == "Player")
			{
			// ***** Esto soluciona el problema de que el personaje se queda pegado a la plataforma cuando este se cae de la misma
				if (jInput.groundedPlatform || jInput.groundedPlatform2)
					other.gameObject.transform.parent = transform;
				else
					other.gameObject.transform.parent = null;
			}
			
		}

		if (option == 1){
			
			if(other.gameObject.tag == "Player")
			{
				if (jInput.groundedPlatform || jInput.groundedPlatform2)
					other.gameObject.transform.parent = transform;
				else
					other.gameObject.transform.parent = null;
			}
			
		}
	}
}
