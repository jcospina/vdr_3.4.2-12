﻿using UnityEngine;
using System.Collections;

public class MenuPlayerControl : MonoBehaviour {
	
	public GameObject afro1Select;
	public GameObject afro2Select;
	public GameObject camp1Select;
	public GameObject camp2Select;
	public GameObject guamb1Select;
	public GameObject guamb2Select;
	public GameObject nasa1Select;
	public GameObject nasa2Select;

	public UISprite sprite;
	public int optionSelect;
	
	void OnClick (){
		
		switch (optionSelect) {
			
		case 1:
			afro1Select.SetActive (true);
			sprite.spriteName = "Seleccionado";
			PlayerScorer.playerActive = 1;
			break;

		case 2:
			afro2Select.SetActive (true);
			sprite.spriteName = "Seleccionado";
			PlayerScorer.playerActive = 2;
			break;

		case 3:
			camp1Select.SetActive (true);
			sprite.spriteName = "Seleccionado";
			PlayerScorer.playerActive = 3;
			break;

		case 4:
			camp2Select.SetActive (true);
			sprite.spriteName = "Seleccionado";
			PlayerScorer.playerActive = 4;
			break;

		case 5:
			guamb1Select.SetActive (true);
			sprite.spriteName = "Seleccionado";
			PlayerScorer.playerActive = 5;
			break;

		case 6:
			guamb2Select.SetActive (true);
			sprite.spriteName = "Seleccionado";
			PlayerScorer.playerActive = 6;
			break;

		case 7:
			nasa1Select.SetActive (true);
			sprite.spriteName = "Seleccionado";
			PlayerScorer.playerActive = 7;
			break;

		case 8:
			nasa2Select.SetActive (true);
			sprite.spriteName = "Seleccionado";
			PlayerScorer.playerActive = 8;
			break;
			
			
		}
		
	}
}
