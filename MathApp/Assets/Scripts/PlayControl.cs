﻿using UnityEngine;
using System.Collections;

public class PlayControl : MonoBehaviour {


	public GameObject pauseCamera;

	void OnClick (){

		pauseCamera.SetActive (true);

		Time.timeScale = 0.0f;
		TutorControl.tutoPause = true;
	}
}
