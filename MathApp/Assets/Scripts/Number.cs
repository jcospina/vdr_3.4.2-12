﻿using UnityEngine;
using System.Collections;

public class Number : MonoBehaviour {

	public GameObject thisObject;
	public float floatingSpeed = 1f;
	public float objectHeight = 0.5f;
	//public int numberValue = 0; 
	public int numberValue; 
	//public int numberValue = 0; 

	private float currentPosX, currentPosY;

	void Start () {
		currentPosX = transform.position.x;
		currentPosY = transform.position.y;
	}

	void Update (){
		transform.position = new Vector2 (currentPosX, currentPosY + ((Mathf.Cos((Time.time) * floatingSpeed)+1)/2) * objectHeight); 	
	}

	void OnTriggerEnter2D (Collider2D other)
	{

		if(other.gameObject.tag == "Player")
		{
			//Debug.Log ("Numero recogido con valor: "+numberValue);
			//gameObject.SetActive (false);
			PlayerScorer.numbersCollected++; 
			TutorControl.tutoCollect = true;
			Destroy (thisObject);
			//Debug.Log ("Numeros Recogidos = "+PlayerScorer.numbersCollected);
		}
	}

}
