﻿using UnityEngine;
using System.Collections;

public class AnswerCreator2 : MonoBehaviour {
	
	public GameObject thePrefab0;
	public GameObject thePrefab1;
	public GameObject thePrefab2;
	public GameObject thePrefab3;
	public GameObject thePrefab4;
	public GameObject thePrefab5;
	public GameObject thePrefab6;
	public GameObject thePrefab7;
	public GameObject thePrefab8;
	public GameObject thePrefab9;
	public GameObject symbolObject;
	public GameObject equalObject;
	public GameObject questionObject;
	
	private GameObject  newNumber1;
	private GameObject  newNumber2;
	private GameObject  newSymbol;
	private GameObject  newQuestion;
	private GameObject  newEqual;

	private UILabel aLabel;
	private UILabel bLabel;
	private UILabel cLabel;
	private UILabel dLabel;

	private float posX;
	private float posY;
	private float posXS;
	private float posXIni;
	
	public static int resultSubstraction = 0;
	public static bool isPlaying = true;
	
	// Use this for initialization
	void Start () {
		
		aLabel = GameObject.Find("OptionA").GetComponent<UILabel>();	
		bLabel = GameObject.Find("OptionB").GetComponent<UILabel>();	
		cLabel = GameObject.Find("OptionC").GetComponent<UILabel>();	
		dLabel = GameObject.Find("OptionD").GetComponent<UILabel>();	
		
		posX = GameObject.Find("Reference").transform.position.x;
		posXS = GameObject.Find("Reference").transform.position.x;
		posXIni = GameObject.Find("Reference").transform.position.x;
		posY = GameObject.Find("Reference").transform.position.y + 0.3f;

		resultSubstraction = NumberCreator2.resultSubstractionIni;

		for (int i=1; i<NumberCreator2.numberList.Count; i++) {
			resultSubstraction = resultSubstraction-NumberCreator2.numberList[i];
			Debug.Log ("Numero de Lista = "+NumberCreator2.numberList [i]);
		}
		
		Debug.Log ("Resultado Resta = "+resultSubstraction);
		
		Creator ();
		OptionsCreator ();
	}
	
	
	void Creator (){
		
		posX = posXIni - 1.25f;
		posXS = posXIni - 0.85f;
		
		for (int i=0; i<4; i++) {
			
			if (NumberCreator2.numberList[i]<10){
				DrawNumber(NumberCreator2.numberList[i], posX+0.10f,posY);
			}
			
			if (NumberCreator2.numberList[i]>=10){
				
				int digit1t = NumberCreator2.numberList[i]/10;
				int digit2t = NumberCreator2.numberList[i]%10;
				
				DrawNumber (digit1t,posX,posY);
				DrawNumber (digit2t,posX+0.225f,posY);
			}
			
			posX = posX + 0.60f;
		}
		
		for (int i=1; i<4; i++){
			DrawSymbol (posXS,posY);
			posXS = posXS + 0.60f;
		}
		
		DrawEqual (posX-0.15f, posY);
		DrawQuestion (posX+0.10f, posY);
	}
	
	public void OptionsCreator (){
		
		int number;
		int correctPos = Random.Range (1,4);
		
		for (int i=1; i<=4; i++) {
			
			number = Random.Range (0,resultSubstraction+10);
			if (number == resultSubstraction)
				number = number+5;
			
			switch (i){
				
			case 1:
				aLabel.text = "";
				aLabel.text = "A) = "+ number;
				break;
				
			case 2:
				bLabel.text = "";
				bLabel.text = "B) = "+ number;
				break;
				
			case 3:
				cLabel.text = "";
				cLabel.text = "C) = "+ number;
				break;
				
			case 4:
				dLabel.text = "";
				dLabel.text = "D) = "+ number;
				break;
				
			}
			
		}
		
		switch (correctPos){
			
		case 1:
			aLabel.text = "";
			aLabel.text = "A) = "+ resultSubstraction;
			AnswerNumber2.optionCorrect = "A";
			break;
			
		case 2:
			bLabel.text = "";
			bLabel.text = "B) = "+ resultSubstraction;
			AnswerNumber2.optionCorrect = "B";
			break;
			
		case 3:
			cLabel.text = "";
			cLabel.text = "C) = "+ resultSubstraction;
			AnswerNumber2.optionCorrect = "C";
			break;
			
		case 4:
			dLabel.text = "";
			dLabel.text = "D) = "+ resultSubstraction;
			AnswerNumber2.optionCorrect = "D";	
			break;
		}
		
	}
	
	void DrawSymbol (float posX, float posY){
		
		newSymbol = NGUITools.AddChild(GameObject.Find("PanelNumbers"), symbolObject) as GameObject;
		newSymbol.transform.position = new Vector2(posX,posY);		
	}

	void DrawQuestion (float posX, float posY){
		
		newQuestion = NGUITools.AddChild(GameObject.Find("PanelNumbers"), questionObject) as GameObject;
		newQuestion.transform.position = new Vector2(posX,posY);		
	}
	
	void DrawEqual (float posX, float posY){
		
		newEqual = NGUITools.AddChild(GameObject.Find("PanelNumbers"), equalObject) as GameObject;
		newEqual.transform.position = new Vector2(posX,posY);		
	}
	
	void DrawNumber (int number1, float posX, float posY){
		
		switch (number1) {
			
		case 0:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab0) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 1:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab1) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 2:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab2) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 3:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab3) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 4:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab4) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 5:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab5) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 6:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab6) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 7:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab7) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 8:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab8) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 9:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab9) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
		}
		
	}
	
}
