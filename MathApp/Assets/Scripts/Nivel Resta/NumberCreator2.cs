﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NumberCreator2 : MonoBehaviour {

	public GameObject level1;
	public GameObject level2;
	public GameObject level3;
	public GameObject level4;
	public GameObject level5;
	public GameObject level6;
	public GameObject level7;
	public GameObject level8;
	public GameObject level9;
	public GameObject level10;
	
	public UILabel levelLabel;
	
	public GameObject numberObject0;
	public GameObject numberObject1;
	public GameObject numberObject2;
	public GameObject numberObject3;
	public GameObject numberObject4;
	public GameObject numberObject5;
	public GameObject numberObject6;
	public GameObject numberObject7;
	public GameObject numberObject8;
	public GameObject numberObject9;
	public GameObject platformObject;
	public int totalNumbers;
	
	private float posX = 3.0f;
	private float posY = 0.0f;
	private GameObject  newNumber;
	private GameObject  newNumber2;
	private GameObject  newPlatform;
	private float tempX1;
	private Number number;
	private int numberRandom;
	public static int resultSubstraction=0;
	public static int resultSubstractionIni = 0;

	public static  List<int> numberList = new List<int>();
	public static int tNumbers;

	void Awake (){

		if (PlayerScorer.currentlLevelResta == 1) {
			levelLabel.text = "Nivel 1-1";
			level1.SetActive (true);
		}
		if (PlayerScorer.currentlLevelResta == 2) {
			levelLabel.text = "Nivel 2-2";
			level2.SetActive (true);
		}
		if (PlayerScorer.currentlLevelResta == 3) {
			levelLabel.text = "Nivel 2-3";
			level3.SetActive (true);
		}
		if (PlayerScorer.currentlLevelResta == 4) {
			levelLabel.text = "Nivel 2-4";
			level4.SetActive (true);
		}
		if (PlayerScorer.currentlLevelResta == 5) {
			levelLabel.text = "Nivel 2-5";
			level5.SetActive (true);
		}
		if (PlayerScorer.currentlLevelResta == 6) {
			levelLabel.text = "Nivel 2-6";
			level6.SetActive (true);
		}
		if (PlayerScorer.currentlLevelResta == 7) {
			levelLabel.text = "Nivel 2-7";
			level7.SetActive (true);
		}
		if (PlayerScorer.currentlLevelResta == 8) {
			levelLabel.text = "Nivel 2-8";
			level8.SetActive (true);
		}
		if (PlayerScorer.currentlLevelResta == 9) {
			levelLabel.text = "Nivel 2-9";
			level9.SetActive (true);
		}
		if (PlayerScorer.currentlLevelResta == 10) {
			levelLabel.text = "Nivel 2-10";
			level10.SetActive (true);
		}
		
	}

	void Start () {
		StartCoroutine(Creator());
		Debug.Log ("Nivel "+PlayerScorer.contLevelResta);
	}
	
	
	void CreatePlatform (float posX, float posY){
		
		newPlatform = Instantiate (platformObject) as GameObject;
		newPlatform.transform.position = new Vector2(posX, posY);
	} 
	
	IEnumerator Creator (){

		if (PlayerScorer.currentlLevelResta == 1){
			resultSubstraction = Random.Range (6,10);
			resultSubstractionIni = resultSubstraction;
		}

		if (PlayerScorer.currentlLevelResta == 2){
			resultSubstraction = Random.Range (10,20);
			resultSubstractionIni = resultSubstraction;
		}
		if (PlayerScorer.currentlLevelResta == 3){
			resultSubstraction = Random.Range (15,20);
			resultSubstractionIni = resultSubstraction;
		}
		
		if (PlayerScorer.currentlLevelResta == 4){
			resultSubstraction = Random.Range (20,25);
			resultSubstractionIni = resultSubstraction;
		}		
		if (PlayerScorer.currentlLevelResta == 5){
			resultSubstraction = Random.Range (25,30);
			resultSubstractionIni = resultSubstraction;
		}
		if (PlayerScorer.currentlLevelResta == 6){
			resultSubstraction = Random.Range (30,40);
			resultSubstractionIni = resultSubstraction;
		}
		if (PlayerScorer.currentlLevelResta == 7){
			resultSubstraction = Random.Range (40,50);
			resultSubstractionIni = resultSubstraction;
		}
		if (PlayerScorer.currentlLevelResta == 8){
			resultSubstraction = Random.Range (50,60);
			resultSubstractionIni = resultSubstraction;
		}
		if (PlayerScorer.currentlLevelResta == 9){
			resultSubstraction = Random.Range (60,70);
			resultSubstractionIni = resultSubstraction;
		}
		if (PlayerScorer.currentlLevelResta == 10){
			resultSubstraction = Random.Range (70,80);
			resultSubstractionIni = resultSubstraction;
		}

		if (resultSubstraction < 10){
			
			DrawNumber (resultSubstraction,posX+2.0f,1.0f);
			posX = 12.0f;
		}
		
		if (resultSubstraction >= 10){
			
			int digit1 = resultSubstraction/10;
			int digit2 = resultSubstraction%10;
			
			DrawNumber (digit1,posX+2.0f,1.0f);
			DrawNumber (digit2,posX+3.0f,1.0f);
			posX = 12.0f;
		}

		Debug.Log ("Numero Inicial = "+resultSubstraction);
		// Aqui agrega el numero inicial de la resta a la lista
		StartCoroutine (Delay());
		
		for (int i=1; i<4; i++){

			int posYRandom = Random.Range (1,10);

			numberRandom = Random.Range (1,resultSubstraction-5);
			posX = Random.Range(posX,posX+7.0f);
			if (posX >=43.0f){
				posX = 42.0f;
			}

			if  (PlayerScorer.currentlLevelResta == 3 || PlayerScorer.currentlLevelResta ==5 || PlayerScorer.currentlLevelResta ==7 || PlayerScorer.currentlLevelResta ==9)
				posYRandom = 1;

			if  (PlayerScorer.currentlLevelResta == 4 || PlayerScorer.currentlLevelResta ==6 || PlayerScorer.currentlLevelResta ==8 || PlayerScorer.currentlLevelResta ==10)
				posYRandom = 11;

			// AQUI SE DEBE DIFERENCIAR PARA PLATAFORMAS ALTAS Y BAJAS
			if (posYRandom <= 5){
				posY = 1.0f;
			}
			
			if (posYRandom > 5 && posYRandom <= 10){
				posY = 3.2f;
				CreatePlatform (posX, 0.5f);
			}

			if (posYRandom >= 11){
				posY = 3.2f;
			}
			
			if (numberRandom <10){
				DrawNumber (numberRandom,posX,posY);
			}
			
			if (numberRandom >=10){
				
				int digit1t = numberRandom/10;
				int digit2t = numberRandom%10;
				
				DrawNumber (digit1t,posX-0.4f,posY);
				DrawNumber (digit2t,posX+0.5f,posY);

			}

			resultSubstraction = resultSubstraction - numberRandom;	

			posX = posX + 7.0f;
			yield return new WaitForSeconds(0.1f);
			numberList.Add (numberRandom);

			if (numberRandom <10){
				tNumbers++;
			}
			
			if (numberRandom >=10){
				tNumbers = tNumbers+2;
			}
		}

		//Debug.Log ("Total Numeros: "+tNumbers);
		//Debug.Log ("Resultado Resta: "+resultSubstraction);
		
		for (int i=0; i<numberList.Count; i++){
			Debug.Log ("Numero "+i+"= "+numberList[i]);
		};
		
	}

	IEnumerator Delay (){
			yield return new WaitForSeconds (0.1f);
			numberList.Add (resultSubstractionIni);
			if (resultSubstractionIni <10){
					tNumbers++;
				}
				
			if (resultSubstractionIni >=10){
					tNumbers = tNumbers+2;
				}
	}
	
	void DrawNumber (int number, float posX, float posY){
		
		if (number == 0){
			newNumber = Instantiate (numberObject0) as GameObject;
		}
		
		if (number == 1){
			newNumber = Instantiate (numberObject1) as GameObject;
		}
		
		if (number == 2){
			newNumber = Instantiate (numberObject2) as GameObject;
		}
		
		if (number == 3){
			newNumber = Instantiate (numberObject3) as GameObject;
		}
		
		if (number == 4){
			newNumber = Instantiate (numberObject4) as GameObject;
		}
		
		if (number == 5){
			newNumber = Instantiate (numberObject5) as GameObject;
		}
		
		if (number == 6){
			newNumber = Instantiate (numberObject6) as GameObject;
		}
		
		if (number == 7){
			newNumber = Instantiate (numberObject7) as GameObject;
		}
		
		if (number == 8){
			newNumber = Instantiate (numberObject8) as GameObject;
		}
		
		if (number == 9){
			newNumber = Instantiate (numberObject9) as GameObject;
		}
		
		newNumber.transform.position = new Vector2(posX, posY);	
	}
	
	
}
