﻿using UnityEngine;
using System.Collections;

public class MenuLevelResta : MonoBehaviour {
	
	public UISprite sprite;
	public GameObject objectOpen;
	public GameObject objectLabel;
	public int numberLevel;
	
	void Start (){
		
		PlayerScorer.numbersCollected = 0;
		AnswerCreator2.resultSubstraction = 0;
		
		if (NumberCreator2.numberList.Count >= 0) {

			NumberCreator2.numberList.Clear();			
		}
		
		//if (PlayerScorer.contLevelResta>= numberLevel) {
		if (PlayerPrefs.GetInt("nivelResta")>= numberLevel) {
			objectOpen.SetActive (true);
			objectLabel.SetActive (true);
		}
		
		//if (PlayerScorer.contLevelResta > numberLevel){
		if (PlayerPrefs.GetInt("nivelResta")> numberLevel) {
			objectOpen.SetActive (false);
			sprite.spriteName = "nivel_superado";
		} 

		//if (PlayerScorer.contLevelResta == 11) {
//		if (PlayerPrefs.GetInt("nivelResta") == 11) {
//			
//			Debug.Log ("Has conseguido la medalla de resta!!!");
//			//ControlLogros.logro2Obtenido = true;
//			//ControlLogros.logro1Obtenido = true;
//			PlayerPrefs.SetInt ("logro2Obtenido",1);
//		}
		
	}

	void OnClick (){
		PlayerScorer.currentlLevelResta= numberLevel;
		//if (numberLevel <= PlayerScorer.contLevelResta)
		if (numberLevel <= PlayerPrefs.GetInt("nivelResta"))
			Application.LoadLevel ("Scene2");
	}
	
}
