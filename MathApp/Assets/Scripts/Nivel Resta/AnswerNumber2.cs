﻿using UnityEngine;
using System.Collections;

public class AnswerNumber2 : MonoBehaviour {
	
	public AnswerCreator2 aCreator;
	public string option;
	public static string optionCorrect;
	
	public AudioClip incorrectSound;
	public AudioClip correctSound;
	public UISprite button;

	void Start () {
		AnswerCreator2.isPlaying = true;
	}
	
	void OnClick(){

		button.spriteName = "fondo_respuesta_selec";

		if (AnswerCreator2.isPlaying == true) {
			
			if (option == optionCorrect) {
				Debug.Log ("Respuesta Correcta");
				//PlayerScorer.contLevelResta++;
				//if (PlayerScorer.currentlLevelResta >= PlayerScorer.contLevelResta)
				//	PlayerScorer.contLevelResta = PlayerScorer.currentlLevelResta+1;
				if (PlayerScorer.currentlLevelResta >= PlayerPrefs.GetInt("nivelResta")){
					PlayerScorer.contLevelResta = PlayerScorer.contLevelResta+1;
					PlayerPrefs.SetInt("nivelResta",PlayerScorer.contLevelResta);
					Debug.Log ("AQUI SETEA= "+PlayerScorer.contLevelResta);
				}
				JoystickInput.isWin = true;
				NGUITools.PlaySound(correctSound);
				AnswerCreator2.isPlaying = false;
				TutorControl.tutoOperation = true;
				StartCoroutine (Delay ());
				
			} 
			
			else {
				Debug.Log ("Respuesta Incorrecta");	
				NGUITools.PlaySound(incorrectSound);
				//AQUI SE DEBE PERDER UNA VIDA
				//aCreator.OptionsCreator();
			}		
			
		}
		else{
			Debug.Log ("Juego Terminado");	
		}
		
	}
	
	IEnumerator Delay (){
		
		NumberCreator2.numberList.Clear();
		yield return new WaitForSeconds (3.0f);
		//if (PlayerScorer.contLevelResta <=10)
		if (PlayerPrefs.GetInt("nivelSuma") <=10)
			Application.LoadLevel ("MenuNivelResta");
		//if (PlayerScorer.currentlLevelResta == 10 && PlayerScorer.contLevelResta == 11) 
		if (PlayerScorer.currentlLevelResta == 10 && PlayerPrefs.GetInt("nivelSuma") == 11) 
			Application.LoadLevel ("Enemy2");

		else 
			Application.LoadLevel ("MenuNivelResta");
	}
	
}
