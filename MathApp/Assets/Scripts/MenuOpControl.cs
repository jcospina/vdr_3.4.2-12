﻿using UnityEngine;
using System.Collections;

public class MenuOpControl : MonoBehaviour {

	public GameObject sumaSelect;
	public GameObject restaSelect;
	public GameObject multSelect;
	public GameObject div2Select;

	public int optionSelect;

	void OnClick (){
		
		switch (optionSelect) {
			
		case 1:
			sumaSelect.SetActive (true);
			break;
			
		case 2:
			restaSelect.SetActive (true);
			break;
			
		case 3:
			multSelect.SetActive (true);
			break;
			
		case 4:
			div2Select.SetActive (true);
			break;
		}
		
	}
}
