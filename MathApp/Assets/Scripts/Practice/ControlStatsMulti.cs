﻿using UnityEngine;
using System.Collections;

public class ControlStatsMulti : MonoBehaviour {
	
	// ESTADISTICAS RESTA
	public UIProgressBar correctMultiPBar;
	public UIProgressBar incorrectMultiPBar;
	public UILabel correctsLabel;
	public UILabel incorrectsLabel;
	public UILabel correctsLabelPercent;
	public UILabel incorrectsLabelPercent;
	public UILabel timeLabel;
	private float correctsValue;
	private float incorrectsValue;
	private float timeValue;
	private int totalAnswers = 0;
	
	void Awake (){
		
		//timeValue = GameControl.tiempoSuma;
		timeValue = PlayerPrefs.GetInt ("tiempoMulti");
		//totalAnswers = GameControl.correctasSuma + GameControl.incorrectasSuma;
		totalAnswers = PlayerPrefs.GetInt ("correctasMulti") + PlayerPrefs.GetInt ("incorrectasMulti");
		
		if (totalAnswers == 0)
			totalAnswers = totalAnswers + 1;
		
		correctsValue = (PlayerPrefs.GetInt ("correctasMulti") * 1.0f) / totalAnswers;
		incorrectsValue = 1.0f - correctsValue;
		Debug.Log ("Tiempo = "+ PlayerPrefs.GetInt ("tiempoResta"));
		Debug.Log ("Total Respuestas = "+totalAnswers);
		Debug.Log ("Numero Correctas = "+correctsValue);
		Debug.Log ("Numero Inorrectas = "+incorrectsValue);
		
	}
	
	void Start () {
		
		timeLabel.text = "Tiempo Total = " + timeValue/60 + " min";
		correctMultiPBar.value = correctsValue;
		incorrectMultiPBar.value = incorrectsValue;
		correctsLabel.text = "Correctas = "+ PlayerPrefs.GetInt ("correctasMulti");
		incorrectsLabel.text = "Incorrectas = " + PlayerPrefs.GetInt ("incorrectasMulti");
		correctsLabelPercent.text =  System.Math.Round(correctsValue * 100.0f,2)+"%";
		incorrectsLabelPercent.text = System.Math.Round(incorrectsValue * 100.0f,2)+"%";
		
	}
	
}