﻿using UnityEngine;
using System.Collections;

public class Practice2Creator : MonoBehaviour {
	
	public GameObject thePrefab0;
	public GameObject thePrefab1;
	public GameObject thePrefab2;
	public GameObject thePrefab3;
	public GameObject thePrefab4;
	public GameObject thePrefab5;
	public GameObject thePrefab6;
	public GameObject thePrefab7;
	public GameObject thePrefab8;
	public GameObject thePrefab9;
	public GameObject theSymbolPrefab;
	public GameObject theLinePrefab;
	//public int totalNumbers = 0;
	int totalNumbers = 0;
	
	public static int digitCount = 1;
	public static int resultSubstraction = 0;
	
	private GameObject newNumber1;
	private GameObject newNumber2;
	private GameObject newLine;
	private GameObject newSymbol;

	//Logro Sabiduria
	public GameObject logro;
	public GameObject panelNumbers;

	
	void Awake (){
		
		GameControl.time = 60;
		GameControl.score = 0;
		GameControl.touchCount = 1;
		GameControl.isPlaying = true;
		GameControl.contResta = 1;
		GameControl.sabiduria = 0;
	}

	void Start () {
		resultSubstraction = 0;
		PlayerPrefs.SetInt ("tiempoResta", GameControl.tiempoResta + 60);
		Creator ();
		
	}
	
	public void Creator (){

		if (GameControl.sabiduria >= 10) {
			if (PlayerPrefs.GetInt("logro6Obtenido")==0){
				logro.SetActive (true);
				panelNumbers.transform.localScale = new Vector2 (0,1);
				Debug.Log ("Has conseguido el sombrero de sabiduría!!");
				//ControlLogros.logro6Obtenido = true;	
				PlayerPrefs.SetInt ("logro6Obtenido",1);
			}
		}

		
		//totalNumbers = Random.Range (2,4);
		totalNumbers = 1;
		//Debug.Log ("Total Numeros = "+totalNumbers);
		resultSubstraction = Random.Range (1,99);
		int tempNumber = 0;
		float posY = 0.05f;
		Debug.Log ("Numero Inicial = "+resultSubstraction);
		if (resultSubstraction < 10){

			DrawNumber1 (resultSubstraction,-0.2f,0.4f);
		}

		if (resultSubstraction >= 10){

			int digit1 = resultSubstraction/10;
			int digit2 = resultSubstraction%10;

			DrawNumber1 (digit1,-0.5f,0.4f);
			DrawNumber2 (digit2,-0.2f,0.4f);

		}

		for (int i=1; i<=totalNumbers; i++) {
			 
			tempNumber = Random.Range (0,resultSubstraction);
			resultSubstraction = resultSubstraction - tempNumber;
			//Debug.Log (tempNumber);

			if (tempNumber < 10){

				DrawNumber1 (tempNumber,-0.2f,posY);
			}

			if (tempNumber >= 10){
				
				int digit1t = tempNumber/10;
				int digit2t = tempNumber%10;

				//Debug.Log ("Digito 1= "+digit1t+"Digito 2= "+digit2t);

				DrawNumber1 (digit1t,-0.5f,posY);
				DrawNumber2 (digit2t,-0.2f,posY);
				
			}

			posY = posY - 0.3f;
		}

		DrawLine (-0.5f,posY+0.1f);
		DrawSymbol (-0.9f, (posY+0.6f)/2.0f);
		Debug.Log ("Resultado Resta = "+resultSubstraction);
		
	}

	void DrawLine (float posX, float posY){
		
		newLine = NGUITools.AddChild(GameObject.Find("PanelNumbers"), theLinePrefab) as GameObject;
		newLine.transform.position = new Vector2(posX,posY);
		
	}
	
	void DrawSymbol (float posX, float posY){
		
		newSymbol = NGUITools.AddChild(GameObject.Find("PanelNumbers"), theSymbolPrefab) as GameObject;
		newSymbol.transform.position = new Vector2(posX,posY);
		
	}

	void DrawNumber1 (int number1, float posX, float posY){

		switch (number1) {
			
			case 0:
				newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab0) as GameObject;
				newNumber1.transform.position = new Vector2(posX,posY);
				break;
				
			case 1:
				newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab1) as GameObject;
				newNumber1.transform.position = new Vector2(posX,posY);
				break;
				
			case 2:
				newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab2) as GameObject;
				newNumber1.transform.position = new Vector2(posX,posY);
				break;
				
			case 3:
				newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab3) as GameObject;
				newNumber1.transform.position = new Vector2(posX,posY);
				break;
				
			case 4:
				newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab4) as GameObject;
				newNumber1.transform.position = new Vector2(posX,posY);
				break;
				
			case 5:
				newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab5) as GameObject;
				newNumber1.transform.position = new Vector2(posX,posY);
				break;
				
			case 6:
				newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab6) as GameObject;
				newNumber1.transform.position = new Vector2(posX,posY);
				break;
				
			case 7:
				newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab7) as GameObject;
				newNumber1.transform.position = new Vector2(posX,posY);
				break;
				
			case 8:
				newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab8) as GameObject;
				newNumber1.transform.position = new Vector2(posX,posY);
				break;
				
			case 9:
				newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab9) as GameObject;
				newNumber1.transform.position = new Vector2(posX,posY);
				break;
		}

	}

	void DrawNumber2 (int number2, float posX, float posY){

		switch (number2){
			
			case 0:
				newNumber2 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab0) as GameObject;
				newNumber2.transform.position = new Vector2(posX,posY);
				break;
				
			case 1:
				newNumber2 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab1) as GameObject;
				newNumber2.transform.position = new Vector2(posX,posY);
				break;
				
			case 2:
				newNumber2 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab2) as GameObject;
				newNumber2.transform.position = new Vector2(posX,posY);
				break;
				
			case 3:
				newNumber2 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab3) as GameObject;
				newNumber2.transform.position = new Vector2(posX,posY);
				break;
				
			case 4:
				newNumber2 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab4) as GameObject;
				newNumber2.transform.position = new Vector2(posX,posY);
				break;
				
			case 5:
				newNumber2 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab5) as GameObject;
				newNumber2.transform.position = new Vector2(posX,posY);
				break;
				
			case 6:
				newNumber2 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab6) as GameObject;
				newNumber2.transform.position = new Vector2(posX,posY);
				break;
				
			case 7:
				newNumber2 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab7) as GameObject;
				newNumber2.transform.position = new Vector2(posX,posY);
				break;
				
			case 8:
				newNumber2 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab8) as GameObject;
				newNumber2.transform.position = new Vector2(posX,posY);
				break;
				
			case 9:
				newNumber2 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab9) as GameObject;
				newNumber2.transform.position = new Vector2(posX,posY);
				break;
			
		}

	}
	
}
