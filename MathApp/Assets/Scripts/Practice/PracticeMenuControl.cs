﻿using UnityEngine;
using System.Collections;

public class PracticeMenuControl : MonoBehaviour {

	public GameObject sumaSelect;
	public GameObject restaSelect;
	public GameObject multSelect;
	public GameObject divSelect;
	public UISprite sprite;
	public int optionSelect;

	void Start (){

		Time.timeScale = 1.0f;
	}	

	void OnClick (){

		switch (optionSelect) {
				
		case 1:
			sumaSelect.SetActive (true);
			sprite.spriteName = "Seleccionado";
			break;

		case 2:
			restaSelect.SetActive (true);
			sprite.spriteName = "Seleccionado";
			break;

		case 3:
			multSelect.SetActive (true);
			sprite.spriteName = "Seleccionado";
			break;

		case 4:
			divSelect.SetActive (true);
			sprite.spriteName = "Seleccionado";
			break;
					
		}

	}
}
