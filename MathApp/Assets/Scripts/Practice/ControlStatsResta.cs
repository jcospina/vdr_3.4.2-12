﻿using UnityEngine;
using System.Collections;

public class ControlStatsResta : MonoBehaviour {
	
	// ESTADISTICAS RESTA
	public UIProgressBar correctRestaPBar;
	public UIProgressBar incorrectRestaPBar;
	public UILabel correctsLabel;
	public UILabel incorrectsLabel;
	public UILabel correctsLabelPercent;
	public UILabel incorrectsLabelPercent;
	public UILabel timeLabel;
	private float correctsValue;
	private float incorrectsValue;
	private float timeValue;
	private int totalAnswers = 0;
	
	void Awake (){
		
		//timeValue = GameControl.tiempoSuma;
		timeValue = PlayerPrefs.GetInt ("tiempoResta");
		//totalAnswers = GameControl.correctasSuma + GameControl.incorrectasSuma;
		totalAnswers = PlayerPrefs.GetInt ("correctasResta") + PlayerPrefs.GetInt ("incorrectasResta");
		
		if (totalAnswers == 0)
			totalAnswers = totalAnswers + 1;
		
		correctsValue = (PlayerPrefs.GetInt ("correctasResta") * 1.0f) / totalAnswers;
		incorrectsValue = 1.0f - correctsValue;
		Debug.Log ("Tiempo = "+ PlayerPrefs.GetInt ("tiempoResta"));
		Debug.Log ("Total Respuestas = "+totalAnswers);
		Debug.Log ("Numero Correctas = "+correctsValue);
		Debug.Log ("Numero Inorrectas = "+incorrectsValue);
		
	}
	
	void Start () {
		
		timeLabel.text = "Tiempo Total = " + timeValue/60 + " min";
		correctRestaPBar.value = correctsValue;
		incorrectRestaPBar.value = incorrectsValue;
		correctsLabel.text = "Correctas = "+ PlayerPrefs.GetInt ("correctasResta");
		incorrectsLabel.text = "Incorrectas = " + PlayerPrefs.GetInt ("incorrectasResta");
		correctsLabelPercent.text =  System.Math.Round(correctsValue * 100.0f,2)+"%";
		incorrectsLabelPercent.text = System.Math.Round(incorrectsValue * 100.0f,2)+"%";
		
	}
	
}