﻿using UnityEngine;
using System.Collections;

public class ControlStats : MonoBehaviour {

	// ESTADISTICAS SUMA
	public UIProgressBar correctSumaPBar;
	public UIProgressBar incorrectSumaPBar;
	public UILabel correctsLabel;
	public UILabel incorrectsLabel;
	public UILabel correctsLabelPercent;
	public UILabel incorrectsLabelPercent;
	public UILabel timeLabel;
	private float correctsValue;
	private float incorrectsValue;
	private float timeValue;
	private int totalAnswers = 0;

	void Awake (){

		//timeValue = GameControl.tiempoSuma;
		timeValue = PlayerPrefs.GetInt ("tiempoSuma");
		//totalAnswers = GameControl.correctasSuma + GameControl.incorrectasSuma;
		totalAnswers = PlayerPrefs.GetInt ("correctasSuma") + PlayerPrefs.GetInt ("incorrectasSuma");

		if (totalAnswers == 0)
			totalAnswers = totalAnswers + 1;	
		correctsValue = (PlayerPrefs.GetInt ("correctasSuma") * 1.0f) / totalAnswers;
		incorrectsValue = 1.0f - correctsValue;
		Debug.Log ("Tiempo Suma= "+ PlayerPrefs.GetInt ("tiempoSuma"));
//		Debug.Log ("Total Respuestas = "+totalAnswers);
//		Debug.Log ("Numero Correctas = "+correctsValue);
//		Debug.Log ("Numero Inorrectas = "+incorrectsValue);
	}
	
	void Start () {

		timeLabel.text = "Tiempo Total = " + timeValue/60 + " min";
		correctSumaPBar.value = correctsValue;
		incorrectSumaPBar.value = incorrectsValue;
		correctsLabel.text = "Correctas = "+ PlayerPrefs.GetInt ("correctasSuma");
		incorrectsLabel.text = "Incorrectas = " + PlayerPrefs.GetInt ("incorrectasSuma");
		correctsLabelPercent.text =  System.Math.Round(correctsValue * 100.0f,2)+"%";
		incorrectsLabelPercent.text = System.Math.Round(incorrectsValue * 100.0f,2)+"%";

	}
	
}