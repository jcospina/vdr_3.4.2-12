﻿using UnityEngine;
using System.Collections;

public class Practice3Creator : MonoBehaviour {
	
	public GameObject thePrefab0;
	public GameObject thePrefab1;
	public GameObject thePrefab2;
	public GameObject thePrefab3;
	public GameObject thePrefab4;
	public GameObject thePrefab5;
	public GameObject thePrefab6;
	public GameObject thePrefab7;
	public GameObject thePrefab8;
	public GameObject thePrefab9;
	public GameObject theSymbolPrefab;
	public GameObject theLinePrefab;
	//public int totalNumbers = 0;
	int totalNumbers = 0;
	
	public static int digitCount = 1;
	public static int resultProduct = 0;
	
	private GameObject newNumber1;
	private GameObject newLine;
	private GameObject newSymbol;
	//private GameObject newNumber2;

	//Logro Sabiduria
	public GameObject logro;
	public GameObject panelNumbers;

	
	void Awake (){
		
		GameControl.time = 60;
		GameControl.score = 0;
		GameControl.touchCount = 1;
		GameControl.isPlaying = true;
		GameControl.contMulti = 1;
		GameControl.sabiduria = 0;
	}

	void Start () {
		
		PlayerPrefs.SetInt ("tiempoMulti",GameControl.tiempoMulti+60);
		resultProduct = 1;
		Creator ();
		
	}
	
	public void Creator (){

		if (GameControl.sabiduria >= 10) {
			if (PlayerPrefs.GetInt("logro6Obtenido")==0){
				logro.SetActive (true);
				panelNumbers.transform.localScale = new Vector2 (0,1);
				Debug.Log ("Has conseguido el sombrero de sabiduría!!");
				//ControlLogros.logro6Obtenido = true;	
				PlayerPrefs.SetInt ("logro6Obtenido",1);
			}
		}

		
		//totalNumbers = Random.Range (2,2);
		totalNumbers = 2;
		int number = 0;
		float posY = 0.4f;
		
		
		for (int i=1; i<=totalNumbers; i++) {	
			number = Random.Range (0,9);	
			resultProduct = resultProduct * number;
			DrawNumber (number,-0.4f,posY);
			posY = posY - 0.30f;
		}

		DrawLine (-0.5f,posY+0.1f);
		DrawSymbol (-0.9f, (posY+0.6f)/2.0f);
		Debug.Log ("Resultado = "+resultProduct);
		
	}

	void DrawLine (float posX, float posY){
		
		newLine = NGUITools.AddChild(GameObject.Find("PanelNumbers"), theLinePrefab) as GameObject;
		newLine.transform.position = new Vector2(posX,posY);
		
	}
	
	void DrawSymbol (float posX, float posY){
		
		newSymbol = NGUITools.AddChild(GameObject.Find("PanelNumbers"), theSymbolPrefab) as GameObject;
		newSymbol.transform.position = new Vector2(posX,posY);
		
	}

	void DrawNumber (int number1, float posX, float posY){
		
		switch (number1) {
			
		case 0:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab0) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 1:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab1) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 2:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab2) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 3:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab3) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 4:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab4) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 5:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab5) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 6:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab6) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 7:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab7) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 8:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab8) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 9:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab9) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
		}
		
	}
	
}
