﻿using UnityEngine;
using System.Collections;

public class PracticeCreator : MonoBehaviour {

	public GameObject thePrefab0;
	public GameObject thePrefab1;
	public GameObject thePrefab2;
	public GameObject thePrefab3;
	public GameObject thePrefab4;
	public GameObject thePrefab5;
	public GameObject thePrefab6;
	public GameObject thePrefab7;
	public GameObject thePrefab8;
	public GameObject thePrefab9;
	public GameObject theLinePrefab;
	public GameObject theSymbolPrefab;

	public static int digitCount = 1;
	public static int resultAddition = 0;

	private GameObject newNumber1;
	private GameObject newLine;
	private GameObject newSymbol;

	private int randomRange1;
	private int randomRange2;
	private int totalNumbers = 2;

	//Logro Sabiduria
	public GameObject logro;
	public GameObject panelNumbers;

	void Awake (){

		GameControl.time = 60;
		GameControl.score = 0;
		GameControl.touchCount = 1;
		GameControl.isPlaying = true;
		GameControl.contSuma = 1;
		GameControl.sabiduria = 0;
	}

	void Start () {

		resultAddition = 0;
		//GameControl.tiempoSuma = GameControl.tiempoSuma + 20;
		PlayerPrefs.SetInt ("tiempoSuma",GameControl.tiempoSuma+60);
		Creator ();
	}

	public void Creator (){

		int numberRandom = 0;
		float posY = 0.4f;

		Debug.Log ("Valor Contador Dificultad = "+GameControl.contSuma);
		Debug.Log ("Valor Sabiduria = "+GameControl.sabiduria);

		switch (GameControl.contSuma){

			case 1:
			randomRange1 = 1;
			randomRange2 = 10;
			break;

			case 2:
			randomRange1 = 5;
			randomRange2 = 10;
			break;

			case 3:
			randomRange1 = 10;
			randomRange2 = 15;
			break;

			case 4:
			randomRange1 = 15;
			randomRange2 = 20;
			break;

			case 5:
			randomRange1 = 20;
			randomRange2 = 25;
			break;

			case 6:	
			randomRange1 = 30;
			randomRange2 = 40;
			break;

			case 7:	
			randomRange1 = 40;
			randomRange2 = 50;
			break;

			case 8:	
			randomRange1 = 50;
			randomRange2 = 60;
			break;

			case 9:	
			randomRange1 = 60;
			randomRange2 = 70;
			break;

			case 10:	
			randomRange1 = 70;
			randomRange2 = 80;
			break;

			default:
			randomRange1 = 80;
			randomRange2 = 90;
			break;
		}

		if (GameControl.sabiduria >= 10) {
			if (PlayerPrefs.GetInt("logro6Obtenido")==0){
				logro.SetActive (true);
				panelNumbers.transform.localScale = new Vector2 (0,1);
				Debug.Log ("Has conseguido el sombrero de sabiduría!!");
				//ControlLogros.logro6Obtenido = true;	
				PlayerPrefs.SetInt ("logro6Obtenido",1);
			}
		}

		for (int i=1; i<=totalNumbers; i++) {	

			numberRandom = Random.Range (randomRange1,randomRange2);	

			if (numberRandom<10){
				DrawNumber(numberRandom, -0.5f,posY);
			}

			if (numberRandom>=10){
				
				int digit1t = numberRandom/10;
				int digit2t = numberRandom%10;
				
				DrawNumber (digit1t,-0.7f,posY);
				DrawNumber (digit2t,-0.4f,posY);
			}

			posY = posY - 0.35f;
			resultAddition = resultAddition + numberRandom;
		}

		DrawLine (-0.5f,posY+0.1f);
		DrawSymbol (-1.05f, (posY+0.6f)/2.0f);
		Debug.Log ("Resultado = "+resultAddition);

	}

	void DrawNumber (int number1, float posX, float posY){
		
		switch (number1) {
			
		case 0:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab0) as GameObject;
			break;
			
		case 1:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab1) as GameObject;
			break;
			
		case 2:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab2) as GameObject;
			break;
			
		case 3:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab3) as GameObject;
			break;
			
		case 4:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab4) as GameObject;
			break;
			
		case 5:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab5) as GameObject;
			break;
			
		case 6:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab6) as GameObject;
			break;
			
		case 7:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab7) as GameObject;
			break;
			
		case 8:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab8) as GameObject;
			break;
			
		case 9:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab9) as GameObject;
			break;
		}
		newNumber1.transform.position = new Vector2(posX,posY);	
	}

	void DrawLine (float posX, float posY){
	
		newLine = NGUITools.AddChild(GameObject.Find("PanelNumbers"), theLinePrefab) as GameObject;
		newLine.transform.position = new Vector2(posX,posY);

	}

	void DrawSymbol (float posX, float posY){
		
		newSymbol = NGUITools.AddChild(GameObject.Find("PanelNumbers"), theSymbolPrefab) as GameObject;
		newSymbol.transform.position = new Vector2(posX,posY);
		
	}

}
