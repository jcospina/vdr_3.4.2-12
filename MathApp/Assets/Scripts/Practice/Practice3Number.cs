﻿using UnityEngine;
using System.Collections;

public class Practice3Number : MonoBehaviour {
	
	public int numberValue;
	public GameObject thePrefab;
	public AudioClip incorrectSound;
	public AudioClip correctSound;
	
	private GameObject answerNumber1;
	private GameObject answerNumber2;
	
	private int result;
	private string txtResultNumber;
	public Practice3Creator pCreator;
	
	
	void Start (){
		
	}
	
	void OnClick (){
		
		if (GameControl.time >= 0) {
			
			if (GameControl.isPlaying == true) {
				
				//Debug.Log ("Numero Seleccionado = "+numberValue);
				
				if (Practice3Creator.resultProduct <10) {
					GameControl.txtResult = numberValue.ToString();
					answerNumber1 = NGUITools.AddChild (GameObject.Find ("PanelAnswer"), thePrefab) as GameObject;
					answerNumber1.transform.position = new Vector2 (-0.4f, -0.35f);
					ResultCheck ();	
				}	
				else if (Practice3Creator.resultProduct >=10){
					
					if (GameControl.touchCount == 1){
						
						answerNumber1 = NGUITools.AddChild (GameObject.Find ("PanelAnswer"), thePrefab) as GameObject;
						answerNumber1.transform.position = new Vector2 (-0.6f, -0.35f);
						GameControl.txtResult = numberValue.ToString();
						GameControl.touchCount = 2;
					}
					
					else if (GameControl.touchCount == 2){
						
						answerNumber2 = NGUITools.AddChild (GameObject.Find ("PanelAnswer"), thePrefab) as GameObject;
						answerNumber2.transform.position = new Vector2 (-0.3f, -0.35f);
						GameControl.txtResult += numberValue.ToString();
						Debug.Log ("Resultado = "+GameControl.txtResult);
						ResultCheck();
						
					}
					
				}
			} 
			else {
				
				Debug.Log ("Momento!!");
			}
		}
		
		else {
			Debug.Log ("GAME OVER");
		}
		
		
	}
	
	void ResultCheck (){
		
		//Debug.Log ("VALOR RESULTADO EN TEXTO = " + GameControl.txtResult);
		
		if (int.Parse(GameControl.txtResult) == Practice3Creator.resultProduct) {
			Debug.Log ("Resultado Correcto");
			NGUITools.PlaySound(correctSound);
			GameControl.isPlaying = false;
			GameControl.touchCount = 1;
			GameControl.score = GameControl.score +10;
			GameControl.sabiduria++;
			GameControl.contMulti++;
			GameControl.correctasMulti++;
			PlayerPrefs.SetInt ("correctasMulti",GameControl.correctasMulti);
			Practice3Creator.resultProduct = 1;
			StartCoroutine(DelayWin());
		}
		else{
			
			//Destroy(answerNumber1);
			//Destroy(answerNumber1);
			Debug.Log ("Resultado Incorrecto");
			NGUITools.PlaySound(incorrectSound);
			GameControl.isPlaying = false;
			GameControl.sabiduria = 0;
			GameControl.incorrectasMulti++;
			PlayerPrefs.SetInt ("incorrectasMulti",GameControl.incorrectasMulti);
			GameControl.txtResult = "";
			GameControl.touchCount = 1;
			StartCoroutine(Delay());
			
		}
	}
	
	IEnumerator Delay(){
		yield return new WaitForSeconds(2);
		GameObject panelAnswer = GameObject.Find("PanelAnswer"); 
		NGUITools.SetActiveChildren(panelAnswer,false);
		GameControl.isPlaying = true;
	}
	
	IEnumerator DelayWin(){
		yield return new WaitForSeconds(2);
		GameObject panelAnswer = GameObject.Find("PanelAnswer"); 
		GameObject panelNumbers = GameObject.Find("PanelNumbers"); 
		NGUITools.SetActiveChildren(panelAnswer,false);
		NGUITools.SetActiveChildren(panelNumbers,false);
		pCreator.Creator ();
		GameControl.isPlaying = true;
	}
}
