﻿using UnityEngine;
using System.Collections;

public class Practice2Number : MonoBehaviour {
	
	public int numberValue;
	public GameObject thePrefab;
	public AudioClip incorrectSound;
	public AudioClip correctSound;
	
	private GameObject answerNumber1;
	private GameObject answerNumber2;
	
	private int result;
	private string txtResultNumber;
	public Practice2Creator p2Creator;
	
	
	void Start (){
		
	}
	
	void OnClick (){
		
		if (GameControl.time >= 0) {
			
			if (GameControl.isPlaying == true) {
				
				//Debug.Log ("Numero Seleccionado = "+numberValue);
				
				if (Practice2Creator.resultSubstraction <10) {
					GameControl.txtResult = numberValue.ToString();
					answerNumber1 = NGUITools.AddChild (GameObject.Find ("PanelAnswer"), thePrefab) as GameObject;
					answerNumber1.transform.position = new Vector2 (-0.2f, -0.4f);
					ResultCheck ();	
				}	
				else if (Practice2Creator.resultSubstraction >=10){
					
					if (GameControl.touchCount == 1){
						
						answerNumber1 = NGUITools.AddChild (GameObject.Find ("PanelAnswer"), thePrefab) as GameObject;
						answerNumber1.transform.position = new Vector2 (-0.5f, -0.4f);
						GameControl.txtResult = numberValue.ToString();
						GameControl.touchCount = 2;
					}
					
					else if (GameControl.touchCount == 2){
						
						answerNumber2 = NGUITools.AddChild (GameObject.Find ("PanelAnswer"), thePrefab) as GameObject;
						answerNumber2.transform.position = new Vector2 (-0.2f, -0.4f);
						GameControl.txtResult += numberValue.ToString();
						Debug.Log ("Resultado = "+GameControl.txtResult);
						ResultCheck();
						
					}
					
				}
			} 
			else {
				
				Debug.Log ("Momento!!");
			}
		}
		
		else {
			Debug.Log ("GAME OVER");
		}
		
		
	}
	
	void ResultCheck (){
		
		//Debug.Log ("VALOR RESULTADO EN TEXTO = " + GameControl.txtResult);
		
		if (int.Parse(GameControl.txtResult) == Practice2Creator.resultSubstraction) {
			Debug.Log ("Resultado Correcto");
			NGUITools.PlaySound(correctSound);
			GameControl.isPlaying = false;
			GameControl.touchCount = 1;
			GameControl.score = GameControl.score +10;
			GameControl.sabiduria++;
			Practice2Creator.resultSubstraction = 0;
			GameControl.contResta++;
			GameControl.correctasResta++;
			PlayerPrefs.SetInt ("correctasResta",GameControl.correctasResta);
			StartCoroutine(DelayWin());
		}
		else{
			
			//Destroy(answerNumber1);
			//Destroy(answerNumber1);
			Debug.Log ("Resultado Incorrecto");
			NGUITools.PlaySound(incorrectSound);
			GameControl.sabiduria = 0;
			GameControl.isPlaying = false;
			GameControl.txtResult = "";
			GameControl.touchCount = 1;
			GameControl.incorrectasResta++;
			PlayerPrefs.SetInt ("incorrectasResta",GameControl.incorrectasResta);
			StartCoroutine(Delay());
		}
	}
	
	IEnumerator Delay(){
		yield return new WaitForSeconds(2);
		GameObject panelAnswer = GameObject.Find("PanelAnswer"); 
		NGUITools.SetActiveChildren(panelAnswer,false);
		GameControl.isPlaying = true;
	}
	
	IEnumerator DelayWin(){
		yield return new WaitForSeconds(2);
		GameObject panelAnswer = GameObject.Find("PanelAnswer"); 
		GameObject panelNumbers = GameObject.Find("PanelNumbers"); 
		NGUITools.SetActiveChildren(panelAnswer,false);
		NGUITools.SetActiveChildren(panelNumbers,false);
		p2Creator.Creator ();
		GameControl.isPlaying = true;
	}
}
