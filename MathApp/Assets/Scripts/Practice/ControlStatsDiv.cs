﻿using UnityEngine;
using System.Collections;

public class ControlStatsDiv : MonoBehaviour {
	
	// ESTADISTICAS DIVISION
	public UIProgressBar correctDivPBar;
	public UIProgressBar incorrectDivPBar;
	public UILabel correctsLabel;
	public UILabel incorrectsLabel;
	public UILabel correctsLabelPercent;
	public UILabel incorrectsLabelPercent;
	public UILabel timeLabel;
	private float correctsValue;
	private float incorrectsValue;
	private float timeValue;
	private int totalAnswers = 0;
	
	void Awake (){
		
		//timeValue = GameControl.tiempoSuma;
		timeValue = PlayerPrefs.GetInt ("tiempoDiv");
		//totalAnswers = GameControl.correctasSuma + GameControl.incorrectasSuma;
		totalAnswers = PlayerPrefs.GetInt ("correctasDiv") + PlayerPrefs.GetInt ("incorrectasDiv");
		
		if (totalAnswers == 0)
			totalAnswers = totalAnswers + 1;
		
		correctsValue = (PlayerPrefs.GetInt ("correctasDiv") * 1.0f) / totalAnswers;
		incorrectsValue = 1.0f - correctsValue;
		Debug.Log ("Tiempo = "+ PlayerPrefs.GetInt ("tiempoDiv"));
		Debug.Log ("Total Respuestas = "+totalAnswers);
		Debug.Log ("Numero Correctas = "+correctsValue);
		Debug.Log ("Numero Inorrectas = "+incorrectsValue);
		
	}
	
	void Start () {
		
		timeLabel.text = "Tiempo Total = " + timeValue/60 + " min";
		correctDivPBar.value = correctsValue;
		incorrectDivPBar.value = incorrectsValue;
		correctsLabel.text = "Correctas = "+ PlayerPrefs.GetInt ("correctasDiv");
		incorrectsLabel.text = "Incorrectas = " + PlayerPrefs.GetInt ("incorrectasDiv");
		correctsLabelPercent.text =  System.Math.Round(correctsValue * 100.0f,2)+"%";
		incorrectsLabelPercent.text = System.Math.Round(incorrectsValue * 100.0f,2)+"%";
		
	}
	
}