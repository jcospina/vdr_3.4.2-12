﻿using UnityEngine;
using System.Collections;

public class PracticeNumber : MonoBehaviour {

	public int numberValue;
	public GameObject thePrefab;
	public AudioClip incorrectSound;
	public AudioClip correctSound;

	private GameObject answerNumber1;
	private GameObject answerNumber2;

	private int result;
	private string txtResultNumber;
	public PracticeCreator pCreator;


	void Start (){

	}

	void OnClick (){

		if (GameControl.time >= 0) {

			if (GameControl.isPlaying == true) {
				
				if (PracticeCreator.resultAddition <10) {
					GameControl.txtResult = numberValue.ToString();
					answerNumber1 = NGUITools.AddChild (GameObject.Find ("PanelAnswer"), thePrefab) as GameObject;
					answerNumber1.transform.position = new Vector2 (-0.5f, -0.4f);
					ResultCheck ();	
				}	

				if (PracticeCreator.resultAddition >= 10 && PracticeCreator.resultAddition < 100){
					
					if (GameControl.touchCount == 1){
						
						answerNumber1 = NGUITools.AddChild (GameObject.Find ("PanelAnswer"), thePrefab) as GameObject;
						answerNumber1.transform.position = new Vector2 (-0.7f, -0.4f);
						GameControl.txtResult = numberValue.ToString();
						GameControl.touchCount = 2;
					}
					
					else if (GameControl.touchCount == 2){
						
						answerNumber2 = NGUITools.AddChild (GameObject.Find ("PanelAnswer"), thePrefab) as GameObject;
						answerNumber2.transform.position = new Vector2 (-0.4f, -0.4f);
						GameControl.txtResult += numberValue.ToString();
						//Debug.Log ("Resultado = "+GameControl.txtResult);
						ResultCheck();
						
					}
					
				}

				if (PracticeCreator.resultAddition >= 100){

					if (GameControl.touchCount == 1){
						
						answerNumber1 = NGUITools.AddChild (GameObject.Find ("PanelAnswer"), thePrefab) as GameObject;
						answerNumber1.transform.position = new Vector2 (-0.8f, -0.4f);
						GameControl.txtResult = numberValue.ToString();
						GameControl.touchCount = 2;
					}

					else if (GameControl.touchCount == 2){
						
						answerNumber2 = NGUITools.AddChild (GameObject.Find ("PanelAnswer"), thePrefab) as GameObject;
						answerNumber2.transform.position = new Vector2 (-0.5f, -0.4f);
						GameControl.txtResult += numberValue.ToString();
						GameControl.touchCount = 3;

					}

					else if (GameControl.touchCount == 3){
						
						answerNumber2 = NGUITools.AddChild (GameObject.Find ("PanelAnswer"), thePrefab) as GameObject;
						answerNumber2.transform.position = new Vector2 (-0.2f, -0.4f);
						GameControl.txtResult += numberValue.ToString();
						GameControl.touchCount = 4;
						//Debug.Log ("Resultado = "+GameControl.txtResult);
						ResultCheck();			
					}

				}
			} 
			else {
				
				Debug.Log ("Momento!!");
			}
		}

		else {
			Debug.Log ("Tiempo Terminado!!");
		}
	}

	void ResultCheck (){

		if (int.Parse(GameControl.txtResult) == PracticeCreator.resultAddition) {
			Debug.Log ("Resultado Correcto");
			NGUITools.PlaySound(correctSound);
			GameControl.isPlaying = false;
			GameControl.touchCount = 1;
			GameControl.score = GameControl.score +10;
			GameControl.sabiduria++;
			GameControl.contSuma++;
			GameControl.correctasSuma++;
			PlayerPrefs.SetInt ("correctasSuma",GameControl.correctasSuma);
			PracticeCreator.resultAddition = 0;
			StartCoroutine(DelayWin());
		}
		else{
			Debug.Log ("Resultado Incorrecto");
			NGUITools.PlaySound(incorrectSound);
			GameControl.isPlaying = false;
			GameControl.sabiduria = 0;
			GameControl.incorrectasSuma++;
			PlayerPrefs.SetInt ("incorrectasSuma",GameControl.incorrectasSuma);
			GameControl.txtResult = "";
			GameControl.touchCount = 1;
			StartCoroutine(Delay());

		}
	}

	IEnumerator Delay(){
		yield return new WaitForSeconds(2);
		GameObject panelAnswer = GameObject.Find("PanelAnswer"); 
		NGUITools.SetActiveChildren(panelAnswer,false);
		GameControl.isPlaying = true;
	}

	IEnumerator DelayWin(){
		yield return new WaitForSeconds(2);
		GameObject panelAnswer = GameObject.Find("PanelAnswer"); 
		GameObject panelNumbers = GameObject.Find("PanelNumbers"); 
		NGUITools.SetActiveChildren(panelAnswer,false);
		NGUITools.SetActiveChildren(panelNumbers,false);
		pCreator.Creator ();
		GameControl.isPlaying = true;
	}
}
