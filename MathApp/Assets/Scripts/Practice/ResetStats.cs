﻿using UnityEngine;
using System.Collections;

public class ResetStats : MonoBehaviour {

	void OnClick(){
		//PlayerPrefs.DeleteAll ();
		PlayerPrefs.DeleteKey ("tiempoSuma");
		PlayerPrefs.DeleteKey ("tiempoResta");
		PlayerPrefs.DeleteKey ("tiempoMulti");
		PlayerPrefs.DeleteKey ("tiempoDiv");
		PlayerPrefs.DeleteKey ("correctasSuma");
		PlayerPrefs.DeleteKey ("incorrectasSuma");
		PlayerPrefs.DeleteKey ("correctasResta");
		PlayerPrefs.DeleteKey ("incorrectasResta");
		PlayerPrefs.DeleteKey ("correctasMulti");
		PlayerPrefs.DeleteKey ("incorrectasMulti");
		PlayerPrefs.DeleteKey ("correctasDiv");
		PlayerPrefs.DeleteKey ("incorrectasDiv");
		Application.LoadLevel(Application.loadedLevel);
	}
}


