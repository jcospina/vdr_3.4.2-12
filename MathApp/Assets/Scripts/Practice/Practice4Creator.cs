﻿using UnityEngine;
using System.Collections;

public class Practice4Creator : MonoBehaviour {
	
	public GameObject thePrefab0;
	public GameObject thePrefab1;
	public GameObject thePrefab2;
	public GameObject thePrefab3;
	public GameObject thePrefab4;
	public GameObject thePrefab5;
	public GameObject thePrefab6;
	public GameObject thePrefab7;
	public GameObject thePrefab8;
	public GameObject thePrefab9;
	public GameObject theSymbolPrefab;
	public GameObject theLinePrefab;
	//public int totalNumbers = 0;
	//int totalNumbers = 0;
	//int tempNumber = 1;
	int primeNumber;
	
	public static int digitCount = 1;
	public static int resultDivision = 1;
	
	private GameObject newNumber1;
	private GameObject newNumber2;
	private GameObject newLine;
	private GameObject newSymbol;

	//Logro Sabiduria
	public GameObject logro;
	public GameObject panelNumbers;

	
	void Awake (){
		
		GameControl.time = 60;
		GameControl.score = 0;
		GameControl.touchCount = 1;
		GameControl.isPlaying = true;
		GameControl.contDiv = 1;
		GameControl.sabiduria = 0;
	}

	void Start () {
		PlayerPrefs.SetInt ("tiempoDiv",GameControl.tiempoDiv+60);
		resultDivision = 1;
		Creator ();
		
	}
	
	public void Creator (){

		if (GameControl.sabiduria >= 10) {
			if (PlayerPrefs.GetInt("logro6Obtenido")==0){
				logro.SetActive (true);
				panelNumbers.transform.localScale = new Vector2 (0,1);
				Debug.Log ("Has conseguido el sombrero de sabiduría!!");
				//ControlLogros.logro6Obtenido = true;	
				PlayerPrefs.SetInt ("logro6Obtenido",1);
			}
		}


		//Resultado
		int number1 = Random.Range (1,10);
		//Divisor
		int number2 = Random.Range (1,10);
		//Dividendo
		int number3 = number1 * number2;
		Debug.Log ("Numero 1= "+number1);
		Debug.Log ("Numero 2= "+number2);
		Debug.Log ("Numero 3= "+number3);
		//totalNumbers = Random.Range (2,4);
		//totalNumbers = 2;
		//Debug.Log ("Total Numeros = "+totalNumbers);
		//tempNumber = GetPrime(resultDivision);
		resultDivision = number1;

		//Debug.Log ("Numero Inicial = "+resultDivision);

		if (number3 < 10){
			
			DrawNumber1 (number3,-0.2f,0.4f);
		}
		
		if (number3 >= 10){
			
			int digit1 = number3/10;
			int digit2 = number3%10;
			
			DrawNumber1 (digit1,-0.5f,0.4f);
			DrawNumber2 (digit2,-0.2f,0.4f);
			
		}

		if (number2 < 10){
				
			DrawNumber1 (number2,-0.2f,0.1f);
		}
			
		if (number2 >= 10){
				
				int digit1t = number2/10;
				int digit2t = number2%10;
				
				DrawNumber1 (digit1t,-0.5f,0.1f);
				DrawNumber2 (digit2t,-0.2f,0.1f);
				
		}

		DrawLine (-0.5f,-0.1f);
		DrawSymbol (-0.9f, 0.25f);
		Debug.Log ("Resultado Division = "+resultDivision);
		
	}

	void DrawLine (float posX, float posY){
		
		newLine = NGUITools.AddChild(GameObject.Find("PanelNumbers"), theLinePrefab) as GameObject;
		newLine.transform.position = new Vector2(posX,posY);
		
	}
	
	void DrawSymbol (float posX, float posY){
		
		newSymbol = NGUITools.AddChild(GameObject.Find("PanelNumbers"), theSymbolPrefab) as GameObject;
		newSymbol.transform.position = new Vector2(posX,posY);
		
	}
	
	void DrawNumber1 (int number1, float posX, float posY){
		
		switch (number1) {
			
		case 0:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab0) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 1:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab1) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 2:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab2) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 3:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab3) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 4:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab4) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 5:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab5) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 6:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab6) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 7:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab7) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 8:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab8) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 9:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab9) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
		}
		
	}
	
	void DrawNumber2 (int number2, float posX, float posY){
		
		switch (number2){
			
		case 0:
			newNumber2 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab0) as GameObject;
			newNumber2.transform.position = new Vector2(posX,posY);
			break;
			
		case 1:
			newNumber2 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab1) as GameObject;
			newNumber2.transform.position = new Vector2(posX,posY);
			break;
			
		case 2:
			newNumber2 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab2) as GameObject;
			newNumber2.transform.position = new Vector2(posX,posY);
			break;
			
		case 3:
			newNumber2 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab3) as GameObject;
			newNumber2.transform.position = new Vector2(posX,posY);
			break;
			
		case 4:
			newNumber2 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab4) as GameObject;
			newNumber2.transform.position = new Vector2(posX,posY);
			break;
			
		case 5:
			newNumber2 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab5) as GameObject;
			newNumber2.transform.position = new Vector2(posX,posY);
			break;
			
		case 6:
			newNumber2 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab6) as GameObject;
			newNumber2.transform.position = new Vector2(posX,posY);
			break;
			
		case 7:
			newNumber2 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab7) as GameObject;
			newNumber2.transform.position = new Vector2(posX,posY);
			break;
			
		case 8:
			newNumber2 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab8) as GameObject;
			newNumber2.transform.position = new Vector2(posX,posY);
			break;
			
		case 9:
			newNumber2 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab9) as GameObject;
			newNumber2.transform.position = new Vector2(posX,posY);
			break;
			
		}
		
	}
	
}
