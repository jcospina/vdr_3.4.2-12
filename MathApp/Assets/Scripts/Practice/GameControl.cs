﻿using UnityEngine;
using System.Collections;

public class GameControl : MonoBehaviour {

	public static bool isPlaying = true;
	public static int touchCount = 1;
	public static string txtResult;
	public static int score=0;
	public static float time=60;
	public static int enemyLife = 100;
	public static int sabiduria = 0;
	public static int contSuma = 1;
	public static int contResta = 1;
	public static int contMulti = 1;
	public static int contDiv = 1;
	public static int correctasSuma = 0;
	public static int incorrectasSuma = 0;
	public static int correctasResta = 0;
	public static int incorrectasResta = 0;
	public static int correctasMulti = 0;
	public static int incorrectasMulti = 0;
	public static int correctasDiv = 0;
	public static int incorrectasDiv = 0;
	public static int tiempoSuma = 0;
	public static int tiempoResta = 0;
	public static int tiempoMulti = 0;
	public static int tiempoDiv = 0;
	private UILabel scoreLabel;
	private UILabel timeLabel;

	void Awake (){

		// Leer valores iniciales Suma
		tiempoSuma = PlayerPrefs.GetInt ("tiempoSuma");
		correctasSuma = PlayerPrefs.GetInt ("correctasSuma");
		correctasSuma = PlayerPrefs.GetInt ("incorrectasSuma");
		Debug.Log ("Tiempo Suma = "+tiempoSuma);

		// Leer valores iniciales Resta
		tiempoResta = PlayerPrefs.GetInt ("tiempoResta");
		correctasResta = PlayerPrefs.GetInt ("correctasResta");
		correctasResta = PlayerPrefs.GetInt ("incorrectasResta");
		Debug.Log ("Tiempo Resta = "+tiempoResta);
	
		// Leer valores iniciales Multiplicacion
		tiempoMulti = PlayerPrefs.GetInt ("tiempoMulti");
		correctasMulti = PlayerPrefs.GetInt ("correctasMulti");
		correctasMulti = PlayerPrefs.GetInt ("incorrectasMulti");
		Debug.Log ("Tiempo Multiplicacion = "+tiempoMulti);

		// Leer valores iniciales Division
		tiempoDiv = PlayerPrefs.GetInt ("tiempoDiv");
		correctasDiv = PlayerPrefs.GetInt ("correctasDiv");
		correctasDiv = PlayerPrefs.GetInt ("incorrectasDiv");
		Debug.Log ("Tiempo Multiplicacion = "+tiempoDiv);

	}

	// Use this for initialization
	void Start () {

		scoreLabel = GameObject.Find("ScoreLabel").GetComponent<UILabel>();	
		timeLabel = GameObject.Find("TimeLabel").GetComponent<UILabel>();	
	
	}

	void Update () {
		time -= Time.deltaTime;
		if (time >= 0) {
			timeLabel.text = ""+ (int)time;
		}

		scoreLabel.text = "Sabiduría = "+sabiduria*10;
	}

}
