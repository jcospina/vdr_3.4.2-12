﻿using UnityEngine;
using System.Collections;

public class AnswerCreator3 : MonoBehaviour {
	
	public GameObject thePrefab0;
	public GameObject thePrefab1;
	public GameObject thePrefab2;
	public GameObject thePrefab3;
	public GameObject thePrefab4;
	public GameObject thePrefab5;
	public GameObject thePrefab6;
	public GameObject thePrefab7;
	public GameObject thePrefab8;
	public GameObject thePrefab9;
	public GameObject thePrefabQ;
	public GameObject symbolObject;
	public GameObject questionObject;
	public GameObject equalObject;
	
	private GameObject  newNumber1;
	private GameObject  newNumber2;
	private GameObject  newSymbol;
	private GameObject  newQuestion;
	private GameObject  newEqual;

	private UILabel aLabel;
	private UILabel bLabel;
	private UILabel cLabel;
	private UILabel dLabel;

	private float posX;
	private float posY;
	private float posXS;

	public static int productResult = 1;
	public static bool isPlaying = true;
	
	// Use this for initialization
	void Start () {
		
		aLabel = GameObject.Find("OptionA").GetComponent<UILabel>();	
		bLabel = GameObject.Find("OptionB").GetComponent<UILabel>();	
		cLabel = GameObject.Find("OptionC").GetComponent<UILabel>();	
		dLabel = GameObject.Find("OptionD").GetComponent<UILabel>();	
		
		posX = GameObject.Find("Reference").transform.position.x;
		posXS = GameObject.Find("Reference").transform.position.x;
		posY = GameObject.Find("Reference").transform.position.y + 0.3f;

		if (PlayerScorer.currentlLevelMulti < 6){
			productResult = NumberCreator3.numberList [0] * NumberCreator3.numberList [1];
			Debug.Log ("Resultado Multiplicacion = "+ productResult);
			Creator ();
			OptionsCreator (1);
		}
		
		if (PlayerScorer.currentlLevelMulti >= 6){
			productResult = NumberCreator3.numberList[2];
			Debug.Log ("Resultado Multiplicacion = "+ productResult);
			Creator2 ();
			OptionsCreator (2);
		}		
	}

	void Creator (){

		for (int i=0; i<NumberCreator3.numberList.Count-1; i++) {
			
			if (NumberCreator3.numberList[i]<10){
				DrawNumber (NumberCreator3.numberList[i], posX-0.95f,posY);
			}
			
			if (NumberCreator3.numberList[i]>=10){
				
				int digit1t = NumberCreator3.numberList[i]/10;
				int digit2t = NumberCreator3.numberList[i]%10;
				
				DrawNumber (digit1t,posX-1.05f,posY);
				DrawNumber (digit2t,posX-0.80f,posY);
			}
			
			posX = posX + 0.70f;
		}
		
		DrawSymbol (posXS-0.575f,posY);
		DrawQuestion (posXS+0.65f, posY);
		DrawEqual (posXS + 0.25f, posY);
	}

	void Creator2 (){
	

		for (int i=1; i<NumberCreator3.numberList.Count; i++) {
			
			if (NumberCreator3.numberList[i] < 10){
				DrawNumber (NumberCreator3.numberList[i], posX-0.75f,posY);
			}
			
			if (NumberCreator3.numberList[i] >= 10 && NumberCreator3.numberList[i] < 100){
				
				int digit1t = NumberCreator3.numberList[i]/10;
				int digit2t = NumberCreator3.numberList[i]%10;
				
				DrawNumber (digit1t,posX-0.95f,posY);
				DrawNumber (digit2t,posX-0.70f,posY);
			}

			if (NumberCreator3.numberList[i] >= 100){

				int digit1t = NumberCreator3.numberList[i]/100;
				int digit2t = (NumberCreator3.numberList[i]%100)/10;
				int digit3t = (NumberCreator3.numberList[i]%100)%10;
				DrawNumber (digit1t,posXS+0.55f,posY);	
				DrawNumber (digit2t,posXS+0.85f,posY);
				DrawNumber (digit3t,posXS+1.15f,posY);
			}
			
			posX = posX + 1.6f;
		}

		DrawSymbol (posXS - 0.45f, posY );
		DrawQuestion (posXS-0.15f, posY);
		DrawEqual (posXS + 0.25f, posY);
				


	}

	// Creador de opciones de respuesta
	public void OptionsCreator (int Correct){

//		if (Correct == 1)
//			productResult = productResult;
		if (Correct == 2)
			productResult = NumberCreator3.numberList [0];


		int number;
		int correctPos = Random.Range (1,4);
		
		for (int i=1; i<=4; i++) {
			
			number = Random.Range (1,productResult+15);
				if (number == productResult)
					number = number+5;
				
			switch (i){
				
			case 1:
				aLabel.text = "";
				aLabel.text = "A) = "+ number;
				break;
				
			case 2:
				bLabel.text = "";
				bLabel.text = "B) = "+ number;
				break;
				
			case 3:
				cLabel.text = "";
				cLabel.text = "C) = "+ number;
				break;
				
			case 4:
				dLabel.text = "";
				dLabel.text = "D) = "+ number;
				break;
				
			}
			
		}
		
		switch (correctPos){
			
		case 1:
			aLabel.text = "";
			aLabel.text = "A) = "+ productResult;
			AnswerNumber3.optionCorrect = "A";
			break;
			
		case 2:
			bLabel.text = "";
			bLabel.text = "B) = "+ productResult;
			AnswerNumber3.optionCorrect = "B";
			break;
			
		case 3:
			cLabel.text = "";
			cLabel.text = "C) = "+ productResult;
			AnswerNumber3.optionCorrect = "C";
			break;
			
		case 4:
			dLabel.text = "";
			dLabel.text = "D) = "+ productResult;
			AnswerNumber3.optionCorrect = "D";	
			break;
		}
		
	}
	
	void DrawSymbol (float posX, float posY){
		
		newSymbol = NGUITools.AddChild(GameObject.Find("PanelNumbers"), symbolObject) as GameObject;
		newSymbol.transform.position = new Vector2(posX,posY);		
	}

	void DrawQuestion (float posX, float posY){
		
		newQuestion = NGUITools.AddChild(GameObject.Find("PanelNumbers"), questionObject) as GameObject;
		newQuestion.transform.position = new Vector2(posX,posY);		
	}

	void DrawEqual (float posX, float posY){
		
		newEqual = NGUITools.AddChild(GameObject.Find("PanelNumbers"), equalObject) as GameObject;
		newEqual.transform.position = new Vector2(posX,posY);		
	}
	
	// Dibuja numero de NGUI
	void DrawNumber (int number1, float posX, float posY){
		
		switch (number1) {
			
		case 0:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab0) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 1:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab1) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 2:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab2) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 3:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab3) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 4:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab4) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 5:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab5) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 6:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab6) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 7:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab7) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 8:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab8) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 9:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab9) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
		}
		
	}
	
}
