﻿using UnityEngine;
using System.Collections;

public class AnswerNumber3 : MonoBehaviour {
	
	public AnswerCreator3 aCreator;
	public string option;
	public static string optionCorrect;
	
	public UISprite button;

	public AudioClip incorrectSound;
	public AudioClip correctSound;
	
	void Start () {
		AnswerCreator3.isPlaying = true;
	}
	
	void OnClick(){

		button.spriteName = "fondo_respuesta_selec";
		
		if (AnswerCreator3.isPlaying == true) {
			
			if (option == optionCorrect) {
				Debug.Log ("Respuesta Correcta");
				//PlayerScorer.contLevelMulti++;
				//if (PlayerScorer.currentlLevelMulti >= PlayerScorer.contLevelMulti)
				//	PlayerScorer.contLevelMulti = PlayerScorer.currentlLevelMulti+1;
				if (PlayerScorer.currentlLevelMulti >= PlayerPrefs.GetInt("nivelMulti")){
					PlayerScorer.contLevelMulti = PlayerScorer.contLevelMulti+1;
					PlayerPrefs.SetInt("nivelMulti",PlayerScorer.contLevelMulti);
					Debug.Log ("AQUI SETEA= "+PlayerScorer.contLevelMulti);
				}
				JoystickInput.isWin = true;
				NGUITools.PlaySound(correctSound);
				AnswerCreator3.isPlaying = false;
				TutorControl.tutoOperation = true;
				StartCoroutine (Delay ());
				
			} 
			
			else {
				Debug.Log ("Respuesta Incorrecta");	
				NGUITools.PlaySound(incorrectSound);
				//AQUI SE DEBE PERDER UNA VIDA
				//aCreator.OptionsCreator();
			}		
			
		}
		else{
			Debug.Log ("Juego Terminado");	
		}
		
	}
	
	IEnumerator Delay (){
		
		NumberCreator3.numberList.Clear();
		AnswerCreator3.productResult = 1;
		yield return new WaitForSeconds(3.0f);

		//if (PlayerScorer.contLevelMulti <=10)
		if (PlayerPrefs.GetInt("nivelMulti") <=10)
			Application.LoadLevel ("MenuNivelMulti");
		//if (PlayerScorer.currentlLevelMulti == 10 && PlayerScorer.contLevelMulti == 11) 
		if (PlayerScorer.currentlLevelMulti == 10 && PlayerPrefs.GetInt("nivelMulti") == 11) 
			Application.LoadLevel ("Enemy3");
		else 
			Application.LoadLevel ("MenuNivelMulti");

	}
	
}
