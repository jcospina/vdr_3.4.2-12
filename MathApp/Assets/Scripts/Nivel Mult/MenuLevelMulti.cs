﻿using UnityEngine;
using System.Collections;

public class MenuLevelMulti : MonoBehaviour {
	
	public UISprite sprite;
	public GameObject objectOpen;
	public GameObject objectLabel;
	public int numberLevel;
	
	void Start (){
		
		PlayerScorer.numbersCollected = 0;

		if (NumberCreator3.numberList.Count >= 0) {
			
			NumberCreator3.numberList.Clear();
		}
		
		//if (PlayerScorer.contLevelMulti>= numberLevel) {
		if (PlayerPrefs.GetInt("nivelMulti")>= numberLevel) {
			objectOpen.SetActive (true);
			objectLabel.SetActive (true);
		}
		
		//if (PlayerScorer.contLevelMulti > numberLevel){
		if (PlayerPrefs.GetInt("nivelMulti")> numberLevel) {
			objectOpen.SetActive (false);
			sprite.spriteName = "nivel_superado";
		} 

//		//if (PlayerScorer.contLevelMulti == 11) {
//		if (PlayerPrefs.GetInt("nivelMulti") == 11) {
//			Debug.Log ("Has conseguido la medalla de Multiplicación!!!");
//			//ControlLogros.logro3Obtenido = true;
//			PlayerPrefs.SetInt ("logro3Obtenido",1);
//		}
		
	}

	void OnClick (){
		PlayerScorer.currentlLevelMulti = numberLevel;
		//if (numberLevel <= PlayerScorer.contLevelMulti)
		if (numberLevel <= PlayerPrefs.GetInt("nivelMulti"))
			Application.LoadLevel ("Scene3");
	}
	
}
