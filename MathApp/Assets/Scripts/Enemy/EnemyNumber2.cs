﻿using UnityEngine;
using System.Collections;

public class EnemyNumber2 : MonoBehaviour {
	
	private GameObject answerNumber1;
	private GameObject answerNumber2;
	
	public EnemyCreator2 eCreator;
	
	public string option;
	public static string optionCorrect;
	public UISprite button;
	
	void Start (){
		button.spriteName = "fondo_respuesta";
	}
	
	void OnClick (){
		
		//button.spriteName = "fondo_respuesta_selec";
		
		if (EnemyControl.enemyLife > 0 && EnemyControl.life >0) {
			
			if (EnemyControl.isPlaying == true) {
				
				if (option == optionCorrect) {
					Debug.Log ("Respuesta Correcta");
					EnemyControl.contResta++;
					EnemyControl.enemyLife = EnemyControl.enemyLife-10;
					EnemyControl.isPlaying = false;
					StartCoroutine(DelayWin());
					
				} 
				
				else {
					Debug.Log ("Respuesta Incorrecta");	
					EnemyControl.life = EnemyControl.life -10;
					StartCoroutine(Delay());
				}		
				
			}
			else{
				Debug.Log ("Juego Terminado");	
			}		
		}
	}
	
	
	IEnumerator Delay(){
		yield return new WaitForSeconds(1);
		GameObject panelNumbers = GameObject.Find("PanelNumbers"); 
		NGUITools.SetActiveChildren(panelNumbers,false);
		eCreator.Creator();
		eCreator.OptionsCreator ();
		EnemyControl.isPlaying = true;
	}
	
	IEnumerator DelayWin(){
		yield return new WaitForSeconds(1);
		GameObject panelNumbers = GameObject.Find("PanelNumbers"); 
		NGUITools.SetActiveChildren(panelNumbers,false);
		eCreator.Creator ();
		eCreator.OptionsCreator ();
		EnemyControl.isPlaying = true;
	}
	
}