﻿using UnityEngine;
using System.Collections;

public class EnemyControl : MonoBehaviour {
	
	public static bool isPlaying = true;
	public static int touchCount = 1;
	public static string txtResult;
	public static int life;
	public static int enemyLife;
	public static int contSuma=1;
	public static int contResta=1;
	public static int contMulti=1;
	public static int contDiv=1;

	public UIProgressBar enemyPBar;
	public UIProgressBar playerPBar;

	// Estados del Jefe Enemigo
	public GameObject enemyState1;
	public GameObject enemyState2;
	public GameObject enemyState3;
	public GameObject enemyState4;
	public GameObject enemyState5;
	public GameObject enemyStateFinal;


	private UILabel lifeLabel;
	private UILabel timeLabel;
	private UILabel enemyLabel;

	public GameObject logro;
	public GameObject logroVara;
	public GameObject panelNumbers;
	public int numberEnemy;

	// Use this for initialization
	void Start () {
		
		lifeLabel = GameObject.Find("LifeLabel").GetComponent<UILabel>();	
		//timeLabel = GameObject.Find("TimeLabel").GetComponent<UILabel>();	
		enemyLabel = GameObject.Find("EnemyLabel").GetComponent<UILabel>();

		//maxHealth = enemyPBar.backgroundWidget.localSize.x;
		//Debug.Log ("Salud maxima = " +maxHealth);
		//Debug.Log ("Salud maxima = " +enemyPBar.backgroundWidget.localSize.x);
		
	}
	
	void Update () {
		//time -= Time.deltaTime;
		//if (time >= 0) {
		//	timeLabel.text = "Time = "+ (int)time;
		//}
		lifeLabel.text = "Tu Vida = "+life;
		enemyLabel.text = "Vida Jefe = "+enemyLife;
		enemyPBar.value = enemyLife / 100.0f;
		playerPBar.value = life / 100.0f;

		if (enemyLife <= 0 || life <= 0) {
			Debug.Log ("Juego Terminado");	
		}

		// *** Manejo de estados para Jefes Enemigos
		if (enemyLife >= 60 && enemyLife <= 80) {
			enemyState1.SetActive (false);
			enemyState2.SetActive (true);		
		}

		if (enemyLife >= 40 && enemyLife <= 60) {
			enemyState2.SetActive (false);
			enemyState3.SetActive (true);		
		}

		if (enemyLife >= 20 && enemyLife <= 40) {
			enemyState3.SetActive (false);
			enemyState4.SetActive (true);		
		}

		if (enemyLife >= 00 && enemyLife <= 20) {
			enemyState4.SetActive (false);
			enemyState5.SetActive (true);		
		}

		if (enemyLife <= 00) {
			enemyState5.SetActive (false);
			enemyStateFinal.SetActive (true);
			if (numberEnemy == 1){
				if (PlayerPrefs.GetInt ("logro1Obtenido") == 0){
					Debug.Log ("Has conseguido la medalla de suma!!!");
					//ControlLogros.logro1Obtenido = true;
					PlayerPrefs.SetInt ("logro1Obtenido",1);
					logro.SetActive (true);
					panelNumbers.transform.localScale = new Vector2 (0,1);
				}
			}
			if (numberEnemy == 2){
				if (PlayerPrefs.GetInt ("logro2Obtenido") == 0){
					Debug.Log ("Has conseguido la medalla de resta!!!");
					//ControlLogros.logro2Obtenido = true;
					PlayerPrefs.SetInt ("logro2Obtenido",1);
					logro.SetActive (true);
					panelNumbers.transform.localScale = new Vector2 (0,1);
				}
			}
			if (numberEnemy == 3){
				if (PlayerPrefs.GetInt ("logro3Obtenido") == 0){
					Debug.Log ("Has conseguido la medalla de multiplicacion!!!");
					//ControlLogros.logro3Obtenido = true;
					PlayerPrefs.SetInt ("logro3Obtenido",1);
					logro.SetActive (true);
					panelNumbers.transform.localScale = new Vector2 (0,1);
				}
			}
			if (numberEnemy == 4){
				if (PlayerPrefs.GetInt ("logro4Obtenido") == 0){
					Debug.Log ("Has conseguido la medalla de division!!!");
					//ControlLogros.logro4Obtenido = true;
					PlayerPrefs.SetInt ("logro4Obtenido",1);
					logro.SetActive (true);
					panelNumbers.transform.localScale = new Vector2 (0,1);
				}
			}
		}

		// *** Para conseguir la barra de poder
		if (enemyLife <= 0 && life == 100) {
			if (PlayerPrefs.GetInt ("logro5Obtenido") == 0){
				panelNumbers.transform.localScale = new Vector2 (0,1);
				logroVara.SetActive (true);
				StartCoroutine(DelayLogro());
				Debug.Log ("Has conseguido la vara de poder!!");
				//ControlLogros.logro5Obtenido = true;
				PlayerPrefs.SetInt ("logro5Obtenido",1);
			}
		}

	}

	IEnumerator DelayLogro(){
		yield return new WaitForSeconds(5);
		logro.SetActive (false);
		logroVara.SetActive (true);
	}
	
}
