﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyCreator3 : MonoBehaviour {
	
	public GameObject thePrefab0;
	public GameObject thePrefab1;
	public GameObject thePrefab2;
	public GameObject thePrefab3;
	public GameObject thePrefab4;
	public GameObject thePrefab5;
	public GameObject thePrefab6;
	public GameObject thePrefab7;
	public GameObject thePrefab8;
	public GameObject thePrefab9;
	public GameObject symbolObject;
	
	private GameObject  newNumber1;
	private GameObject  newNumber2;
	private GameObject  newSymbol;
	public static int productResult = 1;
	
	private float posX = -0.7f;
	private float posY;
	private float posXS = 0.0f ;
	private int totalNumbers = 0;
	
	private UILabel aLabel;
	private UILabel bLabel;
	private UILabel cLabel;
	private UILabel dLabel;
	
	private int randomRange1;
	private int randomRange2;
	private int rSubstraction;
	
	//private GameObject newNumber2;
	
	// Use this for initialization
	void Start () {
		
		aLabel = GameObject.Find("OptionA").GetComponent<UILabel>();	
		bLabel = GameObject.Find("OptionB").GetComponent<UILabel>();	
		cLabel = GameObject.Find("OptionC").GetComponent<UILabel>();	
		dLabel = GameObject.Find("OptionD").GetComponent<UILabel>();	
		
		EnemyControl.enemyLife = 100;
		EnemyControl.life = 100;
		EnemyControl.touchCount = 1;
		EnemyControl.isPlaying = true;
		EnemyControl.contMulti = 1;
		Creator ();
		OptionsCreator ();
		
	}
	
	public void Creator (){
		
		Debug.Log ("Valor Contador Dificultad = "+EnemyControl.contMulti);
		switch (EnemyControl.contMulti){
			
		case 1:
			randomRange1 = 1;
			randomRange2 = 5;
			totalNumbers = 2;
			break;
			
		case 2:
			randomRange1 = 5;
			randomRange2 = 10;
			totalNumbers = 2;
			break;
			
		case 3:
			randomRange1 = 10;
			randomRange2 = 15;
			totalNumbers = 2;
			break;
			
		case 4:
			randomRange1 = 15;
			randomRange2 = 20;
			totalNumbers = 2;
			break;
			
		case 5:
			randomRange1 = 20;
			randomRange2 = 25;
			totalNumbers = 2;
			break;
			
		case 6:	
			randomRange1 = 1;
			randomRange2 = 5;
			totalNumbers = 3;
			break;
			
		case 7:	
			randomRange1 = 5;
			randomRange2 = 10;
			totalNumbers = 3;
			break;
			
		case 8:	
			randomRange1 = 5;
			randomRange2 = 15;
			totalNumbers = 3;
			break;
			
		case 9:	
			randomRange1 = 1;
			randomRange2 = 20;
			totalNumbers = 3;
			break;
			
		case 10:	
			randomRange1 = 5;
			randomRange2 = 10;
			totalNumbers = 4;
			break;
			
		default:
			randomRange1 = 1;
			randomRange2 = 15;
			totalNumbers = 4;
			break;
		}
		
		
		productResult = 1;
		
		posY = 0.35f;
		int number;
		//totalNumbers = Random.Range (2,4);
		//totalNumbers = 4;
		
		switch (totalNumbers) {
			
		case 2:
			posX = -0.9f;
			posXS = -0.75f;
			break;
			
		case 3:
			posX = -1.0f;
			posXS = -0.85f;
			break;
			
		case 4:
			posX = -1.0f;
			posXS = -0.85f;
			break;
		}
		
		for (int i=1; i<=totalNumbers; i++) {
			//number = Random.Range (0,9);
			number = Random.Range (randomRange1,randomRange2);	
			productResult = productResult * number;
			if (number<10){
				DrawNumber1(number,posX-0.10f,posY);
			}
			
			if (number>=10){
				
				int digit1t = number/10;
				int digit2t = number%10;
				
				DrawNumber1 (digit1t,posX-0.125f,posY);
				DrawNumber1 (digit2t,posX,posY);
			}
			
			posX = posX + 0.40f;
			//Debug.Log ("Numero "+i+"="+number	);
			
		}
		
		for (int i=1; i<totalNumbers; i++){
			DrawSymbol (posXS,posY);
			posXS = posXS + 0.40f;
		}
		
		Debug.Log ("Resultado = "+productResult);
		
	}
	
	public void OptionsCreator (){
		
		int number;
		int correctPos = Random.Range (1,4);
		
		for (int i=1; i<=4; i++) {
			
			number = Random.Range (1,productResult+10);
			
			if (number == productResult){
				Debug.Log ("AQUI ESTABA IGUAL");
				number = number+2;
			}
			
			switch (i){
				
			case 1:
				aLabel.text = "";
				aLabel.text = "A) = "+ number;
				break;
				
			case 2:
				bLabel.text = "";
				bLabel.text = "B) = "+ number;
				break;
				
			case 3:
				cLabel.text = "";
				cLabel.text = "C) = "+ number;
				break;
				
			case 4:
				dLabel.text = "";
				dLabel.text = "D) = "+ number;
				break;
				
			}
			
		}
		
		switch (correctPos){
			
		case 1:
			aLabel.text = "";
			aLabel.text = "A) = "+ productResult;
			EnemyNumber3.optionCorrect = "A";
			break;
			
		case 2:
			bLabel.text = "";
			bLabel.text = "B) = "+ productResult;
			EnemyNumber3.optionCorrect = "B";
			break;
			
		case 3:
			cLabel.text = "";
			cLabel.text = "C) = "+ productResult;
			EnemyNumber3.optionCorrect = "C";
			break;
			
		case 4:
			dLabel.text = "";
			dLabel.text = "D) = "+ productResult;
			EnemyNumber3.optionCorrect = "D";	
			break;
		}
		
	}
	
	void DrawSymbol (float posX, float posY){
		
		newSymbol = NGUITools.AddChild(GameObject.Find("PanelNumbers"), symbolObject) as GameObject;
		newSymbol.transform.position = new Vector2(posX,posY);		
	}
	
	void DrawNumber1 (int number1, float posX, float posY){
		
		
		switch (number1) {
			
		case 0:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab0) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 1:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab1) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 2:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab2) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 3:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab3) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 4:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab4) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 5:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab5) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 6:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab6) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 7:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab7) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 8:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab8) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 9:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab9) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
		}
	}
}

