﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyCreator2 : MonoBehaviour {
	
	public GameObject thePrefab0;
	public GameObject thePrefab1;
	public GameObject thePrefab2;
	public GameObject thePrefab3;
	public GameObject thePrefab4;
	public GameObject thePrefab5;
	public GameObject thePrefab6;
	public GameObject thePrefab7;
	public GameObject thePrefab8;
	public GameObject thePrefab9;
	public GameObject symbolObject;
	
	private GameObject  newNumber1;
	private GameObject  newNumber2;
	private GameObject  newSymbol;
	public static int substractionResult;
	
	private float posX = -0.7f;
	private float posY;
	private float posXS = 0.0f ;
	private int totalNumbers = 0;
	
	private UILabel aLabel;
	private UILabel bLabel;
	private UILabel cLabel;
	private UILabel dLabel;
	
	private int randomRange1;
	private int randomRange2;
	
	//private GameObject newNumber2;
	
	// Use this for initialization
	void Start () {
		
		aLabel = GameObject.Find("OptionA").GetComponent<UILabel>();	
		bLabel = GameObject.Find("OptionB").GetComponent<UILabel>();	
		cLabel = GameObject.Find("OptionC").GetComponent<UILabel>();	
		dLabel = GameObject.Find("OptionD").GetComponent<UILabel>();	
		
		EnemyControl.enemyLife = 100;
		EnemyControl.life = 100;
		EnemyControl.touchCount = 1;
		EnemyControl.isPlaying = true;
		EnemyControl.contResta = 1;
		Creator ();
		OptionsCreator ();
		
	}
	
	public void Creator (){
		
		Debug.Log ("Valor Contador Dificultad = "+EnemyControl.contResta);
		switch (EnemyControl.contResta){
			
		case 1:
			substractionResult = Random.Range (10,15);
			break;
			
		case 2:
			substractionResult = Random.Range (15,25);
			break;
			
		case 3:
			substractionResult = Random.Range (25,35);
			break;
			
		case 4:
			substractionResult = Random.Range (35,45);
			break;
			
		case 5:
			substractionResult = Random.Range (45,55);
			break;
			
		case 6:	
			substractionResult = Random.Range (55,65);
			break;
			
		case 7:	
			substractionResult = Random.Range (65,70);
			break;
			
		case 8:	
			substractionResult = Random.Range (70,75);
			break;
			
		case 9:	
			substractionResult = Random.Range (75,80);
			break;
			
		case 10:	
			substractionResult = Random.Range (80,90);
			break;
			
		default:
			substractionResult = Random.Range (90,99);
			break;
		}

		posY = 0.35f;

		if (substractionResult < 10){
			DrawNumber1(substractionResult,-1.0f,posY);
		}

		if (substractionResult >= 10){
			int digit1t = substractionResult/10;
			int digit2t = substractionResult%10;
			
			DrawNumber1 (digit1t,-1.2f,posY);
			DrawNumber1 (digit2t,-1.05f,posY);
		}

		int number;
		//totalNumbers = Random.Range (2,4);
		totalNumbers = 4;
		
		switch (totalNumbers) {
			
		case 2:
			posX = -0.9f;
			posXS = -0.6f;
			break;
			
		case 3:
			posX = -1.2f;
			posXS = -0.90f;
			break;
			
		case 4:
			posX = -0.6f;
			posXS = -0.85f;
			break;
		}
		
		for (int i=1; i<totalNumbers; i++) {

			number = Random.Range (1, substractionResult-5);	

			if (number<10){
				DrawNumber1(number,posX-0.10f,posY);
			}
			
			if (number>=10){
				
				int digit1t = number/10;
				int digit2t = number%10;
				
				DrawNumber1 (digit1t,posX-0.125f,posY);
				DrawNumber1 (digit2t,posX,posY);
			}
			
			posX = posX + 0.40f;
			substractionResult = substractionResult - number;
		}
		
		for (int i=1; i<totalNumbers; i++){
			DrawSymbol (posXS,posY);
			posXS = posXS + 0.40f;
		}
		
		Debug.Log ("Resultado = "+ substractionResult);
		
	}
	
	public void OptionsCreator (){
		
		int number;
		int correctPos = Random.Range (1,4);
		
		for (int i=1; i<=4; i++) {
			
			number = Random.Range (1,substractionResult+10);
			
			if (number == substractionResult){
				Debug.Log ("AQUI ESTABA IGUAL");
				number = number+2;
			}
			
			switch (i){
				
			case 1:
				aLabel.text = "";
				aLabel.text = "A) = "+ number;
				break;
				
			case 2:
				bLabel.text = "";
				bLabel.text = "B) = "+ number;
				break;
				
			case 3:
				cLabel.text = "";
				cLabel.text = "C) = "+ number;
				break;
				
			case 4:
				dLabel.text = "";
				dLabel.text = "D) = "+ number;
				break;
				
			}
			
		}
		
		switch (correctPos){
			
		case 1:
			aLabel.text = "";
			aLabel.text = "A) = "+ substractionResult;
			EnemyNumber2.optionCorrect = "A";
			break;
			
		case 2:
			bLabel.text = "";
			bLabel.text = "B) = "+ substractionResult;
			EnemyNumber2.optionCorrect = "B";
			break;
			
		case 3:
			cLabel.text = "";
			cLabel.text = "C) = "+ substractionResult;
			EnemyNumber2.optionCorrect = "C";
			break;
			
		case 4:
			dLabel.text = "";
			dLabel.text = "D) = "+ substractionResult;
			EnemyNumber2.optionCorrect = "D";	
			break;
		}
		
	}
	
	void DrawSymbol (float posX, float posY){
		
		newSymbol = NGUITools.AddChild(GameObject.Find("PanelNumbers"), symbolObject) as GameObject;
		newSymbol.transform.position = new Vector2(posX,posY);		
	}
	
	void DrawNumber1 (int number1, float posX, float posY){
		
		
		switch (number1) {
			
		case 0:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab0) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 1:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab1) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 2:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab2) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 3:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab3) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 4:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab4) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 5:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab5) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 6:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab6) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 7:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab7) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 8:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab8) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
			
		case 9:
			newNumber1 = NGUITools.AddChild(GameObject.Find("PanelNumbers"), thePrefab9) as GameObject;
			newNumber1.transform.position = new Vector2(posX,posY);
			break;
		}
	}
}

