﻿using UnityEngine;
using System.Collections;

public class ControlLogros : MonoBehaviour {

	public GameObject objectLogro1;
	public GameObject objectLogro2;
	public GameObject objectLogro3;
	public GameObject objectLogro4;
	public GameObject objectLogro5;
	public GameObject objectLogro6;
	public GameObject objectLogro7;
	public GameObject objectLogro8;
	public GameObject objectLogro9;
	public GameObject objectLogro10;


//	public static bool logro1Obtenido;
//	public static bool logro2Obtenido;
//	public static bool logro3Obtenido;
//	public static bool logro4Obtenido;
//	public static bool logro5Obtenido;
//	public static bool logro6Obtenido;
//	public static bool logro7Obtenido;
//	public static bool logro8Obtenido;
//	public static bool logro9Obtenido;
//	public static bool logro10Obtenido;



	void Start () {


		//if (logro1Obtenido){
		if (PlayerPrefs.GetInt("logro1Obtenido") == 1){
			//Medalla de Suma
			objectLogro1.SetActive (true);
		} 

		if (PlayerPrefs.GetInt("logro2Obtenido") == 1){
			//Medalla de Resta
			objectLogro2.SetActive (true);
		} 

		if (PlayerPrefs.GetInt("logro3Obtenido") == 1){
			//Medalla de Multiplicación
			objectLogro3.SetActive (true);
		} 

		if (PlayerPrefs.GetInt("logro4Obtenido") == 1){
			//Medalla de División
			objectLogro4.SetActive (true);
		} 

		if (PlayerPrefs.GetInt("logro5Obtenido") == 1){
			//Vara de Poder (Vencer jefe sin fallar ninguna operación)
			objectLogro5.SetActive (true);
		} 

		if (PlayerPrefs.GetInt("logro6Obtenido") == 1){
			//Sombrero de sabiduría (10 operaciones sin fallar en modo práctica)
			objectLogro6.SetActive (true);
		} 

		if (PlayerPrefs.GetInt("logro7Obtenido") == 1){
			//Dije Multicultural (Jugar con cada personaje de cada etnia, hombre o mujer)
			objectLogro7.SetActive (true);
		} 

		if (PlayerPrefs.GetInt("logro8Obtenido") == 1){
			//Tambor de la persistencia (Reintentar un nivel 3 veces)
			objectLogro8.SetActive (true);
		} 

		if (PlayerPrefs.GetInt("logro9Obtenido") == 1){
			//Alpargatas de Juan Tama (Recorrer 100 Niveles, cada nivel son 250m)
			objectLogro9.SetActive (true);
		} 

		if (PlayerPrefs.GetInt("logro1Obtenido") == 1 && PlayerPrefs.GetInt("logro3Obtenido") == 1 && PlayerPrefs.GetInt("logro3Obtenido") == 1 && PlayerPrefs.GetInt("logro4Obtenido") == 1){
			//Trucha Dorada (Al completar todos los niveles del juego) 
			objectLogro10.SetActive (true);
		} 
	}

}
