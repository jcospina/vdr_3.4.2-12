﻿using UnityEngine;
using System.Collections;

public class PauseWindowControl : MonoBehaviour {


	public GameObject pauseCamera;

	void OnClick (){

		pauseCamera.SetActive (false);
		Time.timeScale = 1.0f;
	}

}
