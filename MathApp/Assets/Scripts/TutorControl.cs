﻿using UnityEngine;
using System.Collections;

public class TutorControl : MonoBehaviour {

	public static int tutoCont = 1;
	public static bool tutoMov = false;
	public static bool tutoJump = false;
	public static bool tutoCollect = false;
	public static bool tutoPause = false;
	public static bool tutoOperation = false;

	public GameObject lblMov;
	public GameObject lblJump;
	public GameObject lblCollect;
	public GameObject lblPause;
	public GameObject lblOperation;
	public GameObject lblContinua;

	public GameObject pauseButton;
	public GameObject pauseButtonTween;

	public Animator animJoystick;

	void Start () {
		animJoystick = GameObject.Find ("SingleJoystick").GetComponent <Animator>();
	}
	
	void Update () {

		switch (tutoCont){

			case 1:
			StartCoroutine (TutoMov());
			break;

			case 2:
			StartCoroutine (TutoJump());
			break;

			case 3:
			StartCoroutine (TutoCollect());
			break;

			case 4:
			StartCoroutine (TutoPause());
			break;

			case 5:
			StartCoroutine (TutoOperation());
			break;
		}
	
	}

	IEnumerator TutoMov (){
		animJoystick.SetTrigger ("JOn");
		lblMov.SetActive (true);
		if (tutoMov == true){
			yield return new WaitForSeconds(2);
			animJoystick.SetTrigger ("JOff");
			lblMov.SetActive (false);
			tutoCont = 2;
		}
	}

	IEnumerator TutoJump (){
		lblJump.SetActive (true);
		if (tutoJump == true) {
			yield return new WaitForSeconds(2);
			lblJump.SetActive (false);
			tutoCont = 3;
		}
			
	}

	IEnumerator TutoCollect (){
		lblCollect.SetActive (true);
		if (tutoCollect == true) {
			yield return new WaitForSeconds(2);
			lblCollect.SetActive (false);
			tutoCont = 4;
		}
	}

	IEnumerator TutoPause (){
		lblPause.SetActive (true);
		pauseButton.SetActive (false);
		pauseButtonTween.SetActive (true);
		if (tutoPause == true) {
			yield return new WaitForSeconds(2);
			pauseButton.SetActive (true);
			pauseButtonTween.SetActive (false);
			lblPause.SetActive (false);
			lblContinua.SetActive (true);
			tutoCont = 0;
		}
	}

	IEnumerator TutoOperation (){
		lblContinua.SetActive (false);
		lblOperation.SetActive (true);
		if (tutoOperation == true) {
			yield return new WaitForSeconds(2);
			lblOperation.SetActive (false);
			PlayerPrefs.SetInt ("tutorialChecked",1);
		}
	}

}
