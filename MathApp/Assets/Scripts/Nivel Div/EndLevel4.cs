﻿using UnityEngine;
using System.Collections;

public class EndLevel4 : MonoBehaviour {
	
	public GameObject thisObject;
	public GameObject mainCamera;
	public GameObject nguiCamera;
	
	private int totalNumbers;
	
	
	void Start () {
		
	}
	
	void Update () {
		
	}
	
	void OnTriggerEnter2D (Collider2D other)
		
	{
		totalNumbers = NumberCreator4.tNumbers;
		
		//Debug.Log ("ESTOS SON= " + totalNumbers);
		
		if(other.gameObject.tag == "Player")
		{
			if (totalNumbers == PlayerScorer.numbersCollected){
				
				Debug.Log("SI has recogido todos los numeros");
				JoystickInput.isEndLevel = true;
				//mainCamera.SetActive (false);
				//Suma Kilometros Para logro de Alpargatas de Juan Tama
				PlayerScorer.kilometros++;
				TutorControl.tutoCont = 5;
				nguiCamera.SetActive (true);
				Destroy (thisObject);
			}
			else{
				Debug.Log("NO has recogido todos los numeros");
			}
		}
	}
	
}
