﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NumberCreator4 : MonoBehaviour {


	public GameObject level1;
	public GameObject level2;
	public GameObject level3;
	public GameObject level4;
	public GameObject level5;
	public GameObject level6;
	public GameObject level7;
	public GameObject level8;
	public GameObject level9;
	public GameObject level10;
	
	public UILabel levelLabel;

	public GameObject numberObject0;
	public GameObject numberObject1;
	public GameObject numberObject2;
	public GameObject numberObject3;
	public GameObject numberObject4;
	public GameObject numberObject5;
	public GameObject numberObject6;
	public GameObject numberObject7;
	public GameObject numberObject8;
	public GameObject numberObject9;
	public GameObject platformObject;
	public int totalNumbers;
	
//	private float posX = 5.0f;
	//private float posY = 0.0f;
	private GameObject  newNumber;
	private GameObject  newNumber2;
	private GameObject  newPlatform;

	private int numberRange1 = 1;
	private int numberRange2 = 1;
	private int numberRandom;

	public static int tempDiv=1;
	public static  List<int> numberList = new List<int>();
	public static int tNumbers;
	
	void Awake (){
		
		//Define el rango para los numeros aleatorios de la operacion
		if (PlayerScorer.currentlLevelDiv == 1) {
			numberRange1 = 1;	
			numberRange2 = 10;
			levelLabel.text = "Nivel 4-1";
			level1.SetActive (true);
		}
		
		if (PlayerScorer.currentlLevelDiv == 2){
			numberRange1 = 5;
			numberRange2 = 10;
			levelLabel.text = "Nivel 4-2";
			level2.SetActive (true);
		}
		
		if (PlayerScorer.currentlLevelDiv== 3){
			numberRange1 = 5;
			numberRange2 = 15;
			levelLabel.text = "Nivel 4-3";
			level3.SetActive (true);
		}
		
		if (PlayerScorer.currentlLevelDiv == 4){
			numberRange1 = 10;
			numberRange2 = 20;
			levelLabel.text = "Nivel 4-4";
			level4.SetActive (true);
		}
		
		if (PlayerScorer.currentlLevelDiv == 5){
			numberRange1 = 15;
			numberRange2 = 25;
			levelLabel.text = "Nivel 4-5";
			level5.SetActive (true);
		}
		
		if (PlayerScorer.currentlLevelDiv == 6){
			numberRange1 = 1;
			numberRange2 = 10;
			levelLabel.text = "Nivel 4-6";
			level6.SetActive (true);
		}
		
		if (PlayerScorer.currentlLevelDiv == 7){
			numberRange1 = 5;
			numberRange2 = 10;
			levelLabel.text = "Nivel 4-7";
			level7.SetActive (true);
		}
		
		if (PlayerScorer.currentlLevelDiv == 8){
			numberRange1 = 10;
			numberRange2 = 15;
			levelLabel.text = "Nivel 4-8";
			level8.SetActive (true);
		}
		
		if (PlayerScorer.currentlLevelDiv== 9){
			numberRange1 = 10;
			numberRange2 = 20;
			levelLabel.text = "Nivel 4-9";
			level9.SetActive (true);
		}
		
		if (PlayerScorer.currentlLevelDiv == 10){
			numberRange1 = 15;
			numberRange2 = 25;
			levelLabel.text = "Nivel 4-10";
			level10.SetActive (true);
		}
	}

	void Start () {
		StartCoroutine(Creator());
		Debug.Log ("Nivel "+PlayerScorer.currentlLevelDiv);
	}
	
	IEnumerator Creator (){
		
		for (int i=0; i<totalNumbers; i++){

			numberRandom = Random.Range (numberRange1,numberRange2);
			yield return new WaitForSeconds(0.1f);
			numberList.Add (numberRandom);
			tempDiv = tempDiv * numberList[i];
		}
		
		numberList.Add (tempDiv);

		for (int i=0; i<numberList.Count; i++){
			Debug.Log ("Numero "+i+"= "+numberList[i]);
		};

		if (PlayerScorer.currentlLevelDiv < 6)
			CreateNumbers ();
		
		if (PlayerScorer.currentlLevelDiv >= 6)
			CreateNumbers2 ();
		
		Debug.Log ("Total Numeros Bloques: "+tNumbers);
	}

	// Dibuja los 2 números para obtener la respuesta de la operacion (Hasta el nivel 5 de Division)
	void CreateNumbers (){
		
		float posXDraw1 = Random.Range (8,15);
		float posXDraw2 = Random.Range (25,42);
		float posYDraw1 = Random.Range (1,10);
		float posYDraw2 = Random.Range (1,10);

		// Para que no se superpongan con las plataformas aleatorias altas
		if  (PlayerScorer.currentlLevelDiv == 3 || PlayerScorer.currentlLevelDiv == 5){
			posYDraw1 = 1;
			posYDraw2 = 1;
		}

		if  (PlayerScorer.currentlLevelDiv == 4){
			posYDraw1 = 11;
			posYDraw2 = 11;
		}

		// Posiciones como tal 
		
		if (posYDraw1 <= 5){
			posYDraw1 = 1.0f;
		}
		
		if (posYDraw1 > 5 && posYDraw1 <= 10){
			posYDraw1 = 3.2f;
			CreatePlatform (posXDraw1, 0.6f);
		}
		
		if (posYDraw1 == 11) {
			posYDraw1 = 3.2f;
		}
		
		if (posYDraw2 <= 5){
			posYDraw2 = 1.0f;
		}
		
		if (posYDraw2 > 5 && posYDraw2 <= 10){
			posYDraw2 = 3.2f;
			CreatePlatform (posXDraw2, 0.6f);
		}
		
		if (posYDraw2 == 11) {
			posYDraw2 = 3.2f;
		}
		
		if (numberList[2] < 10){
			DrawNumber (numberList[2],posXDraw1,posYDraw1);	
			tNumbers++;
		}
		if (numberList[2] >= 10 && numberList[2]<100){
			int digit1t = numberList[2]/10;
			int digit2t = numberList[2]%10;
			DrawNumber (digit1t,posXDraw1-0.4f,posYDraw1);	
			DrawNumber (digit2t,posXDraw1+0.5f,posYDraw1);	
			tNumbers = tNumbers + 2;
		}

		if (numberList[2] >= 100){
			
			int digit1t = numberList[2]/100;
			int digit2t = (numberList[2]%100)/10;
			int digit3t = (numberList[2]%100)%10;
			DrawNumber (digit1t,posXDraw1-0.7f,posYDraw1);	
			DrawNumber (digit2t,posXDraw1+0.2f,posYDraw1);
			DrawNumber (digit3t,posXDraw1+1.1f,posYDraw1);
			tNumbers = tNumbers + 3;
		}
		
		if (numberList[1] < 10){
			DrawNumber (numberList[1],posXDraw2,posYDraw2);	
			tNumbers++;
		}
		if (numberList[1] >= 10){
			int digit1t = numberList[1]/10;
			int digit2t = numberList[1]%10;
			DrawNumber (digit1t,posXDraw2-0.4f,posYDraw2);	
			DrawNumber (digit2t,posXDraw2+0.5f,posYDraw2);	
			tNumbers = tNumbers + 2;
		}
		
	}

	// Dibuja alguno de los 2 números, oculta el otro y dibuja la respuesta (A partir del nivel 6 de Division)
	void CreateNumbers2 (){
		
		float posXDraw1 = Random.Range (8,15);
		float posXDraw2 = Random.Range (25,42);
		float posYDraw1 = Random.Range (1,10);
		float posYDraw2 = Random.Range (1,10);

		// Para que no se superpongan con las plataformas aleatorias altas
		if  (PlayerScorer.currentlLevelDiv == 6){
			posYDraw1 = 1;
			posYDraw2 = 11;
		}
		
		if  (PlayerScorer.currentlLevelDiv == 7 || PlayerScorer.currentlLevelDiv == 10){
			posYDraw1 = 11;
			posYDraw2 = 11;
		}

		if  (PlayerScorer.currentlLevelDiv == 8 || PlayerScorer.currentlLevelDiv == 9){
			posYDraw1 = 1;
			posYDraw2 = 1;
		}

		// Posiciones como tal 
		
		if (posYDraw1 <= 5){
			posYDraw1 = 1.0f;
		}
		
		if (posYDraw1 > 5 && posYDraw1 <= 10){
			posYDraw1 = 3.2f;
			CreatePlatform (posXDraw1, 0.6f);
		}
		
		if (posYDraw1 == 11) {
			posYDraw1 = 3.2f;
		}
		
		if (posYDraw2 <= 5){
			posYDraw2 = 1.0f;
		}
		
		if (posYDraw2 > 5 && posYDraw2 <= 10){
			posYDraw2 = 3.2f;
			CreatePlatform (posXDraw2, 0.6f);
		}
		
		if (posYDraw2 == 11) {
			posYDraw2 = 3.2f;
		}
		
		if (numberList[2] < 10){
			
			DrawNumber (numberList[2],posXDraw1,posYDraw1);	
			tNumbers++;
		}
		if (numberList[2] >= 10 && numberList[2] < 100){
			
			int digit1t = numberList[2]/10;
			int digit2t = numberList[2]%10;
			DrawNumber (digit1t,posXDraw1-0.4f,posYDraw1);	
			DrawNumber (digit2t,posXDraw1+0.5f,posYDraw1);
			tNumbers = tNumbers + 2;
		}

		if (numberList[2] >= 100){
			
			int digit1t = numberList[2]/100;
			int digit2t = (numberList[2]%100)/10;
			int digit3t = (numberList[2]%100)%10;
			DrawNumber (digit1t,posXDraw1-0.7f,posYDraw1);	
			DrawNumber (digit2t,posXDraw1+0.2f,posYDraw1);
			DrawNumber (digit3t,posXDraw1+1.1f,posYDraw1);
			tNumbers = tNumbers + 3;
		}
		
		if (numberList[0] < 10){
			
			DrawNumber (numberList[0],posXDraw2,posYDraw2);	
			tNumbers++;
		}
		
		if (numberList[0] >= 10){
			
			int digit1t = numberList[0]/10;
			int digit2t = numberList[0]%10;
			DrawNumber (digit1t,posXDraw2-0.4f,posYDraw2);	
			DrawNumber (digit2t,posXDraw2+0.5f,posYDraw2);
			tNumbers = tNumbers + 2;
		}

	}

	// Dibuja Plataformas para alcanzar numeros muy altos
	void CreatePlatform (float posX, float posY){
		
		newPlatform = Instantiate (platformObject) as GameObject;
		newPlatform.transform.position = new Vector2(posX, posY);
	}

	// Recibe como parametro un numero y lo dibuja
	void DrawNumber (int number, float posX, float posY){
		
		if (number == 0){
			newNumber = Instantiate (numberObject0) as GameObject;
		}
		
		if (number == 1){
			newNumber = Instantiate (numberObject1) as GameObject;
		}
		
		if (number == 2){
			newNumber = Instantiate (numberObject2) as GameObject;
		}
		
		if (number == 3){
			newNumber = Instantiate (numberObject3) as GameObject;
		}
		
		if (number == 4){
			newNumber = Instantiate (numberObject4) as GameObject;
		}
		
		if (number == 5){
			newNumber = Instantiate (numberObject5) as GameObject;
		}
		
		if (number == 6){
			newNumber = Instantiate (numberObject6) as GameObject;
		}
		
		if (number == 7){
			newNumber = Instantiate (numberObject7) as GameObject;
		}
		
		if (number == 8){
			newNumber = Instantiate (numberObject8) as GameObject;
		}
		
		if (number == 9){
			newNumber = Instantiate (numberObject9) as GameObject;
		}
		
		newNumber.transform.position = new Vector2(posX, posY);	
	}
	
	
}
