﻿using UnityEngine;
using System.Collections;

public class AnswerNumber4 : MonoBehaviour {
	
	public AnswerCreator4 aCreator;
	public string option;
	public static string optionCorrect;
	
	public AudioClip incorrectSound;
	public AudioClip correctSound;

	public UISprite button;

	void Start () {
		AnswerCreator4.isPlaying = true;
	}
	
	void OnClick(){

		button.spriteName = "fondo_respuesta_selec";
		
		if (AnswerCreator4.isPlaying == true) {
			
			if (option == optionCorrect) {
				Debug.Log ("Respuesta Correcta");
				//PlayerScorer.contLevelDiv++;
				//if (PlayerScorer.currentlLevelDiv >= PlayerScorer.contLevelDiv)
				//	PlayerScorer.contLevelDiv = PlayerScorer.currentlLevelDiv+1;
				if (PlayerScorer.currentlLevelDiv >= PlayerPrefs.GetInt("nivelDiv")){
					PlayerScorer.contLevelDiv = PlayerScorer.contLevelDiv+1;
					PlayerPrefs.SetInt("nivelDiv",PlayerScorer.contLevelDiv);
					Debug.Log ("AQUI SETEA= "+PlayerScorer.contLevelDiv);
				}
				JoystickInput.isWin = true;
				NGUITools.PlaySound(correctSound);
				AnswerCreator4.isPlaying = false;
				TutorControl.tutoOperation = true;
				StartCoroutine (Delay ());
				
			} 
			
			else {
				Debug.Log ("Respuesta Incorrecta");	
				NGUITools.PlaySound(incorrectSound);
				//AQUI SE DEBE PERDER UNA VIDA
				//aCreator.OptionsCreator();
			}		
			
		}
		else{
			Debug.Log ("Juego Terminado");	
		}
		
	}
	
	IEnumerator Delay (){
		
		NumberCreator4.numberList.Clear();
		AnswerCreator4.divisionResult = 1;
		yield return new WaitForSeconds(3.0f);
		//if (PlayerScorer.contLevelDiv <=10)
		if (PlayerPrefs.GetInt("nivelDiv") <=10)
			Application.LoadLevel ("MenuNivelDiv");
		//if (PlayerScorer.currentlLevelDiv == 10 && PlayerScorer.contLevelDiv == 11) 
		if (PlayerScorer.currentlLevelDiv == 10 && PlayerPrefs.GetInt("nivelDiv") == 11) 
			Application.LoadLevel ("Enemy4");
		else 
			Application.LoadLevel ("MenuNivelDiv");
	}
	
}
