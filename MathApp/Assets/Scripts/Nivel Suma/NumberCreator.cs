﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NumberCreator : MonoBehaviour {

	public GameObject level1;
	public GameObject level2;
	public GameObject level3;
	public GameObject level4;
	public GameObject level5;
	public GameObject level6;
	public GameObject level7;
	public GameObject level8;
	public GameObject level9;
	public GameObject level10;

	public UILabel levelLabel;

	public GameObject numberObject0;
	public GameObject numberObject1;
	public GameObject numberObject2;
	public GameObject numberObject3;
	public GameObject numberObject4;
	public GameObject numberObject5;
	public GameObject numberObject6;
	public GameObject numberObject7;
	public GameObject numberObject8;
	public GameObject numberObject9;
	public GameObject platformObject;
	public int totalNumbers;

	private float posX = 10.0f;
	private float posY = 0.0f;
	private GameObject  newNumber;
	private GameObject  newNumber2;
	private GameObject  newPlatform;
	private float tempX1;
	private Number number;
	private int numberRandom;

	public static  List<int> numberList = new List<int>();
	public static int tNumbers;

	void Awake (){

		//Debug.Log ("Level = "+Player);
		if (PlayerScorer.currentlLevelSuma == 1) {
			levelLabel.text = "Nivel 1-1";
			level1.SetActive (true);
		}
		if (PlayerScorer.currentlLevelSuma == 2) {
			//level1.SetActive (false);
			levelLabel.text = "Nivel 1-2";
			level2.SetActive (true);
		}
		if (PlayerScorer.currentlLevelSuma == 3) {
			//level2.SetActive (false);
			levelLabel.text = "Nivel 1-3";
			level3.SetActive (true);
		}
		if (PlayerScorer.currentlLevelSuma == 4) {
			//level3.SetActive (false);
			levelLabel.text = "Nivel 1-4";
			level4.SetActive (true);
		}
		if (PlayerScorer.currentlLevelSuma == 5) {
			//level4.SetActive (false);
			levelLabel.text = "Nivel 1-5";
			level5.SetActive (true);
		}
		if (PlayerScorer.currentlLevelSuma == 6) {
			//level5.SetActive (false);
			levelLabel.text = "Nivel 1-6";
			level6.SetActive (true);
		}
		if (PlayerScorer.currentlLevelSuma == 7) {
			//level6.SetActive (false);
			levelLabel.text = "Nivel 1-7";
			level7.SetActive (true);
		}
		if (PlayerScorer.currentlLevelSuma == 8) {
			//level7.SetActive (false);
			levelLabel.text = "Nivel 1-8";
			level8.SetActive (true);
		}
		if (PlayerScorer.currentlLevelSuma == 9) {
			//level8.SetActive (false);
			levelLabel.text = "Nivel 1-9";
			level9.SetActive (true);
		}
		if (PlayerScorer.currentlLevelSuma == 10) {
			//level9.SetActive (false);
			levelLabel.text = "Nivel 1-10";
			level10.SetActive (true);
		}
	
	}

	void Start () {
		StartCoroutine(Creator());
		Debug.Log ("Nivel "+PlayerScorer.currentlLevelSuma);
	}


	void CreatePlatform (float posX, float posY){

		newPlatform = Instantiate (platformObject) as GameObject;
		newPlatform.transform.position = new Vector2(posX, posY);
	} 

	IEnumerator Creator (){
					
		for (int i=0; i<4; i++){

			int posYRandom = Random.Range (1,10);
			//Debug.Log (posYRandom);

			if (PlayerScorer.currentlLevelSuma == 1)
				numberRandom = Random.Range (1,5);

			if (PlayerScorer.currentlLevelSuma == 2)
				numberRandom = Random.Range (1,10);

			if (PlayerScorer.currentlLevelSuma == 3)
				numberRandom = Random.Range (1,15);

			if (PlayerScorer.currentlLevelSuma == 4){
				numberRandom = Random.Range (1,20);
				posYRandom = 1;
			}

			if (PlayerScorer.currentlLevelSuma == 5)
				numberRandom = Random.Range (1,25);

			if (PlayerScorer.currentlLevelSuma == 6)
				numberRandom = Random.Range (5,30);

			if (PlayerScorer.currentlLevelSuma == 7){
				numberRandom = Random.Range (5,35);
				posYRandom = 0;
			}

			if (PlayerScorer.currentlLevelSuma == 8){
				numberRandom = Random.Range (10,40);
				posYRandom = 0;
			}

			if (PlayerScorer.currentlLevelSuma == 9){
				numberRandom = Random.Range (10,50);
				posYRandom = 1;
			}

			if (PlayerScorer.currentlLevelSuma == 10){
				numberRandom = Random.Range (10,60);
				posYRandom = 0;
			}

			posX = Random.Range(posX,posX+7.0f);

			if (posX >=43.0f){
				posX = 42.0f;
			}

			if (posYRandom <= 5){
				posY = 1.0f;
			}

			if (posYRandom == 0){
				posY = 3.2f;
			}

			if (posYRandom > 5){
				posY = 3.2f;
				CreatePlatform (posX, 0.5f);
			}


			if (numberRandom <10){
				DrawNumber (numberRandom,posX,posY);
			}

			if (numberRandom >=10){

				int digit1t = numberRandom/10;
				int digit2t = numberRandom%10;

				DrawNumber (digit1t,posX-0.4f,posY);
				DrawNumber (digit2t,posX+0.5f,posY);

			}
			posX = posX + 7.0f;
			yield return new WaitForSeconds(0.1f);
			numberList.Add (numberRandom);
			if (numberRandom <10){
				tNumbers++;
			}
			
			if (numberRandom >=10){
				tNumbers = tNumbers+2;
			}

		}

		//Debug.Log ("Total Numeros Suma: "+numberList.Count);
		//Debug.Log ("Total Numeros Bloques: "+tNumbers);
//
//		for (int i=0; i<numberList.Count; i++){
//			Debug.Log ("Numero "+i+"= "+numberList[i]);
//		};

	}

	void DrawNumber (int number, float posX, float posY){

		if (number == 0){
			newNumber = Instantiate (numberObject0) as GameObject;
		}

		if (number == 1){
			newNumber = Instantiate (numberObject1) as GameObject;
		}
		
		if (number == 2){
			newNumber = Instantiate (numberObject2) as GameObject;
		}
		
		if (number == 3){
			newNumber = Instantiate (numberObject3) as GameObject;
		}
		
		if (number == 4){
			newNumber = Instantiate (numberObject4) as GameObject;
		}
		
		if (number == 5){
			newNumber = Instantiate (numberObject5) as GameObject;
		}
		
		if (number == 6){
			newNumber = Instantiate (numberObject6) as GameObject;
		}
		
		if (number == 7){
			newNumber = Instantiate (numberObject7) as GameObject;
		}
		
		if (number == 8){
			newNumber = Instantiate (numberObject8) as GameObject;
		}
		
		if (number == 9){
			newNumber = Instantiate (numberObject9) as GameObject;
		}

		newNumber.transform.position = new Vector2(posX, posY);	
	}


}
