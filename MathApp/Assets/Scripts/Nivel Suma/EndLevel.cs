﻿using UnityEngine;
using System.Collections;

public class EndLevel : MonoBehaviour {

	public GameObject thisObject;

	public GameObject mainCamera;
	public GameObject nguiCamera;

	private int totalNumbers;


	void Start () {

	}
	
	void Update () {

	}

	void OnTriggerEnter2D (Collider2D other)

	{
		totalNumbers = NumberCreator.tNumbers;

		//Debug.Log ("ESTOS SON= " + totalNumbers);

		if(other.gameObject.tag == "Player")
		{
			if (totalNumbers == PlayerScorer.numbersCollected){

				Debug.Log("SI has recogido todos los numeros");
				JoystickInput.isEndLevel = true;
				//Suma Kilometros Para logro de Alpargatas de Juan Tama
				PlayerScorer.kilometros++;
				//mainCamera.SetActive (false);
				nguiCamera.SetActive (true);
				Destroy (thisObject);
				TutorControl.tutoCont = 5;
			}
			else{
				Debug.Log("NO has recogido todos los numeros");
			}
		}
	}

}
