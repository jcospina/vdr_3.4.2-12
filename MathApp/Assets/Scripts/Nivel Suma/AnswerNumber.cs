﻿using UnityEngine;
using System.Collections;

public class AnswerNumber : MonoBehaviour {

	public AnswerCreator aCreator;
	public string option;
	public static string optionCorrect;

	public AudioClip incorrectSound;
	public AudioClip correctSound;

	public UISprite button;

	private bool isPressed = false;

	void Start () {
		AnswerCreator.isPlaying = true;
		isPressed = false;
		Debug.Log ("ACTUAL VALOR ACTUAL = "+PlayerScorer.contLevelSuma);
	}

	void OnClick(){

//		if (!isPressed){
		button.spriteName = "fondo_respuesta_selec";
//			isPressed = true;
//		}
		//Time.timeScale = 0.0f;

		if (AnswerCreator.isPlaying == true) {

			if (option == optionCorrect) {
				Debug.Log ("Respuesta Correcta");
				//PlayerScorer.contLevelSuma++;
				//if (PlayerScorer.currentlLevelSuma >= PlayerScorer.contLevelSuma)
				//	PlayerScorer.contLevelSuma = PlayerScorer.currentlLevelSuma+1;
				if (PlayerScorer.currentlLevelSuma >= PlayerPrefs.GetInt("nivelSuma")){
					//PlayerScorer.contLevelSuma++;
					PlayerScorer.contLevelSuma = PlayerScorer.contLevelSuma+1;
					PlayerPrefs.SetInt("nivelSuma",PlayerScorer.contLevelSuma);
					Debug.Log ("AQUI SETEA= "+PlayerScorer.contLevelSuma);
				}
					
				JoystickInput.isWin = true;
				NGUITools.PlaySound(correctSound);
				AnswerCreator.isPlaying = false;
				TutorControl.tutoOperation = true;
				//Time.timeScale = 1.0f;
				StartCoroutine (Delay ());

			} 
			
			else {
				Debug.Log ("Respuesta Incorrecta");	
				NGUITools.PlaySound(incorrectSound);
				//Time.timeScale = 1.0f;
				//AQUI SE DEBE PERDER UNA VIDA
				//aCreator.OptionsCreator();
			}		
		
		}
		else{
			Debug.Log ("Juego Terminado");	
		}

	}

	IEnumerator Delay (){

		NumberCreator.numberList.Clear();
		//AnswerCreator.additionResult = 0;
		yield return new WaitForSeconds(3.0f);
		//if (PlayerScorer.contLevelSuma <=10)
		if (PlayerPrefs.GetInt("nivelSuma") <=10)
			Application.LoadLevel ("MenuNivelSuma");
		//if (PlayerScorer.currentlLevelSuma == 10 && PlayerScorer.contLevelSuma == 11) 
		if (PlayerScorer.currentlLevelSuma == 10 && PlayerPrefs.GetInt("nivelSuma") == 11) 
			Application.LoadLevel ("Enemy1");
		else 
			Application.LoadLevel ("MenuNivelSuma");
	}


}
