﻿using UnityEngine;
using System.Collections;

public class MenuLevelSumaControl : MonoBehaviour {

	public UISprite sprite;
	public GameObject objectOpen;
	public GameObject objectLabel;
	public int numberLevel;

	void Start (){

		PlayerScorer.numbersCollected = 0;
		AnswerCreator.additionResult = 0;

		if (NumberCreator.numberList.Count >= 0) {

			NumberCreator.numberList.Clear();
		}

		//if (PlayerScorer.contLevelSuma>= numberLevel) {
		if (PlayerPrefs.GetInt("nivelSuma")>= numberLevel) {
			objectOpen.SetActive (true);
			objectLabel.SetActive (true);
		}

		//if (PlayerScorer.contLevelSuma > numberLevel){
		if (PlayerPrefs.GetInt("nivelSuma") > numberLevel){
			objectOpen.SetActive (false);
			sprite.spriteName = "nivel_superado";
		} 

		//if (PlayerScorer.contLevelSuma == 11) {
//		if (PlayerPrefs.GetInt("nivelSuma") == 11) {
//			Debug.Log ("Has conseguido la medalla de suma!!!");
//			//ControlLogros.logro1Obtenido = true;
//			PlayerPrefs.SetInt ("logro1Obtenido",1);
//		}

	}

	void OnClick (){
		PlayerScorer.currentlLevelSuma = numberLevel;
		 //if (numberLevel <= PlayerScorer.contLevelSuma)
		if (numberLevel <= PlayerPrefs.GetInt("nivelSuma"))
			Application.LoadLevel ("Scene1");
	}

}
