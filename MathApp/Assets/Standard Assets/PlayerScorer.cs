﻿using UnityEngine;
using System.Collections;

public class PlayerScorer : MonoBehaviour {


	public GameObject playerA1;
	public GameObject playerA2;
	public GameObject playerA3;
	public GameObject playerA4;
	public GameObject playerA5;
	public GameObject playerA6;
	public GameObject playerA7;
	public GameObject playerA8;
	public GameObject tutorObject;

	public static int playerActive = 1;
	public static int numbersCollected = 0;
	public static int currentlLevelSuma = 11;
	public static int contLevelSuma = 1;
	public static int currentlLevelResta = 1;
	public static int contLevelResta = 1;
	public static int currentlLevelMulti = 1;
	public static int contLevelMulti = 1;
	public static int currentlLevelDiv = 1;
	public static int contLevelDiv = 1;
	
	// Variables de Logros *****
//	public static bool p1Chosen = false;
//	public static bool p2Chosen = false;
//	public static bool p3Chosen = false;
//	public static bool p4Chosen = false;
//	public static bool p5Chosen = false;
//	public static bool p6Chosen = false;
//	public static bool p7Chosen = false;
//	public static bool p8Chosen = false;
	public static int perseverancia = 0;
	public static int kilometros = 0;

	void Awake () {

		contLevelSuma = PlayerPrefs.GetInt ("nivelSuma");
		contLevelResta = PlayerPrefs.GetInt ("nivelResta");
		contLevelMulti = PlayerPrefs.GetInt ("nivelMulti");
		contLevelDiv = PlayerPrefs.GetInt ("nivelDiv");
		//Debug.Log ("ACTUAL = "+contLevelSuma);

		 //Control Tutorial
		if (PlayerPrefs.GetInt ("tutorialChecked") == 1) {
			Debug.Log ("El tutorial ya ha sido realizado");
			tutorObject.SetActive (false);
		}
		
		if (PlayerPrefs.GetInt ("tutorialChecked") != 1) {
			Debug.Log ("El tutorial no ha sido realizado");
			tutorObject.SetActive (true);
		}

		switch (playerActive) {
			
		case 1:
			playerA1.SetActive (true);
			//p1Chosen = true;
			PlayerPrefs.SetInt ("p1Chosen",1);
			break;

		case 2:
			playerA2.SetActive (true);
			//p2Chosen = true;
			PlayerPrefs.SetInt ("p2Chosen",1);
			break;

		case 3:
			playerA3.SetActive (true);
			//p3Chosen = true;
			PlayerPrefs.SetInt ("p3Chosen",1);
			break;

		case 4:
			playerA4.SetActive (true);
			//p4Chosen = true;
			PlayerPrefs.SetInt ("p4Chosen",1);
			break;

		case 5:
			playerA5.SetActive (true);
			//p5Chosen = true;
			PlayerPrefs.SetInt ("p5Chosen",1);
			break;

		case 6:
			playerA6.SetActive (true);
			//p6Chosen = true;
			PlayerPrefs.SetInt ("p6Chosen",1);
			break;

		case 7:
			playerA7.SetActive (true);
			//p7Chosen = true;
			PlayerPrefs.SetInt ("p7Chosen",1);
			break;

		case 8:
			playerA8.SetActive (true);
			//p8Chosen = true;
			PlayerPrefs.SetInt ("p8Chosen",1);
			break;

			
		}
	
	}
}
